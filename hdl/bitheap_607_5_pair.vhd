
-- This is a pair of chains of compressors 6,0,7:5
-- It produces two partial sums
-- Total 2*WDATA luts (+2 luts with sign support)
-- Delay 1 lut + carry (+1 lut with sign support)

-- Note : The two partial sum results have actual width WDATA+2 each, if unsigned
-- Note : And it is WDATA+3 if sign extension is performed

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT primitives
library unisim;
use unisim.vcomponents.all;

entity bitheap_607_5_pair is
	generic (
		WDATA : natural := 8;
		WOUT  : natural := 12;
		NBIN  : natural := 6;
		SIGN  : boolean := false
	);
	port (
		data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
		data_1   : in  std_logic;
		data_2   : in  std_logic;
		sign_in  : in  std_logic;
		res_idx0 : out std_logic_vector(WOUT-1 downto 0);
		res_idx1 : out std_logic_vector(WOUT-1 downto 0)
	);
end bitheap_607_5_pair;

architecture synth of bitheap_607_5_pair is

	-- The offset (0 or 1) where the MSB is generated, in order to perform sign extension
	-- Offset is 0 if WDATA is odd, 1 if WDATA is even
	constant OFFSET_OF_MSB : natural := (WDATA + 1) mod 2;

	-- The maximum offset to use : 0 if WDATA <= 1 else 1
	constant OFFSET_MAX : natural := 1 - ((WDATA + 1) / (2 * WDATA));

	signal partial_results : std_logic_vector(2*WOUT-1 downto 0) := (others => '0');

	component bitheap_607_5 is
		port (
			cy  : in  std_logic;
			ci  : in  std_logic;
			di0 : in  std_logic_vector(5 downto 0);
			di2 : in  std_logic_vector(5 downto 0);
			do  : out std_logic_vector(3 downto 0);
			co  : out std_logic_vector(3 downto 0)
		);
	end component;

begin

	assert NBIN <= 6 report "Entity bitheap_607_5_pair : Number of inputs too large" severity failure;

	gen_off : for off in 0 to OFFSET_MAX generate

		-- The maximum index of data input handled : WDATA-1 if this is OFFSET_OF_MSB, else WDATA-2
		constant DATA_IDX_MAX : natural := (WDATA - 1) - ((OFFSET_OF_MSB + off) mod 2);

		-- The number of CARRY4 chunks needed
		constant NBC4 : natural := (WDATA - off + 3) / 4;

		-- The wrapper signals for inputs and outputs
		signal wrap_in  : std_logic_vector(6*4*NBC4-1 downto 0) := (others => '0');
		signal inst_out : std_logic_vector(4*NBC4-1 downto 0) := (others => '0');
		signal inst_co  : std_logic_vector(4*NBC4-1 downto 0) := (others => '0');

		-- The wrapper signal for local result
		signal wrap_res : std_logic_vector(DATA_IDX_MAX+2 downto 0) := (others => '0');

	begin

		-- Resize inputs to the full size of the series of CARRY4
		gen_wrapin : for i in 0 to NBIN-1 generate
			gen_dim : if 4*NBC4 >= WDATA generate
				wrap_in(i*4*NBC4+WDATA-1 downto i*4*NBC4) <= data_in((i+1)*WDATA-1 downto i*WDATA);
			else generate
				wrap_in((i+1)*4*NBC4-1 downto i*4*NBC4) <= data_in(i*WDATA+4*NBC4-1 downto i*WDATA);
			end generate;
		end generate;

		-- Generate individual 6,0,6:5 compressors
		gen_comp : for c in 0 to NBC4-1 generate
			-- Signals for inputs
			signal sig_cy : std_logic := '0';
			signal sig_ci : std_logic := '0';
			signal sigin0 : std_logic_vector(5 downto 0) := (others => '0');
			signal sigin2 : std_logic_vector(5 downto 0) := (others => '0');
		begin

			-- Generate inputs
			gen_in : for i in 0 to 5 generate
				sigin0(i) <= wrap_in(i*4*NBC4 + c*4 + 0 + off);
				sigin2(i) <= wrap_in(i*4*NBC4 + c*4 + 2 + off);
			end generate;

			-- Set the carry input
			gen_ci : if c = 0 generate
				sig_cy <= data_2 when off = 0 else '0';
			else generate
				sig_ci <= inst_co((c-1)*4 + 3);
			end generate;

			-- The compressor instance
			comp : bitheap_607_5
				port map (
					cy  => sig_cy,
					ci  => sig_ci,
					di0 => sigin0,
					di2 => sigin2,
					do  => inst_out((c+1)*4-1 downto c*4),
					co  => inst_co((c+1)*4-1 downto c*4)
				);

		end generate;  -- Generate compressors

		-- The last carry out is part of the result
		wrap_res(DATA_IDX_MAX+2 downto off) <= inst_co(DATA_IDX_MAX+1-off) & inst_out(DATA_IDX_MAX+1-off downto 0);

		-- One extra 1b input
		gen_di1 : if off = 1 generate
			wrap_res(0) <= data_1;
		end generate;

		-- Save the partial result

		gen_sign : if (SIGN = false) or (off /= OFFSET_OF_MSB) generate

			partial_results((off+1)*WOUT-1 downto off*WOUT) <= std_logic_vector(resize(unsigned(wrap_res), WOUT));

		else generate

			-- Signed case : Fix the MSB of partial result idx1
			signal sig7 : std_logic := '0';
			signal sig6 : std_logic := '0';
			signal sig5 : std_logic := '0';

			signal lut_in : std_logic_vector(3 downto 0) := (others => '0');

		begin

			gen_sign_in0 : if WDATA = 1 generate
				-- Here we have off=0
				lut_in(0) <= data_2;
			elsif WDATA = 2 generate
				-- Here we have off=1
				lut_in(0) <= '0';
			else generate
				-- Here WDATA is always high enough
				lut_in(0) <= inst_co(DATA_IDX_MAX-1-off);
			end generate;

			lut_in(1) <= inst_out(DATA_IDX_MAX-off);
			lut_in(2) <= inst_out(DATA_IDX_MAX+1-off);
			lut_in(3) <= inst_co(DATA_IDX_MAX+1-off);

			lut: LUT6_2
				generic map (
					INIT => x"2BD4BF009694B0F0"
				)
				port map (
					O6 => sig6,
					O5 => sig5,
					I0 => lut_in(0),
					I1 => lut_in(1),
					I2 => lut_in(2),
					I3 => lut_in(3),
					I4 => sign_in,
					I5 => '1'  -- To have something different for O6 and O5
				);

			lut2: LUT5
				generic map (
					INIT => x"BFD40000"
				)
				port map (
					O  => sig7,
					I0 => lut_in(0),
					I1 => lut_in(1),
					I2 => lut_in(2),
					I3 => lut_in(3),
					I4 => sign_in
				);

			partial_results((off+1)*WOUT-1 downto off*WOUT) <= std_logic_vector(resize(signed(sig7 & sig6 & sig5 & wrap_res(DATA_IDX_MAX downto 0)), WOUT));

		end generate;

	end generate;

	-- Set final outputs

	res_idx0 <= partial_results((0+1)*WOUT-1 downto 0*WOUT);
	res_idx1 <= partial_results((1+1)*WOUT-1 downto 1*WOUT);

end architecture;

