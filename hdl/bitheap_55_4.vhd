
-- This is a compressor 5,5:4
-- Total 4 LUTs, delay is 2 LUTs + carry

-- Architecture :
-- First one 5:3 compressor (for bits position 0)
-- Then a special 1,6:3 compressor that uses the 2 lower LUTs of a carry4

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT and CARRY primitives
library unisim;
use unisim.vcomponents.all;

entity bitheap_55_4 is
	port (
		di0 : in  std_logic_vector(4 downto 0);
		di1 : in  std_logic_vector(4 downto 0);
		p53 : out std_logic_vector(2 downto 0);
		do  : out std_logic_vector(1 downto 0);
		co  : out std_logic_vector(1 downto 0);
		sum : out std_logic_vector(3 downto 0)
	);
end bitheap_55_4;

architecture synth of bitheap_55_4 is

	signal part0 : std_logic_vector(2 downto 0) := (others => '0');

	-- These are for the special 1,6:3 compressor

	signal c4_o  : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_co : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_di : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_s  : std_logic_vector(3 downto 0) := (others => '0');

begin

	-- Fist stage : the 5:3 compressor for the input bits position 0

	lut01 : LUT6_2
		generic map (
			INIT => x"96696996177E7EE8"
		)
		port map (
			O6 => part0(0),
			O5 => part0(1),
			I0 => di0(0),
			I1 => di0(1),
			I2 => di0(2),
			I3 => di0(3),
			I4 => di0(4),
			I5 => '1'   -- To have something different for O6 and O5
		);

	lut02 : LUT5
		generic map (
			INIT => x"E8808000"
		)
		port map (
			O  => part0(2),
			I0 => di0(0),
			I1 => di0(1),
			I2 => di0(2),
			I3 => di0(3),
			I4 => di0(4)
		);

	-- Second stage : a special compressor 1,6:3 compressor
	-- For the input bits position 1 + the partial result of bits position 0

	lutA: LUT6_2
		generic map (
			INIT => x"96696996FFFF0000"
		)
		port map (
			O6 => c4_s(0),
			O5 => c4_di(0),
			I0 => di1(0),
			I1 => di1(1),
			I2 => di1(2),
			I3 => part0(1),
			I4 => di1(3),
			I5 => '1'   -- To have something different for O6 and O5
		);

	lutB: LUT6_2
		generic map (
			INIT => x"17177EE8E8E8E8E8"
		)
		port map (
			O6 => c4_s(1),
			O5 => c4_di(1),
			I0 => di1(0),
			I1 => di1(1),
			I2 => di1(2),
			I3 => part0(1),
			I4 => part0(2),
			I5 => '1'   -- To have something different for O6 and O5
		);

	-- The CARRY4

	c4: CARRY4
		port map (
			CO     => c4_co,
			O      => c4_o,
			DI     => c4_di,
			S      => c4_s,
			CI     => '0',
			CYINIT => di1(4)
		);

	-- Outputs

	p53 <= part0;
	do  <= c4_o(1 downto 0);
	co  <= c4_co(1 downto 0);
	sum <= c4_co(1) & c4_o(1 downto 0) & part0(0);

end architecture;

