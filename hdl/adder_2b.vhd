
-- This is one adder block of 2b numbers, unsigned or signed, up to 7 inputs
-- Optimized at netlist level for Xilinx primitives

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity adder_2b is
	generic (
		NBIN : natural := 7;
		SIGN : boolean := false;
		WOUT : natural := 5
	);
	port (
		data_in : in  std_logic_vector(NBIN*2-1 downto 0);
		res_sum : out std_logic_vector(WOUT-1 downto 0)
	);
end adder_2b;

architecture synth of adder_2b is

begin

	assert NBIN <= 7 report "Entity adder_2b : Number of inputs too large" severity failure;

	-- Cases for 1 input

	gen1u : if (NBIN <= 1) and (SIGN = false) generate
		res_sum <= std_logic_vector(resize(unsigned(data_in), WOUT));
	end generate;

	gen1s : if (NBIN <= 1) and (SIGN = true) generate
		res_sum <= std_logic_vector(resize(signed(data_in), WOUT));
	end generate;

	-- Cases for 2 inputs

	gen2u : if (NBIN = 2) and (SIGN = false) generate
		res_sum <= std_logic_vector(resize(unsigned(data_in(3 downto 2)) + resize(unsigned(data_in(1 downto 0)), 3), WOUT));
	end generate;

	gen2s : if (NBIN = 2) and (SIGN = true) generate
		res_sum <= std_logic_vector(resize(signed(data_in(3 downto 2)) + resize(signed(data_in(1 downto 0)), 3), WOUT));
	end generate;

	-- Cases for 3 inputs

	gen3 : if NBIN = 3 generate

		signal sig0b1 : std_logic := '0';
		signal sig_sum : std_logic_vector(3 downto 0) := (others => '0');

		function calc_false_true(b : boolean; vf : bit_vector; vt : bit_vector) return bit_vector is
		begin
			if b = false then return vf; end if;
			return vt;
		end;

		constant INIT_LUT12 : bit_vector(63 downto 0) := calc_false_true(SIGN,
			x"00007EE800006996",
			x"0000BDD400006996"
		);
		constant INIT_LUT3 : bit_vector(31 downto 0) := calc_false_true(SIGN,
			x"00008000",
			x"0000FDD4"
		);

	begin

		-- Fist stage : the 3:2 compressor for the input bits position 0

		lut0 : LUT6_2
			generic map (
				INIT => x"000000E800000096"
			)
			port map (
				O6 => sig0b1,
				O5 => sig_sum(0),
				I0 => data_in(0*2 + 0),
				I1 => data_in(1*2 + 0),
				I2 => data_in(2*2 + 0),
				I3 => '0',  -- Unused
				I4 => '0',  -- Unused
				I5 => '1'   -- To have something different for O6 and O5
			);

		-- Second stage

		lut12 : LUT6_2
			generic map (
				INIT => INIT_LUT12
			)
			port map (
				O6 => sig_sum(2),
				O5 => sig_sum(1),
				I0 => sig0b1,
				I1 => data_in(0*2 + 1),
				I2 => data_in(1*2 + 1),
				I3 => data_in(2*2 + 1),
				I4 => '0',  -- Unused
				I5 => '1'   -- To have something different for O6 and O5
			);

		lut3 : LUT5
			generic map (
				INIT => INIT_LUT3
			)
			port map (
				O  => sig_sum(3),
				I0 => sig0b1,
				I1 => data_in(0*2 + 1),
				I2 => data_in(1*2 + 1),
				I3 => data_in(2*2 + 1),
				I4 => '0'  -- Unused
			);

		-- Assign result

		gen_ext : if SIGN generate
			res_sum <= std_logic_vector(resize(signed(sig_sum), WOUT));
		else generate
			res_sum <= std_logic_vector(resize(unsigned(sig_sum), WOUT));
		end generate;

	end generate;

	-- Cases for 4 inputs

	-- This costs 3 LUTs, delay 2 LUTs + carry
	gen4u : if (NBIN = 4) and (SIGN = false) generate

		-- Wrappers for compressor interface

		signal di0 : std_logic_vector(3 downto 0) := (others => '0');
		signal di1 : std_logic_vector(3 downto 0) := (others => '0');
		signal sum : std_logic_vector(3 downto 0) := (others => '0');

		-- The underlying compressor 4,4:4

		component bitheap_44_4 is
			port (
				ci  : in  std_logic;
				di0 : in  std_logic_vector(3 downto 0);
				di1 : in  std_logic_vector(3 downto 0);
				sum : out std_logic_vector(3 downto 0)
			);
		end component;

	begin

		-- Wrappers for inputs

		gen_in : for i in 0 to NBIN-1 generate
			di0(i) <= data_in(2*i + 0);
			di1(i) <= data_in(2*i + 1);
		end generate;

		-- The compressor 44:4

		comp : bitheap_44_4
			port map (
				ci  => '0',
				di0 => di0,
				di1 => di1,
				sum => sum
			);

		-- Output

		res_sum <= std_logic_vector(resize(unsigned(sum), WOUT));

	end generate;

	-- This costs 4 LUTs, delay 2 LUTs
	-- Note : This implementation can also handle unsigned, but not as optimized as implementation above
	gen4s : if (NBIN = 4) and (SIGN = true) generate

		signal sig0b1 : std_logic := '0';
		signal sig1b0 : std_logic := '0';
		signal sig1b1 : std_logic := '0';
		signal sig_sum : std_logic_vector(3 downto 0) := (others => '0');

		function calc_false_true(b : boolean; vf : bit_vector; vt : bit_vector) return bit_vector is
		begin
			if b = false then return vf; end if;
			return vt;
		end;

		constant INIT_LUT12 : bit_vector(63 downto 0) := calc_false_true(SIGN,
			x"23EFDC1033CC33CC",
			x"DCEF231033CC33CC"
		);
		constant INIT_LUT3 : bit_vector(63 downto 0) := calc_false_true(SIGN,
			x"DC1000FFDC100000",
			x"FFEF23FFFFEF2300"
		);

	begin

		-- Fist stage : the 4:2 compressors for the input bits position 0 and position 1

		lut0 : LUT6_2
			generic map (
				INIT => x"00007EE800006996"
			)
			port map (
				O6 => sig0b1,
				O5 => sig_sum(0),
				I0 => data_in(0*2 + 0),
				I1 => data_in(1*2 + 0),
				I2 => data_in(2*2 + 0),
				I3 => data_in(3*2 + 0),
				I4 => '0',  -- Unused
				I5 => '1'   -- To have something different for O6 and O5
			);

		lut1 : LUT6_2
			generic map (
				INIT => x"00007EE800006996"
			)
			port map (
				O6 => sig1b1,
				O5 => sig1b0,
				I0 => data_in(0*2 + 1),
				I1 => data_in(1*2 + 1),
				I2 => data_in(2*2 + 1),
				I3 => data_in(3*2 + 1),
				I4 => '0',  -- Unused
				I5 => '1'   -- To have something different for O6 and O5
			);

		-- Second stage

		lut12 : LUT6_2
			generic map (
				INIT => INIT_LUT12
			)
			port map (
				O6 => sig_sum(2),
				O5 => sig_sum(1),
				I0 => sig_sum(0),
				I1 => sig0b1,
				I2 => data_in(0*2 + 0),
				I3 => sig1b0,
				I4 => sig1b1,
				I5 => '1'   -- To have something different for O6 and O5
			);

		lut3 : LUT6
			generic map (
				INIT => INIT_LUT3
			)
			port map (
				O  => sig_sum(3),
				I0 => sig_sum(0),
				I1 => sig0b1,
				I2 => data_in(0*2 + 0),
				I3 => sig1b0,
				I4 => sig1b1,
				I5 => data_in(0*2 + 1)
			);

		-- Assign result

		gen_ext : if SIGN generate
			res_sum <= std_logic_vector(resize(signed(sig_sum), WOUT));
		else generate
			res_sum <= std_logic_vector(resize(unsigned(sig_sum), WOUT));
		end generate;

	end generate;

	-- Cases for 5 inputs

	-- This costs 4 LUTs, delay 2 LUTs + carry
	gen5u : if (NBIN = 5) and (SIGN = false) generate

		-- Wrappers for compressor interface

		signal di0 : std_logic_vector(4 downto 0) := (others => '0');
		signal di1 : std_logic_vector(4 downto 0) := (others => '0');
		signal sum : std_logic_vector(3 downto 0) := (others => '0');

		-- The underlying compressor 5,5:4

		component bitheap_55_4 is
			port (
				di0 : in  std_logic_vector(4 downto 0);
				di1 : in  std_logic_vector(4 downto 0);
				p53 : out std_logic_vector(2 downto 0);
				do  : out std_logic_vector(1 downto 0);
				co  : out std_logic_vector(1 downto 0);
				sum : out std_logic_vector(3 downto 0)
			);
		end component;

	begin

		-- Wrappers for inputs

		gen_in : for i in 0 to NBIN-1 generate
			di0(i) <= data_in(2*i + 0);
			di1(i) <= data_in(2*i + 1);
		end generate;

		-- The compressor 55:4

		comp : bitheap_55_4
			port map (
				di0 => di0,
				di1 => di1,
				p53 => open,
				do  => open,
				co  => open,
				sum => sum
			);

		-- Output

		res_sum <= std_logic_vector(resize(unsigned(sum), WOUT));

	end generate;

	-- This costs 5 LUTs, delay 3 LUTs (no carry4 involved)
	-- Note : This implementation can also handle unsigned, but not as optimized as implementation above
	gen5s : if (NBIN = 5) and (SIGN = true) generate

		signal sig0b1 : std_logic := '0';
		signal sig0b2 : std_logic := '0';
		signal sig1b0 : std_logic := '0';
		signal sig1b1 : std_logic := '0';
		signal sig_sum : std_logic_vector(4 downto 0) := (others => '0');

		function calc_false_true(b : boolean; vf : bit_vector; vt : bit_vector) return bit_vector is
		begin
			if b = false then return vf; end if;
			return vt;
		end;

		constant INIT_LUT12 : bit_vector(63 downto 0) := calc_false_true(SIGN,
			x"0000136400005252",
			x"0000631400005252"
		);
		constant INIT_LUT34 : bit_vector(63 downto 0) := calc_false_true(SIGN,
			x"000000005F535350",
			x"EFEBEBE8E7EBEBE8"
		);

	begin

		-- Fist stage : the 5:3 compressors for input bits position 0, and 5:2 for position 1

		lut0 : LUT6_2
			generic map (
				INIT => x"177E7EE896696996"
			)
			port map (
				O6 => sig0b1,
				O5 => sig_sum(0),
				I0 => data_in(0*2 + 0),
				I1 => data_in(1*2 + 0),
				I2 => data_in(2*2 + 0),
				I3 => data_in(3*2 + 0),
				I4 => data_in(4*2 + 0),
				I5 => '1'   -- To have something different for O6 and O5
			);

		lut02 : LUT5
			generic map (
				INIT => x"E8808000"
			)
			port map (
				O  => sig0b2,
				I0 => data_in(0*2 + 0),
				I1 => data_in(1*2 + 0),
				I2 => data_in(2*2 + 0),
				I3 => data_in(3*2 + 0),
				I4 => data_in(4*2 + 0)
			);

		lut1 : LUT6_2
			generic map (
				INIT => x"177E7EE896696996"
			)
			port map (
				O6 => sig1b1,
				O5 => sig1b0,
				I0 => data_in(0*2 + 1),
				I1 => data_in(1*2 + 1),
				I2 => data_in(2*2 + 1),
				I3 => data_in(3*2 + 1),
				I4 => data_in(4*2 + 1),
				I5 => '1'   -- To have something different for O6 and O5
			);

		-- Second stage : generate output bits 1 and 2

		lut12 : LUT6_2
			generic map (
				INIT => INIT_LUT12
			)
			port map (
				O6 => sig_sum(2),
				O5 => sig_sum(1),
				I0 => sig0b1,
				I1 => sig0b2,
				I2 => sig1b0,
				I3 => sig1b1,
				I4 => '0',
				I5 => '1'   -- To have something different for O6 and O5
			);

		-- Third stage : generate output bits 3 and 4

		lut34 : LUT6_2
			generic map (
				INIT => INIT_LUT34
			)
			port map (
				O6 => sig_sum(4),
				O5 => sig_sum(3),
				I0 => sig_sum(2),
				I1 => sig1b0,
				I2 => sig1b1,
				I3 => data_in(0*2 + 1),
				I4 => data_in(1*2 + 1),
				I5 => '1'   -- To have something different for O6 and O5
			);

		-- Assign result

		gen_ext : if SIGN generate
			res_sum <= std_logic_vector(resize(signed(sig_sum), WOUT));
		else generate
			res_sum <= std_logic_vector(resize(unsigned(sig_sum), WOUT));
		end generate;

	end generate;

	-- Cases for 6 inputs

	-- This costs 5 LUTs, delay 2 LUTs + carry
	gen6u : if (NBIN = 6) and (SIGN = false) generate

		-- Wrappers for compressor interface

		signal di0 : std_logic_vector(5 downto 0) := (others => '0');
		signal di1 : std_logic_vector(5 downto 0) := (others => '0');
		signal di3 : std_logic_vector(0 downto 0) := (others => '0');
		signal sum : std_logic_vector(4 downto 0) := (others => '0');

		-- The underlying compressor 1,0,6,6:5

		component bitheap_1066_5 is
			port (
				ci  : in  std_logic;
				di0 : in  std_logic_vector(5 downto 0);
				di1 : in  std_logic_vector(5 downto 0);
				di3 : in  std_logic_vector(0 downto 0);
				sum : out std_logic_vector(4 downto 0)
			);
		end component;

	begin

		-- Wrappers for inputs

		gen_in : for i in 0 to NBIN-1 generate
			di0(i) <= data_in(2*i + 0);
			di1(i) <= data_in(2*i + 1);
		end generate;

		-- The compressor 1066:5

		comp : bitheap_1066_5
			port map (
				ci  => '0',
				di0 => di0,
				di1 => di1,
				di3 => di3,
				sum => sum
			);

		-- Output

		res_sum <= std_logic_vector(resize(unsigned(sum), WOUT));

	end generate;

	-- This costs 6 LUTs, delay 2 LUTs + carry
	-- Note : This implementation can also handle unsigned, but not as optimized as implementation above
	gen6s : if (NBIN = 6) and (SIGN = true) generate

		-- Wrappers for compressor interface

		signal di0   : std_logic_vector(5 downto 0) := (others => '0');
		signal di2   : std_logic_vector(5 downto 0) := (others => '0');
		signal c4_do : std_logic_vector(3 downto 0) := (others => '0');
		signal c4_co : std_logic_vector(3 downto 0) := (others => '0');

		-- Wrapper post-recoding
		signal sum : std_logic_vector(4 downto 0) := (others => '0');

		-- The underlying compressor 6,0,7:5

		component bitheap_607_5 is
			port (
				cy  : in  std_logic;
				ci  : in  std_logic;
				di0 : in  std_logic_vector(5 downto 0);
				di2 : in  std_logic_vector(5 downto 0);
				do  : out std_logic_vector(3 downto 0);
				co  : out std_logic_vector(3 downto 0)
			);
		end component;

		-- The LUT configs

		function calc_false_true(b : boolean; vf : bit_vector; vt : bit_vector) return bit_vector is
		begin
			if b = false then return vf; end if;
			return vt;
		end;

		constant INIT_LUT12 : bit_vector(63 downto 0) := calc_false_true(SIGN,
			x"9696969617E817E8",
			x"969696862BD42BC4"
		);
		constant INIT_LUT34 : bit_vector(63 downto 0) := calc_false_true(SIGN,
			x"17FFE800E8000000",
			x"F0DB0F04FFDF0F04"
		);

	begin

		-- Wrappers for inputs

		gen_in : for i in 0 to NBIN-1 generate
			di0(i) <= data_in(2*i + 0);
			di2(i) <= data_in(2*i + 1);
		end generate;

		-- First stage

		comp607 : bitheap_607_5
			port map (
				cy  => '0',
				ci  => '0',
				di0 => di0,
				di2 => di2,
				do  => c4_do,
				co  => c4_co
			);

		sum(0) <= c4_do(0);

		-- Second stage

		lut12 : LUT6_2
			generic map (
				INIT => INIT_LUT12
			)
			port map (
				O6 => sum(1),
				O5 => sum(2),
				I0 => c4_do(1),
				I1 => c4_do(2),
				I2 => c4_co(1),
				I3 => c4_do(3),
				I4 => c4_co(3),
				I5 => '1'   -- To have something different for O6 and O5
			);

		lut34 : LUT6_2
			generic map (
				INIT => INIT_LUT34
			)
			port map (
				O6 => sum(3),
				O5 => sum(4),
				I0 => c4_do(1),
				I1 => c4_do(2),
				I2 => c4_co(1),
				I3 => c4_do(3),
				I4 => c4_co(3),
				I5 => '1'   -- To have something different for O6 and O5
			);

		-- Output

		gen_ext : if SIGN generate
			res_sum <= std_logic_vector(resize(signed(sum), WOUT));
		else generate
			res_sum <= std_logic_vector(resize(unsigned(sum), WOUT));
		end generate;

	end generate;

	-- Cases for 7 inputs

	-- This costs 6 LUTs, delay 3 LUTs + carry
	gen7u : if (NBIN = 7) and (SIGN = false) generate

		-- Wrappers for compressor interface

		signal di0 : std_logic_vector(6 downto 0) := (others => '0');
		signal di1 : std_logic_vector(6 downto 0) := (others => '0');
		signal sum : std_logic_vector(4 downto 0) := (others => '0');

		-- The underlying compressor 7,7:5

		component bitheap_77_5 is
			port (
				di0 : in  std_logic_vector(6 downto 0);
				di1 : in  std_logic_vector(6 downto 0);
				sum : out std_logic_vector(4 downto 0)
			);
		end component;

	begin

		-- Wrappers for inputs

		gen_in : for i in 0 to NBIN-1 generate
			di0(i) <= data_in(2*i + 0);
			di1(i) <= data_in(2*i + 1);
		end generate;

		-- The compressor 55:4

		comp : bitheap_77_5
			port map (
				di0 => di0,
				di1 => di1,
				sum => sum
			);

		-- Output

		res_sum <= std_logic_vector(resize(unsigned(sum), WOUT));

	end generate;

	-- This costs 7 LUTs, delay 2 LUTs + carry
	-- Note : This implementation can also handle unsigned, but not as optimized as implementation above
	gen7s: if (NBIN = 7) and (SIGN = true) generate

		-- Wrappers for compressor interface

		signal di0   : std_logic_vector(5 downto 0) := (others => '0');
		signal di2   : std_logic_vector(5 downto 0) := (others => '0');
		signal c4_do : std_logic_vector(3 downto 0) := (others => '0');
		signal c4_co : std_logic_vector(3 downto 0) := (others => '0');

		-- Wrapper post-recoding
		signal sum : std_logic_vector(4 downto 0) := (others => '0');

		-- The underlying compressor 6,0,7:5

		component bitheap_607_5 is
			port (
				cy  : in  std_logic;
				ci  : in  std_logic;
				di0 : in  std_logic_vector(5 downto 0);
				di2 : in  std_logic_vector(5 downto 0);
				do  : out std_logic_vector(3 downto 0);
				co  : out std_logic_vector(3 downto 0)
			);
		end component;

		-- The LUT configs

		function calc_false_true(b : boolean; vf : bit_vector; vt : bit_vector) return bit_vector is
		begin
			if b = false then return vf; end if;
			return vt;
		end;

		constant INIT_LUT12 : bit_vector(63 downto 0) := calc_false_true(SIGN,
			x"6996699681177EE8",
			x"69966996422BBDD4"
		);
		constant INIT_LUT3 : bit_vector(63 downto 0) := calc_false_true(SIGN,
			x"01177FFFFEE88000",
			x"B0F0F2DB4F0F0D04"
		);
		constant INIT_LUT4 : bit_vector(63 downto 0) := calc_false_true(SIGN,
			x"FEE8800000000000",
			x"FFFFFFDF4F0F0D04"
		);

	begin

		-- Wrappers for inputs
		-- Just inputs 0 to 5, the input 6 is handled in a more specific way

		gen_in : for i in 0 to 5 generate
			di0(i) <= data_in(2*i + 0);
			di2(i) <= data_in(2*i + 1);
		end generate;

		-- First stage

		comp607 : bitheap_607_5
			port map (
				cy  => data_in(2*6 + 0),
				ci  => '0',
				di0 => di0,
				di2 => di2,
				do  => c4_do,
				co  => c4_co
			);

		sum(0) <= c4_do(0);

		-- Second stage

		lut12 : LUT6_2
			generic map (
				INIT => INIT_LUT12
			)
			port map (
				O6 => sum(1),
				O5 => sum(2),
				I0 => c4_do(1),
				I1 => c4_do(2),
				I2 => c4_co(1),
				I3 => data_in(2*6 + 1),
				I4 => c4_do(3),
				I5 => '1'   -- To have something different for O6 and O5
			);

		lut3 : LUT6
			generic map (
				INIT => INIT_LUT3
			)
			port map (
				O  => sum(3),
				I0 => c4_do(1),
				I1 => c4_do(2),
				I2 => c4_co(1),
				I3 => data_in(2*6 + 1),
				I4 => c4_do(3),
				I5 => c4_co(3)
			);

		lut4 : LUT6
			generic map (
				INIT => INIT_LUT4
			)
			port map (
				O  => sum(4),
				I0 => c4_do(1),
				I1 => c4_do(2),
				I2 => c4_co(1),
				I3 => data_in(2*6 + 1),
				I4 => c4_do(3),
				I5 => c4_co(3)
			);

		-- Output

		gen_ext : if SIGN generate
			res_sum <= std_logic_vector(resize(signed(sum), WOUT));
		else generate
			res_sum <= std_logic_vector(resize(unsigned(sum), WOUT));
		end generate;

	end generate;

end architecture;

