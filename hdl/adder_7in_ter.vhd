
-- This is a ternary adder 7x ter (signed only) -> 4b
-- Architecture : one 607:5 compressor + recoding
-- Total cost 6 LUTs, delay 2 LUTs + carry

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity adder_7in_ter is
	generic (
		WOUT : natural := 7
	);
	port (
		in_ter : in  std_logic_vector(7*2-1 downto 0);
		res    : out std_logic_vector(WOUT-1 downto 0)
	);
end adder_7in_ter;

architecture synth of adder_7in_ter is

	signal sig_cy  : std_logic := '0';
	signal sig_ci  : std_logic := '0';
	signal sig_di0 : std_logic_vector(5 downto 0) := (others => '0');
	signal sig_di2 : std_logic_vector(5 downto 0) := (others => '0');
	signal sig_do  : std_logic_vector(3 downto 0) := (others => '0');
	signal sig_co  : std_logic_vector(3 downto 0) := (others => '0');

	signal sigSum : std_logic_vector(3 downto 0) := (others => '0');

	component bitheap_607_5 is
		port (
			cy  : in  std_logic;
			ci  : in  std_logic;
			di0 : in  std_logic_vector(5 downto 0);
			di2 : in  std_logic_vector(5 downto 0);
			do  : out std_logic_vector(3 downto 0);
			co  : out std_logic_vector(3 downto 0)
		);
	end component;

begin

	-- The 607:5 compressor

	sig_cy <= in_ter(6*2+0);
	sig_ci <= '0';

	gen_di : for i in 0 to 5 generate
		sig_di0(i) <= in_ter(i*2+0);
		sig_di2(i) <= in_ter(i*2+1);
	end generate;

	comp607 : bitheap_607_5
		port map (
			cy  => sig_cy,
			ci  => sig_ci,
			di0 => sig_di0,
			di2 => sig_di2,
			do  => sig_do,
			co  => sig_co
		);

	sigSum(0) <= sig_do(0);

	-- The recoding LUTs

	lutF0: LUT6_2
		generic map (
			INIT => x"4B292DA466099926"
		)
		port map (
			O6 => sigSum(2),
			O5 => sigSum(1),
			I0 => sig_do(1),
			I1 => sig_do(2),
			I2 => sig_do(3),
			I3 => sig_co(1),
			I4 => in_ter(6*2 + 1),
			I5 => '1'  -- To have something different for O6 and O5
		);

	lutF1: LUT6
		generic map (
			INIT => x"AF400029BD0000A4"
		)
		port map (
			O  => sigSum(3),
			I0 => sig_do(1),
			I1 => sig_do(2),
			I2 => sig_do(3),
			I3 => sig_co(3),
			I4 => sig_co(1),
			I5 => in_ter(6*2 + 1)
		);

	-- Output

	res <= std_logic_vector(resize(signed(sigSum), WOUT));

end architecture;

