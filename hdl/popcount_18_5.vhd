
-- This is a popcount 17b -> 5b
-- Total 10 LUTs, delay 3 LUTs + 2 carry

-- Architecture : first one 6:3 compressor that handles 6 bits
-- Then one 1,1,7:4 compressor that adds 6 extra bits
-- Then one 1,1,1,7:5 compressor that adds 6 extra bits

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT primitives
library unisim;
use unisim.vcomponents.all;

entity popcount_18_5 is
	generic (
		BITS : natural := 18;
		WOUT : natural := 5
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount_18_5;

architecture synth of popcount_18_5 is

	signal sigBits : std_logic_vector(17 downto 0) := (others => '0');

	signal comp1_sum : std_logic_vector(2 downto 0) := (others => '0');

	signal comp2_di0 : std_logic_vector(6 downto 0) := (others => '0');
	signal comp2_di1 : std_logic_vector(0 downto 0) := (others => '0');
	signal comp2_di2 : std_logic_vector(0 downto 0) := (others => '0');
	signal comp2_sum : std_logic_vector(3 downto 0) := (others => '0');

	signal comp3_di0 : std_logic_vector(6 downto 0) := (others => '0');
	signal comp3_di1 : std_logic_vector(0 downto 0) := (others => '0');
	signal comp3_di2 : std_logic_vector(0 downto 0) := (others => '0');
	signal comp3_di3 : std_logic_vector(1 downto 0) := (others => '0');
	signal comp3_sum : std_logic_vector(4 downto 0) := (others => '0');

	component popcount_6_3 is
		generic (
			BITS : natural := 6;
			WOUT : natural := 3
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component bitheap_117_4 is
		port (
			ci  : in  std_logic;
			di0 : in  std_logic_vector(6 downto 0);
			di1 : in  std_logic_vector(0 downto 0);
			di2 : in  std_logic_vector(0 downto 0);
			do  : out std_logic_vector(3 downto 0);
			co  : out std_logic_vector(3 downto 0);
			sum : out std_logic_vector(3 downto 0)
		);
	end component;

	component bitheap_2117_5 is
		port (
			ci  : in  std_logic;
			di0 : in  std_logic_vector(6 downto 0);
			di1 : in  std_logic_vector(0 downto 0);
			di2 : in  std_logic_vector(0 downto 0);
			di3 : in  std_logic_vector(1 downto 0);
			do  : out std_logic_vector(3 downto 0);
			co  : out std_logic_vector(3 downto 0);
			sum : out std_logic_vector(4 downto 0)
		);
	end component;

begin

	assert BITS <= 18 report "Number of inputs too large for module popcount_18_5" severity failure;

	sigBits(BITS-1 downto 0) <= bits_in;

	popc63 : popcount_6_3
		port map (
			bits_in => sigBits(5 downto 0),
			sum     => comp1_sum
		);

	comp2_di0    <= sigBits(11 downto 6) & comp1_sum(0);
	comp2_di1(0) <= comp1_sum(1);
	comp2_di2(0) <= comp1_sum(2);

	comp2 : bitheap_117_4
		port map (
			ci  => '0',
			di0 => comp2_di0,
			di1 => comp2_di1,
			di2 => comp2_di2,
			do  => open,
			co  => open,
			sum => comp2_sum
		);

	comp3_di0    <= sigBits(17 downto 12) & comp2_sum(0);
	comp3_di1(0) <= comp2_sum(1);
	comp3_di2(0) <= comp2_sum(2);
	comp3_di3(0) <= comp2_sum(3);

	comp3 : bitheap_2117_5
		port map (
			ci  => '0',
			di0 => comp3_di0,
			di1 => comp3_di1,
			di2 => comp3_di2,
			di3 => comp3_di3,
			do  => open,
			co  => open,
			sum => comp3_sum
		);

	sum <= std_logic_vector(resize(unsigned(comp3_sum), WOUT));

end architecture;

