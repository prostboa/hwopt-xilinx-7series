
-- This is a ternary adder 3x ter (signed only) -> 3b implemented using 2 LUT6

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity adder_4in_ter is
	generic (
		WOUT : natural := 4
	);
	port (
		in_ter : in  std_logic_vector(4*2-1 downto 0);
		res    : out std_logic_vector(WOUT-1 downto 0)
	);
end adder_4in_ter;

architecture synth of adder_4in_ter is

	signal sig15 : std_logic := '0';
	signal sig16 : std_logic := '0';

	signal sigSum : std_logic_vector(3 downto 0) := (others => '0');

	constant INIT_LUT1 : bit_vector(63 downto 0) := x"00007EE800006996";
	constant INIT_LUT2 : bit_vector(63 downto 0) := x"81167EE869966996";
	constant INIT_LUT3 : bit_vector(63 downto 0) := x"66CC081166CC8810";

begin

	lut1: LUT6_2
		generic map (
			INIT => INIT_LUT1
		)
		port map (
			O6 => sig16,
			O5 => sig15,
			I0 => in_ter(2*0 + 1),
			I1 => in_ter(2*1 + 1),
			I2 => in_ter(2*2 + 1),
			I3 => in_ter(2*3 + 1),
			I4 => '0',  -- Unused input
			I5 => '1'   -- To have something different for O6 and O5
		);

	lut2: LUT6_2
		generic map (
			INIT => INIT_LUT2
		)
		port map (
			O6 => sigSum(1),
			O5 => sigSum(0),
			I0 => in_ter(2*0 + 0),
			I1 => in_ter(2*1 + 0),
			I2 => in_ter(2*2 + 0),
			I3 => in_ter(2*3 + 0),
			I4 => sig15,
			I5 => '1'   -- To have something different for O6 and O5
		);

	lut3: LUT6_2
		generic map (
			INIT => INIT_LUT3
		)
		port map (
			O6 => sigSum(2),
			O5 => sigSum(3),
			I0 => sigSum(0),
			I1 => sigSum(1),
			I2 => in_ter(2*0 + 1),
			I3 => sig15,
			I4 => sig16,
			I5 => in_ter(2*3 + 0)  -- THE trick
		);

	res <= std_logic_vector(resize(signed(sigSum), WOUT));

end architecture;

