
-- This is a ternary multiplier + adder tree, for ternary weights
-- It multiplies input values (signed) by ternary weights

-- Architecture :
--   - one layer of multiplication operations, combinational
--   - then one adder tree

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity muladdtree_ter_weights is
	generic(
		WDATA : natural := 8;
		NBIN  : natural := 16;
		WOUT  : natural := 12;
		-- Tag-related information
		TAGW  : natural := 1;
		TAGEN : boolean := true;
		TAGZC : boolean := false;
		-- How to add pipeline registers
		REGEN  : natural := 1;  -- 0=none, 1=all, else every EN stages
		REGIDX : natural := 0   -- Start index (from the leaves)
	);
	port(
		clk       : in  std_logic;
		clear     : in  std_logic;
		-- Weight and data input
		weight_in : in  std_logic_vector(NBIN*2-1 downto 0);
		data_in   : in  std_logic_vector(NBIN*WDATA-1 downto 0);
		-- Data output
		data_out  : out std_logic_vector(WOUT-1 downto 0);
		-- Tag, input and output
		tag_in    : in  std_logic_vector(TAGW-1 downto 0);
		tag_out   : out std_logic_vector(TAGW-1 downto 0)
	);
end muladdtree_ter_weights;

architecture synth of muladdtree_ter_weights is

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	-- Utility function to calculate next REGIDX
	function calc_regidx_next(REG_CREATE : boolean) return natural is
	begin
		if REGEN = 0 then
			return 0;
		end if;
		if REG_CREATE = true then
			return 0;
		end if;
		return (REGIDX + 1) mod REGEN;
	end function;

	-- Add pipeline registers when IDX = EN - 1
	-- Force creation of registers at last stage
	constant REG_CREATE  : boolean := (REGEN > 0) and ((REGIDX = REGEN-1) or (NBIN = 1));
	constant REGIDX_NEXT : natural := calc_regidx_next(REG_CREATE);

	constant TERNARY : boolean := (WDATA <= 2);

	signal multres_sig : std_logic_vector(NBIN*WDATA-1 downto 0);

	signal multres_reg : std_logic_vector(NBIN*WDATA-1 downto 0);
	signal multtag_reg : std_logic_vector(TAGW-1 downto 0);

	component mul_ter_weight is
		generic (
			WDATA : natural := 12
		);
		port (
			weight_in : in  std_logic_vector(1 downto 0);
			data_in   : in  std_logic_vector(WDATA-1 downto 0);
			data_out  : out std_logic_vector(WDATA-1 downto 0)
		);
	end component;

	component addtree is
		generic(
			-- Data type and width
			WDATA : natural := 8;
			SDATA : boolean := true;
			NBIN  : natural := 20;
			WOUT  : natural := 12;
			-- User-specified radix, for testing purposes (0 means automatic)
			RADIX : natural := 0;
			-- Special considerations about data nature
			BINARY : boolean := false;
			BINARY_RADIX : natural := 0;
			TERNARY : boolean := false;
			TERNARY_RADIX : natural := 0;
			-- An optional tag
			TAGW  : natural := 1;
			TAGEN : boolean := true;
			TAGZC : boolean := true;
			-- How to add pipeline registers
			REGEN  : natural := 1;  -- 0=none, 1=all, else every EN stages
			REGIDX : natural := 0   -- Start index (from the leaves)
		);
		port(
			clk      : in  std_logic;
			clear    : in  std_logic;
			-- Data, input and output
			data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
			data_out : out std_logic_vector(WOUT-1 downto 0);
			-- Tag, input and output
			tag_in   : in  std_logic_vector(TAGW-1 downto 0);
			tag_out  : out std_logic_vector(TAGW-1 downto 0)
		);
	end component;

begin

	-- Instantiate the ternary multipliers

	gen_mul : for i in 0 to NBIN-1 generate

		i_mul : mul_ter_weight
			generic map (
				WDATA => WDATA
			)
			port map (
				weight_in => weight_in((i+1)*2-1 downto i*2),
				data_in   => data_in((i+1)*WDATA-1 downto i*WDATA),
				data_out  => multres_sig((i+1)*WDATA-1 downto i*WDATA)
			);

	end generate;

	-- Registers after multiplier

	gen_bufmul_res : if REG_CREATE = true generate

		process(clk)
		begin
			if rising_edge(clk) then

				multres_reg <= multres_sig;

			end if;
		end process;

	else generate

		multres_reg <= multres_sig;

	end generate;

	gen_bufmul_tag : if (REG_CREATE = true) and (TAGEN = true) generate

		process(clk)
		begin
			if rising_edge(clk) then

				multtag_reg <= tag_in;
				if (clear = '1') and (TAGZC = true) then
					multtag_reg <= (others => '0');
				end if;

			end if;
		end process;

	else generate

		multtag_reg <= tag_in when TAGEN = true else (others => '0');

	end generate;

	-- The adder tree

	adder : addtree
		generic map (
			-- Data type and width
			WDATA => WDATA,
			SDATA => true,
			NBIN  => NBIN,
			WOUT  => WOUT,
			-- User-specified radix, for testing purposes (0 means automatic)
			RADIX => 0,
			-- Special considerations about data nature
			BINARY => false,
			BINARY_RADIX => 0,
			TERNARY => TERNARY,
			TERNARY_RADIX => 0,
			-- An optional tag
			TAGW  => TAGW,
			TAGEN => TAGEN,
			TAGZC => TAGZC,
			-- How to add pipeline registers
			REGEN  => REGEN,
			REGIDX => REGIDX_NEXT
		)
		port map (
			clk      => clk,
			clear    => clear,
			-- Data, input and output
			data_in  => multres_reg,
			data_out => data_out,
			-- Tag, input and output
			tag_in   => multtag_reg,
			tag_out  => tag_out
		);

end architecture;

