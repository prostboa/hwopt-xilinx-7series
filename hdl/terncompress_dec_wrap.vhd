
-- This is a wrapper for binary-to-ternary decompressors

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity terncompress_dec_wrap is
	generic(
		NTER : natural := 3;
		BINW : natural := 5
	);
	port(
		bin_in  : in  std_logic_vector(BINW-1 downto 0);
		ter_out : out std_logic_vector(2*NTER-1 downto 0)
	);
end terncompress_dec_wrap;

architecture synth of terncompress_dec_wrap is

	-- Component to decompress 5b -> 3T
	component terncompress_5b_3t is
		port(
			bin_in  : in  std_logic_vector(4 downto 0);
			ter_out : out std_logic_vector(5 downto 0)
		);
	end component;

	-- Component to decompress 8b -> 5T
	component terncompress_8b_5t is
		port(
			bin_in  : in  std_logic_vector(7 downto 0);
			ter_out : out std_logic_vector(9 downto 0)
		);
	end component;

begin

	gen_3t : if NTER = 3 generate

		comp : terncompress_5b_3t
			port map (
				bin_in  => bin_in,
				ter_out => ter_out
			);

	end generate;

	gen_5t : if NTER = 5 generate

		comp : terncompress_8b_5t
			port map (
				bin_in  => bin_in,
				ter_out => ter_out
			);

	end generate;

end architecture;

