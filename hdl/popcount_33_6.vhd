
-- This is a popcount 33b -> 6b
-- Total size : 8x2 + 4 = 20 LUTs
-- Delay : 3 LUTs + 3*carry propag

-- Architecture : 2 popcounts 16:4 then one adder 2x 5b + 1b

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity popcount_33_6 is
	generic (
		BITS : natural := 33;
		WOUT : natural := 6
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount_33_6;

architecture synth of popcount_33_6 is

	signal sigBits : std_logic_vector(32 downto 0) := (others => '0');

	signal sum1 : std_logic_vector(4 downto 0) := (others => '0');
	signal sum2 : std_logic_vector(4 downto 0) := (others => '0');

	component popcount_16_5 is
		generic (
			BITS : natural := 16;
			WOUT : natural := 5
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component adder_2in_5b_plus1 is
		generic (
			WOUT : natural := 6
		);
		port (
			inA : in  std_logic_vector(4 downto 0);
			inB : in  std_logic_vector(4 downto 0);
			in1 : in  std_logic;
			res : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

begin

	assert BITS <= 33 report "Number of inputs too large for module popcount_33_6" severity failure;

	sigBits(BITS-1 downto 0) <= bits_in;

	popc_1 : popcount_16_5
		port map (
			bits_in => sigBits(15 downto 0),
			sum     => sum1
		);

	popc_2 : popcount_16_5
		port map (
			bits_in => sigBits(31 downto 16),
			sum     => sum2
		);

	comp_3 : adder_2in_5b_plus1
		generic map(
			WOUT => WOUT
		)
		port map (
			inA => sum1,
			inB => sum2,
			in1 => sigBits(32),
			res => sum
		);

end architecture;

