
-- This is a popcount 15b -> 4b
-- Total size : 2x3 + 2 = 8 LUTs
-- Delay : 4 LUTs

-- Architecture : first two popcounts 7:3 then one compressor 2,2,3:4

-- Note : A faster version would be possibe with a 3-lut, 1-lut delay adder implementation
-- Result would be 9 luts, delay 3 luts

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT primitives
library unisim;
use unisim.vcomponents.all;

entity popcount_15_4_slow is
	generic (
		BITS : natural := 15;
		WOUT : natural := 4
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount_15_4_slow;

architecture synth of popcount_15_4_slow is

	signal sigBits : std_logic_vector(14 downto 0) := (others => '0');
	signal sigSum  : std_logic_vector(3 downto 0) := (others => '0');

	signal sum73_1 : std_logic_vector(2 downto 0) := (others => '0');
	signal sum73_2 : std_logic_vector(2 downto 0) := (others => '0');

	component popcount_7_3 is
		generic (
			BITS : natural := 7;
			WOUT : natural := 3
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component adder_2in_3b is
		generic (
			IS_SIGNED : boolean := false;
			WOUT      : natural := 4
		);
		port (
			inA : in  std_logic_vector(2 downto 0);
			inB : in  std_logic_vector(2 downto 0);
			in1 : in  std_logic;
			res : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

begin

	assert BITS <= 15 report "Number of inputs too large for module popcount_15_4" severity failure;

	sigBits(BITS-1 downto 0) <= bits_in;

	popc73_1 : popcount_7_3
		port map (
			bits_in => sigBits(6 downto 0),
			sum     => sum73_1
		);

	popc73_2 : popcount_7_3
		port map (
			bits_in => sigBits(13 downto 7),
			sum     => sum73_2
		);

	add2p1 : adder_2in_3b
		port map (
			inA => sum73_1,
			inB => sum73_2,
			in1 => sigBits(14),
			res => sigSum
		);

	sum <= std_logic_vector(resize(unsigned(sigSum), WOUT));

end architecture;

