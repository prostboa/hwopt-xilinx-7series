
-- This is multi-stage MIN or MAX operator designed for performance
-- The registers are created with radix 2

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity minmaxtree is
	generic(
		WDATA : natural := 8;
		SDATA : boolean := true;
		NBIN  : natural := 20;
		OPMAX : boolean := true;
		-- Optional tag propagation
		TAGW  : natural := 1;
		TAGEN : boolean := true;
		TAGZC : boolean := true;
		-- How to add pipeline registers
		REGEN  : natural := 1;  -- 0=none, 1=all, else every EN stages
		REGIDX : natural := 0   -- Start index (from the leaves)
	);
	port(
		clk       : in  std_logic;
		clear     : in  std_logic;
		-- Data : input and output
		data_in   : in  std_logic_vector(NBIN*WDATA-1 downto 0);
		data_out  : out std_logic_vector(WDATA-1 downto 0);
		-- Tag : input and output
		tag_in    : in  std_logic_vector(TAGW-1 downto 0);
		tag_out   : out std_logic_vector(TAGW-1 downto 0)
	);
end minmaxtree;

architecture synth of minmaxtree is

	-- Utility function to get the desired min or max
	function local_max(inA: std_logic_vector; inB: std_logic_vector) return std_logic_vector is
	begin
		if
			((SDATA =  true) and (  signed(inA) >=   signed(inB))) or
			((SDATA = false) and (unsigned(inA) >= unsigned(inB)))
		then
			-- Here inA >= inB
			if OPMAX = true then
				return inA;
			end if;
			return inB;
		else
			-- Here inA < inB
			if OPMAX = true then
				return inB;
			end if;
			return inA;
		end if;
		-- Unreachable line
		return inA;
	end function;

	-- Utility function to calculate next REGIDX
	function calc_regidx_next(REG_CREATE : boolean) return natural is
	begin
		if REGEN = 0 then
			return 0;
		end if;
		if REG_CREATE = true then
			return 0;
		end if;
		return (REGIDX + 1) mod REGEN;
	end function;

	-- The number of registers needed in the local stage
	constant NBREGS : natural := (NBIN + 1) / 2;

	-- Add pipeline registers when IDX = EN - 1
	-- Force creation of registers at last stage
	constant REG_CREATE  : boolean := (NBIN > 1) and (REGEN > 0) and ((REGIDX = REGEN-1) or (NBREGS = 1));
	constant REGIDX_NEXT : natural := calc_regidx_next(REG_CREATE);

	-- The registers of the current level
	signal regs_data, regs_data_n : std_logic_vector(NBREGS*WDATA-1 downto 0);
	signal regs_tag : std_logic_vector(TAGW-1 downto 0);

begin

	-- Compute the MIN or MAX operations

	gen_leaves : if NBIN >= 2 generate
		gen_loop : for i in 0 to NBIN/2-1 generate
			regs_data_n((i+1)*WDATA-1 downto i*WDATA) <= local_max(data_in((2*i+2)*WDATA-1 downto (2*i+1)*WDATA), data_in((2*i+1)*WDATA-1 downto 2*i*WDATA));
		end generate;
	end generate;

	gen_last_leaf : if NBIN / 2 < NBREGS generate
		regs_data_n(NBREGS*WDATA-1 downto (NBREGS-1)*WDATA) <= data_in(NBIN*WDATA-1 downto (NBIN-1)*WDATA);
	end generate;

	-- Create pipeline registers

	gen_regs_data : if (NBIN = 1) or (REG_CREATE = false) generate

		-- No pipeline registers, just passthrough
		regs_data <= regs_data_n;

	else generate

		process(clk)
		begin
			if rising_edge(clk) then

				regs_data <= regs_data_n;

			end if;
		end process;

	end generate;

	gen_regs_tag : if (NBIN = 1) or (REG_CREATE = false) or (TAGEN = false) generate

		-- No pipeline registers, just passthrough
		regs_tag <= tag_in when TAGEN = true else (others => '0');

	else generate

		process(clk)
		begin
			if rising_edge(clk) then

				regs_tag <= tag_in;
				if (clear = '1') and (TAGZC = true) then
					regs_tag <= (others => '0');
				end if;

			end if;
		end process;

	end generate;

	-- Instantiate the upper stages of the tree

	gen_notree : if NBREGS <= 1 generate

		data_out <= regs_data;
		tag_out  <= regs_tag;

	end generate;

	-- WARNING Many versions of Vivado (2017.2 up to 2021.1 at least) require special recursive instantiation pattern
	-- Vivado segfaults when the instantiation is in an "else generate" statement
	-- So, isolate that in the most direct "if generate" part of the statement
	gen_tree : if NBREGS > 1 generate

		-- Instantiate the sub-stage
		i_stage: entity minmaxtree
			generic map (
				WDATA => WDATA,
				SDATA => SDATA,
				NBIN  => NBREGS,
				OPMAX => OPMAX,
				-- Optional tag propagation
				TAGW  => TAGW,
				TAGEN => TAGEN,
				TAGZC => TAGZC,
				-- How to add pipeline registers
				REGEN  => REGEN,       -- 0=none, 1=all, else every EN stages
				REGIDX => REGIDX_NEXT  -- Start index (from the leaves)
			)
			port map (
				clk      => clk,
				clear    => clear,
				-- Data : input and output
				data_in  => regs_data,
				data_out => data_out,
				-- Tag : input and output
				tag_in   => regs_tag,
				tag_out  => tag_out
			);

	end generate;

end architecture;

