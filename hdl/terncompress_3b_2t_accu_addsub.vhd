
-- This is one mult+add+accumulator for ternary weight
-- It performs a ternary weight decompression from a 3b code
-- Warning : the pair of ternary weights {-1;-1} can't be generated, this is hardcoded
-- Optimized at netlist level for Xilinx primitives

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity terncompress_3b_2t_accu_addsub is
	generic(
		WDATA : natural := 8;
		WACCU : natural := 8;
		COM3B2T_EN  : boolean := false;
		COM3B2T_IDX : natural := 0
	);
	port(
		clk       : in  std_logic;
		-- Control signals
		cmd_clear : in  std_logic;
		cmd_add   : in  std_logic;
		-- Data input
		bin_in    : in  std_logic_vector(2 downto 0);
		weight_in : in  std_logic_vector(1 downto 0);
		data_in   : in  std_logic_vector(WDATA-1 downto 0);
		-- Accumulator output
		accu_out  : out std_logic_vector(WACCU-1 downto 0)
	);
end terncompress_3b_2t_accu_addsub;

architecture synth of terncompress_3b_2t_accu_addsub is

	-- The resized input vector
	signal sigdata : std_logic_vector(WACCU-1 downto 0);

	-- The output of the 2-bit command generator, 1 LUT6
	signal sigcmd : std_logic_vector(1 downto 0);

	-- The ACCU register
	signal regaccu : std_logic_vector(WACCU-1 downto 0);

	-- The number of CARRY4 primitives to instantiate
	constant CNT_NBC4  : natural := (WACCU+3) / 4;

	-- Temp signals to connect the CARRY4 components, for ALU of accumulator
	-- Initialization to "don't care" for partial CARRY4 utilization
	signal inst_accu_c4_o      : std_logic_vector(4*CNT_NBC4-1 downto 0) := (others => '-');
	signal inst_accu_c4_co     : std_logic_vector(4*CNT_NBC4-1 downto 0) := (others => '-');
	signal inst_accu_c4_di     : std_logic_vector(4*CNT_NBC4-1 downto 0) := (others => '-');
	signal inst_accu_c4_s      : std_logic_vector(4*CNT_NBC4-1 downto 0) := (others => '-');
	signal inst_accu_c4_ci     : std_logic_vector(CNT_NBC4-1 downto 0) := (others => '-');
	signal inst_accu_c4_cyinit : std_logic_vector(CNT_NBC4-1 downto 0) := (others => '-');

begin

	-------------------------------------------------------------------
	-- Sequential process
	-------------------------------------------------------------------

	process(clk)
	begin
		if rising_edge(clk) then

			-- Note: Intentionally not using WE so the other slice FFs are still usable
			regaccu <= inst_accu_c4_o(WACCU-1 downto 0);

		end if;
	end process;


	-------------------------------------------------------------------
	-- 2-bit command generator
	-------------------------------------------------------------------

	-- One LUT6_2 is enough: 2 bits (ports) + 2 bits (weight) -> 2 bits command
	--
	-- Command encoding:
	--    ZERO : 00  Mult by zero, or no add
	--     ADD : 01  Mult by 1
	--     SUB : 10  Mult by -1
	-- Note: the port accu_clear is sent to the ALU
	--
	-- Weight encoding: natural signed binary
	--   -1 = 11
	--    0 = 00
	--   +1 = 01
	--
	-- Truth table for LUT contents
	-- pa = port add
	--
	--    | pa w1 w0 | O6 O5 |  output: bit 0 is output O5, bit 1 is output O6
	-- =======================
	--  0 |  0  0  0 |  0  0 |  ZERO
	--  1 |  0  0  1 |  0  0 |  ZERO
	--  2 |  0  1  0 |  0  0 |  ZERO
	--  3 |  0  1  1 |  0  0 |  ZERO (invalid)
	-- -----------------------
	--  4 |  1  0  0 |  0  0 |  ZERO
	--  5 |  1  0  1 |  0  1 |  ADD
	--  6 |  1  1  0 |  0  0 |  ZERO (invalid)
	--  7 |  1  1  1 |  1  0 |  SUB
	--
	-- Config for O6 is 32 upper bits, O5 is 32 lower bits: 00000080 00000020
	--
	-- The binary-to-ternary codes are distributed like this:
	-- 000 -> 0000 ->  0  0
	-- 001 -> 0001 ->  0  1
	-- 010 -> 0011 ->  0 -1
	-- 011 -> 0100 ->  1  0
	-- 100 -> 0101 ->  1  1
	-- 101 -> 0111 ->  1 -1
	-- 110 -> 1100 -> -1  0
	-- 111 -> 1101 -> -1  1
	--
	-- Truth table for LUT contents, IDX = 0
	-- pa = port add
	--
	--    | pa b2 b1 b0 | O6 O5 |  output: bit 0 is output O5, bit 1 is output O6
	-- ==========================
	--  0 |  0  0  0  0 |  0  0 |  ZERO
	--  1 |  0  0  0  1 |  0  0 |  ZERO
	--  2 |  0  0  1  0 |  0  0 |  ZERO
	--  3 |  0  0  1  1 |  0  0 |  ZERO
	--  4 |  0  1  0  0 |  0  0 |  ZERO
	--  5 |  0  1  0  1 |  0  0 |  ZERO
	--  6 |  0  1  1  0 |  0  0 |  ZERO
	--  7 |  0  1  1  1 |  0  0 |  ZERO
	-- --------------------------
	--  8 |  1  0  0  0 |  0  0 |  weight =  0 -> ZERO
	--  9 |  1  0  0  1 |  0  1 |  weight =  1 -> ADD
	-- 10 |  1  0  1  0 |  1  0 |  weight = -1 -> SUB
	-- 11 |  1  0  1  1 |  0  0 |  weight =  0 -> ZERO
	-- 12 |  1  1  0  0 |  0  1 |  weight =  1 -> ADD
	-- 13 |  1  1  0  1 |  1  0 |  weight = -1 -> SUB
	-- 14 |  1  1  1  0 |  0  0 |  weight =  0 -> ZERO
	-- 15 |  1  1  1  1 |  0  1 |  weight =  1 -> ADD
	--
	-- Config for O6 is 32 upper bits, O5 is 32 lower bits: 00002400 00009200
	--
	-- Truth table for LUT contents, IDX = 1
	-- pa = port add
	--
	--    | pa b2 b1 b0 | O6 O5 |  output: bit 0 is output O5, bit 1 is output O6
	-- ==========================
	--  0 |  0  0  0  0 |  0  0 |  ZERO
	--  1 |  0  0  0  1 |  0  0 |  ZERO
	--  2 |  0  0  1  0 |  0  0 |  ZERO
	--  3 |  0  0  1  1 |  0  0 |  ZERO
	--  4 |  0  1  0  0 |  0  0 |  ZERO
	--  5 |  0  1  0  1 |  0  0 |  ZERO
	--  6 |  0  1  1  0 |  0  0 |  ZERO
	--  7 |  0  1  1  1 |  0  0 |  ZERO
	-- --------------------------
	--  8 |  1  0  0  0 |  0  0 |  weight =  0 -> ZERO
	--  9 |  1  0  0  1 |  0  0 |  weight =  0 -> ZERO
	-- 10 |  1  0  1  0 |  0  0 |  weight =  0 -> ZERO
	-- 11 |  1  0  1  1 |  0  1 |  weight =  1 -> ADD
	-- 12 |  1  1  0  0 |  0  1 |  weight =  1 -> ADD
	-- 13 |  1  1  0  1 |  0  1 |  weight =  1 -> ADD
	-- 14 |  1  1  1  0 |  1  0 |  weight = -1 -> SUB
	-- 15 |  1  1  1  1 |  1  0 |  weight = -1 -> SUB
	--
	-- Config for O6 is 32 upper bits, O5 is 32 lower bits: 0000C000 00003800

	gen_nocomp : if COM3B2T_EN = false generate

		lutcmd: LUT6_2
			generic map (
				INIT => x"0000008000000020"
			)
			port map (
				O6 => sigcmd(1),
				O5 => sigcmd(0),
				I0 => weight_in(0),
				I1 => weight_in(1),
				I2 => cmd_add,
				I3 => '0',  -- Unused input
				I4 => '0',  -- Unused input
				I5 => '1'   -- To have something different for O6 and O5
			);

	end generate;

	gen_comp0 : if (COM3B2T_EN = true) and (COM3B2T_IDX = 0) generate

		lutcmd: LUT6_2
			generic map (
				INIT => x"0000240000009200"
			)
			port map (
				O6 => sigcmd(1),
				O5 => sigcmd(0),
				I0 => bin_in(0),
				I1 => bin_in(1),
				I2 => bin_in(2),
				I3 => cmd_add,
				I4 => '0',  -- Unused input
				I5 => '1'   -- To have something different for O6 and O5
			);

	end generate;

	gen_comp1 : if (COM3B2T_EN = true) and (COM3B2T_IDX = 1) generate

		lutcmd: LUT6_2
			generic map (
				INIT => x"0000C00000003800"
			)
			port map (
				O6 => sigcmd(1),
				O5 => sigcmd(0),
				I0 => bin_in(0),
				I1 => bin_in(1),
				I2 => bin_in(2),
				I3 => cmd_add,
				I4 => '0',  -- Unused input
				I5 => '1'   -- To have something different for O6 and O5
			);

	end generate;


	-------------------------------------------------------------------
	-- ALU for accumulator
	-------------------------------------------------------------------

	-- Instantiation of the LUT / CARRY4 components for ALU of the accumulator
	--
	-- Truth table for LUT contents
	--
	--    |  clr cmd1 cmd0 data accu |  O6  O5  |  O6 = input S (data), O5 = input DI (carry in)
	-- ==============================================
	--  0 |   0    0    0    0    0  |   0   0  |  KEEP => O6 = accu, O5 = 0
	--  1 |   0    0    0    0    1  |   1   0  |  KEEP
	--  2 |   0    0    0    1    0  |   0   0  |  KEEP
	--  3 |   0    0    0    1    1  |   1   0  |  KEEP
	--  4 |   0    0    1    0    0  |   0   0  |  ADD => O6 = accu xor data, O5 = accu and data
	--  5 |   0    0    1    0    1  |   1   0  |  ADD
	--  6 |   0    0    1    1    0  |   1   0  |  ADD
	--  7 |   0    0    1    1    1  |   0   1  |  ADD
	-- -------------------------------------------------
	--  8 |   0    1    0    0    0  |   1   0  |  SUB => O6 = accu nxor data, O5 = accu and not(data)
	--  9 |   0    1    0    0    1  |   0   1  |  SUB
	-- 10 |   0    1    0    1    0  |   0   0  |  SUB
	-- 11 |   0    1    0    1    1  |   1   0  |  SUB
	-- 12 |   0    1    1    0    0  |   0   0  |  INVALID CMD
	-- 13 |   0    1    1    0    1  |   1   0  |  INVALID CMD
	-- 14 |   0    1    1    1    0  |   0   0  |  INVALID CMD
	-- 15 |   0    1    1    1    1  |   1   0  |  INVALID CMD
	-- -------------------------------------------------
	-- 16 |   1    0    0    0    0  |   0   0  |  SET ZERO
	-- 17 |   1    0    0    0    1  |   0   0  |  SET ZERO
	-- 18 |   1    0    0    1    0  |   0   0  |  SET ZERO
	-- 19 |   1    0    0    1    1  |   0   0  |  SET ZERO
	-- 20 |   1    0    1    0    0  |   0   0  |  SET
	-- 21 |   1    0    1    0    1  |   0   0  |  SET
	-- 22 |   1    0    1    1    0  |   1   0  |  SET
	-- 32 |   1    0    1    1    1  |   1   0  |  SET
	-- -------------------------------------------------
	-- 24 |   1    1    0    0    0  |   1   0  |  SET NEG => O6 = not(data), O5 = 0
	-- 25 |   1    1    0    0    1  |   1   0  |  SET NEG
	-- 26 |   1    1    0    1    0  |   0   0  |  SET NEG
	-- 27 |   1    1    0    1    1  |   0   0  |  SET NEG
	-- 28 |   1    1    1    0    0  |   0   0  |  CLEAR + INVALID CMD
	-- 29 |   1    1    1    0    1  |   0   0  |  CLEAR + INVALID CMD
	-- 30 |   1    1    1    1    0  |   0   0  |  CLEAR + INVALID CMD
	-- 31 |   1    1    1    1    1  |   0   0  |  CLEAR + INVALID CMD
	--
	-- Config for O6 is 32 upper bits, O5 is 32 lower bits: 03C0A96A 00000280

	sigdata <= std_logic_vector(resize(signed(data_in), WACCU));

	-- The LUTs of the ALU
	gen_accu_luts: for l in 0 to WACCU-1 generate

		i_lut: LUT6_2
			generic map (
				INIT => x"03C0A96A00000280"
			)
			port map (
				O6 => inst_accu_c4_s(l),
				O5 => inst_accu_c4_di(l),
				I0 => regaccu(l),
				I1 => sigdata(l),
				I2 => sigcmd(0),
				I3 => sigcmd(1),
				I4 => cmd_clear,
				I5 => '1'   -- To have something different for O6 and O5
			);

	end generate;

	-- Generate the CARRY4 components
	gen_accu_c4: for i in 0 to CNT_NBC4-1 generate

		i_c4: CARRY4
			port map (
				CO     => inst_accu_c4_co(4*(i+1)-1 downto 4*i),
				O      => inst_accu_c4_o (4*(i+1)-1 downto 4*i),
				DI     => inst_accu_c4_di(4*(i+1)-1 downto 4*i),
				S      => inst_accu_c4_s (4*(i+1)-1 downto 4*i),
				CI     => inst_accu_c4_ci(i),
				CYINIT => inst_accu_c4_cyinit(i)
			);

	end generate;

	-- Inter-CARRY4 connections
	gen_accu_c4_conn: for i in 1 to CNT_NBC4-1 generate

		inst_accu_c4_ci(i)     <= inst_accu_c4_co(4*i-1);
		inst_accu_c4_cyinit(i) <= '0';  -- Always propagate the carry

	end generate;

	inst_accu_c4_ci(0)     <= '0';  -- Unused
	inst_accu_c4_cyinit(0) <= sigcmd(1);  -- For subtraction: the carry is not(borrow)  FIXME This +1 could easily be done in the first LUT


	-------------------------------------------------------------------
	-- Output port
	-------------------------------------------------------------------

	accu_out <= regaccu;

end architecture;

