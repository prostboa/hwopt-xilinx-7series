
-- This is a ternary-to-binary decompressor 8b -> 5T
-- Based on look-up tables
-- Implementation that leads to small decoder when implemented with Xilinx LUT6_2 LUTs

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT primitives
library unisim;
use unisim.vcomponents.all;

entity terncompress_8b_5t is
	port(
		bin_in  : in  std_logic_vector(7 downto 0);
		ter_out : out std_logic_vector(9 downto 0)
	);
end terncompress_8b_5t;

architecture synth of terncompress_8b_5t is

	signal dec_t0 : std_logic_vector(1 downto 0);
	signal dec_t1 : std_logic_vector(1 downto 0);
	signal dec_t2 : std_logic_vector(1 downto 0);

	signal extra_flag : std_logic;

	signal gen_t0 : std_logic_vector(1 downto 0);
	signal gen_t1 : std_logic_vector(1 downto 0);
	signal gen_t2 : std_logic_vector(1 downto 0);
	signal gen_t3 : std_logic_vector(1 downto 0);
	signal gen_t4 : std_logic_vector(1 downto 0);

begin

	-- First stage of decompression of 3 lower trits

	lut_dec_t0: LUT6_2
		generic map (
			INIT => x"4C9249242EDB6DB6"
		)
		port map (
			O6 => dec_t0(1),
			O5 => dec_t0(0),
			I0 => bin_in(0),
			I1 => bin_in(1),
			I2 => bin_in(2),
			I3 => bin_in(3),
			I4 => bin_in(4),
			I5 => '1'   -- To have something different for O6 and O5
		);

	lut_dec_t1: LUT6_2
		generic map (
			INIT => x"4F0381C02FE3F1F8"
		)
		port map (
			O6 => dec_t1(1),
			O5 => dec_t1(0),
			I0 => bin_in(0),
			I1 => bin_in(1),
			I2 => bin_in(2),
			I3 => bin_in(3),
			I4 => bin_in(4),
			I5 => '1'   -- To have something different for O6 and O5
		);

	lut_dec_t2: LUT6_2
		generic map (
			INIT => x"4FFC00002FFFFE00"
		)
		port map (
			O6 => dec_t2(1),
			O5 => dec_t2(0),
			I0 => bin_in(0),
			I1 => bin_in(1),
			I2 => bin_in(2),
			I3 => bin_in(3),
			I4 => bin_in(4),
			I5 => '1'   -- To have something different for O6 and O5
		);

	-- Generate the flag that indicates we are in the 5 extra codes for the 5b lower range of binary input

	lut_extra_flag: LUT5
		generic map (
			INIT => x"78000000"
		)
		port map (
			O  => extra_flag,
			I0 => bin_in(0),
			I1 => bin_in(1),
			I2 => bin_in(2),
			I3 => bin_in(3),
			I4 => bin_in(4)
		);

	-- Second stage, generate final lower 3 trits

	lut_gen_t0_0: LUT6
		generic map (
			INIT => x"6A3ADAEA3ADAEA0A"
		)
		port map (
			O  => gen_t0(0),
			I0 => dec_t0(0),
			I1 => dec_t0(1),
			I2 => extra_flag,
			I3 => bin_in(5),
			I4 => bin_in(6),
			I5 => bin_in(7)
		);

	lut_gen_t0_1: LUT6
		generic map (
			INIT => x"482818C82818C808"
		)
		port map (
			O  => gen_t0(1),
			I0 => dec_t0(0),
			I1 => dec_t0(1),
			I2 => extra_flag,
			I3 => bin_in(5),
			I4 => bin_in(6),
			I5 => bin_in(7)
		);

	lut_gen_t1_0: LUT6
		generic map (
			INIT => x"0AFA9A7ABA6AFA0A"
		)
		port map (
			O  => gen_t1(0),
			I0 => dec_t1(0),
			I1 => dec_t1(1),
			I2 => extra_flag,
			I3 => bin_in(5),
			I4 => bin_in(6),
			I5 => bin_in(7)
		);

	lut_gen_t1_1: LUT6
		generic map (
			INIT => x"08489808B8087808"
		)
		port map (
			O  => gen_t1(1),
			I0 => dec_t1(0),
			I1 => dec_t1(1),
			I2 => extra_flag,
			I3 => bin_in(5),
			I4 => bin_in(6),
			I5 => bin_in(7)
		);

	lut_gen_t2_0: LUT6
		generic map (
			INIT => x"0A0A6AFAFAFAFA0A"
		)
		port map (
			O  => gen_t2(0),
			I0 => dec_t2(0),
			I1 => dec_t2(1),
			I2 => extra_flag,
			I3 => bin_in(5),
			I4 => bin_in(6),
			I5 => bin_in(7)
		);

	lut_gen_t2_1 : LUT6
		generic map (
			INIT => x"0808080848F8F808"
		)
		port map (
			O  => gen_t2(1),
			I0 => dec_t2(0),
			I1 => dec_t2(1),
			I2 => extra_flag,
			I3 => bin_in(5),
			I4 => bin_in(6),
			I5 => bin_in(7)
		);

	-- Second stage, generate final upper 2 trits

	lut_gen_t3_0: LUT6
		generic map (
			INIT => x"7FF0FFFFF0FFFF00"
		)
		port map (
			O  => gen_t3(0),
			I0 => bin_in(0),
			I1 => bin_in(1),
			I2 => extra_flag,
			I3 => bin_in(5),
			I4 => bin_in(6),
			I5 => bin_in(7)
		);

	lut_gen_t3_1: LUT6
		generic map (
			INIT => x"70F0FFF0F0FFF000"
		)
		port map (
			O  => gen_t3(1),
			I0 => bin_in(0),
			I1 => bin_in(1),
			I2 => extra_flag,
			I3 => bin_in(5),
			I4 => bin_in(6),
			I5 => bin_in(7)
		);

	lut_gen_t4_0: LUT6
		generic map (
			INIT => x"7FFFFFFFFFF0F000"
		)
		port map (
			O  => gen_t4(0),
			I0 => bin_in(0),
			I1 => bin_in(1),
			I2 => extra_flag,
			I3 => bin_in(5),
			I4 => bin_in(6),
			I5 => bin_in(7)
		);

	lut_gen_t4_1: LUT6
		generic map (
			INIT => x"7FFFF0F0F0F0F000"
		)
		port map (
			O  => gen_t4(1),
			I0 => bin_in(0),
			I1 => bin_in(1),
			I2 => extra_flag,
			I3 => bin_in(5),
			I4 => bin_in(6),
			I5 => bin_in(7)
		);

	-- Assign output port

	ter_out <= gen_t4 & gen_t3 & gen_t2 & gen_t1 & gen_t0;

end architecture;

