
-- This is one BRAM, with a different aspect ratio on write side and read side
-- This component chooses between BRAM 36k and BRAM 18k

-- Note : This component was designed for a specific use case with ternary numbers and for write width larger than read width
-- It could be generalized

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For BRAM macro declaration
library unimacro;
use unimacro.vcomponents.all;

entity largemem_block_asym is
	generic (
		-- Data width and depth
		WDATA   : natural := 2;
		CELLS   : natural := 1024;
		WADDR   : natural := 10;
		-- Desired geometry, read/write
		NPERBLK : natural := 18;
		WRNB    : natural := 2;
		-- Whether to use the output register
		REGDOEN : boolean := true;
		-- Optional tag
		TAGW    : natural := 1;
		TAGEN   : boolean := true;
		TAGZC   : boolean := true
	);
	port (
		clk        : in  std_logic;
		clear      : in  std_logic;
		-- Write side
		write_en   : in  std_logic;
		write_addr : in  std_logic_vector(WADDR-1 downto 0);
		write_data : in  std_logic_vector(WDATA*NPERBLK*WRNB-1 downto 0);
		-- Read side
		read_en    : in  std_logic;
		read_addr  : in  std_logic_vector(WADDR-1 downto 0);
		read_data  : out std_logic_vector(WDATA*NPERBLK-1 downto 0);
		-- Tag, input and output
		tag_in     : in  std_logic_vector(TAGW-1 downto 0);
		tag_out    : out std_logic_vector(TAGW-1 downto 0)
	);
end largemem_block_asym;

architecture synth of largemem_block_asym is

	-- The BRAM data widthes
	constant WRD : natural := WDATA * NPERBLK;
	constant WWR : natural := WDATA * NPERBLK * WRNB;

	-- A data type to hold BRAM configuration
	type bram_t is record
		-- The block size to use: 18 or 36
		BLKSZ  : natural;
		-- The address width for the BRAMs
		WWADDR : natural;
		RWADDR : natural;
		-- The width for the WE port of the BRAM: 4 or 8 bits
		WWE    : natural;
	end record;

	function calc_waddr36k(dw : natural) return natural is
		variable aw: natural := 0;
	begin
		if dw <= 1 then
			aw := 15;
		elsif dw <= 2 then
			aw := 14;
		elsif dw <= 4 then
			aw := 13;
		elsif dw <= 9 then
			aw := 12;
		elsif dw <= 18 then
			aw := 11;
		elsif dw <= 36 then
			aw := 10;
		elsif dw <= 72 then
			aw := 9;
		end if;
		return aw;
	end function;

	function calc_bram(phony : boolean) return bram_t is
		variable bram: bram_t := (0, 0, 0, 0);
	begin
		-- Sanity checks
		if WWR > 72 then
			report "Implementation not possible because BRAM36 write width too large" severity FAILURE;
		end if;
		if WRD*CELLS > 36 * 1024 then
			report "Implementation not possible because BRAM36 size exceeded" severity FAILURE;
		end if;

		-- Check if BRAM 36k is mandatory
		if WWR > 36 then
			bram.BLKSZ := 36;
		end if;
		if WRD*CELLS > 18 * 1024 then
			bram.BLKSZ := 36;
		end if;
		-- Otherwise, use BRAM 18k
		if bram.BLKSZ = 0 then
			bram.BLKSZ := 18;
		end if;

		-- Calculate width of addresses
		bram.WWADDR := calc_waddr36k(WWR);
		bram.RWADDR := calc_waddr36k(WRD);
		if bram.BLKSZ = 18 then
			bram.WWADDR := bram.WWADDR - 1;
			bram.RWADDR := bram.RWADDR - 1;
		end if;

		-- Calculate width of WE signal
		-- FIXME Here we assume that WWR is an appropriate enough multiple of 8...
		bram.WWE := WWR / 8;
		if bram.WWE = 0 then
			bram.WWE := 1;
		end if;
		if bram.WWE > 8 then
			bram.WWE := 8;
		end if;

		--report "BRAM" & natural'image(bram.BLKSZ) & "Kb" &
		--	" wwaddr " & natural'image(bram.WWADDR) &
		--	" rwaddr " & natural'image(bram.RWADDR) &
		--	" wwr "    & natural'image(WWR) &
		--	" wrd "    & natural'image(WRD) &
		--	" wrnb "   & natural'image(WRNB) &
		--	" cells "  & natural'image(CELLS);

		return bram;
	end function;

	-- Select an appropriate BRAM configuration
	constant BRAM : bram_t := calc_bram(false);
	constant BRAM_STR : string := natural'image(BRAM.BLKSZ) & "Kb";
	constant BRAM_WE  : std_logic_vector(BRAM.WWE-1 downto 0) := (others => '1');

	function calc_do_reg(b: boolean) return integer is
	begin
		if b = false then
			return 0;
		end if;
		return 1;
	end function;

	constant DO_REG : integer := calc_do_reg(REGDOEN);

	-- Intermediate signals to instantiate the BRAM
	signal sigrdaddr : std_logic_vector(BRAM.RWADDR-1 downto 0);
	signal sigwraddr : std_logic_vector(BRAM.WWADDR-1 downto 0);

begin

	-------------------------------------------------------------------
	-- Instantiate the BRAM
	-------------------------------------------------------------------

	sigrdaddr <= std_logic_vector(resize(unsigned(read_addr), BRAM.RWADDR));
	sigwraddr <= std_logic_vector(resize(unsigned(write_addr), BRAM.WWADDR));

	bram_i : BRAM_SDP_MACRO
		generic map (
			BRAM_SIZE   => BRAM_STR,              -- Target BRAM, "18Kb" or "36Kb"
			DEVICE      => "7SERIES",             -- Target device: "VIRTEX5", "VIRTEX6", "7SERIES", "SPARTAN6"
			WRITE_WIDTH => WWR,                   -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
			READ_WIDTH  => WRD,                   -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
			DO_REG      => DO_REG,                -- Optional output register (0 or 1)
			INIT_FILE   => "NONE",
			SRVAL       => X"000000000000000000", -- Set/Reset value for port output
			WRITE_MODE  => "READ_FIRST",          -- Specify "READ_FIRST" for same clock or synchronous clocks, or "WRITE_FIRST for asynchronous clocks on ports
			INIT        => X"000000000000000000"  -- Initial value on output port
		)
		port map (
			-- Output read data port, width defined by READ_WIDTH parameter
			DO     => read_data,
			-- Input write data port, width defined by WRITE_WIDTH parameter
			DI     => write_data,
			-- Input read address, width defined by read port depth
			RDADDR => sigrdaddr,
			-- 1-bit input read clock
			RDCLK  => clk,
			-- 1-bit input read port enable
			RDEN   => read_en,
			-- 1-bit input read output register clock enable
			REGCE  => '1',
			-- 1-bit input reset
			RST    => clear,
			-- Input write enable, width defined by write port depth
			WE     => BRAM_WE,
			-- Input write address, width defined by write port depth
			WRADDR => sigwraddr,
			-- 1-bit input write clock
			WRCLK  => clk,
			-- 1-bit input write port enable
			WREN   => write_en
		);

	-------------------------------------------------------------------
	-- The tag
	-------------------------------------------------------------------

	gen_notag: if TAGEN = false generate

		tag_out <= (others => '0');

	end generate;

	gen_tag: if TAGEN = true generate
		signal regtag1 : std_logic_vector(TAGW-1 downto 0);
		signal regtag2 : std_logic_vector(TAGW-1 downto 0);
	begin

		process(clk)
		begin
			if rising_edge(clk) then
				regtag1 <= tag_in;
				if (clear = '1') and (TAGZC = true) then
					regtag1 <= (others => '0');
				end if;
			end if;
		end process;

		gen_regdo: if REGDOEN = true generate

			process(clk)
			begin
				if rising_edge(clk) then
					regtag2 <= regtag1;
					if (clear = '1') and (TAGZC = true) then
						regtag2 <= (others => '0');
					end if;
				end if;
			end process;

		end generate;

		gen_noregdo: if REGDOEN = false generate

			regtag2 <= regtag1;

		end generate;

		tag_out <= regtag2;

	end generate;

end architecture;


