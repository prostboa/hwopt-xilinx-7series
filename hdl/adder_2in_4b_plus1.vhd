
-- This is an adder 4b + 4b + 1b
-- Total size : 3 LUTs + the lower half of a carry4
-- Delay : 1 LUTs + 1 carry propagation

-- This is just a convenient wrapper around compressor 2,2,2,3:5

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity adder_2in_4b_plus1 is
	generic (
		WOUT : natural := 5
	);
	port (
		inA : in  std_logic_vector(3 downto 0);
		inB : in  std_logic_vector(3 downto 0);
		in1 : in  std_logic;
		res : out std_logic_vector(WOUT-1 downto 0)
	);
end adder_2in_4b_plus1;

architecture synth of adder_2in_4b_plus1 is

	-- Wrappers for compressor interface
	signal di0 : std_logic_vector(2 downto 0) := (others => '0');
	signal di1 : std_logic_vector(1 downto 0) := (others => '0');
	signal di2 : std_logic_vector(1 downto 0) := (others => '0');
	signal di3 : std_logic_vector(1 downto 0) := (others => '0');
	signal sum : std_logic_vector(4 downto 0) := (others => '0');

	-- The underlying compressor
	component bitheap_2223_5 is
		port (
			di0 : in  std_logic_vector(2 downto 0);
			di1 : in  std_logic_vector(1 downto 0);
			di2 : in  std_logic_vector(1 downto 0);
			di3 : in  std_logic_vector(1 downto 0);
			sum : out std_logic_vector(4 downto 0)
		);
	end component;

begin

	-- Wrappers for inputs

	di0 <= inB(0 downto 0) & inA(0) & in1;
	di1 <= inB(1 downto 1) & inA(1);
	di2 <= inB(2 downto 2) & inA(2);
	di3 <= inB(3 downto 3) & inA(3);

	-- The compressor

	comp : bitheap_2223_5
		port map (
			di0 => di0,
			di1 => di1,
			di2 => di2,
			di3 => di3,
			sum => sum
		);

	-- Output ports

	res <= std_logic_vector(resize(unsigned(sum), WOUT));

end architecture;

