
-- This is a popcount 11b -> 4b
-- Total 5 LUTs, delay 2 LUTs + 1 carry

-- Architecture : first one 5:3 compressor that handles 5 bits
-- Then one 1,1,7:4 compressor that adds 6 extra bits

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT primitives
library unisim;
use unisim.vcomponents.all;

entity popcount_11_4 is
	generic (
		BITS : natural := 11;
		WOUT : natural := 4
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount_11_4;

architecture synth of popcount_11_4 is

	signal sigBits : std_logic_vector(10 downto 0) := (others => '0');

	signal sum53 : std_logic_vector(2 downto 0) := (others => '0');

	signal inst117_di0 : std_logic_vector(6 downto 0) := (others => '0');
	signal inst117_di1 : std_logic_vector(0 downto 0) := (others => '0');
	signal inst117_di2 : std_logic_vector(0 downto 0) := (others => '0');
	signal inst117_sum : std_logic_vector(3 downto 0) := (others => '0');

	component popcount_5_3 is
		generic (
			BITS : natural := 5;
			WOUT : natural := 3
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component bitheap_117_4 is
		port (
			ci  : in  std_logic;
			di0 : in  std_logic_vector(6 downto 0);
			di1 : in  std_logic_vector(0 downto 0);
			di2 : in  std_logic_vector(0 downto 0);
			do  : out std_logic_vector(3 downto 0);
			co  : out std_logic_vector(3 downto 0);
			sum : out std_logic_vector(3 downto 0)
		);
	end component;

begin

	assert BITS <= 11 report "Number of inputs too large for module popcount_11_4" severity failure;

	sigBits(BITS-1 downto 0) <= bits_in;

	popc53 : popcount_5_3
		port map (
			bits_in => sigBits(4 downto 0),
			sum     => sum53
		);

	inst117_di0    <= sigBits(10 downto 5) & sum53(0);
	inst117_di1(0) <= sum53(1);
	inst117_di2(0) <= sum53(2);

	comp117 : bitheap_117_4
		port map (
			ci  => '0',
			di0 => inst117_di0,
			di1 => inst117_di1,
			di2 => inst117_di2,
			do  => open,
			co  => open,
			sum => inst117_sum
		);

	sum <= std_logic_vector(resize(unsigned(inst117_sum), WOUT));

end architecture;

