
-- This is a compressor 6,6,7:6
-- Total size : 8 LUTs
-- Delay : 2 LUTs + 2 carry propagation

-- Architecture : First one compressor 607:5, then one compressor 1117:5

library ieee;
use ieee.std_logic_1164.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity bitheap_667_6 is
	port (
		di0 : in  std_logic_vector(6 downto 0);
		di1 : in  std_logic_vector(5 downto 0);
		di2 : in  std_logic_vector(5 downto 0);
		sum : out std_logic_vector(5 downto 0)
	);
end bitheap_667_6;

architecture synth of bitheap_667_6 is

	signal c4_do : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_co : std_logic_vector(3 downto 0) := (others => '0');

	signal sum607  : std_logic_vector(4 downto 0) := (others => '0');
	signal sum1117 : std_logic_vector(4 downto 0) := (others => '0');

	component bitheap_607_5 is
		port (
			cy  : in  std_logic;
			ci  : in  std_logic;
			di0 : in  std_logic_vector(5 downto 0);
			di2 : in  std_logic_vector(5 downto 0);
			do  : out std_logic_vector(3 downto 0);
			co  : out std_logic_vector(3 downto 0)
		);
	end component;

	component bitheap_2117_5 is
		port (
			ci  : in  std_logic;
			di0 : in  std_logic_vector(6 downto 0);
			di1 : in  std_logic_vector(0 downto 0);
			di2 : in  std_logic_vector(0 downto 0);
			di3 : in  std_logic_vector(1 downto 0);
			do  : out std_logic_vector(3 downto 0);
			co  : out std_logic_vector(3 downto 0);
			sum : out std_logic_vector(4 downto 0)
		);
	end component;

begin

	-- First stage

	comp607_1 : bitheap_607_5
		port map (
			cy  => di0(6),
			ci  => '0',
			di0 => di0(5 downto 0),
			di2 => di2,
			do  => c4_do,
			co  => c4_co
		);

	sum607 <= c4_co(3) & c4_do;

	-- Second stage

	comp1117 : bitheap_2117_5
		port map (
			ci  => '0',
			di0 => sum607(1) & di1,
			di1 => sum607(2 downto 2),
			di2 => sum607(3 downto 3),
			di3 => sum607(4 downto 4) & '0',
			do  => open,
			co  => open,
			sum => sum1117
		);

	-- Output ports

	sum <= sum1117 & sum607(0);

end architecture;

