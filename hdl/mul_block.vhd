
-- This is a multiplier component based on Xilinx DSP primitives
-- It explicitly instantiates the DSP blocks, which enables end users to decide to use DSPs regardless of the behaviour of the synthesis tool

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For DSP48E1
library unisim;
use unisim.vcomponents.all;

-- For MULT_MACRO
library unimacro;
use unimacro.vcomponents.all;

entity mul_block is
	generic(
		-- Parameters for data and signedness
		WDATA   : natural := 4;     -- The data bit width
		SDATA   : boolean := true;  -- The data signedness
		WWEIGHT : natural := 4;     -- The weight bit width
		SWEIGHT : boolean := true;  -- The weight signedness
		WMUL    : natural := 8;     -- The multiplication bit width
		-- Tag-related information
		TAGW    : natural := 1;
		TAGEN   : boolean := true;
		-- Enable pipeline registers to improve timings
		REG_M   : boolean := false;
		REG_P   : boolean := false
	);
	port(
		clk       : in  std_logic;
		-- Control signals
		data_in   : in  std_logic_vector(WDATA-1 downto 0);
		weight_in : in  std_logic_vector(WWEIGHT-1 downto 0);
		-- Data output
		mul_out   : out std_logic_vector(WMUL-1 downto 0);
		-- Tag, input and output
		tag_in    : in  std_logic_vector(TAGW-1 downto 0);
		tag_out   : out std_logic_vector(TAGW-1 downto 0)
	);
end mul_block;

architecture synth of mul_block is

	-- Utility function to generate integer from boolean
	function to_integer(b : boolean) return natural is
	begin
		if b = true then
			return 1;
		end if;
		return 0;
	end function;

	-- Utility function to generate std_logic from boolean
	function to_std_logic(b : boolean) return std_logic is
		variable i : std_logic := '0';
	begin
		i := '1' when b = true else '0';
		return i;
	end function;

	-- To decide what implementation to use
	-- There is a fallback with a generic code
	constant USE_DIRECT_DSP : boolean := false;
	constant USE_MULT_MACRO : boolean := true;

	signal post_m_tag : std_logic_vector(TAGW-1 downto 0) := (others => '0');
	signal post_p_tag : std_logic_vector(TAGW-1 downto 0) := (others => '0');

begin

	-- Direct instantiation of the DSP primitive
	-- FIXME It is limited to just one DSP
	gen : if USE_DIRECT_DSP generate

		constant WIDTH_A : natural := 30;
		constant WIDTH_B : natural := 18;
		constant WIDTH_P : natural := 48;

		signal inst_a : std_logic_vector(WIDTH_A-1 downto 0) := (others => '0');
		signal inst_b : std_logic_vector(WIDTH_B-1 downto 0) := (others => '0');
		signal inst_p : std_logic_vector(WIDTH_P-1 downto 0) := (others => '0');

	begin

		-- FIXME Operands are hard-wired to inputs A and B but in some cases they should be swapped to better fix port widthes
		assert WDATA + to_integer(not SDATA)     <= 25 report "Error in mul_block : WDATA is too large" severity failure;
		assert WWEIGHT + to_integer(not SWEIGHT) <= 18 report "Error in mul_block : WWEIGHT is too large" severity failure;

		gen_A : if SDATA = false generate
			inst_a <= std_logic_vector(resize(signed('0' & data_in), WIDTH_A));
		else generate
			inst_a <= std_logic_vector(resize(signed(data_in), WIDTH_A));
		end generate;

		gen_B : if SWEIGHT = false generate
			inst_b <= std_logic_vector(resize(signed('0' & weight_in), WIDTH_B));
		else generate
			inst_b <= std_logic_vector(resize(signed(weight_in), WIDTH_B));
		end generate;

		DSP48E1_inst : DSP48E1
		generic map (
			-- Feature Control Attributes: Data Path Selection
			A_INPUT   => "DIRECT",             -- Selects A input source, "DIRECT" (A port) or "CASCADE" (ACIN port)
			B_INPUT   => "DIRECT",             -- Selects B input source, "DIRECT" (B port) or "CASCADE" (BCIN port)
			USE_DPORT => FALSE,                -- Select D port usage (TRUE or FALSE)
			USE_MULT  => "MULTIPLY",           -- Select multiplier usage ("MULTIPLY", "DYNAMIC", or "NONE")
			USE_SIMD  => "ONE48",              -- SIMD selection ("ONE48", "TWO24", "FOUR12")
			-- Pattern Detector Attributes: Pattern Detection Configuration
			AUTORESET_PATDET => "NO_RESET",    -- "NO_RESET", "RESET_MATCH", "RESET_NOT_MATCH"
			MASK => X"ffffffffffff",           -- 48-bit mask value for pattern detect (1=ignore)
			PATTERN => X"000000000000",        -- 48-bit pattern match for pattern detect
			SEL_MASK => "MASK",                -- "C", "MASK", "ROUNDING_MODE1", "ROUNDING_MODE2"
			SEL_PATTERN => "PATTERN",          -- Select pattern value ("PATTERN" or "C")
			USE_PATTERN_DETECT => "NO_PATDET", -- Enable pattern detect ("PATDET" or "NO_PATDET")
			-- Register Control Attributes: Pipeline Register Configuration
			ACASCREG      => 0,                -- Number of pipeline stages between A/ACIN and ACOUT (0, 1 or 2)
			ADREG         => 1,                -- Number of pipeline stages for pre-adder (0 or 1)
			ALUMODEREG    => 0,                -- Number of pipeline stages for ALUMODE (0 or 1)
			AREG          => 0,                -- Number of pipeline stages for A (0, 1 or 2)
			BCASCREG      => 0,                -- Number of pipeline stages between B/BCIN and BCOUT (0, 1 or 2)
			BREG          => 0,                -- Number of pipeline stages for B (0, 1 or 2)
			CARRYINREG    => 0,                -- Number of pipeline stages for CARRYIN (0 or 1)
			CARRYINSELREG => 0,                -- Number of pipeline stages for CARRYINSEL (0 or 1)
			CREG          => 0,                -- Number of pipeline stages for C (0 or 1)
			DREG          => 1,                -- Number of pipeline stages for D (0 or 1)
			INMODEREG     => 0,                -- Number of pipeline stages for INMODE (0 or 1)
			MREG          => to_integer(REG_M),  -- Number of multiplier pipeline stages (0 or 1)
			OPMODEREG     => 0,                -- Number of pipeline stages for OPMODE (0 or 1)
			PREG          => to_integer(REG_P)   -- Number of pipeline stages for P (0 or 1)
		)
		port map (
			-- Cascade: 30-bit (each) output: Cascade Ports
			ACOUT        => open,             -- 30-bit output: A port cascade output
			BCOUT        => open,             -- 18-bit output: B port cascade output
			CARRYCASCOUT => open,             -- 1-bit output: Cascade carry output
			MULTSIGNOUT  => open,             -- 1-bit output: Multiplier sign cascade output
			PCOUT        => open,             -- 48-bit output: Cascade output
			-- Control: 1-bit (each) output: Control Inputs/Status Bits
			OVERFLOW       => open,           -- 1-bit output: Overflow in add/acc output
			PATTERNBDETECT => open,           -- 1-bit output: Pattern bar detect output
			PATTERNDETECT  => open,           -- 1-bit output: Pattern detect output
			UNDERFLOW      => open,           -- 1-bit output: Underflow in add/acc output
			-- Data: 4-bit (each) output: Data Ports
			CARRYOUT => open,                 -- 4-bit output: Carry output
			P        => inst_p,               -- 48-bit output: Primary data output
			-- Cascade: 30-bit (each) input: Cascade Ports
			ACIN        => (others => '0'),   -- 30-bit input: A cascade data input
			BCIN        => (others => '0'),   -- 18-bit input: B cascade input
			CARRYCASCIN => '0',               -- 1-bit input: Cascade carry input
			MULTSIGNIN  => '0',               -- 1-bit input: Multiplier sign input
			PCIN        => (others => '0'),   -- 48-bit input: P cascade input
			-- Control: 4-bit (each) input: Control Inputs/Status Bits
			ALUMODE    => "0000",             -- 4-bit input: ALU control input
			CARRYINSEL => "000",              -- 3-bit input: Carry select input
			CLK        => clk,                -- 1-bit input: Clock input
			INMODE     => "00000",            -- 5-bit input: INMODE control input
			OPMODE     => "0000101",          -- 7-bit input: Operation mode input
			-- Data: 30-bit (each) input: Data Ports
			A       => inst_a,                -- 30-bit input: A data input
			B       => inst_b,                -- 18-bit input: B data input
			C       => (others => '0'),       -- 48-bit input: C data input
			CARRYIN => '0',                   -- 1-bit input: Carry input signal
			D       => (others => '0'),       -- 25-bit input: D data input
			-- Reset/Clock Enable: 1-bit (each) input: Reset/Clock Enable Inputs
			CEA1          => '0',             -- 1-bit input: Clock enable input for 1st stage AREG
			CEA2          => '0',             -- 1-bit input: Clock enable input for 2nd stage AREG
			CEAD          => '0',             -- 1-bit input: Clock enable input for ADREG
			CEALUMODE     => '0',             -- 1-bit input: Clock enable input for ALUMODE
			CEB1          => '0',             -- 1-bit input: Clock enable input for 1st stage BREG
			CEB2          => '0',             -- 1-bit input: Clock enable input for 2nd stage BREG
			CEC           => '0',             -- 1-bit input: Clock enable input for CREG
			CECARRYIN     => '0',             -- 1-bit input: Clock enable input for CARRYINREG
			CECTRL        => '0',             -- 1-bit input: Clock enable input for OPMODEREG and CARRYINSELREG
			CED           => '0',             -- 1-bit input: Clock enable input for DREG
			CEINMODE      => '0',             -- 1-bit input: Clock enable input for INMODEREG
			CEM           => to_std_logic(REG_M), -- 1-bit input: Clock enable input for MREG
			CEP           => to_std_logic(REG_M), -- 1-bit input: Clock enable input for PREG
			RSTA          => '0',             -- 1-bit input: Reset input for AREG
			RSTALLCARRYIN => '0',             -- 1-bit input: Reset input for CARRYINREG
			RSTALUMODE    => '0',             -- 1-bit input: Reset input for ALUMODEREG
			RSTB          => '0',             -- 1-bit input: Reset input for BREG
			RSTC          => '0',             -- 1-bit input: Reset input for CREG
			RSTCTRL       => '0',             -- 1-bit input: Reset input for OPMODEREG and CARRYINSELREG
			RSTD          => '0',             -- 1-bit input: Reset input for DREG and ADREG
			RSTINMODE     => '0',             -- 1-bit input: Reset input for INMODEREG
			RSTM          => '0',             -- 1-bit input: Reset input for MREG
			RSTP          => '0'              -- 1-bit input: Reset input for PREG
		);

		gen_out : if SDATA = false and SWEIGHT = false generate
			mul_out <= std_logic_vector(resize(unsigned(inst_p), WMUL));
		else generate
			mul_out <= std_logic_vector(resize(signed(inst_p), WMUL));
		end generate;

	-- Alternative implementation with instantiation of Vivado-specific macro
	elsif USE_MULT_MACRO generate

		constant WIDTH_A : natural := WDATA   + to_integer(not SDATA);
		constant WIDTH_B : natural := WWEIGHT + to_integer(not SWEIGHT);
		constant WIDTH_P : natural := WIDTH_A + WIDTH_B;

		constant LATENCY : natural := to_integer(REG_M) + to_integer(REG_P);

		signal data_in_wrap   : std_logic_vector(WIDTH_A-1 downto 0) := (others => '0');
		signal weight_in_wrap : std_logic_vector(WIDTH_B-1 downto 0) := (others => '0');
		signal mul_out_wrap   : std_logic_vector(WIDTH_P-1 downto 0) := (others => '0');

	begin

		gen_A : if SDATA = false generate
			data_in_wrap <= '0' & data_in;
		else generate
			data_in_wrap <= data_in;
		end generate;

		gen_B : if SWEIGHT = false generate
			weight_in_wrap <= '0' & weight_in;
		else generate
			weight_in_wrap <= weight_in;
		end generate;

		MULT_MACRO_inst : MULT_MACRO
		generic map (
			 DEVICE => "7SERIES",    -- Target Device: "VIRTEX5", "7SERIES", "SPARTAN6"
			 LATENCY => LATENCY,     -- Desired clock cycle latency, 0-4
			 WIDTH_A => WIDTH_A,     -- Multiplier A-input bus width, 1-25
			 WIDTH_B => WIDTH_B)     -- Multiplier B-input bus width, 1-18
		port map (
			 A => data_in_wrap,
			 B => weight_in_wrap,
			 P => mul_out_wrap,
			 CE => '1',  -- 1-bit active high input clock enable
			 CLK => clk, -- 1-bit positive edge clock input
			 RST => '0'  -- 1-bit input active high reset
		);

		gen_P : if SDATA = false and SWEIGHT = false generate
			mul_out <= std_logic_vector(resize(unsigned(mul_out_wrap), WMUL));
		else generate
			mul_out <= std_logic_vector(resize(signed(mul_out_wrap), WMUL));
		end generate;

	-- For reference and validation, and for interoperability between synthesis tools
	-- Generic code that should be synthesized into one unique DSP
	else generate
		signal sig_mul   : std_logic_vector(WMUL-1 downto 0) := (others => '0');
		signal mul_out_m : std_logic_vector(WMUL-1 downto 0) := (others => '0');
		signal mul_out_p : std_logic_vector(WMUL-1 downto 0) := (others => '0');
	begin

		gen : if SDATA = false and SWEIGHT = false generate

			sig_mul <= std_logic_vector( resize(unsigned(data_in) * unsigned(weight_in), WMUL) );

		elsif SDATA = false and SWEIGHT = true generate

			sig_mul <= std_logic_vector( resize(signed('0' & data_in) * signed(weight_in), WMUL) );

		elsif SDATA = true and SWEIGHT = false generate

			sig_mul <= std_logic_vector( resize(signed(data_in) * signed('0' & weight_in), WMUL) );

		else generate

			-- Both inputs are signed
			sig_mul <= std_logic_vector( resize(signed(data_in) * signed(weight_in), WMUL) );

		end generate;

		gen_reg_m : if REG_M = true generate
			process(clk)
			begin
				if rising_edge(clk) then
					mul_out_m <= sig_mul;
				end if;
			end process;
		else generate
			mul_out_m <= sig_mul;
		end generate;

		gen_reg_p : if REG_P = true generate
			process(clk)
			begin
				if rising_edge(clk) then
					mul_out_p <= mul_out_m;
				end if;
			end process;
		else generate
			mul_out_p <= mul_out_m;
		end generate;

		mul_out <= mul_out_p;

	end generate;

	-- Tag propagation
	-- Note : if TAGEN=false then tag_in is directly sent to tag_out

	gen_reg_m : if REG_M = true and TAGEN = true generate
		process(clk)
		begin
			if rising_edge(clk) then
				post_m_tag <= tag_in;
			end if;
		end process;
	else generate
		post_m_tag <= tag_in;
	end generate;

	gen_reg_p : if REG_P = true and TAGEN = true generate
		process(clk)
		begin
			if rising_edge(clk) then
				post_p_tag <= post_m_tag;
			end if;
		end process;
	else generate
		post_p_tag <= post_m_tag;
	end generate;

	tag_out <= post_p_tag;

end architecture;

