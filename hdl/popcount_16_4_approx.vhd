
-- This is a popcount 16b -> 4b, the result is approximate, by design -12.5% to 0% error
-- Total size : 4 + 3 = 7 LUTs
-- Delay : 2 LUTs + 2*carry propag

-- Architecture : first one 6,0,7:5 compressor that handles 13 bits
-- Then a recoding stage that also adds the 3 extra input bits

-- Recoding operation :
--
--     co1  o1  o0
--   + co3  o3  o2
--   -         co1
--   +         i13
--   +         i14
--   +         i15
--   =============
--             sum
--
-- This fits into a special-purpose compressor 1,3,6:4 that is approximate (it brings underestimation)
-- (cost is 3 LUT6, delay 1 LUT + carry)

-- Limitation : Result may be up to 12.5% lower than expected
-- Result is exact if i15 = 0
-- Results up to 7 are correct, results above may be too low by 1 depending on input combinations :
-- Result  8 is approximated to  7
-- Result  9 is approximated to  8
-- Result 10 is approximated to  9
-- Result 11 is approximated to 10
-- Result 12 is approximated to 11
-- Result 13 is approximated to 12
-- Result 14 is approximated to 13
-- Result 15 is approximated to 14
-- Result 16 is approximated to 15

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity popcount_16_4_approx is
	generic (
		BITS : natural := 16;
		WOUT : natural := 4
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount_16_4_approx;

architecture synth of popcount_16_4_approx is

	signal sigBits : std_logic_vector(15 downto 0) := (others => '0');

	signal c4_do : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_co : std_logic_vector(3 downto 0) := (others => '0');

	signal rec_di  : std_logic_vector(3 downto 0) := (others => '0');
	signal rec_s   : std_logic_vector(3 downto 0) := (others => '0');
	signal rec_do  : std_logic_vector(3 downto 0) := (others => '0');
	signal rec_co  : std_logic_vector(3 downto 0) := (others => '0');
	signal rec_sum : std_logic_vector(3 downto 0) := (others => '0');

	component bitheap_607_5 is
		port (
			cy  : in  std_logic;
			ci  : in  std_logic;
			di0 : in  std_logic_vector(5 downto 0);
			di2 : in  std_logic_vector(5 downto 0);
			do  : out std_logic_vector(3 downto 0);
			co  : out std_logic_vector(3 downto 0)
		);
	end component;

begin

	assert BITS <= 16 report "Number of inputs too large for module popcount_16_4_approx_alt" severity failure;

	sigBits(BITS-1 downto 0) <= bits_in;

	-- First stage

	comp607 : bitheap_607_5
		port map (
			cy  => sigBits(12),
			ci  => '0',
			di0 => sigBits(5 downto 0),
			di2 => sigBits(11 downto 6),
			do  => c4_do,
			co  => c4_co
		);

	-- Recoding stage

	lutA : LUT6_2
		generic map (
			INIT => x"19666699E6999966"
		)
		port map (
			O6 => rec_s(0),
			O5 => rec_di(0),
			I0 => c4_do(0),
			I1 => c4_do(2),
			I2 => c4_do(1),
			I3 => c4_co(1),
			I4 => sigBits(15),
			I5 => sigBits(13)
		);

	lutB : LUT6_2
		generic map (
			INIT => x"07E11E87F81EE178"
		)
		port map (
			O6 => rec_s(1),
			O5 => rec_di(1),
			I0 => c4_do(0),
			I1 => c4_do(2),
			I2 => c4_do(1),
			I3 => c4_co(1),
			I4 => sigBits(15),
			I5 => c4_do(3)
		);

	lutC : LUT6_2
		generic map (
			INIT => x"001F017FFFE0FE80"
		)
		port map (
			O6 => rec_s(2),
			O5 => rec_di(2),
			I0 => c4_do(0),
			I1 => c4_do(2),
			I2 => c4_do(1),
			I3 => c4_co(1),
			I4 => sigBits(15),
			I5 => c4_co(3)
		);

	c4 : CARRY4
		port map (
			CO     => rec_co,
			O      => rec_do,
			DI     => rec_di,
			S      => rec_s,
			CI     => '0',
			CYINIT => sigBits(14)
		);

	rec_sum <= rec_co(2) & rec_do(2 downto 0);

	-- Output ports

	sum <= std_logic_vector(resize(unsigned(rec_sum), WOUT));

end architecture;

