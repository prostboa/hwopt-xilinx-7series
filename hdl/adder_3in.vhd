
-- This is a 3-input adder + 1 bit
-- Total WDATA+1 luts (+1 lut with sign support)
-- Delay 2 lut + carry

-- Result has size WDATA+2

-- Recommendation : Use the input data_2 (in addition to using the 3rd data input) only if you are sure about how the CI inputs of CARRY4 are routed by Vivado
-- Vivado 2017.2 moves the LUT functionality to another LUT, the original one is used as passthrough (1 more LUT in resources and delay)

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT primitives
library unisim;
use unisim.vcomponents.all;

entity adder_3in is
	generic (
		WDATA : natural := 8;
		WOUT  : natural := 10;
		NBIN  : natural := 3;
		SIGN  : boolean := false
	);
	port (
		data_in : in  std_logic_vector(NBIN*WDATA-1 downto 0);
		data_1  : in  std_logic;
		data_2  : in  std_logic;
		res     : out std_logic_vector(WOUT-1 downto 0)
	);
end adder_3in;

architecture synth of adder_3in is

	-- Utility function to calculate minimum of two values
	function min(a, b : natural) return natural is
		variable m : natural := 0;
	begin
		m := a when a <= b else b;
		return m;
	end function;

	-- Utility function to generate integer from boolean
	function to_integer(b : boolean) return integer is
		variable i : integer := 0;
	begin
		i := 1 when b = true else 0;
		return i;
	end function;

	-- The number of input bits of CARRY4 needed (1 more LUT than size of inputs for unsigned, 1 more for signed)
	constant INT_SIZE : natural := min(WOUT, WDATA + 1 + to_integer(SIGN));

	-- The number of CARRY4 chunks needed
	constant NBC4 : natural := (INT_SIZE + 3) / 4;

	signal wrap_o5 : std_logic_vector(4*NBC4-1 downto 0) := (others => '0');
	signal wrap_o  : std_logic_vector(4*NBC4-1 downto 0) := (others => '0');
	signal wrap_co : std_logic_vector(4*NBC4-1 downto 0) := (others => '0');

begin

	assert NBIN <= 3 report "Number of inputs too large for module adder_3in" severity failure;

	-- Generate blocks of 4:2 compressors around one CARRY4
	gen_c4 : for c in 0 to NBC4-1 generate

		-- Signals for CARRY inputs
		signal sig_cy : std_logic := '0';
		signal sig_ci : std_logic := '0';
		signal sig_di : std_logic_vector(3 downto 0) := (others => '0');
		signal sig_s  : std_logic_vector(3 downto 0) := (others => '0');

	begin

		-- The compressor LUTs

		gen_luts : for l in 0 to 3 generate
			signal lut_in : std_logic_vector(3 downto 0) := (others => '0');
		begin

			gen_lut : if 4*c+l < INT_SIZE generate

				gen_in : for i in 0 to NBIN-1 generate
					gen_ext : if 4*c+l < WDATA generate
						lut_in(i) <= data_in(i*WDATA + 4*c + l);
					elsif SIGN = true generate
						lut_in(i) <= data_in(i*WDATA + WDATA-1);
					else generate
						lut_in(i) <= '0';
					end generate;
				end generate;

				gen_in3 : if 4*c+l = 0 generate
					lut_in(3) <= data_1;
				else generate
					lut_in(3) <= wrap_o5(4*c+l-1);
				end generate;

				lut : LUT6_2
					generic map (
						INIT => x"000069960000E8E8"
					)
					port map (
						O6 => sig_s(l),
						O5 => wrap_o5(4*c + l),
						I0 => lut_in(0),
						I1 => lut_in(1),
						I2 => lut_in(2),
						I3 => lut_in(3),
						I4 => '0',
						I5 => '1'   -- To have something different for O6 and O5
					);

			end generate;

			sig_di(l) <= lut_in(3);

		end generate;

		-- The CARRY4

		gen_ci : if c = 0 generate
			sig_cy <= data_2;
		else generate
			sig_ci <= wrap_co(4*(c-1) + 3);
		end generate;

		c4 : CARRY4
			port map (
				CO     => wrap_co(4*(c+1)-1 downto 4*c),
				O      => wrap_o (4*(c+1)-1 downto 4*c),
				DI     => sig_di,
				S      => sig_s,
				CI     => sig_ci,
				CYINIT => sig_cy
			);

	end generate;

	-- Set final outputs

	gen_sign : if SIGN = true generate
		signal partial_result : std_logic_vector(INT_SIZE-1 downto 0) := (others => '0');
	begin

		partial_result <= wrap_o(INT_SIZE-1 downto 0);
		res <= std_logic_vector(resize(signed(partial_result), WOUT));

	else generate
		signal partial_result : std_logic_vector(INT_SIZE downto 0) := (others => '0');
	begin

		partial_result <= wrap_co(INT_SIZE-1) & wrap_o(INT_SIZE-1 downto 0);
		res <= std_logic_vector(resize(unsigned(partial_result), WOUT));

	end generate;

end architecture;

