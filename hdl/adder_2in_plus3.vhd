
-- This is a 2-input adder that can also add up to 3 bits
-- It is inspired by the compressor 1,3,2,5:5
-- See paper: Pipelined Compressor Tree Optimization using Integer Linear Programming (FPL2014)

-- Size of output : WDATA + 1 if at most one extra bit is used
-- Size of output : WDATA + 2 above

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity adder_2in_plus3 is
	generic (
		WDATA : natural := 8;
		WOUT  : natural := 10;
		NB1   : natural := 3;
		SIGN  : boolean := false
	);
	port (
		data_A : in  std_logic_vector(WDATA-1 downto 0);
		data_B : in  std_logic_vector(WDATA-1 downto 0);
		data_1 : in  std_logic_vector(NB1-1 downto 0);
		res    : out std_logic_vector(WOUT-1 downto 0)
	);
end adder_2in_plus3;

architecture synth of adder_2in_plus3 is

	-- Utility function to calculate minimum of two values
	function min(a, b : natural) return natural is
		variable m : natural := 0;
	begin
		m := a when a <= b else b;
		return m;
	end function;

	-- Utility function to generate integer from boolean
	function to_integer(b : boolean) return integer is
		variable i : integer := 0;
	begin
		i := 1 when b = true else 0;
		return i;
	end function;

	-- The number of input bits of CARRY4 needed (1 more LUT than size of inputs for unsigned, 1 more for signed)
	constant INT_SIZE : natural := min(WOUT, WDATA + 1 + to_integer(NB1 > 1));

	-- The number of CARRY4 chunks needed
	constant NBC4 : natural := (INT_SIZE + 3) / 4;

	signal wrap_o5 : std_logic_vector(4*NBC4-1 downto 0) := (others => '0');
	signal wrap_o  : std_logic_vector(4*NBC4-1 downto 0) := (others => '0');
	signal wrap_co : std_logic_vector(4*NBC4-1 downto 0) := (others => '0');

begin

	assert NB1 <= 3 report "Number of extra inputs too high for module adder_2in_plus3" severity failure;

	-- Generate blocks of compressors around one CARRY4
	gen_c4 : for c in 0 to NBC4-1 generate

		-- Signals for CARRY inputs
		signal sig_cy : std_logic := '0';
		signal sig_ci : std_logic := '0';
		signal sig_di : std_logic_vector(3 downto 0) := (others => '0');
		signal sig_s  : std_logic_vector(3 downto 0) := (others => '0');

	begin

		-- The compressor LUTs

		gen_luts : for l in 0 to 3 generate

			signal lut_in : std_logic_vector(4 downto 0) := (others => '0');

			function calc_false_true(b : boolean; vf : bit_vector; vt : bit_vector) return bit_vector is
			begin
				if b = false then return vf; end if;
				return vt;
			end;

			constant INIT_LUT : bit_vector(63 downto 0) := calc_false_true(4*c+l > 0,
				x"000069960000FF00",
				x"99969666FFF0F000"
			);

		begin

			-- The first 2 inputs (local input bits)

			gen_in01 : if 4*c+l < WDATA generate

				lut_in(0) <= data_A(4*c + l);
				lut_in(1) <= data_B(4*c + l);

			elsif (4*c+l < INT_SIZE) and (SIGN = true) generate

				lut_in(0) <= data_A(WDATA-1);
				lut_in(1) <= data_B(WDATA-1);

			end generate;

			-- The inputs 2 and 3 (previous input bits)

			gen_in23 : if 4*c+l = 0 generate

				lut_in(2) <= data_1(0);
				gen_1 : if NB1 >= 2 generate
					lut_in(3) <= data_1(1);
				end generate;

			elsif 4*c+l - 1 < WDATA generate

				lut_in(2) <= data_A(4*c + l - 1);
				lut_in(3) <= data_B(4*c + l - 1);

			elsif (4*c+l - 1 < INT_SIZE) and (SIGN = true) generate

				lut_in(2) <= data_A(WDATA-1);
				lut_in(3) <= data_B(WDATA-1);

			end generate;

			-- The input 4 (first extra bit)

			gen_in4 : if 4*c+l = 1 generate

				lut_in(4) <= data_1(0);

			end generate;

			-- The local LUT

			lut : LUT6_2
				generic map (
					INIT => INIT_LUT
				)
				port map (
					O6 => sig_s(l),
					O5 => sig_di(l),
					I0 => lut_in(0),
					I1 => lut_in(1),
					I2 => lut_in(2),
					I3 => lut_in(3),
					I4 => lut_in(4),
					I5 => '1'   -- To have something different for O6 and O5
				);

		end generate;

		-- The CARRY4

		gen_ci : if c = 0 generate
			gen_1 : if NB1 >= 3 generate
				sig_cy <= data_1(2);
			end generate;
		else generate
			sig_ci <= wrap_co(4*(c-1) + 3);
		end generate;

		c4 : CARRY4
			port map (
				CO     => wrap_co(4*(c+1)-1 downto 4*c),
				O      => wrap_o (4*(c+1)-1 downto 4*c),
				DI     => sig_di,
				S      => sig_s,
				CI     => sig_ci,
				CYINIT => sig_cy
			);

	end generate;

	-- Set final outputs

	gen_sign : if SIGN = true generate
		signal partial_result : std_logic_vector(INT_SIZE-1 downto 0) := (others => '0');
	begin

		partial_result <= wrap_o(INT_SIZE-1 downto 0);
		res <= std_logic_vector(resize(signed(partial_result), WOUT));

	else generate
		signal partial_result : std_logic_vector(INT_SIZE-1 downto 0) := (others => '0');
	begin

		partial_result <= wrap_o(INT_SIZE-1 downto 0);
		res <= std_logic_vector(resize(unsigned(partial_result), WOUT));

	end generate;

end architecture;

