
-- This is a popcount 31b -> 5b
-- Total size : 7x2 + 3 = 17 LUTs
-- Delay : 3 LUTs + 3*carry propag

-- Architecture : 2 popcounts 15:4 then one adder 2x 4b + 1b
-- Warning : 3 primitives CARRY4 only use 3 of their 4 LUTs

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity popcount_31_5 is
	generic (
		BITS : natural := 31;
		WOUT : natural := 5
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount_31_5;

architecture synth of popcount_31_5 is

	signal sigBits : std_logic_vector(30 downto 0) := (others => '0');

	signal sum1 : std_logic_vector(3 downto 0) := (others => '0');
	signal sum2 : std_logic_vector(3 downto 0) := (others => '0');

	component popcount_15_4 is
		generic (
			BITS : natural := 15;
			WOUT : natural := 4
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component adder_2in_4b_plus1 is
		generic (
			WOUT : natural := 5
		);
		port (
			inA : in  std_logic_vector(3 downto 0);
			inB : in  std_logic_vector(3 downto 0);
			in1 : in  std_logic;
			res : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

begin

	assert BITS <= 31 report "Number of inputs too large for module popcount_31_5" severity failure;

	sigBits(BITS-1 downto 0) <= bits_in;

	popc_1 : popcount_15_4
		port map (
			bits_in => sigBits(14 downto 0),
			sum     => sum1
		);

	popc_2 : popcount_15_4
		port map (
			bits_in => sigBits(29 downto 15),
			sum     => sum2
		);

	comp_3 : adder_2in_4b_plus1
		generic map(
			WOUT => WOUT
		)
		port map (
			inA => sum1,
			inB => sum2,
			in1 => sigBits(30),
			res => sum
		);

end architecture;

