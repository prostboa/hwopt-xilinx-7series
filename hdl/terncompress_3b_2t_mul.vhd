
-- This is a ternary-to-binary decompressor 3b -> 2T + multiplier
-- The ternary code (-1, -1) is forbidden

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity terncompress_3b_2t_mul is
	generic(
		WDATA : natural := 2;
		IDX   : natural := 0
	);
	port(
		bin_in   : in  std_logic_vector(2 downto 0);
		data_in  : in  std_logic_vector(WDATA-1 downto 0);
		mult_out : out std_logic_vector(WDATA-1 downto 0)
	);
end terncompress_3b_2t_mul;

architecture synth of terncompress_3b_2t_mul is
begin

	-- Short inputs: Use a look-up table, no carry
	gen_short: if WDATA <= 3 generate

		type decomp3b2t_type is array (0 to 7) of std_logic_vector(3 downto 0);
		constant decomp3b2t : decomp3b2t_type := (
			"0000", "0001", "0011",
			"0100", "0101", "0111",
			"1100", "1101"
		);

		constant ROMSIZE  : natural := 2 ** (3 + WDATA);
		constant ROMWDATA : natural := 2;
		type lut_type is array (0 to ROMSIZE-1) of std_logic_vector(ROMWDATA-1 downto 0);

		type drange_t is record
			min : integer;
			max : integer;
		end record;

		function calc_drange(phony : boolean) return drange_t is
			variable drange : drange_t := (0, 0);
		begin
			drange.min := 0;
			drange.max := 1;
			if WDATA > 1 then
				drange.max := 2 ** (WDATA-1) - 1;
				drange.min := 0 - drange.max;
			end if;
			return drange;
		end function;

		constant DRANGE : drange_t := calc_drange(false);

		function gen_lut_contents(phony : natural) return lut_type is
			variable lut     : lut_type;
			variable idx_lut : natural;
			variable w       : integer;
			variable vec4    : std_logic_vector(3 downto 0);
			variable vec3    : std_logic_vector(2 downto 0);
			variable vec     : std_logic_vector(3+WDATA-1 downto 0);
		begin
			for i in 0 to ROMSIZE-1 loop
				lut(i) := (others => '0');
			end loop;
			for i in 0 to 7 loop
				vec4 := decomp3b2t(i);
				w    := to_integer(signed(vec4(2*IDX+1 downto 2*IDX)));
				vec3 := std_logic_vector(to_unsigned(i, 3));
				for d in DRANGE.min to DRANGE.max loop
					vec := vec3 & std_logic_vector(to_signed(d, WDATA));
					idx_lut := to_integer(unsigned(vec));
					lut(idx_lut) := std_logic_vector(to_signed(w * d, ROMWDATA));
				end loop;
			end loop;
			return lut;
		end function;

		constant lut : lut_type := gen_lut_contents(0);

		signal vec : std_logic_vector(3+WDATA-1 downto 0);

	begin

		vec <= bin_in & data_in;
		mult_out <= lut( to_integer(unsigned(vec)) );

	end generate;

	-- Wide inputs: Use a carry chain
	gen_wide: if WDATA > 3 generate

		-- The number of CARRY4 primitives to instantiate
		constant CNT_NBC4 : natural := (WDATA+3) / 4;

		-- Temp signals to connect the CARRY4 components, for ALU
		-- Initialization to "don't care" for partial CARRY4 utilization
		signal inst_c4_o      : std_logic_vector(4*CNT_NBC4-1 downto 0) := (others => '-');
		signal inst_c4_co     : std_logic_vector(4*CNT_NBC4-1 downto 0) := (others => '-');
		signal inst_c4_di     : std_logic_vector(4*CNT_NBC4-1 downto 0) := (others => '-');
		signal inst_c4_s      : std_logic_vector(4*CNT_NBC4-1 downto 0) := (others => '-');
		signal inst_c4_ci     : std_logic_vector(CNT_NBC4-1 downto 0) := (others => '-');
		signal inst_c4_cyinit : std_logic_vector(CNT_NBC4-1 downto 0) := (others => '-');

	begin

		-- Instantiation of the LUT / CARRY4 components for ALU of the accumulator
		--
		-- The binary-to-ternary codes are distributed like this:
		-- 000 -> 0000 ->  0  0
		-- 001 -> 0001 ->  0  1
		-- 010 -> 0011 ->  0 -1
		-- 011 -> 0100 ->  1  0
		-- 100 -> 0101 ->  1  1
		-- 101 -> 0111 ->  1 -1
		-- 110 -> 1100 -> -1  0
		-- 111 -> 1101 -> -1  1
		--
		-- Truth table for LUT contents, IDX = 0
		--
		--    |   b2   b1   b0  data |  O6  O5  |  O6 = input S (data), O5 = input DI (carry in)
		-- ==========================================
		--  0 |    0    0    0    0  |   0   0  |  weight =  0 -> ZERO
		--  1 |    0    0    0    1  |   0   0  |  weight =  0 -> ZERO
		--  2 |    0    0    1    0  |   0   0  |  weight =  1 -> PASS
		--  3 |    0    0    1    1  |   1   0  |  weight =  1 -> PASS
		--  4 |    0    1    0    0  |   1   0  |  weight = -1 -> NEG   (1st bit: O6=0 O5=0)
		--  5 |    0    1    0    1  |   0   0  |  weight = -1 -> NEG   (1st bit: O6=1 O5=1)
		--  6 |    0    1    1    0  |   0   0  |  weight =  0 -> ZERO
		--  7 |    0    1    1    1  |   0   0  |  weight =  0 -> ZERO
		--  -----------------------------------------
		--  8 |    1    0    0    0  |   0   0  |  weight =  1 -> PASS
		--  9 |    1    0    0    1  |   1   0  |  weight =  1 -> PASS
		-- 10 |    1    0    1    0  |   1   0  |  weight = -1 -> NEG   (1st bit: O6=0 O5=0)
		-- 11 |    1    0    1    1  |   0   0  |  weight = -1 -> NEG   (1st bit: O6=1 O5=1)
		-- 12 |    1    1    0    0  |   0   0  |  weight =  0 -> ZERO
		-- 13 |    1    1    0    1  |   0   0  |  weight =  0 -> ZERO
		-- 14 |    1    1    1    0  |   0   0  |  weight =  1 -> PASS
		-- 15 |    1    1    1    1  |   1   0  |  weight =  1 -> PASS
		--
		-- Config for O6 is 32 upper bits, O5 is 32 lower bits: 00008618 00000000
		-- The 1st LUT performs an additional +1 for negation : 00008A28 00000820
		-- Note: NEG, bits > 0: O6 = not(data), O5 = 0
		-- Note: NEG, bit 0   : O6 = data, O5 = data
		--
		-- Truth table for LUT contents, IDX = 1
		--
		--    |   b2   b1   b0  data |  O6  O5  |  O6 = input S (data), O5 = input DI (carry in)
		-- ==========================================
		--  0 |    0    0    0    0  |   0   0  |  weight =  0 -> ZERO
		--  1 |    0    0    0    1  |   0   0  |  weight =  0 -> ZERO
		--  2 |    0    0    1    0  |   0   0  |  weight =  0 -> ZERO
		--  3 |    0    0    1    1  |   0   0  |  weight =  0 -> ZERO
		--  4 |    0    1    0    0  |   0   0  |  weight =  0 -> ZERO
		--  5 |    0    1    0    1  |   0   0  |  weight =  0 -> ZERO
		--  6 |    0    1    1    0  |   0   0  |  weight =  1 -> PASS
		--  7 |    0    1    1    1  |   1   0  |  weight =  1 -> PASS
		--  -----------------------------------------
		--  8 |    1    0    0    0  |   0   0  |  weight =  1 -> PASS
		--  9 |    1    0    0    1  |   1   0  |  weight =  1 -> PASS
		-- 10 |    1    0    1    0  |   0   0  |  weight =  1 -> PASS
		-- 11 |    1    0    1    1  |   1   0  |  weight =  1 -> PASS
		-- 12 |    1    1    0    0  |   1   0  |  weight = -1 -> NEG   (1st bit: O6=0 O5=0)
		-- 13 |    1    1    0    1  |   0   0  |  weight = -1 -> NEG   (1st bit: O6=1 O5=1)
		-- 14 |    1    1    1    0  |   1   0  |  weight = -1 -> NEG   (1st bit: O6=0 O5=0)
		-- 15 |    1    1    1    1  |   0   0  |  weight = -1 -> NEG   (1st bit: O6=1 O5=1)
		--
		-- Config for O6 is 32 upper bits, O5 is 32 lower bits: 00005A80 00000000
		-- The 1st LUT performs an additional +1 for negation : 0000AA80 0000A000
		-- Note: NEG, bits > 0: O6 = not(data), O5 = 0
		-- Note: NEG, bit 0   : O6 = data, O5 = data
		--
		-- The LUTs of the ALU

		gen_idx0: if IDX = 0 generate

			i_lut: LUT6_2
				generic map (
					INIT => x"00008A2800000820"
				)
				port map (
					O6 => inst_c4_s(0),
					O5 => inst_c4_di(0),
					I0 => data_in(0),
					I1 => bin_in(0),
					I2 => bin_in(1),
					I3 => bin_in(2),
					I4 => '0',  -- Unused
					I5 => '1'   -- To have something different for O6 and O5
				);

			gen_luts: for l in 1 to WDATA-1 generate

				i_lut: LUT6_2
					generic map (
						INIT => x"0000861800000000"
					)
					port map (
						O6 => inst_c4_s(l),
						O5 => inst_c4_di(l),
						I0 => data_in(l),
						I1 => bin_in(0),
						I2 => bin_in(1),
						I3 => bin_in(2),
						I4 => '0',  -- Unused
						I5 => '1'   -- To have something different for O6 and O5
					);

			end generate;
		end generate;

		gen_idx1: if IDX = 1 generate

			i_lut: LUT6_2
				generic map (
					INIT => x"0000AA800000A000"
				)
				port map (
					O6 => inst_c4_s(0),
					O5 => inst_c4_di(0),
					I0 => data_in(0),
					I1 => bin_in(0),
					I2 => bin_in(1),
					I3 => bin_in(2),
					I4 => '0',  -- Unused
					I5 => '1'   -- To have something different for O6 and O5
				);

			gen_luts: for l in 1 to WDATA-1 generate

				i_lut: LUT6_2
					generic map (
						INIT => x"00005A8000000000"
					)
					port map (
						O6 => inst_c4_s(l),
						O5 => inst_c4_di(l),
						I0 => data_in(l),
						I1 => bin_in(0),
						I2 => bin_in(1),
						I3 => bin_in(2),
						I4 => '0',  -- Unused
						I5 => '1'   -- To have something different for O6 and O5
					);

			end generate;
		end generate;

		-- Generate the CARRY4 components

		gen_c4: for i in 0 to CNT_NBC4-1 generate

			i_c4: CARRY4
				port map (
					CO     => inst_c4_co(4*(i+1)-1 downto 4*i),
					O      => inst_c4_o (4*(i+1)-1 downto 4*i),
					DI     => inst_c4_di(4*(i+1)-1 downto 4*i),
					S      => inst_c4_s (4*(i+1)-1 downto 4*i),
					CI     => inst_c4_ci(i),
					CYINIT => inst_c4_cyinit(i)
				);

		end generate;

		-- Inter-CARRY4 connections
		gen_c4_conn: for i in 1 to CNT_NBC4-1 generate

			inst_c4_ci(i)     <= inst_c4_co(4*i-1);
			inst_c4_cyinit(i) <= '0';  -- Always propagate the carry

		end generate;

		inst_c4_ci(0)     <= '0';  -- Unused
		inst_c4_cyinit(0) <= '0';  -- Always propagate the carry  FIXME Not sure that's the correct setting

		-- Data output
		mult_out <= inst_c4_o(WDATA-1 downto 0);

	end generate;  -- Wide data, use carry chain

end architecture;

