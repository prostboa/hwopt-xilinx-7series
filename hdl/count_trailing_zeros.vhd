
-- This component counts the number of trailing zeros and generates a mask corresponding to trailing zeros
-- It is combinatorial

-- Potential improvements :
--   - The implementation with carry chain could directly instantiate the LUTs and CARRY to ensure there is only one layer of LUTs-carry before the popcount
--   - The implementation with carry chain can support counting leading zeros with no extra hardware, with a layer of MUX embedded in the first ALU
--   - A MUX-based with binary search implementation could also be considered, especially for ASIC
--     https://electronics.stackexchange.com/questions/196914/verilog-synthesize-high-speed-leading-zero-count
--   - To be compared against similar implementation based on the standard VHDL function count_leftmost() + demux to generate a compatible mask

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity count_trailing_zeros is
	generic (
		BITS  : natural := 16;
		WOUT  : natural := 5;
		-- Enable output of the number of trailing zeros
		CTZ_EN : boolean := true;
		-- Enable output of the "mask" that marks the trailing zeros
		MASK_EN : boolean := false;
		-- Enable output of the "empty" flag
		EMPTY_EN : boolean := false;
		-- Thresholds for implementation selection
		USE_BEHV_UPTO  : natural := 7;
		USE_RADIX_FROM : natural := 64;
		RADIX : natural := 32
	);
	port (
		bits_in   : in  std_logic_vector(BITS-1 downto 0);
		ctz_out   : out std_logic_vector(WOUT-1 downto 0);
		mask_out  : out std_logic_vector(BITS-1 downto 0);
		empty_out : out std_logic
	);
end count_trailing_zeros;

architecture synth of count_trailing_zeros is

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	constant WBITS : natural := storebitsnb(BITS);
	constant IS_POW2 : boolean := (to_unsigned(BITS, WBITS) xnor (to_unsigned(BITS, WBITS) - 1)) = 0;

begin

	assert RADIX < USE_RADIX_FROM severity failure;

	-- This reference implementation can be used for verification
	-- Performance and resource efficiency strongly depends on the synthesis tool
	gen : if BITS <= USE_BEHV_UPTO generate

		signal sig_sum : std_logic_vector(WBITS-1 downto 0) := (others => '0');

	begin

		gen_ctz : if CTZ_EN = true generate

			process(all)
			begin
				sig_sum <= std_logic_vector(to_unsigned(BITS, WBITS));
				for i in 0 to BITS-1 loop
					if bits_in(i) = '1' then
						sig_sum <= std_logic_vector(to_unsigned(i, WBITS));
						exit;
					end if;
				end loop;
			end process;

			ctz_out <= std_logic_vector(resize(unsigned(sig_sum), WOUT));

		else generate

			sig_sum <= (others => '0');

		end generate;

		gen_mask : if MASK_EN = true generate

			process(all)
				variable var_flag : std_logic := '1';
			begin
				var_flag := '1';
				for i in 0 to BITS-1 loop
					if bits_in(i) = '1' and var_flag = '1' then
						var_flag := '0';
					end if;
					mask_out(i) <= var_flag;
				end loop;
			end process;

		else generate

			mask_out <= (others => '0');

		end generate;

		-- Generate the output flag "empty" to indicate that the input vector is empty
		gen_empty : if EMPTY_EN = true generate

			process(all)
			begin
				if (IS_POW2 = true) and (CTZ_EN = true) then
					empty_out <= sig_sum(WBITS-1);
				else
					empty_out <= '1' when unsigned(bits_in) = 0 else '0';
				end if;
			end process;

		else generate

			empty_out <= '0';

		end generate;

	-- Implementation with an operation -1, this is this is still behavioural and rather generic
	-- Drawback : for large inputs, there is a long carry chain that is not friendly for place-and-route tools and for timing
	elsif BITS < USE_RADIX_FROM generate

		-- Implementation and example :
		--   V = 010010000    Input vector
		--   S = 010001111    V - 1
		--   X = 000001111    (V xor S) and S
		--   P = 4            popcount(X)

		-- Intermediate signals
		signal sig_m1  : std_logic_vector(BITS-1 downto 0) := (others => '0');
		signal sig_xor : std_logic_vector(BITS-1 downto 0) := (others => '0');

	begin

		sig_m1 <= std_logic_vector(unsigned(bits_in) - 1);

		gen_xor : if (CTZ_EN = true) or (MASK_EN = true) generate

			-- Component declaration : the optimized adder tree, for popcount operation
			component addtree is
				generic (
					-- Data type and width
					WDATA : natural := 8;
					SDATA : boolean := true;
					NBIN  : natural := 20;
					WOUT  : natural := 12;
					-- User-specified radix, for testing purposes (0 means automatic)
					RADIX : natural := 0;
					-- Special considerations about data nature
					BINARY : boolean := false;
					BINARY_RADIX : natural := 0;
					TERNARY : boolean := false;
					TERNARY_RADIX : natural := 0;
					-- An optional tag
					TAGW  : natural := 1;
					TAGEN : boolean := true;
					TAGZC : boolean := true;
					-- How to add pipeline registers
					REGEN  : natural := 1;  -- 0=none, 1=all, else every EN stages
					REGIDX : natural := 0   -- Start index (from the leaves)
				);
				port (
					clk      : in  std_logic;
					clear    : in  std_logic;
					-- Data, input and output
					data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
					data_out : out std_logic_vector(WOUT-1 downto 0);
					-- Tag, input and output
					tag_in   : in  std_logic_vector(TAGW-1 downto 0);
					tag_out  : out std_logic_vector(TAGW-1 downto 0)
				);
			end component;

		begin

			sig_xor <= (bits_in xor sig_m1) and sig_m1;

			add_i : addtree
				generic map (
					-- Data type and width
					WDATA => 1,
					SDATA => false,
					NBIN  => BITS,
					WOUT  => WOUT,
					-- User-specified radix, for testing purposes (0 means automatic)
					RADIX => 0,
					-- Special considerations about data nature
					BINARY        => true,
					BINARY_RADIX  => 0,
					TERNARY       => false,
					TERNARY_RADIX => 0,
					-- An optional tag
					TAGW  => 1,
					TAGEN => false,
					TAGZC => false,
					-- How to add pipeline registers
					REGEN  => 0,
					REGIDX => 0
				)
				port map (
					clk      => '0',
					clear    => '0',
					-- Data, input and output
					data_in  => sig_xor,
					data_out => ctz_out,
					-- Tag, input and output
					tag_in   => (others => '0'),
					tag_out  => open
				);

			mask_out <= sig_xor;

		else generate

			ctz_out  <= (others => '0');
			mask_out <= (others => '0');

		end generate;

		-- Generate the output flag "empty" to indicate that the input vector is empty
		empty_out <= (bits_in(BITS-1) xor sig_m1(BITS-1)) and sig_m1(BITS-1) when MASK_EN = true else '0';

	-- Implementation that processes reduced-size chunks, then merges results
	-- This should be much more scalable and potentially better for timing for large inputs
	else generate

		-- Use 32b chunks (and 32b individual popcounts), then a dedicated adder tree to compute the final sum
		--   - gather the "empty" bits from each leaf
		--     E = 101101111
		--   - generate a mask up to (and including) the first 0 in that vector
		--     P = 101110000    E + 1
		--     X = 000011111    E xor P
		--   - count the global number of trailing zeros :
		--     for each chunk, take the number of trailing zeros, apply AND with the above mask, and add these all with an adder tree
		--   - re-generate the global mask :
		--     If the leaf ctz components provide their local mask : for each chunk c :
		--       mask(c) <= local_mask and X(c)
		--     If the leaf ctz components don't provide their local mask : for each chunk c and bit i :
		--       mask(c)(i) <= X(c) when i < ctz(c) else '0'
		--       The comparison "i < ctz(c)" does not consider the MSB of the ctz(c) value, so this should use at most one LUT6 per bit
		--       Explanation (1) : the case X(c)=0 obviously leads to mask(c)(*)=0
		--       Explanation (2) : the case E(c)=1 leads to mask(c)(*)=1 because only the MSB in ctz(c) is set so the comparison becomes i < 0 which is always true
		--       Explanation (3) : only remains the case of the first zero in vector E, where the MSB of ctz(c) is zero, so the comparison i < ctz(c) is the expected result
		--     Optimization if the final ctz result is not desired :
		--       Instantiate 2 leaves, and just test local_mask(leaf 0)==0 instead of the 2 popcounts in the leaves
		--       Then generate the final mask :
		--       mask(0) <= local_mask(0)
		--       mask(1) <= local_mask(1) and (local_mask(0)==0)

		-- The number of leaf chunks
		constant LEAVES_NB : natural := (BITS + RADIX - 1) / RADIX;

		-- The size of the result of the ctz() partial sums
		constant WRADIX : natural := storebitsnb(RADIX);
		constant WRADM1 : natural := storebitsnb(RADIX-1);

		-- Signals to instantiate the leaf ctz components
		signal vec_ctz : std_logic_vector(LEAVES_NB*WRADIX-1 downto 0) := (others => '0');
		signal vec_msk : std_logic_vector(LEAVES_NB*RADIX-1 downto 0) := (others => '0');
		signal vec_emp : std_logic_vector(LEAVES_NB-1 downto 0) := (others => '0');

		-- Intermediate signals
		signal sig_p1  : std_logic_vector(LEAVES_NB-1 downto 0) := (others => '0');
		signal sig_xor : std_logic_vector(LEAVES_NB-1 downto 0) := (others => '0');

		-- Component declaration : the optimized adder tree, for popcount operation
		component addtree is
			generic (
				-- Data type and width
				WDATA : natural := 8;
				SDATA : boolean := true;
				NBIN  : natural := 20;
				WOUT  : natural := 12;
				-- User-specified radix, for testing purposes (0 means automatic)
				RADIX : natural := 0;
				-- Special considerations about data nature
				BINARY : boolean := false;
				BINARY_RADIX : natural := 0;
				TERNARY : boolean := false;
				TERNARY_RADIX : natural := 0;
				-- An optional tag
				TAGW  : natural := 1;
				TAGEN : boolean := true;
				TAGZC : boolean := true;
				-- How to add pipeline registers
				REGEN  : natural := 1;  -- 0=none, 1=all, else every EN stages
				REGIDX : natural := 0   -- Start index (from the leaves)
			);
			port (
				clk      : in  std_logic;
				clear    : in  std_logic;
				-- Data, input and output
				data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
				data_out : out std_logic_vector(WOUT-1 downto 0);
				-- Tag, input and output
				tag_in   : in  std_logic_vector(TAGW-1 downto 0);
				tag_out  : out std_logic_vector(TAGW-1 downto 0)
			);
		end component;

	begin

		leaves : for i in 0 to LEAVES_NB-1 generate

			constant BITS_LOC : natural := minimum(BITS - i*RADIX, RADIX);

		begin

			ctz : entity count_trailing_zeros
				generic map (
					BITS  => BITS_LOC,
					WOUT  => WRADIX,
					-- Enable output of the number of trailing zeros
					CTZ_EN => CTZ_EN,
					-- Enable output of the "mask" that marks the trailing zeros
					MASK_EN => MASK_EN,
					-- Enable output of the "empty" flag
					EMPTY_EN => true,
					-- Thresholds for implementation selection
					USE_BEHV_UPTO  => USE_BEHV_UPTO,
					USE_RADIX_FROM => USE_RADIX_FROM,
					RADIX => RADIX
				)
				port map (
					bits_in   => bits_in(i*RADIX+BITS_LOC-1 downto i*RADIX),
					ctz_out   => vec_ctz((i+1)*WRADIX-1 downto i*WRADIX),
					mask_out  => vec_msk(i*RADIX+BITS_LOC-1 downto i*RADIX),
					empty_out => vec_emp(i)
				);

		end generate;

		sig_p1  <= std_logic_vector(unsigned(vec_emp) + 1);
		sig_xor <= vec_emp xor sig_p1;

		-- Generate the total ctz result

		gen_ctz : if CTZ_EN = true generate

			signal vec_and : std_logic_vector(LEAVES_NB*WRADIX-1 downto 0) := (others => '0');

		begin

			process(all)
			begin
				for i in 0 to LEAVES_NB-1 loop
					vec_and((i+1)*WRADIX-1 downto i*WRADIX) <= vec_ctz((i+1)*WRADIX-1 downto i*WRADIX) and sig_xor(i);
				end loop;
			end process;

			add_i : addtree
				generic map (
					-- Data type and width
					WDATA => WRADIX,
					SDATA => false,
					NBIN  => LEAVES_NB,
					WOUT  => WOUT,
					-- User-specified radix, for testing purposes (0 means automatic)
					RADIX => 0,
					-- Special considerations about data nature
					BINARY        => false,
					BINARY_RADIX  => 0,
					TERNARY       => false,
					TERNARY_RADIX => 0,
					-- An optional tag
					TAGW  => 1,
					TAGEN => false,
					TAGZC => false,
					-- How to add pipeline registers
					REGEN  => 0,
					REGIDX => 0
				)
				port map (
					clk      => '0',
					clear    => '0',
					-- Data, input and output
					data_in  => vec_and,
					data_out => ctz_out,
					-- Tag, input and output
					tag_in   => (others => '0'),
					tag_out  => open
				);

		else generate

			ctz_out <= (others => '0');

		end generate;

		-- Generate the output mask

		gen_mask : if MASK_EN = true generate
			leaves : for i in 0 to LEAVES_NB-1 generate

				constant BITS_LOC : natural := minimum(BITS - i*RADIX, RADIX);

			begin

				process(all)
				begin

					mask_out(i*RADIX+BITS_LOC-1 downto i*RADIX) <= vec_msk(i*RADIX+BITS_LOC-1 downto i*RADIX) and sig_xor(i);

					--for b in 0 to BITS_LOC-1 loop
					--	mask_out(i*RADIX + b) <= sig_xor(i) when i < sig_ctz(i*WRADIX + (WRADM1-1) - 1 downto i*WRADIX) else '0';
					--end loop;

				end process;

			end generate;
		else generate

			mask_out <= (others => '0');

		end generate;

		-- Generate the output flag "empty" to indicate that the input vector is empty
		empty_out <= (vec_emp(LEAVES_NB-1) xor sig_p1(LEAVES_NB-1)) and vec_emp(LEAVES_NB-1) when MASK_EN = true else '0';

	end generate;

end architecture;

