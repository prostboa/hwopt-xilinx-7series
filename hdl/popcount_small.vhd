
-- This is a convenient wrapper for one block of combinatorial popcount
-- It supports up to 33 input bits with individual blocks
-- For larger inputs, partial results of size 15 are summed with an adder tree

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity popcount_small is
	generic (
		BITS : natural := 16;
		WOUT : natural := 5
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount_small;

architecture synth of popcount_small is

	-- No declaration needed

begin

	assert BITS <= 33 report "Entity popcount : Number of inputs is too large" severity failure;

	-- Implementation as lookup table up to 6 input bits
	-- This naturally covers popcounts 3:2, 5:3, 6:3
	gen : if BITS <= 6 generate

		-- Compute the minimum number of bits needed to store the input value
		function storebitsnb(vin : natural) return natural is
			variable r : natural := 1;
			variable v : natural := vin;
		begin
			loop
				exit when v <= 1;
				r := r + 1;
				v := v / 2;
			end loop;
			return r;
		end function;

		constant need_out_size : natural := storebitsnb(BITS);
		constant array_size : natural := 2**BITS;
		type array_type is array(0 to array_size-1) of unsigned(need_out_size-1 downto 0);

		-- Simple popcount calculation
		function func_popcount(A : std_logic_vector) return natural is
			variable c : natural;
		begin
			c := 0;
			for i in A'low to A'high loop
				if A(i) = '1' then
					c := c + 1;
				end if;
			end loop;
			return c;
		end function;

		-- Function that generates the precomputed results
		function func_gen_out(b : boolean) return array_type is
			variable arr : array_type;
		begin
			for i in 0 to array_size-1 loop
				arr(i) := to_unsigned(func_popcount(std_logic_vector(to_unsigned(i, BITS))), need_out_size);
			end loop;
			return arr;
		end function;

		-- The precomputed lookup table
		constant array_num : array_type := func_gen_out(true);

	begin

		sum <= std_logic_vector(resize(array_num(to_integer(unsigned(bits_in))), WOUT));

	elsif BITS <= 7 generate

		component popcount_7_3 is
			generic (
				BITS : natural := 7;
				WOUT : natural := 3
			);
			port (
				bits_in : in  std_logic_vector(BITS-1 downto 0);
				sum     : out std_logic_vector(WOUT-1 downto 0)
			);
		end component;

	begin

		-- Usage = 3 LUTs, delay = 2 LUTs
		popc7 : popcount_7_3
			generic map (
				BITS  => BITS,
				WOUT  => WOUT
			)
			port map (
				bits_in => bits_in,
				sum     => sum
			);

	elsif BITS <= 9 generate

		component popcount_9_4 is
			generic (
				BITS : natural := 9;
				WOUT : natural := 4
			);
			port (
				bits_in : in  std_logic_vector(BITS-1 downto 0);
				sum     : out std_logic_vector(WOUT-1 downto 0)
			);
		end component;

	begin

		-- Usage = 4 LUTs, delay = 2 LUTs + carry
		popc9 : popcount_9_4
			generic map (
				BITS  => BITS,
				WOUT  => WOUT
			)
			port map (
				bits_in => bits_in,
				sum     => sum
			);

	elsif BITS <= 11 generate

		component popcount_11_4 is
			generic (
				BITS : natural := 11;
				WOUT : natural := 4
			);
			port (
				bits_in : in  std_logic_vector(BITS-1 downto 0);
				sum     : out std_logic_vector(WOUT-1 downto 0)
			);
		end component;

	begin

		-- Usage = 5 LUTs, delay = 2 LUTs + carry
		popc11 : popcount_11_4
			generic map (
				BITS  => BITS,
				WOUT  => WOUT
			)
			port map (
				bits_in => bits_in,
				sum     => sum
			);

	elsif BITS <= 12 generate

		component popcount_12_4 is
			generic (
				BITS : natural := 12;
				WOUT : natural := 4
			);
			port (
				bits_in : in  std_logic_vector(BITS-1 downto 0);
				sum     : out std_logic_vector(WOUT-1 downto 0)
			);
		end component;

	begin

		-- Usage = 6 LUTs, delay = 2 LUTs + carry
		popc12 : popcount_12_4
			generic map (
				BITS  => BITS,
				WOUT  => WOUT
			)
			port map (
				bits_in => bits_in,
				sum     => sum
			);

	elsif BITS <= 13 generate

		component popcount_13_4 is
			generic (
				BITS : natural := 13;
				WOUT : natural := 4
			);
			port (
				bits_in : in  std_logic_vector(BITS-1 downto 0);
				sum     : out std_logic_vector(WOUT-1 downto 0)
			);
		end component;

	begin

		-- Usage = 6 LUTs, delay = 3 LUTs + carry
		popc13 : popcount_13_4
			generic map (
				BITS  => BITS,
				WOUT  => WOUT
			)
			port map (
				bits_in => bits_in,
				sum     => sum
			);

	elsif BITS <= 15 generate

		component popcount_15_4 is
			generic (
				BITS : natural := 15;
				WOUT : natural := 4
			);
			port (
				bits_in : in  std_logic_vector(BITS-1 downto 0);
				sum     : out std_logic_vector(WOUT-1 downto 0)
			);
		end component;

	begin

		-- Usage = 7 LUTs, delay = 2 LUTs + 2 carry
		popc15 : popcount_15_4
			generic map (
				BITS  => BITS,
				WOUT  => WOUT
			)
			port map (
				bits_in => bits_in,
				sum     => sum
			);

	elsif BITS <= 16 generate

		component popcount_16_5 is
			generic (
				BITS : natural := 16;
				WOUT : natural := 5
			);
			port (
				bits_in : in  std_logic_vector(BITS-1 downto 0);
				sum     : out std_logic_vector(WOUT-1 downto 0)
			);
		end component;

	begin

		-- Usage = 8 LUTs, delay = 2 LUTs + 2 carry
		popc16 : popcount_16_5
			generic map (
				BITS  => BITS,
				WOUT  => WOUT
			)
			port map (
				bits_in => bits_in,
				sum     => sum
			);

	elsif BITS <= 17 generate

		component popcount_17_5 is
			generic (
				BITS : natural := 17;
				WOUT : natural := 5
			);
			port (
				bits_in : in  std_logic_vector(BITS-1 downto 0);
				sum     : out std_logic_vector(WOUT-1 downto 0)
			);
		end component;

	begin

		-- Usage = 9 LUTs, delay = 3 LUTs + 2 carry propag
		popc17 : popcount_17_5
			generic map (
				BITS  => BITS,
				WOUT  => WOUT
			)
			port map (
				bits_in => bits_in,
				sum     => sum
			);

	elsif BITS <= 18 generate

		component popcount_18_5 is
			generic (
				BITS : natural := 18;
				WOUT : natural := 5
			);
			port (
				bits_in : in  std_logic_vector(BITS-1 downto 0);
				sum     : out std_logic_vector(WOUT-1 downto 0)
			);
		end component;

	begin

		-- Usage = 10 LUTs, delay = 3 LUTs + 2 carry propag
		popc18 : popcount_18_5
			generic map (
				BITS  => BITS,
				WOUT  => WOUT
			)
			port map (
				bits_in => bits_in,
				sum     => sum
			);

	elsif BITS <= 31 generate

		component popcount_31_5 is
			generic (
				BITS : natural := 31;
				WOUT : natural := 5
			);
			port (
				bits_in : in  std_logic_vector(BITS-1 downto 0);
				sum     : out std_logic_vector(WOUT-1 downto 0)
			);
		end component;

	begin

		-- Usage = 17 LUTs, delay = 3 LUTs + 3 carry propag
		popc31 : popcount_31_5
			generic map (
				BITS  => BITS,
				WOUT  => WOUT
			)
			port map (
				bits_in => bits_in,
				sum     => sum
			);

	elsif BITS <= 33 generate

		component popcount_33_6 is
			generic (
				BITS : natural := 33;
				WOUT : natural := 6
			);
			port (
				bits_in : in  std_logic_vector(BITS-1 downto 0);
				sum     : out std_logic_vector(WOUT-1 downto 0)
			);
		end component;

	begin

		-- Usage = 20 LUTs, delay = 3 LUTs + 3 carry propag
		popc33 : popcount_33_6
			generic map (
				BITS  => BITS,
				WOUT  => WOUT
			)
			port map (
				bits_in => bits_in,
				sum     => sum
			);

	end generate;

end architecture;

