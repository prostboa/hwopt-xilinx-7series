
-- This is a one-stage register layer designed to limit fanout
-- Under the specified FANOUT, there is only one register, there may be more depending on wanted fanout
-- For total load higher than FANOUT * FANOUT, the fanout that is used is sqrt(NBOUT*MULT)

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity distribuf_1cy is
	generic(
		WDATA  : natural := 20;
		NBOUT  : natural := 20;
		MULT   : natural := 1;  -- Multiplier for expected load at each output
		FANOUT : natural := 20
	);
	port(
		clk : in std_logic;
		-- Input
		idata : in std_logic_vector(WDATA-1 downto 0);
		-- Output
		odata : out std_logic_vector(WDATA*NBOUT-1 downto 0)
	);
end distribuf_1cy;

architecture synth of distribuf_1cy is

	-- Get square root
	function sqrt(v : natural) return natural is
		variable sq : natural;
	begin
		if v <= 1 then
			return v;
		end if;
		sq := 1;
		loop
			if sq * sq > v then
				--report "sqrt(" & natural'image(v) & ") = " & natural'image(sq - 1);
				return sq - 1;
			end if;
			sq := sq + 1;
		end loop;
		return 0;
	end function;

	-- Get square root, ceil value
	function sqrt_ceil(v : natural) return natural is
		variable sq : natural;
	begin
		sq := sqrt(v);
		if sq * sq < v then
			return sq + 1;
		end if;
		return sq;
	end function;

	function calc_nbregs(phony : natural) return natural is
		variable nregs : natural;
	begin
		-- Compute the number of registers needed to respect fanout on output side
		nregs := (NBOUT * MULT + FANOUT - 1) / FANOUT;
		-- Handle high load: balance fanout with input side
		if nregs > FANOUT then
			nregs := sqrt_ceil(NBOUT * MULT);
		end if;
		-- Print some details
		report "distribuf_1cy : " &
			" nbout " & natural'image(NBOUT) &
			" mult "  & natural'image(MULT) &
			" fanout " & natural'image(FANOUT) &
			" -> regs " & natural'image(nregs);
		return nregs;
	end function;

	-- The number of registers needed in the local stage
	constant NBREGS : natural := calc_nbregs(0);
	-- Distribute outputs evenly among registers
	constant OUT_PER_REG : natural := (NBOUT + NBREGS - 1) / NBREGS;
	-- The registers, and the signal to compute their input
	signal regs : std_logic_vector(NBREGS*WDATA-1 downto 0) := (others => '0');

begin

	-- Write to registers
	process(clk)
	begin
		if rising_edge(clk) then

			for i in 0 to NBREGS-1 loop
				regs((i+1)*WDATA-1 downto i*WDATA) <= idata;
			end loop;

		end if;
	end process;

	-- Connect outputs: first guaranteed max-fanout outputs
	gen_maxfan: if NBREGS > 1 generate
		gen_maxfan_reg: for r in 0 to NBREGS-2 generate
			gen_maxfan_regloop: for i in r * OUT_PER_REG to (r+1) * OUT_PER_REG - 1 generate
				odata((i+1)*WDATA-1 downto i*WDATA) <= regs((r+1) * WDATA - 1 downto r * WDATA);
			end generate;
		end generate;
	end generate;

	-- Connect last outputs
	gen_lastout_loop: for i in (NBREGS-1) * OUT_PER_REG to NBOUT - 1 generate
		odata((i+1)*WDATA-1 downto i*WDATA) <= regs(NBREGS * WDATA - 1 downto (NBREGS-1) * WDATA);
	end generate;

end architecture;


