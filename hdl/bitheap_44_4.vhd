
-- This is a compressor 4,4:4
-- Total size : 3 LUTs
-- Delay : 2 LUTs + 1 carry propagation

-- Recommendation : Use the input ci only for the purpose of chaining compressors
-- In that case, the input di0(3) must be set to zero

library ieee;
use ieee.std_logic_1164.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity bitheap_44_4 is
	port (
		ci  : in  std_logic;
		di0 : in  std_logic_vector(3 downto 0);
		di1 : in  std_logic_vector(3 downto 0);
		sum : out std_logic_vector(3 downto 0)
	);
end bitheap_44_4;

architecture synth of bitheap_44_4 is

	signal p1 : std_logic := '0';

	signal c4_o  : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_co : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_di : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_s  : std_logic_vector(3 downto 0) := (others => '0');

begin

	-- Input index 0

	lutA : LUT6_2
		generic map (
			INIT => x"000069960000E8E8"
		)
		port map (
			O6 => c4_s(0),
			O5 => p1,
			I0 => di0(0),
			I1 => di0(1),
			I2 => di0(2),
			I3 => di0(3),
			I4 => '0',  -- Unused
			I5 => '1'   -- To have something different for O6 and O5
		);

	c4_di(0) <= di0(3);

	-- Input index 1

	lutB : LUT6
		generic map (
			INIT => x"0000000096696996"
		)
		port map (
			O  => c4_s(1),
			I0 => di1(0),
			I1 => di1(1),
			I2 => di1(2),
			I3 => di1(3),
			I4 => p1,
			I5 => '0'   -- Unused
		);

	c4_di(1) <= p1;

	-- Input index 2

	lutC : LUT6_2
		generic map (
			INIT => x"00007EE800008000"
		)
		port map (
			O6 => c4_s(2),
			O5 => c4_di(2),
			I0 => di1(0),
			I1 => di1(1),
			I2 => di1(2),
			I3 => di1(3),
			I4 => '0',  -- Unused
			I5 => '1'   -- To have something different for O6 and O5
		);

	-- The CARRY4

	c4: CARRY4
		port map (
			CO     => c4_co,
			O      => c4_o,
			DI     => c4_di,
			S      => c4_s,
			CI     => ci,
			CYINIT => '0'
		);

	-- Output ports

	sum <= c4_co(2) & c4_o(2 downto 0);

end architecture;

