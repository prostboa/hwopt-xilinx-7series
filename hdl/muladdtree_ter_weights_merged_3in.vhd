
-- This is a multiplication-accumulation component
-- Special version with 3 inputs only
-- Pipeline registers are created every REGEN stages and at last stage

-- FIXME A generalization of this would be welcome : at stages >= 1 it is possible to perform another ternary mult

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity muladdtree_ter_weights_merged_3in is
	generic(
		-- Data type and width
		WDATA : natural := 8;
		WOUT  : natural := 12;
		-- An optional tag
		TAGW  : natural := 1;
		TAGEN : boolean := true;
		TAGZC : boolean := false;
		-- How to add pipeline registers
		REGEN  : natural := 2;  -- 0=none, 1=all, else every EN stages
		REGIDX : natural := 0   -- Start index (from the leaves)
	);
	port(
		clk      : in  std_logic;
		clear    : in  std_logic;
		-- Data, input and output
		data_in  : in  std_logic_vector(3*WDATA-1 downto 0);
		data_out : out std_logic_vector(WOUT-1 downto 0);
		-- Multiplier and carry output
		mult_in  : in  std_logic_vector(3*2-1 downto 0);
		add1_out : out std_logic;
		-- Tag, input and output
		tag_in   : in  std_logic_vector(TAGW-1 downto 0);
		tag_out  : out std_logic_vector(TAGW-1 downto 0)
	);
end muladdtree_ter_weights_merged_3in;

architecture synth of muladdtree_ter_weights_merged_3in is

	-- Utility function to calculate minimum of two values
	function min(a, b : natural) return natural is
		variable m : natural := 0;
	begin
		m := a when a <= b else b;
		return m;
	end function;

	constant W1 : natural := min(WDATA+1, WOUT);
	constant W2 : natural := min(W1+1, WOUT);

	-- Add pipeline registers when IDX = EN - 1
	-- Force creation of registers at last stage
	constant REG1_CREATE : boolean := (REGEN > 0) and (REGIDX = REGEN-1);
	constant REG2_CREATE : boolean := (REGEN > 0);

	-- Signals at first level
	signal multA, multB : std_logic_vector(1 downto 0);

	-- The registers of 1st level
	signal reg1_data, reg1_data_n : std_logic_vector(W1-1 downto 0);
	signal reg1_add1, reg1_add1_n : std_logic;
	signal reg1_datC, reg1_datC_n : std_logic_vector(WDATA-1 downto 0);
	signal reg1_mulC, reg1_mulC_n : std_logic_vector(1 downto 0);
	signal reg1_tag : std_logic_vector(TAGW-1 downto 0);

	-- The registers of 2nd level
	signal reg2_data, reg2_data_n : std_logic_vector(W2-1 downto 0);
	signal reg2_add1, reg2_add1_n : std_logic;
	signal reg2_tag : std_logic_vector(TAGW-1 downto 0);

	-- Component declaration: Special first stage adder
	component muladdtree_ter_weights_merged_stage0 is
		generic(
			WDATA : natural := 8;
			WOUT  : natural := 9
		);
		port(
			mA  : in  std_logic_vector(1 downto 0);
			mB  : in  std_logic_vector(1 downto 0);
			inA : in  std_logic_vector(WDATA-1 downto 0);
			inB : in  std_logic_vector(WDATA-1 downto 0);
			res : out std_logic_vector(WOUT-1 downto 0);
			neg : out std_logic
		);
	end component;

begin

	-- First stage

	multA <= mult_in(1*2-1 downto 0*2);
	multB <= mult_in(2*2-1 downto 1*2);

	stage0 : muladdtree_ter_weights_merged_stage0
		generic map (
			WDATA => WDATA,
			WOUT  => W1
		)
		port map (
			mA  => multA,
			mB  => multB,
			inA => data_in(1*WDATA-1 downto 0*WDATA),
			inB => data_in(2*WDATA-1 downto 1*WDATA),
			res => reg1_data_n,
			neg => reg1_add1_n
		);

	reg1_datC_n <= data_in(3*WDATA-1 downto 2*WDATA);
	reg1_mulC_n <= mult_in(3*2-1 downto 2*2);

	-- Registers of first stage

	gen_reg1 : if REG1_CREATE = true generate

		process(clk)
		begin
			if rising_edge(clk) then

				reg1_data <= reg1_data_n;
				reg1_add1 <= reg1_add1_n;

				reg1_datC <= reg1_datC_n;
				reg1_mulC <= reg1_mulC_n;

				if TAGEN = true then
					reg1_tag <= tag_in;
					if (clear = '1') and (TAGZC = true) then
						reg1_tag <= (others => '0');
					end if;
				end if;

			end if;
		end process;

	end generate;

	gen_noreg1 : if REG1_CREATE = false generate

		reg1_data <= reg1_data_n;
		reg1_add1 <= reg1_add1_n;

		reg1_datC <= reg1_datC_n;
		reg1_mulC <= reg1_mulC_n;

		reg1_tag  <= tag_in when TAGEN = true else (others => '0');

	end generate;

	-- Second stage

	process(reg1_data, reg1_add1, reg1_datC, reg1_mulC)
		variable vecnotA : std_logic_vector(W1-1 downto 0);
		variable vecnotC : std_logic_vector(WDATA-1 downto 0);
		variable vecclrC : std_logic_vector(WDATA-1 downto 0);
		variable inAnot  : std_logic_vector(W1-1 downto 0);
		variable inCnot  : std_logic_vector(WDATA-1 downto 0);
		variable var_or  : std_logic;
		variable var_and : std_logic;
		variable res     : std_logic_vector(W2 downto 0);
	begin

		vecnotA := (others => reg1_add1);
		vecnotC := (others => reg1_mulC(1));
		vecclrC := (others => reg1_mulC(0));

		inAnot := reg1_data xor vecnotA;
		inCnot := (reg1_datC xor vecnotC) and vecclrC;

		var_or  := reg1_add1 or reg1_mulC(1);
		var_and := reg1_add1 and reg1_mulC(1);

		res := std_logic_vector(resize(signed(inAnot & '1'), W2+1) + resize(signed(inCnot & var_or), W2+1));

		reg2_data_n <= res(W2 downto 1);
		reg2_add1_n <= var_and;

	end process;

	-- Registers of second stage

	gen_reg2 : if REG2_CREATE = true generate

		process(clk)
		begin
			if rising_edge(clk) then

				reg2_data <= reg2_data_n;
				reg2_add1 <= reg2_add1_n;

				if TAGEN = true then
					reg2_tag <= reg1_tag;
					if (clear = '1') and (TAGZC = true) then
						reg2_tag <= (others => '0');
					end if;
				end if;

			end if;
		end process;

	end generate;

	gen_noreg2 : if REG2_CREATE = false generate

		reg2_data <= reg1_data_n;
		reg2_add1 <= reg1_add1_n;

		reg2_tag  <= reg1_tag when TAGEN = true else (others => '0');

	end generate;

	-- Outputs

	data_out <= std_logic_vector(resize(signed(reg2_data), WOUT));
	add1_out <= reg2_add1;
	tag_out  <= reg2_tag;

end architecture;

