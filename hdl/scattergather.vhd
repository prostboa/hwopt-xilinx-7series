
-- This is multi-stage MUX operator
-- The registers are created with radix 2
-- Each input is provided with an "enable" signal to indicate it is the selected one

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity scattergather is
	generic(
		WGATHER  : natural := 8;
		WSCATTER : natural := 1;
		NBIN     : natural := 20;
		WSEL     : natural := 12;
		EGATHER  : boolean := true;
		ESCATTER : boolean := true;
		RADIX    : natural := 2;
		REGALL   : boolean := false
	);
	port(
		clk         : in  std_logic;
		-- Selection input
		sel         : in  std_logic_vector(WSEL-1 downto 0);
		-- Enable input and output
		en_in       : in  std_logic;
		en_out      : out std_logic_vector(NBIN-1 downto 0);
		-- Gather data, input and output
		gather_in   : in  std_logic_vector(NBIN*WGATHER-1 downto 0);
		gather_out  : out std_logic_vector(WGATHER-1 downto 0);
		-- Scatter data, input and output
		scatter_in  : in  std_logic_vector(WSCATTER-1 downto 0);
		scatter_out : out std_logic_vector(NBIN*WSCATTER-1 downto 0)
	);
end scattergather;

architecture synth of scattergather is

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	constant WRADIX : natural := storebitsnb(RADIX - 1);

begin

	assert NBIN >= 1 report "Error NBIN must be >= 1" severity failure;
	assert RADIX = 2 report "Error RADIX must be 2 for now" severity failure;
	assert (RADIX >= 2) and (storebitsnb(RADIX) = storebitsnb(RADIX-1) + 1) report "Error RADIX must be power of 2 and >= 2" severity failure;

	-- Handle when there are RADIX inputs or less
	gen_radix: if (NBIN = 1) or ((NBIN > 1) and (NBIN <= RADIX) and (REGALL = false)) generate

		-- Bufferize the selection
		signal regen : std_logic_vector(NBIN-1 downto 0) := (others => '0');

		-- Bufferize the data
		signal regsca : std_logic_vector(WSCATTER-1 downto 0) := (others => '0');
		signal reggat : std_logic_vector(WGATHER-1 downto 0) := (others => '0');

	begin

		process(clk)
			variable vargat : std_logic_vector(WGATHER-1 downto 0) := (others => '0');
		begin
			if rising_edge(clk) then

				for i in 0 to NBIN-1 loop
					regen(i) <= en_in;
					if (NBIN > 1) and (unsigned(sel(WRADIX-1 downto 0)) /= i) then
						regen(i) <= '0';
					end if;
				end loop;

				if EGATHER = true then
					vargat := (others => '0');
					for i in 0 to NBIN-1 loop
						if regen(i) = '1' then
							vargat := vargat or gather_in((i+1)*WGATHER-1 downto i*WGATHER);
						end if;
					end loop;
					reggat <= vargat;
				end if;

				if ESCATTER = true then
					regsca <= scatter_in;
				end if;

			end if;
		end process;

		-- Assign the output ports
		en_out     <= regen;
		gather_out <= reggat;

		gen_sca: for i in 0 to NBIN-1 generate
			scatter_out((i+1)*WSCATTER-1 downto i*WSCATTER) <= regsca;
		end generate;

	end generate;

	-- Handle when there are more than RADIX inputs
	gen_more: if (NBIN > RADIX) or ((NBIN > 1) and (REGALL = true)) generate

		-- Compute the log of a number vin in the base b
		function logbase(vin : natural; b : natural) return natural is
			variable p : natural := 0;
			variable v : natural := vin;
		begin
			loop
				exit when v < b;
				p := p + 1;
				v := v / b;
			end loop;
			return p;
		end function;

		type cfg_t is record
			FULL_IN : natural;
			FULL_NB : natural;
			LAST_IN : natural;
			ALL_NB  : natural;
			WSEL_L  : natural;
			WSEL_H  : natural;
			WSEL_P  : natural;
		end record;

		-- Calculate the number of blocks that are full
		function calc_blocks(phony : boolean) return cfg_t is
			variable cfg: cfg_t := (0, 0, 0, 0, 0, 0, 0);
			variable cstpow: natural := logbase(nbin-1, radix);
		begin

			assert WSEL >= storebitsnb(NBIN-1) report "Error WSEL too small" severity failure;

			report "CFG enter:" &
				" nbin " & natural'image(nbin) &
				" radix " & natural'image(radix) &
				" cstpow " & natural'image(cstpow);

			if nbin <= radix then
				cfg.FULL_IN := 1;
				cfg.FULL_NB := nbin;
				cfg.WSEL_L  := 0;
			else
				cfg.FULL_IN := radix ** cstpow;
				cfg.FULL_NB := nbin / cfg.FULL_IN;
				cfg.LAST_IN := nbin - cfg.FULL_NB * cfg.FULL_IN;
				cfg.WSEL_L  := WRADIX * cstpow;
			end if;

			assert cfg.WSEL_L < WSEL report "Error WSEL_L too high" severity failure;

			cfg.WSEL_H := WRADIX;
			if cfg.WSEL_L + cfg.WSEL_H > WSEL then
				cfg.WSEL_H := WSEL - cfg.WSEL_L;
			end if;

			cfg.WSEL_P := cfg.WSEL_L;
			if cfg.WSEL_P = 0 then
				cfg.WSEL_P := 1;
			end if;

			cfg.ALL_NB := cfg.FULL_NB;
			if cfg.LAST_IN > 0 then
				cfg.ALL_NB := cfg.ALL_NB + 1;
			end if;

			report "CFG:" &
				" full_in " & natural'image(cfg.FULL_IN) &
				" full_nb " & natural'image(cfg.FULL_NB) &
				" last_nb " & natural'image(cfg.LAST_IN) &
				" all_nb "  & natural'image(cfg.ALL_NB) &
				" wsel_l "  & natural'image(cfg.WSEL_L) &
				" wsel_h "  & natural'image(cfg.WSEL_H);

			return cfg;
		end function;

		-- Select an appropriate BRAM configuration
		constant CFG : cfg_t := calc_blocks(false);

		-- Bufferize the selection
		signal regsel : std_logic_vector(cfg.WSEL_P-1 downto 0) := (others => '0');
		signal regen  : std_logic_vector(cfg.ALL_NB-1 downto 0) := (others => '0');

		-- Bufferize the data
		signal regsca : std_logic_vector(WSCATTER-1 downto 0) := (others => '0');
		signal reggat : std_logic_vector(WGATHER-1 downto 0) := (others => '0');

		-- The outputs of the lower mux components
		signal siggat : std_logic_vector(cfg.ALL_NB*WGATHER-1 downto 0) := (others => '0');

	begin

		-- Instantiate the MUX components: the ones full

		gen_full: for i in 0 to cfg.FULL_NB-1 generate

			i_full: entity scattergather
				generic map (
					WGATHER  => WGATHER,
					WSCATTER => WSCATTER,
					NBIN     => cfg.FULL_IN,
					WSEL     => cfg.WSEL_P,
					EGATHER  => EGATHER,
					ESCATTER => ESCATTER,
					RADIX    => RADIX,
					REGALL   => REGALL
				)
				port map (
					clk         => clk,
					-- Selection input
					sel         => regsel,
					-- Enable input and output
					en_in       => regen(i),
					en_out      => en_out((i+1)*cfg.FULL_IN-1 downto i*cfg.FULL_IN),
					-- Data input and output
					gather_in   => gather_in((i+1)*cfg.FULL_IN*WGATHER-1 downto i*cfg.FULL_IN*WGATHER),
					gather_out  => siggat((i+1)*WGATHER-1 downto i*WGATHER),
					-- Tag input and output
					scatter_in  => regsca,
					scatter_out => scatter_out((i+1)*cfg.FULL_IN*WSCATTER-1 downto i*cfg.FULL_IN*WSCATTER)
				);

		end generate;

		-- Instantiate the last MUX components: the remaining inputs

		gen_last: if cfg.LAST_IN > 0 generate

			i_last: entity scattergather
				generic map (
					WGATHER  => WGATHER,
					WSCATTER => WSCATTER,
					NBIN     => cfg.LAST_IN,
					WSEL     => cfg.WSEL_P,
					EGATHER  => EGATHER,
					ESCATTER => ESCATTER,
					RADIX    => RADIX,
					REGALL   => REGALL
				)
				port map (
					clk         => clk,
					-- Selection input
					sel         => regsel,
					-- Enable input and output
					en_in       => regen(cfg.FULL_NB),
					en_out      => en_out(NBIN-1 downto cfg.FULL_NB*cfg.FULL_IN),
					-- Data input and output
					gather_in   => gather_in(NBIN*WGATHER-1 downto cfg.FULL_NB*cfg.FULL_IN*WGATHER),
					gather_out  => siggat(cfg.ALL_NB*WGATHER-1 downto cfg.FULL_NB*WGATHER),
					-- Tag input and output
					scatter_in  => regsca,
					scatter_out => scatter_out(NBIN*WSCATTER-1 downto cfg.FULL_NB*cfg.FULL_IN*WSCATTER)
				);

		end generate;

		-- The local multiplexer functionality

		process(clk)
			variable vargat : std_logic_vector(WGATHER-1 downto 0) := (others => '0');
		begin
			if rising_edge(clk) then

				if cfg.WSEL_L > 0 then
					regsel <= sel(cfg.WSEL_L-1 downto 0);
				end if;

				for i in 0 to cfg.ALL_NB-1 loop
					regen(i) <= en_in;
					if (cfg.ALL_NB > 1) and (unsigned(sel(cfg.WSEL_L+cfg.WSEL_H-1 downto cfg.WSEL_L)) /= i) then
						regen(i) <= '0';
					end if;
				end loop;

				if EGATHER = true then
					vargat := (others => '0');
					for i in 0 to cfg.ALL_NB-1 loop
						if regen(i) = '1' then
							vargat := vargat or siggat((i+1)*WGATHER-1 downto i*WGATHER);
						end if;
					end loop;
					reggat <= vargat;
				end if;

				if ESCATTER = true then
					regsca <= scatter_in;
				end if;

			end if;
		end process;

		-- Assign the output ports
		gather_out <= reggat;

	end generate;

end architecture;


