
-- This is a pair of chains of compressors 5,5:4
-- It produces two partial sums
-- Total 2*WDATA luts (+2 luts with sign support depending on input width)
-- Delay 2 lut + carry

-- Note : The two partial sum results have actual width WDATA+2 each, if unsigned
-- Note : And it is WDATA+3 if sign extension is performed

-- About sign extension suport :
-- There would be 2 solutions to implement sign extension support. The tricky case is when input data is a multiple of 2b.
-- Solution 1 :
--   Generate the 4b result with the usual 5in 2b adder block,
--   Then add a recoding layer after it : cost is +2 LUTs, delay is +1 LUT
-- Solution 2 :
--   Add one additional input block, for 1b inputs, filled with sign extension of data
--   Cost is +2 LUTs, delay is unchanged compared to unsigned
-- What is implemented here : Solution 2 because it has no impact on delay

-- About interest of this block, compared to other multi-input adders :
--   Interest : No long carry chains that require specific placement & routing in the FPGA
--   Drawback : Usage of resources is not as high as with the 6in adder (same usage, less functionality)
--   Drawback : Slower than 6in and 4in adders, same speed as the 3in adder

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT primitives
library unisim;
use unisim.vcomponents.all;

entity bitheap_55_4_pair is
	generic (
		WDATA : natural := 8;
		WOUT  : natural := 12;
		NBIN  : natural := 5;
		SIGN  : boolean := false
	);
	port (
		data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
		data_1   : in  std_logic;
		res_idx0 : out std_logic_vector(WOUT-1 downto 0);
		res_idx2 : out std_logic_vector(WOUT-1 downto 0)
	);
end bitheap_55_4_pair;

architecture synth of bitheap_55_4_pair is

	-- Utility function to calculate minimum of two values
	function min(a, b : integer) return integer is
		variable m : integer := 0;
	begin
		m := a when a <= b else b;
		return m;
	end function;

	-- Utility function to generate integer from boolean
	function to_integer(b : boolean) return integer is
		variable i : integer := 0;
	begin
		i := 1 when b = true else 0;
		return i;
	end function;

	-- Determine if a virtual block for sign extension is needed
	constant NEED_SGNEXT_BLOCK : boolean := SIGN and (WDATA mod 2 = 0) and (WOUT > WDATA);

	-- Total number of 2b blocks (the highest block may have 1b input size)
	-- If signed and input is a multiple of 2b, then add one extra 1b block for sign extension
	constant NUM_BLOCKS : natural := (WDATA / 2) + (WDATA mod 2) + to_integer(NEED_SGNEXT_BLOCK);

	-- Intermediate signals to assign outputs (also useful to set default values)
	signal sig_res0 : std_logic_vector(WOUT-1 downto 0) := (others => '0');
	signal sig_res2 : std_logic_vector(WOUT-1 downto 0) := (others => '0');

	-- The underlying component 5,5:4
	component bitheap_55_4 is
		port (
			di0 : in  std_logic_vector(4 downto 0);
			di1 : in  std_logic_vector(4 downto 0);
			p53 : out std_logic_vector(2 downto 0);
			do  : out std_logic_vector(1 downto 0);
			co  : out std_logic_vector(1 downto 0);
			sum : out std_logic_vector(3 downto 0)
		);
	end component;

begin

	assert NBIN <= 5 report "Entity bitheap_55_4_pair : Number of inputs too large" severity failure;

	-- Generate individual 6,0,6:5 compressors
	gen_blocks : for b in 0 to NUM_BLOCKS-1 generate

		-- Identify if this is the virtual block for sign extension
		constant IS_SGNEXT_BLOCK : boolean := NEED_SGNEXT_BLOCK and (b = NUM_BLOCKS-1);

		-- Usually 2b input block, can be 1b for last block
		constant BLOCK_WIN : natural :=
			2 * to_integer(WDATA - b*2 > 1) +
			1 * to_integer(WDATA - b*2 = 1) +
			1 * to_integer(IS_SGNEXT_BLOCK);

		-- The partial result that this block is supposed to generate
		-- The virtual block for sign extension has 1b inputs, it outputs 4b
		constant BLOCK_WOUT_INT : natural := BLOCK_WIN + 2 + to_integer(SIGN and ((b = NUM_BLOCKS-1)));

		-- How to resize the partial result if needed
		constant NEED_CROP_OR_EXTEND : boolean := (2*b + BLOCK_WOUT_INT > WOUT) or (b = NUM_BLOCKS-1);
		constant BLOCK_WOUT : natural :=
			BLOCK_WOUT_INT * to_integer(not NEED_CROP_OR_EXTEND) +
			(WOUT - 2*b)   * to_integer(NEED_CROP_OR_EXTEND);

		-- Wrapper signal for partial result of this block
		signal part_res : std_logic_vector(BLOCK_WOUT-1 downto 0) := (others => '0');

	begin

		gen : if BLOCK_WIN = 1 generate

			-- Output is 3b if unsigned (a 5:3 popcount, 2 LUTs)
			-- Output is 4b if signed (also 2 LUTs)

			constant array_size : natural := 2**5;
			type array_type is array(0 to array_size-1) of std_logic_vector(BLOCK_WOUT_INT-1 downto 0);

			-- Simple popcount calculation
			function func_popcount(A : std_logic_vector) return natural is
				variable c : natural;
			begin
				c := 0;
				for i in A'low to A'high loop
					if A(i) = '1' then
						c := c + 1;
					end if;
				end loop;
				return c;
			end function;

			-- Function that generates the precomputed results
			function func_gen_out(phony : boolean) return array_type is
				variable arr : array_type;
				variable c : integer;
			begin
				for i in 0 to array_size-1 loop
					c := func_popcount(std_logic_vector(to_unsigned(i, 5)));
					if SIGN and (b = NUM_BLOCKS-1) then
						c := 0 - c;
						arr(i) := std_logic_vector(to_signed(c, BLOCK_WOUT_INT));
					else
						arr(i) := std_logic_vector(to_unsigned(c, BLOCK_WOUT_INT));
					end if;
				end loop;
				return arr;
			end function;

			-- The precomputed results
			constant rom_res : array_type := func_gen_out(true);

			-- Wrapper signal for inputs of this block
			signal sig_in : std_logic_vector(NBIN-1 downto 0) := (others => '0');

		begin

			-- Assign the input wrapper signal
			gen_in : for i in 0 to NBIN-1 generate
				sig_in(i) <= data_in(i*WDATA + WDATA - 1);
			end generate;

			-- Partial result
			gen_res : if SIGN and (b = NUM_BLOCKS-1) generate
				part_res <= std_logic_vector(resize(signed(rom_res(to_integer(unsigned(sig_in)))), BLOCK_WOUT));
			else generate
				part_res <= std_logic_vector(resize(unsigned(rom_res(to_integer(unsigned(sig_in)))), BLOCK_WOUT));
			end generate;

		-- Inputs of the block are 2b wide
		else generate

			-- Wrappers for compressor interface
			signal di0 : std_logic_vector(4 downto 0) := (others => '0');
			signal di1 : std_logic_vector(4 downto 0) := (others => '0');
			signal sum : std_logic_vector(3 downto 0) := (others => '0');

		begin

			-- Assign the input wrapper signal
			gen_in : for i in 0 to NBIN-1 generate
				di0(i) <= data_in(i*WDATA + b*2 + 0);
				di1(i) <= data_in(i*WDATA + b*2 + 1);
			end generate;

			-- Generate the partial result
			comp55 : bitheap_55_4
				port map (
					di0 => di0,
					di1 => di1,
					p53 => open,
					do  => open,
					co  => open,
					sum => sum
				);

			-- Resize the partial result
			part_res <= std_logic_vector(resize(unsigned(sum), BLOCK_WOUT));

		end generate;

		-- Set partial results in actual outputs
		gen_res : if b mod 2 = 0 generate
			sig_res0(2*b + BLOCK_WOUT - 1 downto 2*b) <= part_res;
		else generate
			sig_res2(2*b + BLOCK_WOUT - 1 downto 2*b) <= part_res;
		end generate;

	end generate;

	-- Assign input data_1 directly in intermediate output signal
	sig_res2(0) <= data_1;

	-- Assign outputs
	res_idx0 <= sig_res0;
	res_idx2 <= sig_res2;

end architecture;

