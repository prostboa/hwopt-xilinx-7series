
-- This is a multi-stage MUX operator where the selection is a one-bit enable
-- The merging tree is a pipelined OR operation
-- If RADIX = 0, generate a combinatorial design
-- If RADIX = 1, generate a register at output only
-- If RADIX > 1, generate a pipelined design

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity muxtree_decoded is
	generic(
		WDATA : natural := 8;
		NBIN  : natural := 8;
		TAGW  : natural := 1;
		TAGEN : boolean := true;
		TAGZC : boolean := true;
		RADIX : natural := 18
	);
	port(
		clk      : in  std_logic;
		clear    : in  std_logic;
		-- Selection input
		sel      : in  std_logic_vector(NBIN-1 downto 0);
		-- Data, input and output
		data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
		data_out : out std_logic_vector(WDATA-1 downto 0);
		-- Tag, input and output
		tag_in   : in  std_logic_vector(TAGW-1 downto 0);
		tag_out  : out std_logic_vector(TAGW-1 downto 0)
	);
end muxtree_decoded;

architecture synth of muxtree_decoded is

	signal sigand : std_logic_vector(NBIN*WDATA-1 downto 0);

	component logictree is
		generic(
			WDATA : natural := 8;
			NBIN  : natural := 20;
			-- The operation to perform : AND, OR, XOR optionally followed by NOT
			OPAND : boolean := false;
			OPOR  : boolean := false;
			OPXOR : boolean := false;
			OPNOT : boolean := false;
			-- Optional tag propagation
			TAGW  : natural := 1;
			TAGEN : boolean := true;
			TAGZC : boolean := true;
			RADIX : natural := 36
		);
		port(
			clk      : in  std_logic;
			clear    : in  std_logic;
			-- Data, input and output
			data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
			data_out : out std_logic_vector(WDATA-1 downto 0);
			-- Tag, input and output
			tag_in   : in  std_logic_vector(TAGW-1 downto 0);
			tag_out  : out std_logic_vector(TAGW-1 downto 0)
		);
	end component;

begin

	process(all)
	begin
		for i in 0 to NBIN-1 loop
			sigand((i+1)*WDATA-1 downto i*WDATA) <= data_in((i+1)*WDATA-1 downto i*WDATA) and sel(i);
		end loop;
	end process;

	i_or : logictree
		generic map (
			WDATA => WDATA,
			NBIN  => NBIN,
			-- The operation to perform : AND, OR, XOR optionally followed by NOT
			OPOR  => true,
			-- Optional tag propagation
			TAGW  => TAGW,
			TAGEN => TAGEN,
			TAGZC => TAGZC,
			RADIX => RADIX
		)
		port map (
			clk      => clk,
			clear    => clear,
			-- Data, input and output
			data_in  => sigand,
			data_out => data_out,
			-- Tag, input and output
			tag_in   => tag_in,
			tag_out  => tag_out
		);

end architecture;

