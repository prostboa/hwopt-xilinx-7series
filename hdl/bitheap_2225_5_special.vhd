
-- This is a special-purpose compressor 2,2,2,5:5
-- Total size : 4 LUTs
-- Delay : 2 LUTs + 1 carry propagation

-- Limitation : The result is correct only if the theoretical result fits in 5 bits
-- The theoretical results 32 and 33 are approximated respectively to 24 and 25 (roughly 25% error)

-- Recommendation : Use the input ci only for the purpose of chaining compressors
-- In that situation, di0(4) must be set to zero

library ieee;
use ieee.std_logic_1164.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity bitheap_2225_5_special is
	port (
		ci  : in  std_logic;
		di0 : in  std_logic_vector(4 downto 0);
		di1 : in  std_logic_vector(1 downto 0);
		di2 : in  std_logic_vector(1 downto 0);
		di3 : in  std_logic_vector(1 downto 0);
		do  : out std_logic_vector(3 downto 0);
		co  : out std_logic_vector(3 downto 0);
		sum : out std_logic_vector(4 downto 0)
	);
end bitheap_2225_5_special;

architecture synth of bitheap_2225_5_special is

	signal inst_accu_c4_o  : std_logic_vector(3 downto 0) := (others => '0');
	signal inst_accu_c4_co : std_logic_vector(3 downto 0) := (others => '0');
	signal inst_accu_c4_di : std_logic_vector(3 downto 0) := (others => '0');
	signal inst_accu_c4_s  : std_logic_vector(3 downto 0) := (others => '0');

begin

	-- Input index 0

	lutA : LUT6_2
		generic map (
			INIT => x"0000699600009696"
		)
		port map (
			O6 => inst_accu_c4_s(0),
			O5 => inst_accu_c4_di(0),
			I0 => di0(0),
			I1 => di0(1),
			I2 => di0(2),
			I3 => di0(3),
			I4 => '0',  -- Unused
			I5 => '1'   -- To have something different for O6 and O5
		);

	-- Input index 1

	lutB : LUT6_2
		generic map (
			INIT => x"E81717E8E8E8E8E8"
		)
		port map (
			O6 => inst_accu_c4_s(1),
			O5 => inst_accu_c4_di(1),
			I0 => di0(0),
			I1 => di0(1),
			I2 => di0(2),
			I3 => di1(0),
			I4 => di1(1),
			I5 => '1'   -- To have something different for O6 and O5
		);

	-- Input index 2

	lutC : LUT6_2
		generic map (
			INIT => x"0000877800000FF0"
		)
		port map (
			O6 => inst_accu_c4_s(2),
			O5 => inst_accu_c4_di(2),
			I0 => di1(0),
			I1 => di1(1),
			I2 => di2(0),
			I3 => di2(1),
			I4 => '0',  -- Unused
			I5 => '1'   -- To have something different for O6 and O5
		);

	-- Input index 3

	lutD: LUT6_2
		generic map (
			INIT => x"FFEAEA80EA95956A"
		)
		port map (
			O6 => sum(4),
			O5 => sum(3),
			I0 => inst_accu_c4_co(2),
			I1 => di2(0),
			I2 => di2(1),
			I3 => di3(0),
			I4 => di3(1),
			I5 => '1'   -- To have something different for O6 and O5
		);

	-- The CARRY4

	c4: CARRY4
		port map (
			CO     => inst_accu_c4_co,
			O      => inst_accu_c4_o,
			DI     => inst_accu_c4_di,
			S      => inst_accu_c4_s,
			CI     => ci,
			CYINIT => di0(4)
		);

	-- Output ports

	do <= inst_accu_c4_o;
	co <= inst_accu_c4_co;
	sum(2 downto 0) <= inst_accu_c4_o(2 downto 0);

end architecture;

