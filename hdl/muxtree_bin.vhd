
-- This is multi-stage MUX operator
-- The registers are created with radix 2
-- Each input is provided with an "enable" signal to indicate it is the selected one

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity muxtree_bin is
	generic(
		WDATA : natural := 8;
		NBIN  : natural := 20;
		WSEL  : natural := 12
	);
	port(
		clk      : in  std_logic;
		-- Selection input
		sel      : in  std_logic_vector(WSEL-1 downto 0);
		-- Enable input and output
		en_in    : in  std_logic;
		en_out   : out std_logic_vector(NBIN-1 downto 0);
		-- Data input and output
		data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
		data_out : out std_logic_vector(WDATA-1 downto 0)
	);
end muxtree_bin;

architecture synth of muxtree_bin is

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	-- There is always one register per stage
	signal regdata : std_logic_vector(WDATA-1 downto 0) := (others => '0');

begin

	-- Handle when there is no register
	gen_nb1 : if NBIN <= 1 generate
		signal regen0 : std_logic := '0';
	begin

		process(clk)
		begin
			if rising_edge(clk) then
				regen0  <= en_in and not sel(0);
				regdata <= data_in;
			end if;
		end process;

		-- Assign the output ports
		data_out  <= regdata;
		en_out(0) <= regen0;

	end generate;

	-- Handle when there are exactly 2 inputs
	gen_nb2 : if NBIN = 2 generate
		-- Bufferize the selection
		signal regen0 : std_logic := '0';
		signal regen1 : std_logic := '0';
	begin

		process(clk)
		begin
			if rising_edge(clk) then

				regen0 <= en_in and not sel(0);
				regen1 <= en_in and sel(0);

				regdata <= (others => '0');
				if regen0 = '1' then
					regdata <= data_in(WDATA-1 downto 0);
				elsif regen1 = '1' then
					regdata <= data_in(2*WDATA-1 downto WDATA);
				end if;

			end if;
		end process;

		-- Assign the output ports
		data_out  <= regdata;
		en_out(0) <= regen0;
		en_out(1) <= regen1;

	end generate;

	-- Handle when there are several registers
	gen_more : if NBIN > 2 generate

		-- Compute the max power of 2 that fits in the input number
		function maxpow2(vin : natural) return natural is
			variable p : natural := 1;
		begin
			loop
				exit when 2*p >= vin;
				p := p * 2;
			end loop;
			return p;
		end function;

		-- The number of registers needed in the local stage
		constant NBIN0 : natural := maxpow2(NBIN);
		constant NBIN1 : natural := NBIN - NBIN0;
		constant IDX   : natural := storebitsnb(NBIN0-1);

		-- Bufferize the selection
		signal regsel : std_logic_vector(IDX-1 downto 0) := (others => '0');
		signal regen0 : std_logic := '0';
		signal regen1 : std_logic := '0';

		-- The outputs of the lower mux components
		signal res0, res1 : std_logic_vector(WDATA-1 downto 0) := (others => '0');

	begin

		assert WSEL >= storebitsnb(NBIN-1) report "Error WSEL too small" severity failure;

		-- Instantiate the lower MUX components

		mux0 : entity muxtree_bin
			generic map (
				WDATA => WDATA,
				NBIN  => NBIN0,
				WSEL  => IDX
			)
			port map (
				clk      => clk,
				-- Selection input
				sel      => regsel,
				-- Enable input and output
				en_in    => regen0,
				en_out   => en_out(NBIN0-1 downto 0),
				-- Data input and output
				data_in  => data_in(NBIN0*WDATA-1 downto 0),
				data_out => res0
			);

		mux1 : entity muxtree_bin
			generic map (
				WDATA => WDATA,
				NBIN  => NBIN1,
				WSEL  => IDX
			)
			port map (
				clk      => clk,
				-- Selection input
				sel      => regsel,
				-- Enable input and output
				en_in    => regen1,
				en_out   => en_out(NBIN-1 downto NBIN0),
				-- Data input and output
				data_in  => data_in(NBIN*WDATA-1 downto NBIN0*WDATA),
				data_out => res1
			);

		-- The local multiplexer functionality

		process(clk)
		begin
			if rising_edge(clk) then

				regen0 <= en_in and not sel(IDX);
				regen1 <= en_in and sel(IDX);
				regsel <= sel(IDX-1 downto 0);

				regdata <= (others => '0');
				if regen0 = '1' then
					regdata <= res0;
				elsif regen1 = '1' then
					regdata <= res1;
				end if;

			end if;
		end process;

		-- Assign the output ports
		data_out <= regdata;

	end generate;

end architecture;

