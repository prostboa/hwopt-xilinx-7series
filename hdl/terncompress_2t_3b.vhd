
-- This is a ternary-to-binary lossy compressor 2T -> 3b
-- Based on look-up tables
-- The ternary code (-1, -1) is forbidden

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity terncompress_2t_3b is
	port(
		ter_in  : in  std_logic_vector(3 downto 0);
		bin_out : out std_logic_vector(2 downto 0)
	);
end terncompress_2t_3b;

architecture synth of terncompress_2t_3b is

	type comp2t3b_type is array (0 to 15) of std_logic_vector(2 downto 0);
	constant comp2t3b_lut : comp2t3b_type := (
		"000", "001", "000", "010",  -- 0000 0001 0010 0011 -> codes 0 1 - 2
		"011", "100", "000", "101",  -- 0100 0101 0110 0111 -> codes 3 4 - 5
		"000", "000", "000", "000",  -- 1000 1001 1010 1011 -> codes - - - -
		"110", "111", "000", "000"   -- 1100 1101 1110 1111 -> codes 6 7 - -
	);

begin

	bin_out <= comp2t3b_lut( to_integer(unsigned(ter_in)) );

end architecture;

