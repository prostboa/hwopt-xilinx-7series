
-- This is a popcount 13b -> 4b
-- Total size : 2 + 3 = 5 LUTs
-- Delay : 2 LUTs + 2 carry propag

-- Architecture : first one 7:3 compressor that handles 7 bits
-- Then one 1,1,7:4 compressor that adds 6 extra bits

-- Warning : Even though this component has very high efficiency, using it extensively would not be wise because of all carry4 only partially used

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT primitives
library unisim;
use unisim.vcomponents.all;

entity popcount_13_4_alt is
	generic (
		BITS : natural := 13;
		WOUT : natural := 4
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount_13_4_alt;

architecture synth of popcount_13_4_alt is

	signal sigBits : std_logic_vector(12 downto 0) := (others => '0');

	signal inst73_sum : std_logic_vector(2 downto 0) := (others => '0');

	signal inst117_di0 : std_logic_vector(6 downto 0) := (others => '0');
	signal inst117_di1 : std_logic_vector(0 downto 0) := (others => '0');
	signal inst117_di2 : std_logic_vector(0 downto 0) := (others => '0');
	signal inst117_sum : std_logic_vector(3 downto 0) := (others => '0');

	component popcount_7_3_carry4 is
		generic (
			BITS : natural := 7;
			WOUT : natural := 3
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component bitheap_117_4 is
		port (
			ci  : in  std_logic;
			di0 : in  std_logic_vector(6 downto 0);
			di1 : in  std_logic_vector(0 downto 0);
			di2 : in  std_logic_vector(0 downto 0);
			do  : out std_logic_vector(3 downto 0);
			co  : out std_logic_vector(3 downto 0);
			sum : out std_logic_vector(3 downto 0)
		);
	end component;

begin

	assert BITS <= 13 report "Number of inputs too large for module popcount_13_4" severity failure;

	sigBits(BITS-1 downto 0) <= bits_in;

	comp73 : popcount_7_3_carry4
		port map (
			bits_in => sigBits(6 downto 0),
			sum     => inst73_sum
		);

	inst117_di0    <= sigBits(12 downto 7) & inst73_sum(0);
	inst117_di1(0) <= inst73_sum(1);
	inst117_di2(0) <= inst73_sum(2);

	comp117 : bitheap_117_4
		port map (
			ci  => '0',
			di0 => inst117_di0,
			di1 => inst117_di1,
			di2 => inst117_di2,
			do  => open,
			co  => open,
			sum => inst117_sum
		);

	sum <= std_logic_vector(resize(unsigned(inst117_sum), WOUT));

end architecture;

