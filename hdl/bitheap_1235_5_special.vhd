
-- This is a compressor 1,2,3,5:5
-- This is a special variant of compressor 1,3,2,5:5 with one common bit between positions 0 and 1
-- Input di0(0) is also added in position 1

-- Recommendation : Use the input ci only for the purpose of chaining compressors
-- In that situation, di0(4) must be set to zero

library ieee;
use ieee.std_logic_1164.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity bitheap_1235_5_special is
	port (
		ci   : in  std_logic;
		di0  : in  std_logic_vector(4 downto 0);
		di1  : in  std_logic_vector(1 downto 0);
		di2  : in  std_logic_vector(1 downto 0);
		di3  : in  std_logic_vector(0 downto 0);
		do   : out std_logic_vector(3 downto 0);
		co   : out std_logic_vector(3 downto 0)
	);
end bitheap_1235_5_special;

architecture synth of bitheap_1235_5_special is

	signal c4_o  : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_co : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_di : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_s  : std_logic_vector(3 downto 0) := (others => '0');

begin

	-- Input index 0

	lutA : LUT6_2
		generic map (
			INIT => x"000069960000FF00"
		)
		port map (
			O6 => c4_s(0),
			O5 => c4_di(0),
			I0 => di0(0),
			I1 => di0(1),
			I2 => di0(2),
			I3 => di0(3),
			I4 => '0',  -- Unused
			I5 => '1'   -- To have something different for O6 and O5
		);

	-- Input index 1

	lutB : LUT6_2
		generic map (
			INIT => x"42BDBD42E8E8E8E8"
		)
		port map (
			O6 => c4_s(1),
			O5 => c4_di(1),
			I0 => di0(0),
			I1 => di0(1),
			I2 => di0(2),
			I3 => di1(0),
			I4 => di1(1),
			I5 => '1'   -- To have something different for O6 and O5
		);

	-- Input index 2

	lutC : LUT6_2
		generic map (
			INIT => x"E81717E800FFFF00"
		)
		port map (
			O6 => c4_s(2),
			O5 => c4_di(2),
			I0 => di1(0),
			I1 => di1(1),
			I2 => di0(0),
			I3 => di2(0),
			I4 => di2(1),
			I5 => '1'   -- To have something different for O6 and O5
		);

	-- Input index 3

	lutD: LUT6_2
		generic map (
			INIT => x"00000078000000F0"
		)
		port map (
			O6 => c4_s(3),
			O5 => c4_di(3),
			I0 => di2(0),
			I1 => di2(1),
			I2 => di3(0),
			I3 => '0',  -- Unused
			I4 => '0',  -- Unused
			I5 => '1'   -- To have something different for O6 and O5
		);

	-- The CARRY4

	c4: CARRY4
		port map (
			CO     => c4_co,
			O      => c4_o,
			DI     => c4_di,
			S      => c4_s,
			CI     => ci,
			CYINIT => di0(4)
		);

	-- Output ports

	do <= c4_o;
	co <= c4_co;

end architecture;

