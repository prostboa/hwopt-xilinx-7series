
-- This is a compressor 6,0,6:5
-- See paper: Pipelined Compressor Tree Optimization using Integer Linear Programming (FPL2014)

-- Recommendation : Use the input cy (in addition to di0(5) being non-zero) only if you are sure about how the CI inputs of CARRY4 are routed by Vivado
-- Vivado 2017.2 moves the LUT functionality to another LUT, the original one is used as passthrough (1 more LUT in resources and delay)

-- Recommendation : Use the input ci only for the purpose of chaining compressors

library ieee;
use ieee.std_logic_1164.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity bitheap_606_5 is
	port (
		cy  : in  std_logic;
		ci  : in  std_logic;
		di0 : in  std_logic_vector(5 downto 0);
		di2 : in  std_logic_vector(5 downto 0);
		do  : out std_logic_vector(3 downto 0);
		co  : out std_logic_vector(3 downto 0)
	);
end bitheap_606_5;

architecture synth of bitheap_606_5 is

	signal c4_o  : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_co : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_di : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_s  : std_logic_vector(3 downto 0) := (others => '0');

	constant INIT_LUT0 : bit_vector(63 downto 0) := x"6996966996696996";
	constant INIT_LUT1 : bit_vector(63 downto 0) := x"177E7EE8E8E8E8E8";

begin

	-- Input index 0

	lutA: LUT6
		generic map (
			INIT => INIT_LUT0
		)
		port map (
			O  => c4_s(0),
			I0 => di0(0),
			I1 => di0(1),
			I2 => di0(2),
			I3 => di0(3),
			I4 => di0(4),
			I5 => di0(5)
		);

	c4_di(0) <= di0(5);

	lutB: LUT6_2
		generic map (
			INIT => INIT_LUT1
		)
		port map (
			O6 => c4_s(1),
			O5 => c4_di(1),
			I0 => di0(0),
			I1 => di0(1),
			I2 => di0(2),
			I3 => di0(3),
			I4 => di0(4),
			I5 => '1'   -- To have something different for O6 and O5
		);

	-- Input index 2

	lutC: LUT6
		generic map (
			INIT => INIT_LUT0
		)
		port map (
			O  => c4_s(2),
			I0 => di2(0),
			I1 => di2(1),
			I2 => di2(2),
			I3 => di2(3),
			I4 => di2(4),
			I5 => di2(5)
		);

	c4_di(2) <= di2(5);

	lutD: LUT6_2
		generic map (
			INIT => INIT_LUT1
		)
		port map (
			O6 => c4_s(3),
			O5 => c4_di(3),
			I0 => di2(0),
			I1 => di2(1),
			I2 => di2(2),
			I3 => di2(3),
			I4 => di2(4),
			I5 => '1'  -- To have something different for O6 and O5
		);

	-- The CARRY4

	c4: CARRY4
		port map (
			CO     => c4_co,
			O      => c4_o,
			DI     => c4_di,
			S      => c4_s,
			CI     => ci,
			CYINIT => cy
		);

	-- Output ports

	do <= c4_o;
	co <= c4_co;

end architecture;

