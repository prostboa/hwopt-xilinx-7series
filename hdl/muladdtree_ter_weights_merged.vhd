
-- This is a multiplication-accumulation component
-- Inputs are handled with radix 2
-- Pipeline registers are created every REGEN stages and at last stage

-- This adder is designed for WDATA >= 4 and NBIN >= 4
-- Adder implementation is always with standard CARRY4 primitives

-- FIXME Starting from STAGE >= 2 better optimization should be possible :
-- Sum the add1_in bits separately with a popcount, and use an efficient adder tree for the main operands
-- Then as last stage, add these tho partial sums together

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity muladdtree_ter_weights_merged is
	generic(
		-- Data type and width
		WDATA : natural := 8;
		NBIN  : natural := 16;
		WOUT  : natural := 12;
		-- An optional tag
		TAGW  : natural := 1;
		TAGEN : boolean := true;
		TAGZC : boolean := false;
		-- How to add pipeline registers
		REGEN  : natural := 2;  -- 0=none, 1=all, else every EN stages
		REGIDX : natural := 0;  -- Start index (from the leaves)
		-- Stage number, for internal implem
		STAGE : natural := 0
	);
	port(
		clk      : in  std_logic;
		clear    : in  std_logic;
		-- Data, input and output
		data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
		data_out : out std_logic_vector(WOUT-1 downto 0);
		-- Multiplier and carry, input and output
		mult_in  : in  std_logic_vector(NBIN*2-1 downto 0);
		add1_in  : in  std_logic_vector(NBIN-1 downto 0);
		add1_out : out std_logic;
		-- Tag, input and output
		tag_in   : in  std_logic_vector(TAGW-1 downto 0);
		tag_out  : out std_logic_vector(TAGW-1 downto 0)
	);
end muladdtree_ter_weights_merged;

architecture synth of muladdtree_ter_weights_merged is

	component mul_ter_weight is
		generic(
			WDATA : natural := 12
		);
		port(
			weight_in : in  std_logic_vector(1 downto 0);
			data_in   : in  std_logic_vector(WDATA-1 downto 0);
			data_out  : out std_logic_vector(WDATA-1 downto 0)
		);
	end component;

	component muladdtree_ter_weights_merged_stage0 is
		generic(
			WDATA : natural := 8;
			WOUT  : natural := 9
		);
		port(
			mA  : in  std_logic_vector(1 downto 0);
			mB  : in  std_logic_vector(1 downto 0);
			inA : in  std_logic_vector(WDATA-1 downto 0);
			inB : in  std_logic_vector(WDATA-1 downto 0);
			res : out std_logic_vector(WOUT-1 downto 0);
			neg : out std_logic
		);
	end component;

begin

	-- One input means no component, no pipeline register
	-- FIXME Here we assume we are at least at STAGE = 2
	gen_nb1: if NBIN <= 1 generate

		data_out <= std_logic_vector(resize(signed(data_in), WOUT));
		add1_out <= add1_in(0);
		tag_out  <= tag_in when TAGEN = true else (others => '0');

	end generate;

	-- Handle when there are several registers
	gen_more: if NBIN > 1 generate

		-- Utility function to calculate minimum of two values
		function min(a, b : natural) return natural is
			variable m : natural := 0;
		begin
			m := a when a <= b else b;
			return m;
		end function;

		constant W : natural := min(WDATA+1, WOUT);

		-- Utility function to perform add
		function local_add(
			inA: std_logic_vector; inB: std_logic_vector; notA: std_logic; notB: std_logic; add1: std_logic
		) return std_logic_vector is
			variable vecnotA : std_logic_vector(inA'length-1 downto 0);
			variable vecnotB : std_logic_vector(inB'length-1 downto 0);
			variable inAnot  : std_logic_vector(inA'length-1 downto 0);
			variable inBnot  : std_logic_vector(inB'length-1 downto 0);
			variable res     : std_logic_vector(W downto 0);
		begin
			vecnotA := (others => notA);
			vecnotB := (others => notB);
			inAnot  := inA xor vecnotA;
			inBnot  := inB xor vecnotB;
			res := std_logic_vector(resize(signed(inAnot & '1'), W+1) + resize(signed(inBnot & add1), W+1));
			return res(W downto 1);
		end function;

		-- Utility function to calculate next REGIDX
		function calc_regidx_next(phony : natural) return natural is
		begin
			if REGEN = 0 then
				return 0;
			end if;
			return (REGIDX + 1) mod REGEN;
		end function;

		-- Add pipeline registers when IDX = EN - 1
		-- Force creation of registers at last stage
		constant REG_CREATE  : boolean := (REGEN > 0) and ((REGIDX = REGEN-1) or (NBIN = 2));
		constant REGIDX_NEXT : natural := calc_regidx_next(0);

		-- The number of registers needed in the local stage
		constant NBREGS : natural := (NBIN + 1) / 2;

		-- The registers of the current level
		signal reg_data, reg_data_n : std_logic_vector(NBREGS*W-1 downto 0);
		signal reg_add1, reg_add1_n : std_logic_vector(NBREGS-1 downto 0);
		signal reg_tag : std_logic_vector(TAGW-1 downto 0);

	begin

		-- Compute the ADD operations
		gen_2p : if NBIN >= 2 generate

			constant NBADD : natural := NBIN / 2;

		begin

			gen_loop : for i in 0 to NBADD-1 generate

				-- In first stage, use mult input and special component distriadd_ternmult_stage0
				gen_st0 : if STAGE = 0 generate
					signal multA, multB : std_logic_vector(1 downto 0) := (others => '0');
				begin

					multA <= mult_in((2*i+1)*2-1 downto (2*i+0)*2);
					multB <= mult_in((2*i+2)*2-1 downto (2*i+1)*2);

					st : muladdtree_ter_weights_merged_stage0
						generic map (
							WDATA => WDATA,
							WOUT  => W
						)
						port map (
							mA  => multA,
							mB  => multB,
							inA => data_in((2*i+1)*WDATA-1 downto (2*i+0)*WDATA),
							inB => data_in((2*i+2)*WDATA-1 downto (2*i+1)*WDATA),
							res => reg_data_n((i+1)*W-1 downto i*W),
							neg => reg_add1_n(i)
						);

				end generate;  -- STAGE = 0

				-- In other stages, use only add1 input
				gen_st1p : if STAGE >= 1 generate
					signal notA, notB, add1or, add1and : std_logic;
				begin

					-- At stage1, the add1 input means negation
					-- For all next stages, it just means add +1
					notA <= add1_in(2*i+0) when STAGE = 1 else '0';
					notB <= add1_in(2*i+1) when STAGE = 1 else '0';

					add1or  <= add1_in(2*i+1)  or add1_in(2*i);
					add1and <= add1_in(2*i+1) and add1_in(2*i);

					reg_data_n((i+1)*W-1 downto i*W) <= local_add(
						data_in((2*i+1)*WDATA-1 downto (2*i+0)*WDATA),
						data_in((2*i+2)*WDATA-1 downto (2*i+1)*WDATA),
						notA, notB, add1or
					);

					reg_add1_n(i) <= add1and;

				end generate;  -- STAGE >= 1

			end generate;  -- Loop that instantiates adders

		end generate;  -- At least 2 inputs, generate adders

		-- Handle when the number of inputs is not a multiple of 2
		gen_odd : if NBIN / 2 < NBREGS generate

			gen_st01 : if STAGE <= 1 generate
				signal mul_tmp : std_logic_vector(1 downto 0) := (others => '0');
				signal res_tmp : std_logic_vector(WDATA-1 downto 0) := (others => '0');
			begin

				mul_tmp <= mult_in(NBIN*2-1 downto (NBIN-1)*2) when STAGE = 0 else add1_in(NBIN-1) & "1";

				i_mul : mul_ter_weight
					generic map (
						WDATA => WDATA
					)
					port map (
						weight_in => mul_tmp,
						data_in   => data_in(NBIN*WDATA-1 downto (NBIN-1)*WDATA),
						data_out  => res_tmp
					);

				reg_data_n(NBREGS*W-1 downto (NBREGS-1)*W) <= std_logic_vector(resize(signed(res_tmp), W));
				reg_add1_n(NBREGS-1) <= '0';

			end generate;

			gen_st2p : if STAGE >= 2 generate

				-- Just propagate the add1 signal
				reg_data_n(NBREGS*W-1 downto (NBREGS-1)*W) <= std_logic_vector(resize(signed(data_in(NBIN*WDATA-1 downto (NBIN-1)*WDATA)), W));
				reg_add1_n(NBREGS-1) <= add1_in(NBIN-1);

			end generate;

		end generate;

		-- Create pipeline registers
		gen_regs : if REG_CREATE = true generate

			process(clk)
			begin
				if rising_edge(clk) then

					reg_data <= reg_data_n;
					reg_add1 <= reg_add1_n;

					if TAGEN = true then
						reg_tag <= tag_in;
						if (clear = '1') and (TAGZC = true) then
							reg_tag <= (others => '0');
						end if;
					end if;

				end if;
			end process;

		end generate;

		-- No pipeline registers
		gen_noregs : if REG_CREATE = false generate

			reg_data <= reg_data_n;
			reg_add1 <= reg_add1_n;
			reg_tag  <= tag_in when TAGEN = true else (others => '0');

		end generate;

		-- Instantiate the sub-stage
		i_stage : entity muladdtree_ter_weights_merged
			generic map (
				-- Data type and width
				WDATA => W,
				NBIN  => NBREGS,
				WOUT  => WOUT,
				-- An optional tag
				TAGW  => TAGW,
				TAGEN => TAGEN,
				TAGZC => TAGZC,
				-- How to add pipeline registers
				REGEN  => REGEN,
				REGIDX => REGIDX_NEXT,
				-- Stage number, for internal implem
				STAGE => STAGE + 1
			)
			port map (
				clk      => clk,
				clear    => clear,
				-- Data, input and output
				data_in  => reg_data,
				data_out => data_out,
				-- Multiplier and carry, input and output
				mult_in  => (others => '0'),
				add1_in  => reg_add1,
				add1_out => add1_out,
				-- Tag, input and output
				tag_in   => reg_tag,
				tag_out  => tag_out
			);

	end generate;

end architecture;

