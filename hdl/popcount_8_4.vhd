
-- This is a popcount 8b -> 4b
-- Total 4 LUTs, delay 3 LUTs

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT primitives
library unisim;
use unisim.vcomponents.all;

entity popcount_8_4 is
	generic (
		BITS : natural := 8;
		WOUT : natural := 4
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount_8_4;

architecture synth of popcount_8_4 is

	signal sigBits : std_logic_vector(7 downto 0) := (others => '0');
	signal sigSum  : std_logic_vector(3 downto 0) := (others => '0');

	signal sum5 : std_logic_vector(2 downto 0) := (others => '0');

begin

	assert BITS <= 8 report "Number of inputs too large for module popcount_8_4" severity failure;

	sigBits(BITS-1 downto 0) <= bits_in;

	lut501 : LUT6_2
		generic map (
			INIT => x"96696996177E7EE8"
		)
		port map (
			O6 => sum5(0),
			O5 => sum5(1),
			I0 => sigBits(0),
			I1 => sigBits(1),
			I2 => sigBits(2),
			I3 => sigBits(3),
			I4 => sigBits(4),
			I5 => '1'   -- To have something different for O6 and O5
		);

	lut52 : LUT5
		generic map (
			INIT => x"E8808000"
		)
		port map (
			O  => sum5(2),
			I0 => sigBits(0),
			I1 => sigBits(1),
			I2 => sigBits(2),
			I3 => sigBits(3),
			I4 => sigBits(4)
		);

	lutS01 : LUT6_2
		generic map (
			INIT => x"9336366C5AA5A55A"
		)
		port map (
			O6 => sigSum(1),
			O5 => sigSum(0),
			I0 => sum5(0),
			I1 => sum5(1),
			I2 => sigBits(5),
			I3 => sigBits(6),
			I4 => sigBits(7),
			I5 => '1'   -- To have something different for O6 and O5
		);

	lutS23 : LUT6_2
		generic map (
			INIT => x"0000002030383C1E"
		)
		port map (
			O6 => sigSum(3),
			O5 => sigSum(2),
			I0 => sum5(0),
			I1 => sum5(1),
			I2 => sum5(2),
			I3 => sigSum(0),
			I4 => sigSum(1),
			I5 => '1'   -- To have something different for O6 and O5
		);

	sum <= std_logic_vector(resize(unsigned(sigSum), WOUT));

end architecture;

