
-- This is a popcount 5b -> 3b
-- Total 2 LUTs, delay 1 LUT

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT primitives
library unisim;
use unisim.vcomponents.all;

entity popcount_5_3 is
	generic (
		BITS : natural := 5;
		WOUT : natural := 3
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount_5_3;

architecture synth of popcount_5_3 is

	signal sigBits : std_logic_vector(4 downto 0) := (others => '0');
	signal sigSum  : std_logic_vector(2 downto 0) := (others => '0');

begin

	assert BITS <= 5 report "Number of inputs too large for module popcount_5_3" severity failure;

	sigBits(BITS-1 downto 0) <= bits_in;

	lut01 : LUT6_2
		generic map (
			INIT => x"96696996177E7EE8"
		)
		port map (
			O6 => sigSum(0),
			O5 => sigSum(1),
			I0 => sigBits(0),
			I1 => sigBits(1),
			I2 => sigBits(2),
			I3 => sigBits(3),
			I4 => sigBits(4),
			I5 => '1'   -- To have something different for O6 and O5
		);

	lut2 : LUT5
		generic map (
			INIT => x"E8808000"
		)
		port map (
			O  => sigSum(2),
			I0 => sigBits(0),
			I1 => sigBits(1),
			I2 => sigBits(2),
			I3 => sigBits(3),
			I4 => sigBits(4)
		);

	sum <= std_logic_vector(resize(unsigned(sigSum), WOUT));

end architecture;

