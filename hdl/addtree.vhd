
-- This is multi-stage ADD operator designed to perform good at high frequency
-- Pipeline registers are created every REGEN stages and at last stage
-- The inputs are handled with radix 2 to 7
--
-- Implementation depending on input data width and number of inputs
--
-- +---------++----+----+----+----+----+----+
-- |  Data   ||      Number of inputs       |
-- |  Width  ||  2 |  3 |  4 |  5 |  6 | 7+ |
-- +=========++====+====+====+====+====+====+
-- |       1 ||  2 |  3 |  4 |  5 |  6 |  7 | <- popcount uses even larger radix
-- +---------++----+----+----+----+----+----+
-- |       2 ||  2 |  3 |  4 |  5 |  6 |  7 | <- ternary : specific adder
-- +---------++----+----+----+----+----+----+
-- |       3 ||  2 |  3 |  4 |  5 |  6 |  6 |
-- +---------++----+----+----+----+----+----+
-- |       4 ||  2 |  3 |  4 |  5 |  6 |  6 |
-- +---------++----+----+----+----+----+----+
-- |       5 ||  2 |  3 |  4 |  5 |  6 |  6 |
-- +---------++----+----+----+----+----+----+
-- |       6 ||  2 |  3 |  4 |  5 |  6 |  6 |
-- +---------++----+----+----+----+----+----+
-- |      7+ ||  2 |  3 |  4 |  5 |  6 |  6 |
-- +---------++----+----+----+----+----+----+
--
-- For large adder trees, the best results are reached with the following radix values, in decreasing order
-- Theoretical results for 128-input, 32b adder tree
--   radix 6 : 2116 LUTs (smallest solution)
--   radix 3 : 2171 LUTs
--   radix 4 : 2174 LUTs
--   radix 5 : 2836 LUTs
--   radix 2 : 4184 LUTs (largest solution)
-- The best solutions are generally obtained with hybrid implementations
-- Important : The result above does not take into account optimizations that the synthesis tool may perform due to some adder output bits stuck at zero
--   radix 6 : 2 bits stuck at zero, one LSB, one MSB
--   radix 4 : 1 LSB bit stuck at zero
--   radix 5 : 4 bits stuck at zero, 2 LSB, 2 MSB
--
-- Note: the 3-input and 4-input adder implementations have roughly the same size
-- The 3-input adder has advantages for routability compared to 4-input :
--   - It generates 1 output, so compression factor is high
--   - It has 12 inputs / 4 outputs per slice, which is lower than for 4-input (16b in, 8b out)
--   - Sign extension costs just 1 more LUT, and does not increase delay
-- The 4-input adder wins over the 3-input adder on the following point :
--   - It only increases output width by 1, so next stages of addtree have shorter inputs
--
-- Note : For specifically 5 inputs, the 5-input adder and the 6-input adder have roughly the same size, so the following considerations may be of interest :
--   Under resource constraints, the 5-input implementation may be better because it leaves more opportunities for logic optimization due to many bits of results stuck at zero
--   Under routing congestion, for large inputs, the 5-input implementation may be better because it does not have chained CARRY primitives, which may make it easier to place and route
--   Under timing pressure, for short inputs, the 6-input implementation may be better because it has lower delay
--   Under timing pressure, for large inputs, the 5-input implementation may be better because it does not have chained CARRY primitives
--
-- Note : For short inputs, the interest of 6-input adder over 3-input and 4-input adders is small
--   The difference is significant only for large inputs
--
-- FIXME Need to setup more pre-computed configurations, especially taking into account results of logic optimization

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity addtree is
	generic (
		-- Data type and width
		WDATA : natural := 8;
		SDATA : boolean := true;
		NBIN  : natural := 20;
		WOUT  : natural := 12;
		-- User-specified radix, for testing purposes (0 means automatic)
		RADIX : natural := 0;
		-- Special considerations about data nature
		BINARY : boolean := false;
		BINARY_RADIX : natural := 0;
		TERNARY : boolean := false;
		TERNARY_RADIX : natural := 0;
		-- An optional tag
		TAGW  : natural := 1;
		TAGEN : boolean := true;
		TAGZC : boolean := true;
		-- How to add pipeline registers
		REGEN  : natural := 1;  -- 0=none, 1=all, else every EN stages
		REGIDX : natural := 0   -- Start index (from the leaves)
	);
	port (
		clk      : in  std_logic;
		clear    : in  std_logic;
		-- Data, input and output
		data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
		data_out : out std_logic_vector(WOUT-1 downto 0);
		-- Tag, input and output
		tag_in   : in  std_logic_vector(TAGW-1 downto 0);
		tag_out  : out std_logic_vector(TAGW-1 downto 0)
	);
end addtree;

architecture synth of addtree is

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	-- Utility function to calculate minimum of two values
	function min(a, b : natural) return natural is
		variable m : natural := 0;
	begin
		m := a when a <= b else b;
		return m;
	end function;

	-- Utility function to generate integer from boolean
	function to_integer(b : boolean) return integer is
		variable i : integer := 0;
	begin
		i := 1 when b = true else 0;
		return i;
	end function;

	-- Utility function to generate std_logic from boolean
	function to_std_logic(b : boolean) return std_logic is
		variable i : std_logic := '0';
	begin
		i := '1' when b = true else '0';
		return i;
	end function;

	-- Return the different parameters of the ader tree
	function calc_in_per_block(result_id : natural) return natural is
		variable in_per_block  : natural := 0;
		variable out_per_block : natural := 0;
		variable num_blocks    : natural := 0;
		variable num_full      : natural := 0;
		variable rem_size      : natural := 0;
		variable num_out       : natural := 0;
		variable block_wout    : natural := 0;
		variable radix_next    : natural := 0;
	begin

		assert (RADIX = 0) or (RADIX = 2) or (RADIX = 3) or (RADIX = 4) or (RADIX = 5) or (RADIX = 6) report "ERROR : Unsupported radix value" & integer'image(RADIX) severity failure;

		radix_next := RADIX;

		-- Decide the size of groups
		if NBIN <= 1 then
			in_per_block := 1;
		elsif BINARY = true then
			-- Inputs will be added by groups of at most 15
			if BINARY_RADIX > 0 then
				assert (BINARY_RADIX > 1) and (BINARY_RADIX <= 16) report "ERROR : Unsupported binary radix value" & integer'image(BINARY_RADIX) severity failure;
				in_per_block := BINARY_RADIX;
			elsif NBIN <= 18 then
				-- There are optimized implementations for up to 18b
				in_per_block := NBIN;
			else
				-- For large number of inputs, best is to group by 15b
				in_per_block := 15;
			end if;
		elsif TERNARY = true then
			-- Inputs will be added by groups of at most 6
			-- Reason : a 6T adder costs 6 luts with delay 2 LUTs + carry
			-- Whereas a 7T adder costs 8 LUTs with 4-lut delay, or 6 luts but with issues on routing CI input of CARRY4
			if TERNARY_RADIX > 0 then
				assert (TERNARY_RADIX > 1) and (TERNARY_RADIX <= 7) report "ERROR : Unsupported ternary radix value" & integer'image(TERNARY_RADIX) severity failure;
				in_per_block := TERNARY_RADIX;
				-- Note : no problem if we have NBIN < in_per_block
				-- Instantiation of adders handles size per-block
			else
				-- For large inputs, best is to group by 7
				in_per_block := min(7, NBIN);
			end if;
		elsif RADIX > 0 then
			in_per_block := RADIX;
			in_per_block := min(in_per_block, NBIN);
		elsif WDATA <= 2 then
			-- For large inputs, best is to group by 7
			in_per_block := 7;
			if NBIN < 7 then
				in_per_block := NBIN;
			end if;
		-- Large inputs
		-- Specific cases for low number of inputs
		elsif NBIN <= 2 then
			in_per_block := 2;
		elsif NBIN <= 3 then
			in_per_block := 3;
		elsif NBIN <= 4 then
			in_per_block := 4;
		elsif NBIN <= 5 then
			in_per_block := 5;
		-- Large number of inputs, large data
		else
			-- For large number of bits, default is always radix6 according to exhaustive search with software tool
			-- FIXME Should add a table of radix for non-full blocks, and a rule for beyond
			in_per_block := 6;
		end if;

		num_blocks := (NBIN + in_per_block - 1) / in_per_block;

		num_full := NBIN / in_per_block;
		rem_size := NBIN mod in_per_block;

		-- Generate all remaining parameters
		if in_per_block <= 1 then
			out_per_block := 1;
			num_out       := 1;
			block_wout    := min(WDATA, WOUT);
		elsif BINARY = true then
			out_per_block := 1;
			num_out       := num_blocks;
			block_wout    := storebitsnb(in_per_block);
			block_wout    := min(block_wout, WOUT);
		elsif TERNARY = true then
			out_per_block := 1;
			num_out       := num_blocks;
			block_wout    := 2 when in_per_block <= 1 else 3 when in_per_block <= 3 else 4;
			block_wout    := min(block_wout, WOUT);
		elsif WDATA <= 2 then
			out_per_block := 1;
			-- Each full block will generate 1 vector of outputs
			-- The last block is propagated as-is in order to form a larger group in next stages
			num_out       := out_per_block * num_full + to_integer(rem_size > 0);
			-- Generate the output size
			block_wout    :=
				3 when in_per_block <= 2 else
				4 when in_per_block <= 4 or (in_per_block = 5 and SDATA = false) else
				5;  -- Up to 7 inputs
			block_wout    := min(block_wout, WOUT);
		elsif in_per_block = 2 then
			out_per_block := 1;
			num_out       := out_per_block * num_blocks;
			block_wout    := min(WDATA+1, WOUT);
		elsif in_per_block = 3 then
			out_per_block := 1;
			-- Each full block will generate 1 vector of outputs
			-- The last block is propagated as-is in order to form a larger group in next stages
			num_out       := out_per_block * num_full + rem_size;
			-- +1 bit from input data size, if unsigned
			-- +2 bits from input data size, if signed
			block_wout    := min(WDATA + 2, WOUT);
		elsif in_per_block = 4 then
			out_per_block := 2;
			-- Each full block will generate 2 vectors of outputs
			-- Blocks with <= 2 inputs will be propagated as-is in order to form larger groups in next stages
			-- Blocks with 3 inputs will generate 1 output (processed with 3-input adder)
			num_out       := out_per_block * num_full + 1 * to_integer(rem_size = 1 or rem_size = 3) + 2 * to_integer(rem_size = 2 or rem_size = 4);
			-- +1 bit from input data size, if unsigned
			-- +3 bits from input data size, if signed
			block_wout    := min(WDATA + 1 + 2*to_integer(SDATA), WOUT);
		elsif in_per_block = 5 then
			out_per_block := 2;
			-- Each full block will generate 2 vectors of outputs
			-- Blocks with <= 2 inputs will be propagated as-is in order to form larger groups in next stages
			-- Blocks with 3 inputs will generate 1 output (processed with 3-input adder)
			num_out       := out_per_block * num_full + 1 * to_integer(rem_size = 1 or rem_size = 3) + 2 * to_integer(rem_size = 2 or rem_size >= 4);
			-- +2 bits from input data size, if unsigned
			-- +3 bits from input data size, if signed
			block_wout    := min(WDATA + 2 + to_integer(SDATA), WOUT);
		elsif in_per_block = 6 then
			out_per_block := 2;
			-- Each full block will generate 2 vectors of outputs
			-- Blocks with <= 2 inputs will be propagated as-is in order to form larger groups in next stages
			-- Blocks with 3 inputs will generate 1 output (processed with 3-input adder)
			num_out       := out_per_block * num_full + 1 * to_integer(rem_size = 1 or rem_size = 3) + 2 * to_integer(rem_size = 2 or rem_size >= 4);
			-- +2 bits from input data size, if unsigned
			-- +3 bits from input data size, if signed
			block_wout    := min(WDATA + 2 + to_integer(SDATA), WOUT);
		end if;

		if result_id = 0 then
			return in_per_block;
		elsif result_id = 1 then
			return out_per_block;
		elsif result_id = 2 then
			return num_blocks;
		elsif result_id = 3 then
			return num_out;
		elsif result_id = 4 then
			return block_wout;
		elsif result_id = 5 then
			return radix_next;
		end if;

		-- Default : error status
		return 0;
	end function;

	-- Initialize general input grouping
	constant IN_PER_BLOCK  : natural := calc_in_per_block(0);
	constant OUT_PER_BLOCK : natural := calc_in_per_block(1);
	constant NUMBLOCKS     : natural := calc_in_per_block(2);
	constant NUMOUTS       : natural := calc_in_per_block(3);
	constant STAGE_WOUT    : natural := calc_in_per_block(4);
	constant RADIX_NEXT    : natural := calc_in_per_block(5);

	-- Constant input bit for sign input
	constant SIGN_IN_BIT : std_logic := to_std_logic(SDATA);

	-- Utility function to calculate next REGIDX
	function calc_regidx_next(REG_CREATE : boolean) return natural is
	begin
		if REGEN = 0 then
			return 0;
		end if;
		if REG_CREATE = true then
			return 0;
		end if;
		return (REGIDX + 1) mod REGEN;
	end function;

	-- Add pipeline registers when IDX = EN - 1
	-- Force creation of registers at last stage
	constant REG_CREATE  : boolean := (NBIN > 1) and (REGEN > 0) and ((REGIDX = REGEN-1) or (NUMBLOCKS = 1));
	constant REGIDX_NEXT : natural := calc_regidx_next(REG_CREATE);

	-- The registers of the current level
	signal regs, regs_n : std_logic_vector(NUMOUTS*STAGE_WOUT-1 downto 0);
	signal reg_tag : std_logic_vector(TAGW-1 downto 0);

	-- Utility function to perform resize
	function local_resize(A : std_logic_vector) return std_logic_vector is
	begin
		if SDATA = true then
			return std_logic_vector(resize(signed(A), STAGE_WOUT));
		end if;
		return std_logic_vector(resize(unsigned(A), STAGE_WOUT));
	end function;

	-- Utility function to perform add
	function local_add(inA : std_logic_vector; inB : std_logic_vector) return std_logic_vector is
	begin
		if SDATA = true then
			return std_logic_vector(resize(signed(inA), STAGE_WOUT) + resize(signed(inB), STAGE_WOUT));
		end if;
		return std_logic_vector(resize(unsigned(inA), STAGE_WOUT) + resize(unsigned(inB), STAGE_WOUT));
	end function;

	-- Optimized 2-input adder for 3b inputs
	component adder_2in_3b is
		generic (
			IS_SIGNED : boolean := false;
			WOUT      : natural := 4
		);
		port (
			inA : in  std_logic_vector(2 downto 0);
			inB : in  std_logic_vector(2 downto 0);
			in1 : in  std_logic;
			res : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	-- Optimized 2-input adder for 4b inputs
	component adder_2in_4b is
		generic (
			IS_SIGNED : boolean := false;
			WOUT      : natural := 5
		);
		port (
			inA : in  std_logic_vector(3 downto 0);
			inB : in  std_logic_vector(3 downto 0);
			res : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	-- Optimized 2-input adder for 5b inputs
	component adder_2in_5b is
		generic (
			IS_SIGNED : boolean := false;
			WOUT      : natural := 6
		);
		port (
			inA : in  std_logic_vector(4 downto 0);
			inB : in  std_logic_vector(4 downto 0);
			res : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	-- Optimized component to perform one 3-input addition
	component adder_3in is
		generic (
			WDATA : natural := 8;
			WOUT  : natural := 10;
			NBIN  : natural := 3;
			SIGN  : boolean := false
		);
		port (
			data_in : in  std_logic_vector(NBIN*WDATA-1 downto 0);
			data_1  : in  std_logic;
			data_2  : in  std_logic;
			res     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	-- Optimized component to perform one compression stage of 4 inputs
	component bitheap_4_2 is
		generic (
			WDATA : natural := 8;
			WOUT  : natural := 11;
			NBIN  : natural := 4;
			SIGN  : boolean := false
		);
		port (
			data_in : in  std_logic_vector(NBIN*WDATA-1 downto 0);
			data_1  : in  std_logic;
			data_2  : in  std_logic;
			sign_in : in  std_logic;
			res_sum : out std_logic_vector(WOUT-1 downto 0);
			res_c   : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

begin

	-- Instanciate optimized adders

	gen_blocks : for i in 0 to NUMBLOCKS-1 generate
		constant BLOCKSIZE : natural := min(IN_PER_BLOCK, NBIN - i*IN_PER_BLOCK);
	begin

		-- One input means no component, so no pipeline registers

		gen : if NBIN <= 1 generate

			regs_n((i*OUT_PER_BLOCK+1)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+0)*STAGE_WOUT) <= local_resize(data_in((i*IN_PER_BLOCK+1)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA));

		-- Handle binary data case : Instantiate popcounts
		elsif BINARY = true generate

			component popcount_small is
				generic (
					BITS : natural := 18;
					WOUT : natural := 5
				);
				port (
					bits_in : in  std_logic_vector(BITS-1 downto 0);
					sum     : out std_logic_vector(WOUT-1 downto 0)
				);
			end component;

		begin

			-- WARNING Vivado 2017.2 at least does not support asserts before the begin keyword
			assert (WDATA = 1) and (SDATA = false) severity failure;

			popc : popcount_small
				generic map (
					BITS  => BLOCKSIZE,
					WOUT  => STAGE_WOUT
				)
				port map (
					bits_in => data_in((i*IN_PER_BLOCK+BLOCKSIZE)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA),
					sum     => regs_n((i+1)*STAGE_WOUT-1 downto (i+0)*STAGE_WOUT)
				);

		-- Handle ternary data case
		elsif TERNARY = true generate

			-- Optimized component to add ternary numbers
			component adder_ter is
				generic (
					NBIN  : natural := 4;
					WOUT  : natural := 4
				);
				port (
					data_in  : in  std_logic_vector(2*NBIN-1 downto 0);
					data_out : out std_logic_vector(WOUT-1 downto 0)
				);
			end component;

		begin

			-- WARNING Vivado 2017.2 at least does not support asserts before the begin keyword
			assert (WDATA = 2) and (SDATA = true) severity failure;

			add_ter : adder_ter
				generic map (
					NBIN  => BLOCKSIZE,
					WOUT  => STAGE_WOUT
				)
				port map (
					data_in  => data_in((i*IN_PER_BLOCK+BLOCKSIZE)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA),
					data_out => regs_n((i+1)*STAGE_WOUT-1 downto (i+0)*STAGE_WOUT)
				);

		-- Use optimized 2b adders
		elsif WDATA = 2 generate

			component adder_2b is
				generic (
					NBIN : natural := 7;
					SIGN : boolean := true;
					WOUT : natural := 5
				);
				port (
					data_in : in  std_logic_vector(NBIN*2-1 downto 0);
					res_sum : out std_logic_vector(WOUT-1 downto 0)
				);
			end component;

		begin

			add2b : adder_2b
				generic map (
					NBIN => BLOCKSIZE,
					SIGN => SDATA,
					WOUT => STAGE_WOUT
				)
				port map (
					data_in => data_in((i*IN_PER_BLOCK+BLOCKSIZE)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA),
					res_sum => regs_n((i+1)*STAGE_WOUT-1 downto i*STAGE_WOUT)
				);

		-- Use usual 2-input adder
		elsif IN_PER_BLOCK = 2 generate

			gen_size : if BLOCKSIZE <= 1 generate

				regs_n((i+1)*STAGE_WOUT-1 downto i*STAGE_WOUT) <= local_resize( data_in((i*IN_PER_BLOCK+BLOCKSIZE)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA) );

			elsif WDATA = 3 generate

				add3b : adder_2in_3b
					generic map (
						IS_SIGNED => SDATA,
						WOUT      => STAGE_WOUT
					)
					port map (
						inA => data_in((i*IN_PER_BLOCK+1)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA),
						inB => data_in((i*IN_PER_BLOCK+2)*WDATA-1 downto (i*IN_PER_BLOCK+1)*WDATA),
						in1 => '0',
						res => regs_n((i+1)*STAGE_WOUT-1 downto (i+0)*STAGE_WOUT)
					);

			elsif WDATA = 4 generate

				add4b : adder_2in_4b
					generic map (
						IS_SIGNED => SDATA,
						WOUT      => STAGE_WOUT
					)
					port map (
						inA => data_in((i*IN_PER_BLOCK+1)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA),
						inB => data_in((i*IN_PER_BLOCK+2)*WDATA-1 downto (i*IN_PER_BLOCK+1)*WDATA),
						res => regs_n((i+1)*STAGE_WOUT-1 downto (i+0)*STAGE_WOUT)
					);

			elsif WDATA = 5 generate

				add5b : adder_2in_5b
					generic map (
						IS_SIGNED => SDATA,
						WOUT      => STAGE_WOUT
					)
					port map (
						inA => data_in((i*IN_PER_BLOCK+1)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA),
						inB => data_in((i*IN_PER_BLOCK+2)*WDATA-1 downto (i*IN_PER_BLOCK+1)*WDATA),
						res => regs_n((i+1)*STAGE_WOUT-1 downto (i+0)*STAGE_WOUT)
					);

			else generate

				-- Default implementation: behavioural VHDL
				regs_n((i+1)*STAGE_WOUT-1 downto (i+0)*STAGE_WOUT) <= local_add(
					data_in((i*IN_PER_BLOCK+1)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA),
					data_in((i*IN_PER_BLOCK+2)*WDATA-1 downto (i*IN_PER_BLOCK+1)*WDATA)
				);

			end generate;

		-- Use 3-input adder
		elsif IN_PER_BLOCK = 3 generate

			gen_size : if BLOCKSIZE <= 1 generate

				regs_n((i*OUT_PER_BLOCK+1)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+0)*STAGE_WOUT) <= local_resize(data_in((i*IN_PER_BLOCK+1)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA));

			elsif BLOCKSIZE = 2 generate

				regs_n((i*OUT_PER_BLOCK+1)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+0)*STAGE_WOUT) <= local_resize(data_in((i*IN_PER_BLOCK+1)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA));
				regs_n((i*OUT_PER_BLOCK+2)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+1)*STAGE_WOUT) <= local_resize(data_in((i*IN_PER_BLOCK+2)*WDATA-1 downto (i*IN_PER_BLOCK+1)*WDATA));

			else generate

				add3in : adder_3in
					generic map (
						WDATA => WDATA,
						WOUT  => STAGE_WOUT,
						NBIN  => BLOCKSIZE,
						SIGN  => SDATA
					)
					port map (
						data_in => data_in((i*IN_PER_BLOCK+BLOCKSIZE)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA),
						data_1  => '0',
						data_2  => '0',
						res     => regs_n((i*OUT_PER_BLOCK+1)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+0)*STAGE_WOUT)
					);

			end generate;

		-- Use 4:2 compressor
		elsif IN_PER_BLOCK = 4 generate

			gen_size : if BLOCKSIZE <= 1 generate

				regs_n((i*OUT_PER_BLOCK+1)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+0)*STAGE_WOUT) <= local_resize(data_in((i*IN_PER_BLOCK+1)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA));

			elsif BLOCKSIZE = 2 generate

				regs_n((i*OUT_PER_BLOCK+1)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+0)*STAGE_WOUT) <= local_resize(data_in((i*IN_PER_BLOCK+1)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA));
				regs_n((i*OUT_PER_BLOCK+2)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+1)*STAGE_WOUT) <= local_resize(data_in((i*IN_PER_BLOCK+2)*WDATA-1 downto (i*IN_PER_BLOCK+1)*WDATA));

			elsif BLOCKSIZE = 3 generate

				adder3 : adder_3in
					generic map (
						WDATA => WDATA,
						WOUT  => STAGE_WOUT,
						NBIN  => BLOCKSIZE,
						SIGN  => SDATA
					)
					port map (
						data_in => data_in((i*IN_PER_BLOCK+BLOCKSIZE)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA),
						data_1  => '0',
						data_2  => '0',
						res     => regs_n((i*OUT_PER_BLOCK+1)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+0)*STAGE_WOUT)
					);

			else generate

				add42: bitheap_4_2
					generic map (
						WDATA => WDATA,
						WOUT  => STAGE_WOUT,
						NBIN  => BLOCKSIZE,
						SIGN  => SDATA
					)
					port map (
						data_in => data_in((i*IN_PER_BLOCK+BLOCKSIZE)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA),
						data_1  => '0',
						data_2  => '0',
						sign_in => SIGN_IN_BIT,
						res_sum => regs_n((i*OUT_PER_BLOCK+1)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+0)*STAGE_WOUT),
						res_c   => regs_n((i*OUT_PER_BLOCK+2)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+1)*STAGE_WOUT)
					);

			end generate;

		-- Use 55:4 compressors
		elsif IN_PER_BLOCK = 5 generate

			-- Optimized component to perform one compression stage
			component bitheap_55_4_pair is
				generic (
					WDATA : natural := 8;
					WOUT  : natural := 12;
					NBIN  : natural := 5;
					SIGN  : boolean := false
				);
				port (
					data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
					data_1   : in  std_logic;
					res_idx0 : out std_logic_vector(WOUT-1 downto 0);
					res_idx2 : out std_logic_vector(WOUT-1 downto 0)
				);
			end component;

		begin

			gen_size : if BLOCKSIZE <= 1 generate

				regs_n((i*OUT_PER_BLOCK+1)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+0)*STAGE_WOUT) <= local_resize(data_in((i*IN_PER_BLOCK+1)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA));

			elsif BLOCKSIZE = 2 generate

				regs_n((i*OUT_PER_BLOCK+1)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+0)*STAGE_WOUT) <= local_resize(data_in((i*IN_PER_BLOCK+1)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA));
				regs_n((i*OUT_PER_BLOCK+2)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+1)*STAGE_WOUT) <= local_resize(data_in((i*IN_PER_BLOCK+2)*WDATA-1 downto (i*IN_PER_BLOCK+1)*WDATA));

			elsif BLOCKSIZE = 3 generate

				add3in : adder_3in
					generic map (
						WDATA => WDATA,
						WOUT  => STAGE_WOUT,
						NBIN  => BLOCKSIZE,
						SIGN  => SDATA
					)
					port map (
						data_in => data_in((i*IN_PER_BLOCK+BLOCKSIZE)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA),
						data_1  => '0',
						data_2  => '0',
						res     => regs_n((i*OUT_PER_BLOCK+1)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+0)*STAGE_WOUT)
					);

			elsif BLOCKSIZE = 4 generate

				add42: bitheap_4_2
					generic map (
						WDATA => WDATA,
						WOUT  => STAGE_WOUT,
						NBIN  => BLOCKSIZE,
						SIGN  => SDATA
					)
					port map (
						data_in => data_in((i*IN_PER_BLOCK+BLOCKSIZE)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA),
						data_1  => '0',
						data_2  => '0',
						sign_in => SIGN_IN_BIT,
						res_sum => regs_n((i*OUT_PER_BLOCK+1)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+0)*STAGE_WOUT),
						res_c   => regs_n((i*OUT_PER_BLOCK+2)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+1)*STAGE_WOUT)
					);

			else generate

				add554 : bitheap_55_4_pair
					generic map (
						WDATA => WDATA,
						WOUT  => STAGE_WOUT,
						NBIN  => BLOCKSIZE,
						SIGN  => SDATA
					)
					port map (
						data_in  => data_in((i*IN_PER_BLOCK+BLOCKSIZE)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA),
						data_1   => '0',
						res_idx0 => regs_n((i*OUT_PER_BLOCK+1)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+0)*STAGE_WOUT),
						res_idx2 => regs_n((i*OUT_PER_BLOCK+2)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+1)*STAGE_WOUT)
					);

			end generate;

		-- Use 607:5 compressors
		elsif IN_PER_BLOCK = 6 generate

			-- Optimized component to perform one compression stage
			component bitheap_607_5_pair is
				generic (
					WDATA : natural := 8;
					WOUT  : natural := 12;
					NBIN  : natural := 6;
					SIGN  : boolean := false
				);
				port (
					data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
					data_1   : in  std_logic;
					data_2   : in  std_logic;
					sign_in  : in  std_logic;
					res_idx0 : out std_logic_vector(WOUT-1 downto 0);
					res_idx1 : out std_logic_vector(WOUT-1 downto 0)
				);
			end component;

		begin

			gen_size : if BLOCKSIZE <= 1 generate

				regs_n((i*OUT_PER_BLOCK+1)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+0)*STAGE_WOUT) <= local_resize(data_in((i*IN_PER_BLOCK+1)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA));

			elsif BLOCKSIZE = 2 generate

				regs_n((i*OUT_PER_BLOCK+1)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+0)*STAGE_WOUT) <= local_resize(data_in((i*IN_PER_BLOCK+1)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA));
				regs_n((i*OUT_PER_BLOCK+2)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+1)*STAGE_WOUT) <= local_resize(data_in((i*IN_PER_BLOCK+2)*WDATA-1 downto (i*IN_PER_BLOCK+1)*WDATA));

			elsif BLOCKSIZE = 3 generate

				add3in : adder_3in
					generic map (
						WDATA => WDATA,
						WOUT  => STAGE_WOUT,
						NBIN  => BLOCKSIZE,
						SIGN  => SDATA
					)
					port map (
						data_in => data_in((i*IN_PER_BLOCK+BLOCKSIZE)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA),
						data_1  => '0',
						data_2  => '0',
						res     => regs_n((i*OUT_PER_BLOCK+1)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+0)*STAGE_WOUT)
					);

			elsif BLOCKSIZE = 4 generate

				add42: bitheap_4_2
					generic map (
						WDATA => WDATA,
						WOUT  => STAGE_WOUT,
						NBIN  => BLOCKSIZE,
						SIGN  => SDATA
					)
					port map (
						data_in => data_in((i*IN_PER_BLOCK+BLOCKSIZE)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA),
						data_1  => '0',
						data_2  => '0',
						sign_in => SIGN_IN_BIT,
						res_sum => regs_n((i*OUT_PER_BLOCK+1)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+0)*STAGE_WOUT),
						res_c   => regs_n((i*OUT_PER_BLOCK+2)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+1)*STAGE_WOUT)
					);

			else generate

				add6075 : bitheap_607_5_pair
					generic map (
						WDATA => WDATA,
						WOUT  => STAGE_WOUT,
						NBIN  => BLOCKSIZE,
						SIGN  => SDATA
					)
					port map (
						data_in  => data_in((i*IN_PER_BLOCK+BLOCKSIZE)*WDATA-1 downto (i*IN_PER_BLOCK+0)*WDATA),
						data_1   => '0',
						data_2   => '0',
						sign_in  => SIGN_IN_BIT,
						res_idx0 => regs_n((i*OUT_PER_BLOCK+1)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+0)*STAGE_WOUT),
						res_idx1 => regs_n((i*OUT_PER_BLOCK+2)*STAGE_WOUT-1 downto (i*OUT_PER_BLOCK+1)*STAGE_WOUT)
					);

			end generate;

		-- Unsupported radix value, just paranoia
		else generate

			assert false report "Unsupported radix value" severity failure;

		end generate;  -- Selection of radix value

	end generate;  -- Loop on blocks

	-- Create pipeline registers

	gen_regs_data : if (NBIN = 1) or (REG_CREATE = false) generate

		-- No pipeline registers, just passthrough
		regs <= regs_n;

	else generate

		process(clk)
		begin
			if rising_edge(clk) then

				regs <= regs_n;

			end if;
		end process;

	end generate;

	gen_regs_tag : if (NBIN = 1) or (REG_CREATE = false) or (TAGEN = false) generate

		-- No pipeline registers, just passthrough
		reg_tag <= tag_in when TAGEN = true else (others => '0');

	else generate

		process(clk)
		begin
			if rising_edge(clk) then

				reg_tag <= tag_in;
				if (clear = '1') and (TAGZC = true) then
					reg_tag <= (others => '0');
				end if;

			end if;
		end process;

	end generate;

	-- Instantiate the upper stages of the adder tree

	gen_notree : if NUMOUTS <= 1 generate

		data_out <=
			std_logic_vector(resize(signed(regs), WOUT)) when SDATA = true else
			std_logic_vector(resize(unsigned(regs), WOUT));

		tag_out <= reg_tag;

	end generate;

	-- WARNING Many versions of Vivado (2017.2 up to 2021.1 at least) require special recursive instantiation pattern
	-- Vivado segfaults when the instantiation is in an "else generate" statement
	-- So, isolate that in the most direct "if generate" part of the statement
	gen_tree : if NUMOUTS > 1 generate

		adder : entity addtree
			generic map (
				-- Data type and width
				WDATA => STAGE_WOUT,
				SDATA => SDATA,
				NBIN  => NUMOUTS,
				WOUT  => WOUT,
				-- User-specified radix, for testing purposes (0 means automatic)
				RADIX => RADIX_NEXT,
				-- Special considerations about data nature
				BINARY => false,
				BINARY_RADIX => 0,
				TERNARY => false,
				TERNARY_RADIX => 0,
				-- An optional tag
				TAGW  => TAGW,
				TAGEN => TAGEN,
				TAGZC => TAGZC,
				-- How to add pipeline registers
				REGEN  => REGEN,
				REGIDX => REGIDX_NEXT
			)
			port map (
				clk      => clk,
				clear    => clear,
				-- Data, input and output
				data_in  => regs,
				data_out => data_out,
				-- Tag, input and output
				tag_in   => reg_tag,
				tag_out  => tag_out
			);

	end generate;

end architecture;

