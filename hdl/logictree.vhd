
-- This is a multi-stage OR operator designed for performance
-- If RADIX = 0, generate a combinatorial design
-- If RADIX = 1, generate a register at output only
-- If RADIX > 1, generate a pipelined design

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity logictree is
	generic(
		WDATA : natural := 8;
		NBIN  : natural := 20;
		-- The operation to perform : AND, OR, XOR optionally followed by NOT
		OPAND : boolean := false;
		OPOR  : boolean := false;
		OPXOR : boolean := false;
		OPNOT : boolean := false;
		-- Optional tag propagation
		TAGW  : natural := 1;
		TAGEN : boolean := true;
		TAGZC : boolean := true;
		RADIX : natural := 36
	);
	port(
		clk      : in  std_logic;
		clear    : in  std_logic;
		-- Data : input and output
		data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
		data_out : out std_logic_vector(WDATA-1 downto 0);
		-- Tag : input and output
		tag_in   : in  std_logic_vector(TAGW-1 downto 0);
		tag_out  : out std_logic_vector(TAGW-1 downto 0)
	);
end logictree;

architecture synth of logictree is

	-- Compute the desired operation (not applying the NOT) of all or a subset of the input values
	impure function calc_op(off : natural; nb : natural) return std_logic_vector is
		variable var_op : std_logic_vector(WDATA-1 downto 0);
	begin
		var_op := data_in((off+1)*WDATA-1 downto off*WDATA);
		if nb > 1 then
			for i in 1 to nb-1 loop
				if OPAND = true then
					var_op := var_op and data_in((off+i+1)*WDATA-1 downto (off+i)*WDATA);
				elsif OPOR = true then
					var_op := var_op  or data_in((off+i+1)*WDATA-1 downto (off+i)*WDATA);
				elsif OPXOR = true then
					var_op := var_op xor data_in((off+i+1)*WDATA-1 downto (off+i)*WDATA);
				end if;
			end loop;
		end if;
		return var_op;
	end function;

begin

	-- Handle when there is no register
	gen_combi: if (RADIX = 0) or (NBIN <= 1) generate

		-- Warning: use a process to have the sentitivity list during simulation
		process(data_in)
		begin
			if OPNOT = false then
				data_out <= calc_op(0, NBIN);
			else
				data_out <= not calc_op(0, NBIN);
			end if;
		end process;

		tag_out <= tag_in when TAGEN = true else (others => '0');

	end generate;

	-- Handle when there is one output register
	gen_regout: if (RADIX = 1) and (NBIN > 1) generate
		signal reg_out : std_logic_vector(WDATA-1 downto 0) := (others => '0');
		signal reg_tag : std_logic_vector(TAGW-1 downto 0) := (others => '0');
	begin

		process(clk)
		begin

			if OPNOT = false then
				data_out <= calc_op(0, NBIN);
			else
				data_out <= not calc_op(0, NBIN);
			end if;

			if TAGEN = true then
				reg_tag <= tag_in;
				if (clear = '1') and (TAGZC = true) then
					reg_tag <= (others => '0');
				end if;
			end if;

		end process;

		data_out <= reg_out;
		tag_out  <= reg_tag;

	end generate;

	-- Pipelined situation
	gen_more: if (RADIX >= 2) and (NBIN > 1) generate

		-- The number of registers needed in the local stage
		constant NBREGS : natural := (NBIN + RADIX - 1) / RADIX;

		-- The registers of the current level
		signal regs : std_logic_vector(NBREGS*WDATA-1 downto 0);
		signal reg_tag : std_logic_vector(TAGW-1 downto 0);

	begin

		-- Compute the logic operations, not applying the optional NOT
		process(clk)
		begin
			if rising_edge(clk) then

				if NBREGS >= 2 then
					for r in 0 to NBREGS-2 loop
						regs((r+1)*WDATA-1 downto r*WDATA) <= calc_op(r*RADIX, RADIX);
					end loop;
				end if;

				regs(NBREGS*WDATA-1 downto (NBREGS-1)*WDATA) <= calc_op((NBREGS-1)*RADIX, NBIN-(NBREGS-1)*RADIX);

				if TAGEN = true then
					reg_tag <= tag_in;
					if (clear = '1') and (TAGZC = true) then
						reg_tag <= (others => '0');
					end if;
				end if;

			end if;
		end process;

		-- Instantiate the sub-stage
		i_stage: entity logictree
			generic map (
				WDATA => WDATA,
				NBIN  => NBREGS,
				-- The operation to perform : AND, OR, XOR optionally followed by NOT
				OPAND => OPAND,
				OPOR  => OPOR,
				OPXOR => OPXOR,
				OPNOT => OPNOT,
				-- Optional tag propagation
				TAGW  => TAGW,
				TAGEN => TAGEN,
				TAGZC => TAGZC,
				RADIX => RADIX
			)
			port map (
				clk      => clk,
				clear    => clear,
				-- Data, input and output
				data_in  => regs,
				data_out => data_out,
				-- Tag, input and output
				tag_in   => reg_tag,
				tag_out  => tag_out
			);

	end generate;

end architecture;

