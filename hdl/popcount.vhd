
-- This is a convenient wrapper for large combinatorial popcounts
-- It is intentionally separate from component "popcount_small" to reduce obscure recursive instantiations

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity popcount is
	generic (
		BITS : natural := 16;
		WOUT : natural := 5
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount;

architecture synth of popcount is

	-- Component declaration : the adder tree
	component addtree is
		generic (
			-- Data type and width
			WDATA : natural := 8;
			SDATA : boolean := true;
			NBIN  : natural := 20;
			WOUT  : natural := 12;
			-- User-specified radix, for testing purposes (0 means automatic)
			RADIX : natural := 0;
			-- Special considerations about data nature
			BINARY        : boolean := false;
			BINARY_RADIX  : natural := 0;
			TERNARY       : boolean := false;
			TERNARY_RADIX : natural := 0;
			-- An optional tag
			TAGW  : natural := 1;
			TAGEN : boolean := true;
			TAGZC : boolean := true;
			-- How to add pipeline registers
			REGEN  : natural := 1;  -- 0=none, 1=all, else every EN stages
			REGIDX : natural := 0   -- Start index (from the leaves)
		);
		port (
			clk      : in  std_logic;
			clear    : in  std_logic;
			-- Data, input and output
			data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
			data_out : out std_logic_vector(WOUT-1 downto 0);
			-- Tag, input and output
			tag_in   : in  std_logic_vector(TAGW-1 downto 0);
			tag_out  : out std_logic_vector(TAGW-1 downto 0)
		);
	end component;

begin

	add_i : addtree
		generic map (
			-- Data type and width
			WDATA => 1,
			SDATA => false,
			NBIN  => BITS,
			WOUT  => WOUT,
			-- User-specified radix, for testing purposes (0 means automatic)
			RADIX => 0,
			-- Special considerations about data nature
			BINARY        => true,
			BINARY_RADIX  => 0,
			TERNARY       => false,
			TERNARY_RADIX => 0,
			-- An optional tag
			TAGW  => 1,
			TAGEN => false,
			TAGZC => false,
			-- How to add pipeline registers
			REGEN  => 0,
			REGIDX => 0
		)
		port map (
			clk      => '0',
			clear    => '0',
			-- Data, input and output
			data_in  => bits_in,
			data_out => sum,
			-- Tag, input and output
			tag_in   => (others => '0'),
			tag_out  => open
		);

end architecture;

