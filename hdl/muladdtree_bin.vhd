
-- This is a binary multiplier + adder tree
-- It multiplies input values (binary) by weights (binary), with either operation AND or XNOR

-- Architecture :
--   - one layer of multiplication operations, combinational (radix is up to 4)
--   - then one adder tree

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

-- For LUT primitives
library unisim;
use unisim.vcomponents.all;

entity muladdtree_bin is
	generic(
		NBIN  : natural := 16;
		WOUT  : natural := 12;
		-- The type of operation to perform : false for operation AND, true for operation XNOR
		IS_XNOR : boolean := false;
		-- Tag-related information
		TAGW  : natural := 1;
		TAGEN : boolean := false;
		TAGZC : boolean := false;
		-- How to add pipeline registers
		REGEN  : natural := 1;  -- 0=none, 1=all, else every EN stages
		REGIDX : natural := 0   -- Start index (from the leaves)
	);
	port(
		clk       : in  std_logic;
		clear     : in  std_logic;
		-- Weight and data input
		weight_in : in  std_logic_vector(NBIN-1 downto 0);
		data_in   : in  std_logic_vector(NBIN-1 downto 0);
		-- Data output
		data_out  : out std_logic_vector(WOUT-1 downto 0);
		-- Tag, input and output
		tag_in    : in  std_logic_vector(TAGW-1 downto 0);
		tag_out   : out std_logic_vector(TAGW-1 downto 0)
	);
end muladdtree_bin;

architecture synth of muladdtree_bin is

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	-- Utility function to generate integer from boolean
	function to_integer(b : boolean) return integer is
		variable i : integer := 0;
	begin
		i := 1 when b = true else 0;
		return i;
	end function;

	-- Utility function to calculate minimum of two values
	function min(a, b : natural) return natural is
		variable m : natural := 0;
	begin
		m := a when a <= b else b;
		return m;
	end function;

	-- Compute the most appropriate radix for individual combinational MAC blocks
	constant RADIX : natural :=
		NBIN * to_integer(NBIN <= 4) +
		3 * to_integer(NBIN > 4);

	-- The number of individual MAC blocks
	constant BLOCKS_NB   : natural := (NBIN + RADIX - 1) / RADIX;
	constant BLOCKS_WOUT : natural := min(storebitsnb(RADIX), WOUT);

	-- Utility function to calculate next REGIDX
	function calc_regidx_next(REG_CREATE : boolean) return natural is
	begin
		if REGEN = 0 then
			return 0;
		end if;
		if REG_CREATE = true then
			return 0;
		end if;
		return (REGIDX + 1) mod REGEN;
	end function;

	-- Add pipeline registers when IDX = EN - 1
	-- Force creation of registers at last stage
	constant REG_CREATE  : boolean := (REGEN > 0) and ((REGIDX = REGEN-1) or (BLOCKS_NB = 1));
	constant REGIDX_NEXT : natural := calc_regidx_next(REG_CREATE);

	signal regs, regs_n : std_logic_vector(BLOCKS_NB*BLOCKS_WOUT-1 downto 0);
	signal reg_tag : std_logic_vector(TAGW-1 downto 0);

	component addtree is
		generic(
			-- Data type and width
			WDATA : natural := 8;
			SDATA : boolean := true;
			NBIN  : natural := 20;
			WOUT  : natural := 12;
			-- User-specified radix, for testing purposes (0 means automatic)
			RADIX : natural := 0;
			-- Special considerations about data nature
			BINARY : boolean := false;
			BINARY_RADIX : natural := 0;
			TERNARY : boolean := false;
			TERNARY_RADIX : natural := 0;
			-- An optional tag
			TAGW  : natural := 1;
			TAGEN : boolean := true;
			TAGZC : boolean := true;
			-- How to add pipeline registers
			REGEN  : natural := 1;  -- 0=none, 1=all, else every EN stages
			REGIDX : natural := 0   -- Start index (from the leaves)
		);
		port(
			clk      : in  std_logic;
			clear    : in  std_logic;
			-- Data, input and output
			data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
			data_out : out std_logic_vector(WOUT-1 downto 0);
			-- Tag, input and output
			tag_in   : in  std_logic_vector(TAGW-1 downto 0);
			tag_out  : out std_logic_vector(TAGW-1 downto 0)
		);
	end component;

begin

	-- Instantiate the multiply-accumulate blocks

	gen_blocks : for i in 0 to BLOCKS_NB-1 generate
		constant BLOCKSIZE : natural := min(RADIX, NBIN - i*RADIX);

		-- Wrappers for inputs
		signal sig_w : std_logic_vector(BLOCKSIZE-1 downto 0) := (others => '0');
		signal sig_d : std_logic_vector(BLOCKSIZE-1 downto 0) := (others => '0');

	begin

		-- Wrappers for inputs

		sig_w <= weight_in(i*RADIX+BLOCKSIZE-1 downto i*RADIX);
		sig_d <= data_in  (i*RADIX+BLOCKSIZE-1 downto i*RADIX);

		-- Block iplementation

		gen : if BLOCKSIZE <= 3 generate

			constant rom_width  : natural := storebitsnb(BLOCKSIZE);
			constant array_size : natural := 2**(2*BLOCKSIZE);
			type array_type is array(0 to array_size-1) of unsigned(rom_width-1 downto 0);

			-- Function that generates the precomputed results
			function func_gen_out(b : boolean) return array_type is
				variable arr : array_type;
				variable vin : unsigned(2*BLOCKSIZE-1 downto 0);
				variable sum : integer;
			begin
				for v in 0 to 2**(2*BLOCKSIZE)-1 loop
					vin := to_unsigned(v, 2*BLOCKSIZE);
					sum := 0;
					for i in 0 to BLOCKSIZE-1 loop
						if IS_XNOR = true then
							sum := sum + to_integer( vin(i downto i) xnor vin(i+BLOCKSIZE downto i+BLOCKSIZE) );
						else
							sum := sum + to_integer( vin(i downto i) * vin(i+BLOCKSIZE downto i+BLOCKSIZE) );
						end if;
					end loop;
					arr(v) := to_unsigned(sum, rom_width);
				end loop;
				return arr;
			end function;

			-- The precomputed lookup table
			constant array_res : array_type := func_gen_out(true);

		begin

			regs_n((i+1)*BLOCKS_WOUT-1 downto (i+0)*BLOCKS_WOUT) <= std_logic_vector(resize(array_res(to_integer(unsigned(sig_w & sig_d))), BLOCKS_WOUT));

		elsif BLOCKSIZE <= 4 generate

			-- Architecture :
			--   First stage  : one partial MAC for 2 inputs, generates 2 bits (one LUT)
			--   Second stage : generate the result position 0 (one LUT)
			--   Third stage  : generate the result positions 1 and 2 (one LUT)
			-- Total 3 LUTs, delay is 3 LUTs

			-- Note : In case delay is an issue, it is possible to use 4 LUTs and get delay of 2 LUTs
			--   Structure : partial sums with radix=2, then 2 LUTs to generate the final sum

			-- Intermediate signals for partial sums
			signal p00 : std_logic := '0';
			signal p01 : std_logic := '0';
			signal p11 : std_logic := '0';

			-- Wrapper for local result
			signal sig_res : std_logic_vector(2 downto 0) := (others => '0');

			function calc_false_true(b : boolean; vf : bit_vector; vt : bit_vector) return bit_vector is
			begin
				if b = false then return vf; end if;
				return vt;
			end;

			-- The config of LUTs depends on the operation to perform
			constant INIT_LUT0 : bit_vector(63 downto 0) := calc_false_true(IS_XNOR,
				x"0000800000006CA0",
				x"0000842100006996"
			);
			constant INIT_LUT1 : bit_vector(63 downto 0) := calc_false_true(IS_XNOR,
				x"ECA08000935F6CA0",
				x"EDB7842196696996"
			);

		begin

			lut0: LUT6_2
				generic map (
					INIT => INIT_LUT0
				)
				port map (
					O6 => p01,
					O5 => p00,
					I0 => sig_d(0),
					I1 => sig_d(1),
					I2 => sig_w(0),
					I3 => sig_w(1),
					I4 => '0',  -- Unused
					I5 => '1'   -- To have something different for O6 and O5
				);

			lut1: LUT6_2
				generic map (
					INIT => INIT_LUT1
				)
				port map (
					O6 => p11,
					O5 => sig_res(0),
					I0 => sig_d(2),
					I1 => sig_d(3),
					I2 => sig_w(2),
					I3 => sig_w(3),
					I4 => p00,
					I5 => '1'   -- To have something different for O6 and O5
				);

			lut12: LUT6_2
				generic map (
					INIT => x"00000400000003C4"
				)
				port map (
					O6 => sig_res(2),
					O5 => sig_res(1),
					I0 => sig_res(0),
					I1 => p11,
					I2 => p00,
					I3 => p01,
					I4 => '0',  -- Unused
					I5 => '1'   -- To have something different for O6 and O5
				);

			regs_n((i+1)*BLOCKS_WOUT-1 downto (i+0)*BLOCKS_WOUT) <= std_logic_vector(resize(unsigned(sig_res), BLOCKS_WOUT));

		else generate

			assert false report "Entity mactree_bin : Unsupported radix value" severity failure;

		end generate;

	end generate;

	-- Create pipeline registers

	gen_regs_data : if REG_CREATE = false generate

		-- No pipeline registers, just passthrough
		regs <= regs_n;

	else generate

		process(clk)
		begin
			if rising_edge(clk) then

				regs <= regs_n;

			end if;
		end process;

	end generate;

	gen_regs_tag : if (REG_CREATE = false) or (TAGEN = false) generate

		-- No pipeline registers, just passthrough
		reg_tag <= tag_in when TAGEN = true else (others => '0');

	else generate

		process(clk)
		begin
			if rising_edge(clk) then

				reg_tag <= tag_in;
				if (clear = '1') and (TAGZC = true) then
					reg_tag <= (others => '0');
				end if;

			end if;
		end process;

	end generate;

	-- Instantiate the upper stages of the adder tree

	gen_notree : if BLOCKS_NB <= 1 generate

		data_out <= std_logic_vector(resize(unsigned(regs), WOUT));
		tag_out <= reg_tag;

	end generate;

	-- WARNING Many versions of Vivado (2017.2 up to 2021.1 at least) require special recursive instantiation pattern
	-- Vivado segfaults when the instantiation is in an "else generate" statement
	-- So, isolate that in the most direct "if generate" part of the statement
	gen_tree : if BLOCKS_NB > 1 generate

		adder : addtree
			generic map (
				-- Data type and width
				WDATA => BLOCKS_WOUT,
				SDATA => false,
				NBIN  => BLOCKS_NB,
				WOUT  => WOUT,
				-- User-specified radix, for testing purposes (0 means automatic)
				RADIX => 0,
				-- Special considerations about data nature
				BINARY => false,
				BINARY_RADIX => 0,
				TERNARY => false,
				TERNARY_RADIX => 0,
				-- An optional tag
				TAGW  => TAGW,
				TAGEN => TAGEN,
				TAGZC => TAGZC,
				-- How to add pipeline registers
				REGEN  => REGEN,
				REGIDX => REGIDX_NEXT
			)
			port map (
				clk      => clk,
				clear    => clear,
				-- Data, input and output
				data_in  => regs,
				data_out => data_out,
				-- Tag, input and output
				tag_in   => reg_tag,
				tag_out  => tag_out
			);

	end generate;

end architecture;

