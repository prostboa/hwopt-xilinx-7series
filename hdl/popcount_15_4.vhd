
-- This is a popcount 15b -> 4b
-- Total size : 4 + 3 = 7 LUTs
-- Delay : 2 LUTs + 2*carry propag

-- Architecture : first one 6,0,7:5 compressor that handles 13 bits
-- Then a recoding stage that also adds the 2 extra input bits

-- Recoding operation :
--
--     co1  o1  o0
--   + co3  o3  o2
--   -         co1
--   +         i13
--   +         i14
--   =============
--             sum
--
-- This is simplified into the following operation :
--
--          o1  o0
--   + co3  o3  o2
--   +     co1 co1
--   +         i13
--   +         i14
--   =============
--             sum
--
-- This fits into a compressor 1,3,5:4
-- (cost is 3 LUT6, delay 1 LUT + carry)

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity popcount_15_4 is
	generic (
		BITS : natural := 15;
		WOUT : natural := 4
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount_15_4;

architecture synth of popcount_15_4 is

	signal sigBits : std_logic_vector(14 downto 0) := (others => '0');

	signal do : std_logic_vector(3 downto 0) := (others => '0');
	signal co : std_logic_vector(3 downto 0) := (others => '0');

	signal recode_di0 : std_logic_vector(4 downto 0) := (others => '0');
	signal recode_di1 : std_logic_vector(2 downto 0) := (others => '0');
	signal recode_di2 : std_logic_vector(0 downto 0) := (others => '0');
	signal recode_sum : std_logic_vector(3 downto 0) := (others => '0');

	component bitheap_607_5 is
		port (
			cy  : in  std_logic;
			ci  : in  std_logic;
			di0 : in  std_logic_vector(5 downto 0);
			di2 : in  std_logic_vector(5 downto 0);
			do  : out std_logic_vector(3 downto 0);
			co  : out std_logic_vector(3 downto 0)
		);
	end component;

	component bitheap_135_4 is
		port (
			ci  : in  std_logic;
			di0 : in  std_logic_vector(4 downto 0);
			di1 : in  std_logic_vector(2 downto 0);
			di2 : in  std_logic_vector(0 downto 0);
			sum : out std_logic_vector(3 downto 0)
		);
	end component;

begin

	assert BITS <= 15 report "Number of inputs too large for module popcount_15_4" severity failure;

	sigBits(BITS-1 downto 0) <= bits_in;

	comp607 : bitheap_607_5
		port map (
			cy  => sigBits(14),
			ci  => '0',
			di0 => sigBits(5 downto 0),
			di2 => sigBits(11 downto 6),
			do  => do,
			co  => co
		);

	recode_di0(0) <= co(1);
	recode_di0(1) <= do(0);
	recode_di0(2) <= do(2);
	recode_di0(3) <= sigBits(12);
	recode_di0(4) <= sigBits(13);

	recode_di1(0) <= do(1);
	recode_di1(1) <= do(3);
	recode_di1(2) <= co(1);

	recode_di2(0) <= co(3);

	comp135 : bitheap_135_4
		port map (
			ci  => '0',
			di0 => recode_di0,
			di1 => recode_di1,
			di2 => recode_di2,
			sum => recode_sum
		);

	-- Output ports

	sum <= std_logic_vector(resize(unsigned(recode_sum), WOUT));

end architecture;

