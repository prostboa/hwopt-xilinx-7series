
-- This is a popcount 43b -> 6b
-- Total size : 3x2 + 3x3 + 8 = 23 LUTs
-- Delay : 4 LUTs + 2*carry propag

-- Architecture : first 3x 2-lut 7:3 + 3x 3-lut 7:3 compressors (to balance distribution of LUTs associated to carry4)
-- Then a compressor 6,6,7:6

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity popcount_43_6 is
	generic (
		BITS : natural := 43;
		WOUT : natural := 6
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount_43_6;

architecture synth of popcount_43_6 is

	signal sigBits : std_logic_vector(42 downto 0) := (others => '0');

	signal c1_sum : std_logic_vector(2 downto 0) := (others => '0');
	signal c2_sum : std_logic_vector(2 downto 0) := (others => '0');
	signal c3_sum : std_logic_vector(2 downto 0) := (others => '0');
	signal c4_sum : std_logic_vector(2 downto 0) := (others => '0');
	signal c5_sum : std_logic_vector(2 downto 0) := (others => '0');
	signal c6_sum : std_logic_vector(2 downto 0) := (others => '0');

	signal rec_di0 : std_logic_vector(6 downto 0) := (others => '0');
	signal rec_di1 : std_logic_vector(5 downto 0) := (others => '0');
	signal rec_di2 : std_logic_vector(5 downto 0) := (others => '0');
	signal rec_sum : std_logic_vector(5 downto 0) := (others => '0');

	component popcount_7_3_carry4 is
		generic (
			BITS : natural := 7;
			WOUT : natural := 3
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component popcount_7_3 is
		generic (
			BITS : natural := 7;
			WOUT : natural := 3
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component bitheap_667_6 is
		port (
			di0 : in  std_logic_vector(6 downto 0);
			di1 : in  std_logic_vector(5 downto 0);
			di2 : in  std_logic_vector(5 downto 0);
			sum : out std_logic_vector(5 downto 0)
		);
	end component;

begin

	assert BITS <= 43 report "Number of inputs too large for module popcount_43_6" severity failure;

	sigBits(BITS-1 downto 0) <= bits_in;

	-- First stage : Compressors 7:3

	comp73_1 : popcount_7_3_carry4
		port map (
			bits_in => sigBits(6 downto 0),
			sum     => c1_sum
		);

	comp73_2 : popcount_7_3_carry4
		port map (
			bits_in => sigBits(13 downto 7),
			sum     => c2_sum
		);

	comp73_3 : popcount_7_3_carry4
		port map (
			bits_in => sigBits(20 downto 14),
			sum     => c3_sum
		);

	comp73_4 : popcount_7_3
		port map (
			bits_in => sigBits(27 downto 21),
			sum     => c4_sum
		);

	comp73_5 : popcount_7_3
		port map (
			bits_in => sigBits(34 downto 28),
			sum     => c5_sum
		);

	comp73_6 : popcount_7_3
		port map (
			bits_in => sigBits(41 downto 35),
			sum     => c6_sum
		);

	-- Second stage : Special-purpose compressor 555:5

	rec_di0(0) <= c1_sum(0);
	rec_di0(1) <= c2_sum(0);
	rec_di0(2) <= c3_sum(0);
	rec_di0(3) <= c4_sum(0);
	rec_di0(4) <= c5_sum(0);
	rec_di0(5) <= c6_sum(0);
	rec_di0(6) <= sigBits(42);

	rec_di1(0) <= c1_sum(1);
	rec_di1(1) <= c2_sum(1);
	rec_di1(2) <= c3_sum(1);
	rec_di1(3) <= c4_sum(1);
	rec_di1(4) <= c5_sum(1);
	rec_di1(5) <= c6_sum(1);

	rec_di2(0) <= c1_sum(2);
	rec_di2(1) <= c2_sum(2);
	rec_di2(2) <= c3_sum(2);
	rec_di2(3) <= c4_sum(2);
	rec_di2(4) <= c5_sum(2);
	rec_di2(5) <= c6_sum(2);

	comp667 : bitheap_667_6
		port map (
			di0 => rec_di0,
			di1 => rec_di1,
			di2 => rec_di2,
			sum => rec_sum
		);

	-- Output portè

	sum <= std_logic_vector(resize(unsigned(rec_sum), WOUT));

end architecture;

