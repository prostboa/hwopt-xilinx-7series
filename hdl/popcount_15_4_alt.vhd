
-- This is a popcount 15b -> 4b
-- Total size : 4 + 3 = 7 LUTs
-- Delay : 2 LUTs + 2*carry propag

-- Architecture : first one 6,0,6:5 compressor that handles 12 bits
-- Then a recoding stage that also adds the 3 extra input bits

-- Recoding operation :
--
--     co1  o1  o0
--   + co3  o3  o2
--   -         co1
--   +         i12
--   +         i13
--   +         i14
--   =============
--             sum
--
-- This is simplified into the following operation :
--
--          o1  o0
--   + co3  o3  o2
--   +     co1 co1
--   +         i12
--   +         i13
--   +         i14
--   =============
--             sum
--
-- This fits into a special-purpose compressor 1,3,6:4
-- (cost is 3 LUT6, delay 1 LUT + carry)

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity popcount_15_4_alt is
	generic (
		BITS : natural := 15;
		WOUT : natural := 4
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount_15_4_alt;

architecture synth of popcount_15_4_alt is

	signal sigBits : std_logic_vector(14 downto 0) := (others => '0');
	signal sigSum  : std_logic_vector(3 downto 0) := (others => '0');

	signal c4_o  : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_co : std_logic_vector(3 downto 0) := (others => '0');

	signal rec_c4_s  : std_logic_vector(3 downto 0) := (others => '0');
	signal rec_c4_di : std_logic_vector(3 downto 0) := (others => '0');
	signal rec_c4_o  : std_logic_vector(3 downto 0) := (others => '0');
	signal rec_c4_co : std_logic_vector(3 downto 0) := (others => '0');

	component bitheap_606_5 is
		port (
			cy  : in  std_logic;
			ci  : in  std_logic;
			di0 : in  std_logic_vector(5 downto 0);
			di2 : in  std_logic_vector(5 downto 0);
			do  : out std_logic_vector(3 downto 0);
			co  : out std_logic_vector(3 downto 0)
		);
	end component;

begin

	assert BITS <= 15 report "Number of inputs too large for module popcount_15_4" severity failure;

	sigBits(BITS-1 downto 0) <= bits_in;

	comp606 : bitheap_606_5
		port map (
			cy  => '0',
			ci  => '0',
			di0 => sigBits(5 downto 0),
			di2 => sigBits(11 downto 6),
			do  => c4_o,
			co  => c4_co
		);

	-- Recoding stage index 0

	lutA : LUT6_2
		generic map (
			INIT => x"96696996FFFF0000"
		)
		port map (
			O6 => rec_c4_s(0),
			O5 => rec_c4_di(0),
			I0 => c4_o(0),
			I1 => c4_o(2),
			I2 => c4_co(1),
			I3 => sigBits(12),
			I4 => sigBits(13),
			I5 => '1'   -- To have something different for O6 and O5
		);

	-- Recoding stage index 1

	lutB : LUT6
		generic map (
			INIT => x"13C936936C36C96C"
		)
		port map (
			O  => rec_c4_s(1),
			I0 => c4_o(0),
			I1 => c4_o(1),
			I2 => c4_o(2),
			I3 => c4_co(1),
			I4 => sigBits(12),
			I5 => c4_o(3)
		);

	rec_c4_di(1) <= c4_o(3);

	-- Recoding stage index 2

	lutC : LUT6
		generic map (
			INIT => x"0037017F7FC8FE80"
		)
		port map (
			O  => rec_c4_s(2),
			I0 => c4_o(0),
			I1 => c4_o(1),
			I2 => c4_o(2),
			I3 => c4_co(1),
			I4 => sigBits(12),
			I5 => c4_co(3)
		);

	rec_c4_di(2) <= c4_co(3);

	-- Recoding stage : the CARRY4

	c4: CARRY4
		port map (
			CO     => rec_c4_co,
			O      => rec_c4_o,
			DI     => rec_c4_di,
			S      => rec_c4_s,
			CI     => '0',
			CYINIT => sigBits(14)
		);

	-- Output ports

	sigSum <= rec_c4_co(2) & rec_c4_o(2 downto 0);

	sum <= std_logic_vector(resize(unsigned(sigSum), WOUT));

end architecture;

