
-- This is a special-purpose compressor 5,5,5:5
-- Total size : 6 LUTs
-- Delay : 2 LUTs + 1 carry propagation

-- Limitation : Result may be up to 16.7% lower than expected
-- Result up to 23 are correct, results above may be too low by 4 (depending on distribution of inputs) :
-- Result 24 is approximated to 20
-- Result 25 is approximated to 21
-- Result 26 is approximated to 22
-- Result 27 is approximated to 23
-- Result 28 is approximated to 24
-- Result 29 is approximated to 25
-- Result 30 is approximated to 26
-- Result 31 is approximated to 27
-- Result 32 is approximated to 28
-- Result 33 is approximated to 29
-- Result 34 is approximated to 30
-- Result 35 is approximated to 31

-- Recommendation : Use the input ci only for the purpose of chaining compressors
-- In that situation, di0(4) must be set to zero

library ieee;
use ieee.std_logic_1164.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity bitheap_555_5_special is
	port (
		ci  : in  std_logic;
		di0 : in  std_logic_vector(4 downto 0);
		di1 : in  std_logic_vector(4 downto 0);
		di2 : in  std_logic_vector(4 downto 0);
		sum : out std_logic_vector(4 downto 0)
	);
end bitheap_555_5_special;

architecture synth of bitheap_555_5_special is

	signal p1 : std_logic := '0';
	signal p2 : std_logic := '0';
	signal q2 : std_logic := '0';
	signal q3 : std_logic := '0';

	signal c4_o  : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_co : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_di : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_s  : std_logic_vector(3 downto 0) := (others => '0');

begin

	-- First stage LUTs

	lutP : LUT6_2
		generic map (
			INIT => x"E81717E8FFE8E800"
		)
		port map (
			O6 => p1,
			O5 => p2,
			I0 => di0(0),
			I1 => di0(1),
			I2 => di0(2),
			I3 => di1(0),
			I4 => di1(1),
			I5 => '1'   -- To have something different for O6 and O5
		);

	lutQ : LUT6_2
		generic map (
			INIT => x"E81717E8FFE8E800"
		)
		port map (
			O6 => q2,
			O5 => q3,
			I0 => di1(2),
			I1 => di1(3),
			I2 => di1(4),
			I3 => di2(0),
			I4 => di2(1),
			I5 => '1'   -- To have something different for O6 and O5
		);

	-- The LUTs associated to the CARRY4

	lutA : LUT6_2
		generic map (
			INIT => x"6969696996969696"
		)
		port map (
			O6 => c4_s(0),
			O5 => c4_di(0),
			I0 => di0(0),
			I1 => di0(1),
			I2 => di0(2),
			I3 => di1(0),
			I4 => di1(1),
			I5 => di0(3)
		);

	lutB : LUT6
		generic map (
			INIT => x"6969696996969696"
		)
		port map (
			O  => c4_s(1),
			I0 => di1(2),
			I1 => di1(3),
			I2 => di1(4),
			I3 => di2(0),
			I4 => di2(1),
			I5 => p1
		);

	c4_di(1) <= p1;

	lutC : LUT6
		generic map (
			INIT => x"16699669E9966996"
		)
		port map (
			O  => c4_s(2),
			I0 => di2(2),
			I1 => di2(3),
			I2 => di2(4),
			I3 => q2,
			I4 => q3,
			I5 => p2
		);

	c4_di(2) <= p2;

	-- Input index 3

	lutD : LUT6_2
		generic map (
			INIT => x"01177EE8FEE88000"
		)
		port map (
			O6 => c4_s(3),
			O5 => c4_di(3),
			I0 => di2(2),
			I1 => di2(3),
			I2 => di2(4),
			I3 => q2,
			I4 => q3,
			I5 => '1'   -- To have something different for O6 and O5
		);

	-- The CARRY4

	c4: CARRY4
		port map (
			CO     => c4_co,
			O      => c4_o,
			DI     => c4_di,
			S      => c4_s,
			CI     => ci,
			CYINIT => di0(4)
		);

	-- Output ports

	sum <= c4_co(3) & c4_o;

end architecture;

