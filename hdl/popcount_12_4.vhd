
-- This is a popcount 12b -> 4b
-- Total 6 LUTs, delay 2 LUTs + 1 carry

-- Architecture : first one 6:3 compressor that handles 6 bits
-- Then one 1,1,7:4 compressor that adds 6 extra bits

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT primitives
library unisim;
use unisim.vcomponents.all;

entity popcount_12_4 is
	generic (
		BITS : natural := 12;
		WOUT : natural := 4
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount_12_4;

architecture synth of popcount_12_4 is

	signal sigBits : std_logic_vector(11 downto 0) := (others => '0');

	signal sum63 : std_logic_vector(2 downto 0) := (others => '0');

	signal inst117_di0 : std_logic_vector(6 downto 0) := (others => '0');
	signal inst117_di1 : std_logic_vector(0 downto 0) := (others => '0');
	signal inst117_di2 : std_logic_vector(0 downto 0) := (others => '0');
	signal inst117_sum : std_logic_vector(3 downto 0) := (others => '0');

	component popcount_6_3 is
		generic (
			BITS : natural := 6;
			WOUT : natural := 3
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component bitheap_117_4 is
		port (
			ci  : in  std_logic;
			di0 : in  std_logic_vector(6 downto 0);
			di1 : in  std_logic_vector(0 downto 0);
			di2 : in  std_logic_vector(0 downto 0);
			do  : out std_logic_vector(3 downto 0);
			co  : out std_logic_vector(3 downto 0);
			sum : out std_logic_vector(3 downto 0)
		);
	end component;

begin

	assert BITS <= 12 report "Number of inputs too large for module popcount_12_4" severity failure;

	sigBits(BITS-1 downto 0) <= bits_in;

	popc63 : popcount_6_3
		port map (
			bits_in => sigBits(5 downto 0),
			sum     => sum63
		);

	inst117_di0    <= sigBits(11 downto 6) & sum63(0);
	inst117_di1(0) <= sum63(1);
	inst117_di2(0) <= sum63(2);

	comp117 : bitheap_117_4
		port map (
			ci  => '0',
			di0 => inst117_di0,
			di1 => inst117_di1,
			di2 => inst117_di2,
			do  => open,
			co  => open,
			sum => inst117_sum
		);

	sum <= std_logic_vector(resize(unsigned(inst117_sum), WOUT));

end architecture;

