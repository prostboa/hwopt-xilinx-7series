
-- This is a ternary adder 5x ter (signed only) -> 4b implemented using 6 LUT6
-- Architecture : first stage is partial adders, second stage is final recoding
-- Total 6 LUTs, delay is 2 LUTs

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT primitives
library unisim;
use unisim.vcomponents.all;

entity adder_5in_ter is
	generic (
		WOUT : natural := 5
	);
	port (
		in_ter : in  std_logic_vector(5*2-1 downto 0);
		res    : out std_logic_vector(WOUT-1 downto 0)
	);
end adder_5in_ter;

architecture synth of adder_5in_ter is

	signal partSum0 : std_logic_vector(2 downto 0) := (others => '0');
	signal partSum1 : std_logic_vector(2 downto 0) := (others => '0');

	signal sigSum : std_logic_vector(3 downto 0) := (others => '0');

begin

	-- Fist stage : partial adders

	lut0 : LUT6_2
		generic map (
			INIT => x"177E7EE896696996"
		)
		port map (
			O6 => partSum0(1),
			O5 => partSum0(0),
			I0 => in_ter(2*0 + 0),
			I1 => in_ter(2*1 + 0),
			I2 => in_ter(2*2 + 0),
			I3 => in_ter(2*3 + 0),
			I4 => in_ter(2*4 + 0),
			I5 => '1'   -- To have something different for O6 and O5
		);

	lut1 : LUT6_2
		generic map (
			INIT => x"177E7EE896696996"
		)
		port map (
			O6 => partSum1(1),
			O5 => partSum1(0),
			I0 => in_ter(2*0 + 1),
			I1 => in_ter(2*1 + 1),
			I2 => in_ter(2*2 + 1),
			I3 => in_ter(2*3 + 1),
			I4 => in_ter(2*4 + 1),
			I5 => '1'   -- To have something different for O6 and O5
		);

	lut02 : LUT5
		generic map (
			INIT => x"E8808000"
		)
		port map (
			O  => partSum0(2),
			I0 => in_ter(2*0 + 0),
			I1 => in_ter(2*1 + 0),
			I2 => in_ter(2*2 + 0),
			I3 => in_ter(2*3 + 0),
			I4 => in_ter(2*4 + 0)
		);

	lut12 : LUT5
		generic map (
			INIT => x"E8808000"
		)
		port map (
			O  => partSum1(2),
			I0 => in_ter(2*0 + 1),
			I1 => in_ter(2*1 + 1),
			I2 => in_ter(2*2 + 1),
			I3 => in_ter(2*3 + 1),
			I4 => in_ter(2*4 + 1)
		);

	-- Second stage : final recoding

	lutS01 : LUT6_2
		generic map (
			INIT => x"0000003C000000AA"
		)
		port map (
			O6 => sigSum(1),
			O5 => sigSum(0),
			I0 => partSum0(0),
			I1 => partSum0(1),
			I2 => partSum1(0),
			I3 => '0',
			I4 => '0',
			I5 => '1'   -- To have something different for O6 and O5
		);

	lutS23 : LUT6_2
		generic map (
			INIT => x"0044621000046214"
		)
		port map (
			O6 => sigSum(3),
			O5 => sigSum(2),
			I0 => partSum0(1),
			I1 => partSum0(2),
			I2 => partSum1(0),
			I3 => partSum1(1),
			I4 => partSum1(2),
			I5 => '1'   -- To have something different for O6 and O5
		);

	-- Output

	res <= std_logic_vector(resize(signed(sigSum), WOUT));

end architecture;

