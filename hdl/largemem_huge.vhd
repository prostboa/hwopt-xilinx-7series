
-- This is a memory component based on Xilinx URAM primitives
-- The read latency depends on the size of the memory, to mask the delay of the output multiplexer

-- There are 2 possible optimization objectives, with parameter OPT_SPEED :
-- OPT_SPEED=true  : optimize for speed, this may use up to 100% more blocks than necessary if the number of lines is just above a power of 2
-- OPT_SPEED=false : optimize the number of blocks, this may require more address decoding and multiplexing (with extra logic) and an additional pipeline register

-- FIXME Add a mode that directly instantiates the URAM primitives to manually control area/speed compromises
-- FIXME Support data width of lass than 72 bits by manipulating WE bits and shifting data

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity largemem_huge is
	generic (
		-- Parameters for memory
		WDATA   : natural := 8;
		CELLS   : natural := 1024;
		WADDR   : natural := 10;
		-- Optimization objective
		OPT_SPEED : boolean := false;
		-- A tag that may be given for each read
		WTAG    : natural := 1;
		-- Enable extra register after the memory blocks, to improve timings
		REG_DO  : boolean := false;
		-- Enable extra register when there are multiple blocks in height
		REG_MUX : boolean := false
	);
	port (
		clk        : in  std_logic;
		clear      : in  std_logic;
		-- Write port
		write_addr : in  std_logic_vector(WADDR-1 downto 0);
		write_en   : in  std_logic;
		write_data : in  std_logic_vector(WDATA-1 downto 0);
		-- Read port, input
		read_addr  : in  std_logic_vector(WADDR-1 downto 0);
		read_en    : in  std_logic;
		read_itag  : in  std_logic_vector(WTAG-1 downto 0);
		-- Read port, output
		read_data  : out std_logic_vector(WDATA-1 downto 0);
		read_valid : out std_logic;
		read_otag  : out std_logic_vector(WTAG-1 downto 0)
	);
end largemem_huge;

architecture synth of largemem_huge is

	-- Utility function to generate integer from boolean
	function to_integer(b : boolean) return natural is
	begin
		if b = true then
			return 1;
		end if;
		return 0;
	end function;

	-- A data type to hold URAM configuration:
	-- number of blocks in height and in width
	type uram_t is record
		CELLS      : natural;  -- Number of lines
		WADDR      : natural;  -- Address port width
		WDATA      : natural;  -- Data port width
		NB_WIDTH   : natural;
		NB_HEIGHT  : natural;
	end record;

	-- Helper function to fill a uram_t structure
	function gen_uram(uram_cells, uram_waddr, uram_wdata : natural) return uram_t is
		variable uram : uram_t := (0, 0, 0, 0, 0);
	begin

		-- Properties of URAM primitives
		uram.CELLS     := uram_cells;
		uram.WADDR     := uram_waddr;
		uram.WDATA     := uram_wdata;
		-- Number of URAM primitives
		uram.NB_WIDTH  := (WDATA + uram_wdata - 1) / uram_wdata;
		uram.NB_HEIGHT := (CELLS + uram_cells - 1) / uram_cells;

		return uram;
	end function;

	-- Select an appropriate URAM configuration
	constant URAM : uram_t := gen_uram(4096, 12, 72);

	-- Get the total data width that URAMs output (for a given height position)
	constant WTOTAL : natural := URAM.NB_WIDTH * URAM.WDATA;

	-- Buffers that stores the Read Enable, to propagate on the output
	signal buf_re1 : std_logic := '0';
	signal buf_re2 : std_logic := '0';
	-- Buffers that stores the Read Tag, to propagate on the output
	signal buf_tag1 : std_logic_vector(WTAG-1 downto 0) := (others => '0');
	signal buf_tag2 : std_logic_vector(WTAG-1 downto 0) := (others => '0');

	-- The output of the URAMs for each level
	signal sig_fullwrite : std_logic_vector(WTOTAL-1 downto 0) := (others => '0');
	signal sig_fullread  : std_logic_vector(URAM.NB_HEIGHT*WTOTAL-1 downto 0) := (others => '0');

	-- The optionally buffered output of the URAMs
	signal buf_fullread2 : std_logic_vector(URAM.NB_HEIGHT*WTOTAL-1 downto 0) := (others => '0');

begin

	-- Instantiate the RAM blocks

	sig_fullwrite <= std_logic_vector(resize(unsigned(write_data), WTOTAL));

	gen_height: for h in 0 to URAM.NB_HEIGHT-1 generate
		signal we : std_logic := '0';
	begin

		gen_we_tall: if URAM.NB_HEIGHT > 1 generate
			we <= write_en when unsigned(write_addr(WADDR-1 downto URAM.WADDR)) = h else '0';
		end generate;

		gen_we_small: if URAM.NB_HEIGHT <= 1 generate
			we <= write_en;
		end generate;

		gen_width: for w in 0 to URAM.NB_WIDTH-1 generate

			type mem_type is array (0 to URAM.CELLS-1) of std_logic_vector(URAM.WDATA-1 downto 0);
			signal mem : mem_type := (others => (others => '0'));

			attribute ram_style : string;
			attribute ram_style of mem : signal is "ultra";

			signal sig_write_addr : std_logic_vector(URAM.WADDR-1 downto 0) := (others => '0');
			signal sig_read_addr  : std_logic_vector(URAM.WADDR-1 downto 0) := (others => '0');

		begin

			sig_write_addr <= std_logic_vector(resize(unsigned(write_addr), URAM.WADDR));
			sig_read_addr  <= std_logic_vector(resize(unsigned(read_addr), URAM.WADDR));

			-- Memory operation
			process(clk)
			begin
				if rising_edge(clk) then

					if we = '1' then
						mem(to_integer(unsigned(sig_write_addr))) <= sig_fullwrite((w+1)*URAM.WDATA-1 downto w*URAM.WDATA);
					end if;

					if read_en = '1' then
						sig_fullread(h*WTOTAL+(w+1)*URAM.WDATA-1 downto h*WTOTAL+w*URAM.WDATA) <= mem(to_integer(unsigned(sig_read_addr)));
					end if;

				end if;  -- Clock edge
			end process;

		end generate;  -- Width

	end generate;  -- Height

	-- Sequential process: propagate the read_en signal

	process(clk)
	begin
		if rising_edge(clk) then
			buf_re1  <= read_en and not clear;
			buf_tag1 <= read_itag;
		end if;
	end process;

	-- Sequential process: the register at output of the URAMs

	gen_reg_do : if REG_DO = true generate
		-- Prevent the register to be absorbed by the URAM
		-- This way the register can be close to the actual readers, which helps to respect timings constraints
		signal reg_fullread2 : std_logic_vector(URAM.NB_HEIGHT*WTOTAL-1 downto 0) := (others => '0');
		attribute DONT_TOUCH : string;
		attribute DONT_TOUCH of reg_fullread2 : signal is "true";
	begin

		process(clk)
		begin
			if rising_edge(clk) then
				reg_fullread2 <= sig_fullread;
				buf_re2       <= buf_re1 and not clear;
				buf_tag2      <= buf_tag1;
			end if;
		end process;

		buf_fullread2 <= reg_fullread2;

	else generate

		buf_fullread2 <= sig_fullread;
		buf_re2       <= buf_re1;
		buf_tag2      <= buf_tag1;

	end generate;

	-- Direct assignment of output port if there is no mux after the URAM

	gen_nomux : if URAM.NB_HEIGHT = 1 generate

		-- Note : Need to explicitly select the WDATA range because the URAM data width may be larger than WDATA
		read_data  <= buf_fullread2(WDATA-1 downto 0);
		read_valid <= buf_re2;
		read_otag  <= buf_tag2;

	end generate;

	-- Post-memory multiplexer

	gen_mux: if URAM.NB_HEIGHT >= 2 generate

		-- Store the MSB bits of the address for the multiplexer
		signal buf_raddr1 : std_logic_vector(WADDR-URAM.WADDR-1 downto 0) := (others => '0');
		signal buf_raddr2 : std_logic_vector(WADDR-URAM.WADDR-1 downto 0) := (others => '0');

		-- The output of the multiplexer on the data read side
		signal sig_mux_rd : std_logic_vector(WDATA-1 downto 0) := (others => '0');

	begin

		-- Sequential process: the buffer on MSBs of read address

		process(clk)
		begin
			if rising_edge(clk) then

				buf_raddr1 <= read_addr(WADDR-1 downto URAM.WADDR);
				buf_raddr2 <= read_addr(WADDR-1 downto URAM.WADDR);
				if REG_DO = true then
					buf_raddr2 <= buf_raddr1;
				end if;

			end if;
		end process;

		-- The multiplexer
		-- FIXME It is unknown how efficient this gets synthesized, whether it is a priority tree, a binary mux or a decoded mux based on OR operation

		process(all)
		begin

			sig_mux_rd <= (others => '0');

			for h in 0 to URAM.NB_HEIGHT-1 loop
				if unsigned(buf_raddr2) = h then
					sig_mux_rd <= buf_fullread2(h*WTOTAL+WDATA-1 downto h*WTOTAL);
				end if;
			end loop;

		end process;

		-- Output buffers

		gen_reg : if REG_MUX = true generate

			process(clk)
			begin
				if rising_edge(clk) then
					read_data  <= sig_mux_rd;
					read_valid <= buf_re2 and not clear;
					read_otag  <= buf_tag2;
				end if;
			end process;

		else generate

			read_data  <= sig_mux_rd;
			read_valid <= buf_re2;
			read_otag  <= buf_tag2;

		end generate;

	end generate;

end architecture;

