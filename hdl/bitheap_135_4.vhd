
-- This is a compressor 1,3,5:4
-- See paper : Area Optimized Synthesis of Compressor Trees on Xilinx FPGAs Using Generalized Parallel Counters (2019)

-- Recommendation : Use the input ci only for the purpose of chaining compressors
-- In that situation, di0(4) must be set to zero

library ieee;
use ieee.std_logic_1164.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity bitheap_135_4 is
	port (
		ci  : in  std_logic;
		di0 : in  std_logic_vector(4 downto 0);
		di1 : in  std_logic_vector(2 downto 0);
		di2 : in  std_logic_vector(0 downto 0);
		sum : out std_logic_vector(3 downto 0)
	);
end bitheap_135_4;

architecture synth of bitheap_135_4 is

	signal c4_o  : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_co : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_di : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_s  : std_logic_vector(3 downto 0) := (others => '0');

begin

	-- Input index 0

	lutA : LUT6_2
		generic map (
			INIT => x"000069960000FF00"
		)
		port map (
			O6 => c4_s(0),
			O5 => c4_di(0),
			I0 => di0(0),
			I1 => di0(1),
			I2 => di0(2),
			I3 => di0(3),
			I4 => '0',  -- Unused
			I5 => '1'   -- To have something different for O6 and O5
		);

	-- Input index 1

	lutB : LUT6
		generic map (
			INIT => x"17E8E817E81717E8"
		)
		port map (
			O  => c4_s(1),
			I0 => di0(0),
			I1 => di0(1),
			I2 => di0(2),
			I3 => di1(0),
			I4 => di1(1),
			I5 => di1(2)
		);

	c4_di(1) <= di1(2);

	-- Input index 2

	lutC : LUT6
		generic map (
			INIT => x"001717FFFFE8E800"
		)
		port map (
			O  => c4_s(2),
			I0 => di0(0),
			I1 => di0(1),
			I2 => di0(2),
			I3 => di1(0),
			I4 => di1(1),
			I5 => di2(0)
		);

	c4_di(2) <= di2(0);

	-- Input index 3 : Left with default values 0

	-- The CARRY4

	c4: CARRY4
		port map (
			CO     => c4_co,
			O      => c4_o,
			DI     => c4_di,
			S      => c4_s,
			CI     => ci,
			CYINIT => di0(4)
		);

	-- Output ports

	sum <= c4_co(2) & c4_o(2 downto 0);

end architecture;

