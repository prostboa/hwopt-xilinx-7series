
-- This is a compressor 6,7:5
-- Total size : 6 LUTs
-- Delay : 2 LUTs + carry propagation

-- Architecture : First one compressor 607:5, then recoding with 2 LUTs

-- Recoding operation :
--             o1  o0
--  + co3  o3  o2
--  +         co1
-- ==================
-- S4  S3  S2  S1  S0

library ieee;
use ieee.std_logic_1164.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity bitheap_67_5 is
	port (
		di0 : in  std_logic_vector(6 downto 0);
		di1 : in  std_logic_vector(5 downto 0);
		sum : out std_logic_vector(4 downto 0)
	);
end bitheap_67_5;

architecture synth of bitheap_67_5 is

	signal c4_do : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_co : std_logic_vector(3 downto 0) := (others => '0');

	component bitheap_607_5 is
		port (
			cy  : in  std_logic;
			ci  : in  std_logic;
			di0 : in  std_logic_vector(5 downto 0);
			di2 : in  std_logic_vector(5 downto 0);
			do  : out std_logic_vector(3 downto 0);
			co  : out std_logic_vector(3 downto 0)
		);
	end component;

begin

	-- First stage

	comp607_1 : bitheap_607_5
		port map (
			cy  => di0(6),
			ci  => '0',
			di0 => di0(5 downto 0),
			di2 => di1(5 downto 0),
			do  => c4_do,
			co  => c4_co
		);

	sum(0) <= c4_do(0);

	-- Second stage

	lut12 : LUT6_2
		generic map (
			INIT => x"9696969617E817E8"
		)
		port map (
			O6 => sum(1),
			O5 => sum(2),
			I0 => c4_do(1),
			I1 => c4_do(2),
			I2 => c4_co(1),
			I3 => c4_do(3),
			I4 => c4_co(3),
			I5 => '1'   -- To have something different for O6 and O5
		);

	lut34 : LUT6_2
		generic map (
			INIT => x"17FFE800E8000000"
		)
		port map (
			O6 => sum(3),
			O5 => sum(4),
			I0 => c4_do(1),
			I1 => c4_do(2),
			I2 => c4_co(1),
			I3 => c4_do(3),
			I4 => c4_co(3),
			I5 => '1'   -- To have something different for O6 and O5
		);

end architecture;

