
-- This is a popcount 33b -> 5b, the result is approximate, by design -16.7% to +25% error
-- Total size : 2x4 + 2 + 6 = 16 LUTs
-- Delay : 3 LUTs + 2 carry propag

-- Architecture : first 2x 6,0,7:3 + 1x 2-lut 7:3 compressors
-- Then a special-purpose compressor 5,5,5:5 that brings underestimation
-- Note : The 2 "free" LUTs of the 5,5,5:5 can be placed in the partially-used slice of the 7:3

-- Analysis of worst cases for under-estimation rate :
--   The 5,5,5:5 emits 20 for a theoretical result of 24, this is -16.7% error rate
-- Analysis of worst cases for over-estimation rate :
--   When one 607:5 has 4 bits in input position 0, result is 5 -> +25% error rate

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity popcount_33_5_approx is
	generic (
		BITS : natural := 33;
		WOUT : natural := 5
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount_33_5_approx;

architecture synth of popcount_33_5_approx is

	signal sigBits : std_logic_vector(32 downto 0) := (others => '0');

	signal c1_do : std_logic_vector(3 downto 0) := (others => '0');
	signal c1_co : std_logic_vector(3 downto 0) := (others => '0');
	signal c2_do : std_logic_vector(3 downto 0) := (others => '0');
	signal c2_co : std_logic_vector(3 downto 0) := (others => '0');

	signal c3_sum : std_logic_vector(2 downto 0) := (others => '0');

	signal rec_di0 : std_logic_vector(4 downto 0) := (others => '0');
	signal rec_di1 : std_logic_vector(4 downto 0) := (others => '0');
	signal rec_di2 : std_logic_vector(4 downto 0) := (others => '0');
	signal rec_sum : std_logic_vector(4 downto 0) := (others => '0');

	component bitheap_607_5 is
		port (
			cy  : in  std_logic;
			ci  : in  std_logic;
			di0 : in  std_logic_vector(5 downto 0);
			di2 : in  std_logic_vector(5 downto 0);
			do  : out std_logic_vector(3 downto 0);
			co  : out std_logic_vector(3 downto 0)
		);
	end component;

	component popcount_7_3_carry4 is
		generic (
			BITS : natural := 7;
			WOUT : natural := 3
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component bitheap_555_5_special is
		port (
			ci  : in  std_logic;
			di0 : in  std_logic_vector(4 downto 0);
			di1 : in  std_logic_vector(4 downto 0);
			di2 : in  std_logic_vector(4 downto 0);
			sum : out std_logic_vector(4 downto 0)
		);
	end component;

begin

	assert BITS <= 33 report "Number of inputs too large for module popcount_33_5_approx" severity failure;

	sigBits(BITS-1 downto 0) <= bits_in;

	-- First stage : Compressors 6,0,7:5 and 7:3

	comp607_1 : bitheap_607_5
		port map (
			cy  => sigBits(12),
			ci  => '0',
			di0 => sigBits(5 downto 0),
			di2 => sigBits(11 downto 6),
			do  => c1_do,
			co  => c1_co
		);

	comp607_2 : bitheap_607_5
		port map (
			cy  => sigBits(25),
			ci  => '0',
			di0 => sigBits(18 downto 13),
			di2 => sigBits(24 downto 19),
			do  => c2_do,
			co  => c2_co
		);

	comp73_1 : popcount_7_3_carry4
		port map (
			bits_in => sigBits(32 downto 26),
			sum     => c3_sum
		);

	-- Second stage : Special-purpose compressor 555:5

	rec_di0(0) <= c1_do(0);
	rec_di0(1) <= c1_do(2);
	rec_di0(2) <= c2_do(0);
	rec_di0(3) <= c2_do(2);
	rec_di0(4) <= c3_sum(0);

	rec_di1(0) <= c1_do(1);
	rec_di1(1) <= c1_do(3);
	rec_di1(2) <= c2_do(1);
	rec_di1(3) <= c2_do(3);
	rec_di1(4) <= c3_sum(1);

	rec_di2(0) <= c1_co(1);
	rec_di2(1) <= c1_co(3);
	rec_di2(2) <= c2_co(1);
	rec_di2(3) <= c2_co(3);
	rec_di2(4) <= c3_sum(2);

	comp555 : bitheap_555_5_special
		port map (
			ci  => '0',
			di0 => rec_di0,
			di1 => rec_di1,
			di2 => rec_di2,
			sum => rec_sum
		);

	-- Output portè

	sum <= std_logic_vector(resize(unsigned(rec_sum), WOUT));

end architecture;

