
-- This is a special-purpose compressor 2,2,5:5
-- Total size : 3 LUTs
-- Delay : 1 LUT + 1 carry propagation

-- Limitation : The result is incorrect for some combinations of inputs
-- Result  8 is approximated to  7
-- Result  9 is approximated to  7-8
-- Result 10 is approximated to  8-9
-- Result 11 is approximated to  9-10
-- Result 12 is approximated to 10-11
-- Result 13 is approximated to 11-12
-- Result 14 is approximated to 12-13
-- Result 15 is approximated to 13-14
-- Result 16 is approximated to 14-15
-- Result 17 is approximated to 15

-- Recommendation : Use the input ci only for the purpose of chaining compressors
-- In that situation, di0(4) must be set to zero

library ieee;
use ieee.std_logic_1164.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity bitheap_225_4_special is
	port (
		ci  : in  std_logic;
		di0 : in  std_logic_vector(4 downto 0);
		di1 : in  std_logic_vector(1 downto 0);
		di2 : in  std_logic_vector(1 downto 0);
		sum : out std_logic_vector(3 downto 0)
	);
end bitheap_225_4_special;

architecture synth of bitheap_225_4_special is

	signal c4_o  : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_co : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_di : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_s  : std_logic_vector(3 downto 0) := (others => '0');

begin

	-- Input index 0

	lutA : LUT6
		generic map (
			INIT => x"01696969FE969696"
		)
		port map (
			O  => c4_s(0),
			I0 => di0(0),
			I1 => di0(1),
			I2 => di0(2),
			I3 => di1(0),
			I4 => di2(0),
			I5 => di0(3)
		);

	c4_di(0) <= di0(3);

	-- Input index 1

	lutB : LUT6
		generic map (
			INIT => x"0017E817FFE817E8"
		)
		port map (
			O  => c4_s(1),
			I0 => di0(0),
			I1 => di0(1),
			I2 => di0(2),
			I3 => di1(0),
			I4 => di2(0),
			I5 => di1(1)
		);

	c4_di(1) <= di1(1);

	-- Input index 2

	lutD: LUT6
		generic map (
			INIT => x"000017FFFFFFE800"
		)
		port map (
			O  => c4_s(2),
			I0 => di0(0),
			I1 => di0(1),
			I2 => di0(2),
			I3 => di1(0),
			I4 => di2(0),
			I5 => di2(1)
		);

	c4_di(2) <= di2(1);

	-- The CARRY4

	c4: CARRY4
		port map (
			CO     => c4_co,
			O      => c4_o,
			DI     => c4_di,
			S      => c4_s,
			CI     => ci,
			CYINIT => di0(4)
		);

	-- Output ports

	sum <= c4_co(2) & c4_o(2 downto 0);

end architecture;

