
-- This is a ternary multiplier + adder tree
-- It multiplies an input value by a ternary weight

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity terncompress_3b_2t_muladd is
	generic(
		WDATA : natural := 12;
		NBIN  : natural := 8;
		WOUT  : natural := 15;
		-- Compression-related information
		COM3B2T_EN  : boolean := false;
		COM3B2T_OFF : natural := 0;
		INSIZE      : natural := 16;
		-- Tag-related information
		TAGW  : natural := 1;
		TAGEN : boolean := true;
		TAGZC : boolean := true
	);
	port(
		clk       : in  std_logic;
		clear     : in  std_logic;
		-- Weight and data input
		weight_in : in  std_logic_vector(INSIZE-1 downto 0);
		data_in   : in  std_logic_vector(NBIN*WDATA-1 downto 0);
		-- Data output
		data_out  : out std_logic_vector(WOUT-1 downto 0);
		-- Tag, input and output
		tag_in    : in  std_logic_vector(TAGW-1 downto 0);
		tag_out   : out std_logic_vector(TAGW-1 downto 0)
	);
end terncompress_3b_2t_muladd;

architecture synth of terncompress_3b_2t_muladd is

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	type radix_t is record
		-- The radix to use: 1, 2, 3, or 4
		RADIX : natural;
		-- The output width of the 1st stage adders
		WOUT : natural;
		-- The total number of groups = number of 1st stage adders
		NGROUPS : natural;
		-- Whether the multiplication result is ternary
		TERNARY : boolean;
		-- Whether or not to enable output buffering
		BUFEN : boolean;
	end record;

	function calc_radix(phony : boolean) return radix_t is
		variable radix : radix_t := (0, 0, 0, false, false);
	begin
		radix.RADIX := 1;
		if WDATA > 2 then
			radix.RADIX := 1;
		elsif NBIN <= 7 then
			radix.RADIX := NBIN;
		else
			radix.RADIX := 7;
		end if;

		-- For debug: Force radix=1, no special first layer adder
		--radix.RADIX := 1;

		radix.WOUT := storebitsnb(radix.RADIX) + 1;
		if radix.RADIX = 1 then
			radix.WOUT := WDATA;
		end if;

		radix.NGROUPS := (NBIN + radix.RADIX - 1) / radix.RADIX;

		radix.TERNARY := false;
		if WDATA <= 2 then
			radix.TERNARY := true;
		end if;

		radix.BUFEN := false;
		if radix.RADIX > 2 then
			radix.BUFEN := true;
		end if;

		return radix;
	end function;

	constant RADIX : radix_t := calc_radix(false);

	signal multres_sig : std_logic_vector(NBIN*WDATA-1 downto 0);
	signal multres_reg : std_logic_vector(NBIN*WDATA-1 downto 0);

	signal multtag_reg : std_logic_vector(TAGW-1 downto 0);

	signal radixres_sig : std_logic_vector(RADIX.NGROUPS * RADIX.WOUT - 1 downto 0);
	signal radixres_reg : std_logic_vector(RADIX.NGROUPS * RADIX.WOUT - 1 downto 0);
	signal radixtag_reg : std_logic_vector(TAGW-1 downto 0);

	-- Number of layers of the adder tree that can have no buffering in between
	-- For unsigned tree, 2 layers is possible, but only one for signed tree because of the extra sign extension LUTs
	constant REGEN_RECURS : natural := 1;

	component mul_ter_weight is
		generic(
			WDATA : natural := 12
		);
		port(
			weight_in : in  std_logic_vector(1 downto 0);
			data_in   : in  std_logic_vector(WDATA-1 downto 0);
			data_out  : out std_logic_vector(WDATA-1 downto 0)
		);
	end component;

	component terncompress_3b_2t_mul is
		generic(
			WDATA : natural := 2;
			IDX   : natural := 0
		);
		port(
			bin_in   : in  std_logic_vector(2 downto 0);
			data_in  : in  std_logic_vector(WDATA-1 downto 0);
			mult_out : out std_logic_vector(WDATA-1 downto 0)
		);
	end component;

	component adder_ter is
		generic(
			NBIN  : natural := 4;
			WOUT  : natural := 4
		);
		port(
			-- Weight and data input
			data_in   : in  std_logic_vector(2*NBIN-1 downto 0);
			-- Data output
			data_out  : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component addtree is
		generic(
			-- Data type and width
			WDATA : natural := 8;
			SDATA : boolean := true;
			NBIN  : natural := 20;
			WOUT  : natural := 12;
			-- Special considerations about data nature
			TERNARY : boolean := false;
			-- An optional tag
			TAGW  : natural := 1;
			TAGEN : boolean := true;
			TAGZC : boolean := true;
			-- How to add pipeline registers
			REGEN  : natural := 1;  -- 0=none, 1=all, else every EN stages
			REGIDX : natural := 0   -- Start index (from the leaves)
		);
		port(
			clk      : in  std_logic;
			clear    : in  std_logic;
			-- Data, input and output
			data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
			data_out : out std_logic_vector(WOUT-1 downto 0);
			-- Tag, input and output
			tag_in   : in  std_logic_vector(TAGW-1 downto 0);
			tag_out  : out std_logic_vector(TAGW-1 downto 0)
		);
	end component;

begin

	-- Instantiate the ternary multipliers

	gen_mult : for i in 0 to NBIN-1 generate

		gen_comp : if COM3B2T_EN = true generate
			constant GLOBAL_IDX : natural := COM3B2T_OFF + i;
			constant CODE_IDX   : natural := GLOBAL_IDX / 2;
			constant LOCAL_IDX  : natural := GLOBAL_IDX - 2*CODE_IDX;
		begin

			i_mul : terncompress_3b_2t_mul
				generic map (
					WDATA => WDATA,
					IDX   => LOCAL_IDX
				)
				port map(
					bin_in   => weight_in((CODE_IDX+1)*3-1 downto CODE_IDX*3),
					data_in  => data_in((i+1)*WDATA-1 downto i*WDATA),
					mult_out => multres_sig((i+1)*WDATA-1 downto i*WDATA)
				);

		else generate

			i_mul : mul_ter_weight
				generic map (
					WDATA => WDATA
				)
				port map (
					weight_in => weight_in((i+1)*2-1 downto i*2),
					data_in   => data_in((i+1)*WDATA-1 downto i*WDATA),
					data_out  => multres_sig((i+1)*WDATA-1 downto i*WDATA)
				);

		end generate;

	end generate;

	-- Registers after multiplier

	process(clk)
	begin
		if rising_edge(clk) then

			-- The registers at output of ternary multipliers
			multres_reg <= multres_sig;

			-- The tag, if any
			if TAGEN = true then
				multtag_reg <= tag_in;
				if (clear = '1') and (TAGZC = true) then
					multtag_reg <= (others => '0');
				end if;
			end if;

		end if;
	end process;

	-- Instantiate 1st stage adders

	gen_radix : if RADIX.RADIX > 1 generate

		-- Generate the optimized adders

		gen_loop : for i in 0 to RADIX.NGROUPS - 1 generate

			-- Utility function to calculate minimum of two values
			function min(a, b : natural) return natural is
				variable m : natural := 0;
			begin
				m := a when a <= b else b;
				return m;
			end function;

			constant LOCALSIZE : natural := min(RADIX.RADIX, NBIN - i*RADIX.RADIX);

		begin

			adder : adder_ter
				generic map (
					NBIN  => LOCALSIZE,
					WOUT  => RADIX.WOUT
				)
				port map (
					data_in  => multres_reg((i*RADIX.RADIX+LOCALSIZE)*WDATA-1 downto i*RADIX.RADIX*WDATA),
					data_out => radixres_sig((i+1)*RADIX.WOUT-1 downto i*RADIX.WOUT)
				);

		end generate;

		-- Generate the buffered tag, if any

		gen_buf : if RADIX.BUFEN = true generate

			process(clk)
			begin
				if rising_edge(clk) then

					radixres_reg <= radixres_sig;

					if TAGEN = true then
						radixtag_reg <= multtag_reg;
						if (clear = '1') and (TAGZC = true) then
							radixtag_reg <= (others => '0');
						end if;
					end if;

				end if;
			end process;

		else generate

			radixres_reg <= radixres_sig;
			radixtag_reg <= multtag_reg;

		end generate;

	end generate;

	gen_noradix : if RADIX.RADIX = 1 generate

		radixres_reg <= multres_reg;
		radixtag_reg <= multtag_reg;

	end generate;

	-- The adder tree

	i_add : addtree
		generic map (
			-- Data type and width
			WDATA => RADIX.WOUT,
			SDATA => true,
			NBIN  => RADIX.NGROUPS,
			WOUT  => WOUT,
			-- Special considerations about data nature
			TERNARY => false,
			-- An optional tag
			TAGW  => TAGW,
			TAGEN => TAGEN,
			TAGZC => TAGZC,
			-- How to add pipeline registers
			REGEN  => REGEN_RECURS,
			REGIDX => 0
		)
		port map (
			clk      => clk,
			clear    => clear,
			-- Data, input and output
			data_in  => radixres_reg,
			data_out => data_out,
			-- Tag, input and output
			tag_in   => radixtag_reg,
			tag_out  => tag_out
		);

end architecture;

