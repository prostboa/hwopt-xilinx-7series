
-- This is a popcount 9b -> 4b
-- Total 4 LUTs, delay 2 LUTs + 1 carry

-- Architecture : first one 3:2 compressor that handles 3 bits
-- Then one 1,1,7:4 compressor that adds 6 extra bits

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT primitives
library unisim;
use unisim.vcomponents.all;

entity popcount_9_4 is
	generic (
		BITS : natural := 9;
		WOUT : natural := 4
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount_9_4;

architecture synth of popcount_9_4 is

	signal sigBits : std_logic_vector(8 downto 0) := (others => '0');

	signal sum3 : std_logic_vector(1 downto 0) := (others => '0');

	signal inst117_di0 : std_logic_vector(6 downto 0) := (others => '0');
	signal inst117_di1 : std_logic_vector(0 downto 0) := (others => '0');
	signal inst117_di2 : std_logic_vector(0 downto 0) := (others => '0');
	signal inst117_sum : std_logic_vector(3 downto 0) := (others => '0');

	component bitheap_117_4 is
		port (
			ci  : in  std_logic;
			di0 : in  std_logic_vector(6 downto 0);
			di1 : in  std_logic_vector(0 downto 0);
			di2 : in  std_logic_vector(0 downto 0);
			do  : out std_logic_vector(3 downto 0);
			co  : out std_logic_vector(3 downto 0);
			sum : out std_logic_vector(3 downto 0)
		);
	end component;

begin

	assert BITS <= 9 report "Number of inputs too large for module popcount_9_4" severity failure;

	sigBits(BITS-1 downto 0) <= bits_in;

	lut32 : LUT6_2
		generic map (
			INIT => x"00000096000000E8"
		)
		port map (
			O6 => sum3(0),
			O5 => sum3(1),
			I0 => sigBits(0),
			I1 => sigBits(1),
			I2 => sigBits(2),
			I3 => '0',
			I4 => '0',
			I5 => '1'   -- To have something different for O6 and O5
		);

	inst117_di0    <= sigBits(8 downto 3) & sum3(0);
	inst117_di1(0) <= sum3(1);
	inst117_di2(0) <= '0';

	comp117 : bitheap_117_4
		port map (
			ci  => '0',
			di0 => inst117_di0,
			di1 => inst117_di1,
			di2 => inst117_di2,
			do  => open,
			co  => open,
			sum => inst117_sum
		);

	sum <= std_logic_vector(resize(unsigned(inst117_sum), WOUT));

end architecture;

