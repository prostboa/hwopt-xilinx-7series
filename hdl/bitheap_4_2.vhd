
-- This is a compressor 4:2
-- It produces two partial sums
-- Total WDATA+1 luts (+1 lut with sign support)
-- Delay 1 lut + carry (+1 lut with sign support)

-- Without sign extension support :
-- Output res_sum actually has size WDATA+1 bits
-- Output res_c   actually has size WDATA+1 bits, bit 0 is always zero

-- With sign extension support :
-- Output res_sum actually has size WDATA+3 bits
-- Output res_c   actually has size WDATA+2 bits, bit 0 is always zero, bit WDATA+1 is always zero (positive number)

-- Recommendation : Use the input data_2 only if you are sure about how the CI inputs of CARRY4 are routed by Vivado
-- Vivado 2017.2 moves the LUT functionality to another LUT, the original one is used as passthrough (1 more LUT in resources and delay)

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT primitives
library unisim;
use unisim.vcomponents.all;

entity bitheap_4_2 is
	generic (
		WDATA : natural := 8;
		WOUT  : natural := 11;
		NBIN  : natural := 4;
		SIGN  : boolean := false
	);
	port (
		data_in : in  std_logic_vector(NBIN*WDATA-1 downto 0);
		data_1  : in  std_logic;
		data_2  : in  std_logic;
		sign_in : in  std_logic;
		res_sum : out std_logic_vector(WOUT-1 downto 0);
		res_c   : out std_logic_vector(WOUT-1 downto 0)
	);
end bitheap_4_2;

architecture synth of bitheap_4_2 is

	-- Utility function to calculate minimum of two values
	function min(a, b : natural) return natural is
		variable m : natural := 0;
	begin
		m := a when a <= b else b;
		return m;
	end function;

	-- The number of CARRY4 chunks needed (need 1 more LUT than size of inputs)
	constant NBC4 : natural := (min(WDATA + 1, WOUT) + 3) / 4;

	signal wrap_o  : std_logic_vector(4*NBC4-1 downto 0) := (others => '0');
	signal wrap_co : std_logic_vector(4*NBC4-1 downto 0) := (others => '0');
	signal wrap_o5 : std_logic_vector(WDATA downto 0) := (others => '0');

begin

	assert NBIN <= 4 report "Entity bitheap_4_2 : Number of inputs too large" severity failure;

	-- Generate blocks of 4:2 compressors around one CARRY4
	gen_c4 : for c in 0 to NBC4-1 generate

		-- Signals for CARRY inputs
		signal sig_cy : std_logic := '0';
		signal sig_ci : std_logic := '0';
		signal sig_di : std_logic_vector(3 downto 0) := (others => '0');
		signal sig_s  : std_logic_vector(3 downto 0) := (others => '0');

	begin

		-- The compressor LUTs

		gen_luts : for l in 0 to 3 generate
			signal lut_in : std_logic_vector(4 downto 0) := (others => '0');
		begin

			gen_lut : if 4*c+l <= WDATA generate

				gen_in : for i in 0 to NBIN-1 generate
					gen_ext : if 4*c+l < WDATA generate
						lut_in(i) <= data_in(i*WDATA + 4*c + l);
					elsif SIGN = true generate
						lut_in(i) <= data_in(i*WDATA + WDATA-1);
					else generate
						lut_in(i) <= '0';
					end generate;
				end generate;

				lut_in(4) <= sign_in when (SIGN = true) and (4*c+l >= WDATA) else '1';

				lut : LUT6_2
					generic map (
						INIT => x"69960000E8E80000"
					)
					port map (
						O6 => sig_s(l),
						O5 => wrap_o5(4*c + l),
						I0 => lut_in(0),
						I1 => lut_in(1),
						I2 => lut_in(2),
						I3 => lut_in(3),
						I4 => lut_in(4),
						I5 => '1'   -- To have something different for O6 and O5
					);

			end generate;

			sig_di(l) <= lut_in(3);

		end generate;

		-- The CARRY4

		gen_ci : if c = 0 generate
			sig_cy <= data_2;
		else generate
			sig_ci <= wrap_co(4*(c-1) + 3);
		end generate;

		c4 : CARRY4
			port map (
				CO     => wrap_co(4*(c+1)-1 downto 4*c),
				O      => wrap_o (4*(c+1)-1 downto 4*c),
				DI     => sig_di,
				S      => sig_s,
				CI     => sig_ci,
				CYINIT => sig_cy
			);

	end generate;

	-- Fix sign bits if needed

	gen_sign : if SIGN = false generate

		res_sum <= std_logic_vector(resize(unsigned(wrap_o(WDATA downto 0)), WOUT));

	elsif WOUT <= WDATA + 1 generate

		-- Note : Using a plain selection of bits instead of resize(signed())
		-- To intentionally silent GHDL overflow errors that happen with resize()
		res_sum <= wrap_o(WOUT-1 downto 0);

	else generate

		-- Signed case : Fix the 2 MSBs of partial result sum
		signal sig2 : std_logic := '0';
		signal sig1 : std_logic := '0';

		signal lut_in : std_logic_vector(3 downto 0) := (others => '0');

	begin

		lut_in(0) <= wrap_o(WDATA-1);
		lut_in(1) <= wrap_o5(WDATA-1);
		lut_in(2) <= wrap_o(WDATA);
		lut_in(3) <= sign_in;

		lut: LUT6_2
			generic map (
				INIT => x"0000FC0000003C00"
			)
			port map (
				O6 => sig2,
				O5 => sig1,
				I0 => lut_in(0),
				I1 => lut_in(1),
				I2 => lut_in(2),
				I3 => lut_in(3),
				I4 => '0',
				I5 => '1'  -- To have something different for O6 and O5
			);

		res_sum <= std_logic_vector(resize(signed(sig2 & sig1 & wrap_o(WDATA downto 0)), WOUT));

	end generate;

	-- Set final outputs

	res_c <= std_logic_vector(resize(unsigned(wrap_o5(WDATA-1 downto 0) & data_1), WOUT));

end architecture;

