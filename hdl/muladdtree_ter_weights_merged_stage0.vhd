
-- This is special first stage adder / ALU for ADD operator that also does ternary multiplication
-- Optimized at netlist level for Xilinx primitives

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity muladdtree_ter_weights_merged_stage0 is
	generic(
		WDATA : natural := 8;
		WOUT  : natural := 9
	);
	port(
		mA  : in  std_logic_vector(1 downto 0);
		mB  : in  std_logic_vector(1 downto 0);
		inA : in  std_logic_vector(WDATA-1 downto 0);
		inB : in  std_logic_vector(WDATA-1 downto 0);
		res : out std_logic_vector(WOUT-1 downto 0);
		neg : out std_logic
	);
end muladdtree_ter_weights_merged_stage0;

architecture synth of muladdtree_ter_weights_merged_stage0 is

	-- Utility function to calculate minimum of two values
	function min(a, b : natural) return natural is
		variable m : natural := 0;
	begin
		m := a when a <= b else b;
		return m;
	end function;

	constant W : natural := min(WDATA+1, WOUT);

	-- Truth table for command generation (bit 0 of cmd is carry input of first CARRY4)
	--
	--    | mB1 mB0 mA1 mA0 | neg cmd |
	-- ================================
	--  0 |   0   0   0   0 |  0  000 |  0 / 0
	--  1 |   0   0   0   1 |  0  010 |  0 / 1
	--  2 |   0   0   1   0 |  0  000 |  INVALID TERNARY
	--  3 |   0   0   1   1 |  0  001 |  0 / -1
	--  4 |   0   1   0   0 |  0  100 |  1 / 0
	--  5 |   0   1   0   1 |  0  110 |  1 / 1
	--  6 |   0   1   1   0 |  0  000 |  INVALID TERNARY
	--  7 |   0   1   1   1 |  0  011 |  1 / -1
	--  -------------------------------
	--  8 |   1   0   0   0 |  0  000 |  INVALID TERNARY
	--  9 |   1   0   0   1 |  0  000 |  INVALID TERNARY
	-- 10 |   1   0   1   0 |  0  000 |  INVALID TERNARY
	-- 11 |   1   0   1   1 |  0  000 |  INVALID TERNARY
	-- 12 |   1   1   0   0 |  0  101 |  -1 / 0
	-- 13 |   1   1   0   1 |  0  111 |  -1 / 1
	-- 14 |   1   1   1   0 |  0  000 |  INVALID TERNARY
	-- 15 |   1   1   1   1 |  1  110 |  -1 / -1 => negate and use cmd for 1 / 1

	type cmd_type is array (0 to 15) of std_logic_vector(3 downto 0);
	constant cmd_lut : cmd_type := (
		"0000", "0010", "0000", "0001",
		"0100", "0110", "0000", "0011",
		"0000", "0000", "0000", "0000",
		"0101", "0111", "0000", "1110"
	);

	signal cmd4_in : std_logic_vector(3 downto 0) := (others => '0');
	signal cmd4    : std_logic_vector(3 downto 0) := (others => '0');

	signal inA_wrap : std_logic_vector(W-1 downto 0);
	signal inB_wrap : std_logic_vector(W-1 downto 0);

	-- The number of CARRY4 primitives to instantiate
	constant CNT_NBC4 : natural := (W + 3) / 4;

	-- Temp signals to connect the CARRY4 components, for ALU
	-- Initialization to "don't care" for partial CARRY4 utilization
	signal inst_c4_o      : std_logic_vector(4*CNT_NBC4-1 downto 0) := (others => '-');
	signal inst_c4_co     : std_logic_vector(4*CNT_NBC4-1 downto 0) := (others => '-');
	signal inst_c4_di     : std_logic_vector(4*CNT_NBC4-1 downto 0) := (others => '-');
	signal inst_c4_s      : std_logic_vector(4*CNT_NBC4-1 downto 0) := (others => '-');
	signal inst_c4_ci     : std_logic_vector(CNT_NBC4-1 downto 0) := (others => '-');
	signal inst_c4_cyinit : std_logic_vector(CNT_NBC4-1 downto 0) := (others => '-');

begin

	-- Generate the command signal
	cmd4_in <= mB & mA;
	cmd4 <= cmd_lut( to_integer(unsigned(cmd4_in)) );

	-- Instantiation of the LUT / CARRY4 components for ALU
	-- Truth table for LUT contents
	--
	--    | B A  c2 c1 c0 |  O6  O5  |  O6 = input S (data), O5 = input DI (carry in)
	-- =====================================
	--  0 | 0 0   0  0  0 |   0   0  |  mB / mA =  0 /  0  =>  clr B / clr A
	--  1 | 0 0   0  0  1 |   1   0  |  mB / mA =  0 / -1  =>  clr B / not A
	--  2 | 0 0   0  1  0 |   0   0  |  mB / mA =  0 /  1  =>  clr B /     A
	--  3 | 0 0   0  1  1 |   1   0  |  mB / mA =  1 / -1  =>      B / not A
	--  4 | 0 0   1  0  0 |   0   0  |  mB / mA =  1 /  0  =>      B / clr A
	--  5 | 0 0   1  0  1 |   1   0  |  mB / mA = -1 /  0  =>  not B / clr A
	--  6 | 0 0   1  1  0 |   0   0  |  mB / mA =  1 /  1  =>      B /     A
	--  7 | 0 0   1  1  1 |   1   0  |  mB / mA = -1 /  1  =>  not B /     A
	--  -------------------------------
	--  8 | 0 1   0  0  0 |   0   0  |  clr B / clr A
	--  9 | 0 1   0  0  1 |   0   0  |  clr B / not A
	-- 10 | 0 1   0  1  0 |   1   0  |  clr B /     A
	-- 11 | 0 1   0  1  1 |   0   0  |      B / not A
	-- 12 | 0 1   1  0  0 |   0   0  |      B / clr A
	-- 13 | 0 1   1  0  1 |   1   0  |  not B / clr A
	-- 14 | 0 1   1  1  0 |   1   0  |      B /     A
	-- 15 | 0 1   1  1  1 |   0   1  |  not B /     A
	--  -------------------------------
	-- 16 | 1 0   0  0  0 |   0   0  |  clr B / clr A
	-- 17 | 1 0   0  0  1 |   1   0  |  clr B / not A
	-- 18 | 1 0   0  1  0 |   0   0  |  clr B /     A
	-- 19 | 1 0   0  1  1 |   0   1  |      B / not A
	-- 20 | 1 0   1  0  0 |   1   0  |      B / clr A
	-- 21 | 1 0   1  0  1 |   0   0  |  not B / clr A
	-- 22 | 1 0   1  1  0 |   1   0  |      B /     A
	-- 23 | 1 0   1  1  1 |   0   0  |  not B /     A
	--  -------------------------------
	-- 24 | 1 1   0  0  0 |   0   0  |  clr B / clr A
	-- 25 | 1 1   0  0  1 |   0   0  |  clr B / not A
	-- 26 | 1 1   0  1  0 |   1   0  |  clr B /     A
	-- 27 | 1 1   0  1  1 |   1   0  |      B / not A
	-- 28 | 1 1   1  0  0 |   1   0  |      B / clr A
	-- 29 | 1 1   1  0  1 |   0   0  |  not B / clr A
	-- 30 | 1 1   1  1  0 |   0   1  |      B /     A
	-- 31 | 1 1   1  1  1 |   1   0  |  not B /     A
	--
	-- Config for O6 is 32 upper bits, O5 is 32 lower bits: 9C5264AA 40088000

	inA_wrap <= std_logic_vector(resize(signed(inA), W));
	inB_wrap <= std_logic_vector(resize(signed(inB), W));

	-- The LUTs of the ALU
	gen_luts: for i in 0 to W-1 generate

		i_lut: LUT6_2
			generic map (
				INIT => x"9C5264AA40088000"
			)
			port map (
				O6 => inst_c4_s(i),
				O5 => inst_c4_di(i),
				I0 => cmd4(0),
				I1 => cmd4(1),
				I2 => cmd4(2),
				I3 => inA_wrap(i),
				I4 => inB_wrap(i),
				I5 => '1'   -- To have something different for O6 and O5
			);

	end generate;

	-- Generate the CARRY4 components
	gen_c4: for i in 0 to CNT_NBC4-1 generate

		i_c4: CARRY4
			port map (
				CO     => inst_c4_co(4*(i+1)-1 downto 4*i),
				O      => inst_c4_o (4*(i+1)-1 downto 4*i),
				DI     => inst_c4_di(4*(i+1)-1 downto 4*i),
				S      => inst_c4_s (4*(i+1)-1 downto 4*i),
				CI     => inst_c4_ci(i),
				CYINIT => inst_c4_cyinit(i)
			);

	end generate;

	-- Inter-CARRY4 connections
	gen_c4_conn: for i in 1 to CNT_NBC4-1 generate

		inst_c4_ci(i)     <= inst_c4_co(4*i-1);
		inst_c4_cyinit(i) <= '0';  -- Always propagate the carry

	end generate;

	inst_c4_ci(0)     <= '0';  -- Unused
	inst_c4_cyinit(0) <= cmd4(0);  -- For subtraction: the carry is not(borrow)

	-- Data output
	res <= std_logic_vector(resize(signed(inst_c4_o(W-1 downto 0)), WOUT));
	neg <= cmd4(3);

end architecture;

