
-- This is a multiplier component
-- It multiplies an input value by a ternary weight
-- Implementation is optimized at netlist level for Xilinx primitives

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity mul_ter_weight is
	generic(
		WDATA : natural := 12
	);
	port(
		weight_in : in  std_logic_vector(1 downto 0);
		data_in   : in  std_logic_vector(WDATA-1 downto 0);
		data_out  : out std_logic_vector(WDATA-1 downto 0)
	);
end mul_ter_weight;

architecture synth of mul_ter_weight is
begin

	-- 2-bit input
	gen_2b: if WDATA = 2 generate

		lut: LUT6_2
			generic map (
				INIT => x"000020800000A0A0"
			)
			port map (
				O6 => data_out(1),
				O5 => data_out(0),
				I0 => data_in(0),
				I1 => data_in(1),
				I2 => weight_in(0),
				I3 => weight_in(1),
				I4 => '0',  -- Unused input
				I5 => '1'   -- To have something different for O6 and O5
			);

	end generate;

	-- Short inputs: Use a look-up table, no carry
	gen_short: if WDATA > 2 and WDATA <= 4 generate

		constant ROMSIZE : natural := 2**WDATA;
		type lut_type is array (0 to ROMSIZE-1) of std_logic_vector(WDATA-1 downto 0);

		function gen_lut_contents(phony : boolean) return lut_type is
			variable lutneg : lut_type;
		begin
			for i in 0 to ROMSIZE-1 loop
				lutneg(i) := std_logic_vector(to_unsigned(0, WDATA) - to_unsigned(i, WDATA));
			end loop;
			return lutneg;
		end function;

		constant lutneg : lut_type := gen_lut_contents(false);

	begin

		data_out <=
			data_in when weight_in = "01" else
			lutneg(to_integer(unsigned(data_in))) when weight_in = "11" else
			(others => '0');

	end generate;

	-- Wide inputs: Use a carry chain
	gen_wide: if WDATA > 4 generate

		-- The number of CARRY4 primitives to instantiate
		constant CNT_NBC4 : natural := (WDATA+3) / 4;

		-- Temp signals to connect the CARRY4 components, for ALU
		-- Initialization to "don't care" for partial CARRY4 utilization
		signal inst_c4_o      : std_logic_vector(4*CNT_NBC4-1 downto 0) := (others => '-');
		signal inst_c4_co     : std_logic_vector(4*CNT_NBC4-1 downto 0) := (others => '-');
		signal inst_c4_di     : std_logic_vector(4*CNT_NBC4-1 downto 0) := (others => '-');
		signal inst_c4_s      : std_logic_vector(4*CNT_NBC4-1 downto 0) := (others => '-');
		signal inst_c4_ci     : std_logic_vector(CNT_NBC4-1 downto 0) := (others => '-');
		signal inst_c4_cyinit : std_logic_vector(CNT_NBC4-1 downto 0) := (others => '-');

	begin

		-- Instantiation of the LUT / CARRY4 components for ALU of the accumulator
		--
		-- Truth table for LUT contents
		--
		--    |   w1   w0  data |  O6  O5  |  O6 = input S (data), O5 = input DI (carry in)
		-- =====================================
		--  0 |    0    0    0  |   0   0  |  ZERO
		--  1 |    0    0    1  |   0   0  |  ZERO
		--  2 |    0    1    0  |   0   0  |  PASS
		--  3 |    0    1    1  |   1   0  |  PASS
		--  4 |    1    0    0  |   0   0  |  INVALID
		--  5 |    1    0    1  |   0   0  |  INVALID
		--  6 |    1    1    0  |   1   0  |  NEG => O6 = not(data), O5 = 0
		--  7 |    1    1    1  |   0   0  |  NEG
		--
		-- Config for O6 is 32 upper bits, O5 is 32 lower bits: 00000048 00000000

		-- The LUTs of the ALU
		gen_luts: for l in 0 to WDATA-1 generate

			i_lut: LUT6_2
				generic map (
					INIT => x"0000004800000000"
				)
				port map (
					O6 => inst_c4_s(l),
					O5 => inst_c4_di(l),
					I0 => data_in(l),
					I1 => weight_in(0),
					I2 => weight_in(1),
					I3 => '0',  -- Unused
					I4 => '0',  -- Unused
					I5 => '1'   -- To have something different for O6 and O5
				);

		end generate;

		-- Generate the CARRY4 components
		gen_c4: for i in 0 to CNT_NBC4-1 generate

			i_c4: CARRY4
				port map (
					CO     => inst_c4_co(4*(i+1)-1 downto 4*i),
					O      => inst_c4_o (4*(i+1)-1 downto 4*i),
					DI     => inst_c4_di(4*(i+1)-1 downto 4*i),
					S      => inst_c4_s (4*(i+1)-1 downto 4*i),
					CI     => inst_c4_ci(i),
					CYINIT => inst_c4_cyinit(i)
				);

		end generate;

		-- Inter-CARRY4 connections
		gen_c4_conn: for i in 1 to CNT_NBC4-1 generate

			inst_c4_ci(i)     <= inst_c4_co(4*i-1);
			inst_c4_cyinit(i) <= '0';  -- Always propagate the carry

		end generate;

		inst_c4_ci(0)     <= '0';  -- Unused
		inst_c4_cyinit(0) <= weight_in(1);  -- For subtraction: the carry is not(borrow)  FIXME This +1 could be done in the first LUT

		-- Data output
		data_out <= inst_c4_o(WDATA-1 downto 0);

	end generate;  -- Wide data, use carry chain

end architecture;


