
-- This is a popcount 7b -> 3b
-- Total 3 LUTs, delay 1 LUT + 1 carry

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT primitives
library unisim;
use unisim.vcomponents.all;

entity popcount_7_3_carry4 is
	generic (
		BITS : natural := 7;
		WOUT : natural := 3
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount_7_3_carry4;

architecture synth of popcount_7_3_carry4 is

	signal sigBits : std_logic_vector(6 downto 0) := (others => '0');
	signal sigSum  : std_logic_vector(2 downto 0) := (others => '0');

	signal c4_o  : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_co : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_di : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_s  : std_logic_vector(3 downto 0) := (others => '0');

	constant INIT_LUT0 : bit_vector(63 downto 0)  := x"6996966996696996";
	constant INIT_LUT1 : bit_vector(63 downto 0)  := x"177E7EE8E8808000";

begin

	assert BITS <= 7 report "Number of inputs too large for module popcount_7_3_carry4" severity failure;

	sigBits(BITS-1 downto 0) <= bits_in;

	-- Input index 0

	lutA: LUT6_2
		generic map (
			INIT => INIT_LUT0
		)
		port map (
			O6 => c4_s(0),
			O5 => c4_di(0),
			I0 => sigBits(0),
			I1 => sigBits(1),
			I2 => sigBits(2),
			I3 => sigBits(3),
			I4 => sigBits(4),
			I5 => sigBits(5)
		);

	lutB: LUT6_2
		generic map (
			INIT => INIT_LUT1
		)
		port map (
			O6 => c4_s(1),
			O5 => c4_di(1),
			I0 => sigBits(0),
			I1 => sigBits(1),
			I2 => sigBits(2),
			I3 => sigBits(3),
			I4 => sigBits(4),
			I5 => '1'   -- To have something different for O6 and O5
		);

	-- The CARRY4

	c4: CARRY4
		port map (
			CO     => c4_co,
			O      => c4_o,
			DI     => c4_di,
			S      => c4_s,
			CI     => '0',
			CYINIT => sigBits(6)
		);

	-- Output ports

	sigSum <= c4_co(1 downto 1) & c4_o(1 downto 0);

	sum <= std_logic_vector(resize(unsigned(sigSum), WOUT));

end architecture;

