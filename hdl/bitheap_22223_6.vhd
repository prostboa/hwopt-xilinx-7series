
-- This is a compressor 2,2,2,2,3:5
-- Total 4 LUTs, delay 1 LUT + 1 carry

library ieee;
use ieee.std_logic_1164.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity bitheap_22223_6 is
	port (
		di0 : in  std_logic_vector(2 downto 0);
		di1 : in  std_logic_vector(1 downto 0);
		di2 : in  std_logic_vector(1 downto 0);
		di3 : in  std_logic_vector(1 downto 0);
		di4 : in  std_logic_vector(1 downto 0);
		sum : out std_logic_vector(5 downto 0)
	);
end bitheap_22223_6;

architecture synth of bitheap_22223_6 is

	signal c4_o  : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_co : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_di : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_s  : std_logic_vector(3 downto 0) := (others => '0');

begin

	-- First stage : generate S0 and S1

	lutF : LUT6_2
		generic map (
			INIT => x"E81717E896969696"
		)
		port map (
			O6 => sum(1),
			O5 => sum(0),
			I0 => di0(0),
			I1 => di0(1),
			I2 => di0(2),
			I3 => di1(0),
			I4 => di1(1),
			I5 => '1'   -- To have something different for O6 and O5
		);

	-- Inputs index 2

	lutA : LUT6_2
		generic map (
			INIT => x"001717FFFFE8E800"
		)
		port map (
			O6 => c4_s(0),
			O5 => c4_di(0),
			I0 => di0(0),
			I1 => di0(1),
			I2 => di0(2),
			I3 => di1(0),
			I4 => di1(1),
			I5 => di2(0)
		);

	-- Inputs index 3

	lutB : LUT6_2
		generic map (
			INIT => x"0000000600000008"
		)
		port map (
			O6 => c4_s(1),
			O5 => c4_di(1),
			I0 => di3(0),
			I1 => di3(1),
			I2 => '0',  -- Unused
			I3 => '0',  -- Unused
			I4 => '0',  -- Unused
			I5 => '1'   -- To have something different for O6 and O5
		);

	-- Inputs index 4

	lutC : LUT6_2
		generic map (
			INIT => x"0000000600000008"
		)
		port map (
			O6 => c4_s(1),
			O5 => c4_di(1),
			I0 => di4(0),
			I1 => di4(1),
			I2 => '0',  -- Unused
			I3 => '0',  -- Unused
			I4 => '0',  -- Unused
			I5 => '1'   -- To have something different for O6 and O5
		);

	-- The CARRY4

	c4 : CARRY4
		port map (
			CO     => c4_co,
			O      => c4_o,
			DI     => c4_di,
			S      => c4_s,
			CI     => '0',
			CYINIT => di2(1)
		);

	-- Output ports

	sum(5 downto 2) <= c4_co(2) & c4_o(2 downto 0);

end architecture;

