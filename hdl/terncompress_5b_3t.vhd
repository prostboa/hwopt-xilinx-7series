
-- This is a ternary-to-binary decompressor 5b -> 3T
-- Based on look-up tables

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity terncompress_5b_3t is
	port(
		bin_in  : in  std_logic_vector(4 downto 0);
		ter_out : out std_logic_vector(5 downto 0)
	);
end terncompress_5b_3t;

architecture synth of terncompress_5b_3t is

	constant NTER      : natural := 3;
	constant BIN_NBITS : natural := 5;
	constant TER_NBITS : natural := 2 * NTER;

	constant TER_BITCOMBI : natural := 2 ** TER_NBITS;

	constant ROMSIZE  : natural := 2 ** BIN_NBITS;
	constant ROMWDATA : natural := TER_NBITS;
	type lut_type is array (0 to ROMSIZE-1) of std_logic_vector(ROMWDATA-1 downto 0);

	function is_tern_code(vec : std_logic_vector) return boolean is
	begin
		for t in 0 to NTER-1 loop
			if vec(2*t+1 downto 2*t) = "10" then
				return false;
			end if;
		end loop;
		return true;
	end function;

	function gen_lut_contents(phony : natural) return lut_type is
		variable lut : lut_type;
		variable vec : std_logic_vector(TER_NBITS-1 downto 0);
		variable b   : natural;
	begin
		for i in 0 to ROMSIZE-1 loop
			lut(i) := (others => '0');
		end loop;
		b := 0;
		for i in 0 to TER_BITCOMBI-1 loop
			vec := std_logic_vector(to_unsigned(i, TER_NBITS));
			if is_tern_code(vec) = true then
				lut(b) := vec;
				b := b + 1;
			end if;
		end loop;
		return lut;
	end function;

	constant lut : lut_type := gen_lut_contents(ROMWDATA);

begin

	ter_out <= lut( to_integer(unsigned(bin_in)) );

end architecture;

