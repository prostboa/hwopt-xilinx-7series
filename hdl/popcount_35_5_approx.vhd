
-- This is a popcount 35b -> 5b, the result is approximate, by design -16.7% to +0% error
-- Total size : 3x2 + 2x3 + 6 = 18 LUTs
-- Delay : 4 LUTs + 1 carry propag

-- Architecture : first 3x 2-lut 7:3 + 2x 3-lut 7:3 compressors (to balance distribution of LUTs associated to carry4)
-- Then a special-purpose compressor 5,5,5:5 that brings underestimation

-- Analysis of worst cases for under-estimation rate :
-- The 5,5,5:5 emits 20 for a theoretical result of 24, this is -16.7% error rate

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity popcount_35_5_approx is
	generic (
		BITS : natural := 35;
		WOUT : natural := 5
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount_35_5_approx;

architecture synth of popcount_35_5_approx is

	signal sigBits : std_logic_vector(34 downto 0) := (others => '0');

	signal c1_sum : std_logic_vector(2 downto 0) := (others => '0');
	signal c2_sum : std_logic_vector(2 downto 0) := (others => '0');
	signal c3_sum : std_logic_vector(2 downto 0) := (others => '0');
	signal c4_sum : std_logic_vector(2 downto 0) := (others => '0');
	signal c5_sum : std_logic_vector(2 downto 0) := (others => '0');

	signal rec_di0 : std_logic_vector(4 downto 0) := (others => '0');
	signal rec_di1 : std_logic_vector(4 downto 0) := (others => '0');
	signal rec_di2 : std_logic_vector(4 downto 0) := (others => '0');
	signal rec_sum : std_logic_vector(4 downto 0) := (others => '0');

	component popcount_7_3_carry4 is
		generic (
			BITS : natural := 7;
			WOUT : natural := 3
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component popcount_7_3 is
		generic (
			BITS : natural := 7;
			WOUT : natural := 3
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component bitheap_555_5_special is
		port (
			ci  : in  std_logic;
			di0 : in  std_logic_vector(4 downto 0);
			di1 : in  std_logic_vector(4 downto 0);
			di2 : in  std_logic_vector(4 downto 0);
			sum : out std_logic_vector(4 downto 0)
		);
	end component;

begin

	assert BITS <= 35 report "Number of inputs too large for module popcount_35_5_approx" severity failure;

	sigBits(BITS-1 downto 0) <= bits_in;

	-- First stage : Compressors 7:3

	comp73_1 : popcount_7_3_carry4
		port map (
			bits_in => sigBits(6 downto 0),
			sum     => c1_sum
		);

	comp73_2 : popcount_7_3_carry4
		port map (
			bits_in => sigBits(13 downto 7),
			sum     => c2_sum
		);

	comp73_3 : popcount_7_3_carry4
		port map (
			bits_in => sigBits(20 downto 14),
			sum     => c3_sum
		);

	comp73_4 : popcount_7_3
		port map (
			bits_in => sigBits(27 downto 21),
			sum     => c4_sum
		);

	comp73_5 : popcount_7_3
		port map (
			bits_in => sigBits(34 downto 28),
			sum     => c5_sum
		);

	-- Second stage : Special-purpose compressor 555:5

	rec_di0(0) <= c1_sum(0);
	rec_di0(1) <= c2_sum(0);
	rec_di0(2) <= c3_sum(0);
	rec_di0(3) <= c4_sum(0);
	rec_di0(4) <= c5_sum(0);

	rec_di1(0) <= c1_sum(1);
	rec_di1(1) <= c2_sum(1);
	rec_di1(2) <= c3_sum(1);
	rec_di1(3) <= c4_sum(1);
	rec_di1(4) <= c5_sum(1);

	rec_di2(0) <= c1_sum(2);
	rec_di2(1) <= c2_sum(2);
	rec_di2(2) <= c3_sum(2);
	rec_di2(3) <= c4_sum(2);
	rec_di2(4) <= c5_sum(2);

	comp555 : bitheap_555_5_special
		port map (
			ci  => '0',
			di0 => rec_di0,
			di1 => rec_di1,
			di2 => rec_di2,
			sum => rec_sum
		);

	-- Output portè

	sum <= std_logic_vector(resize(unsigned(rec_sum), WOUT));

end architecture;

