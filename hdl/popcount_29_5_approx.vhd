
-- This is a popcount 29b -> 5b, the result is approximate, by design 0% to +25% error
-- Total size : 4 + 4 + 6 = 14 LUTs
-- Delay : 3 LUTs + 2*carry propag

-- Architecture : first two 6,0,7:5 compressors that handle 2x13 = 26 bits
-- Then a recoding stage that also adds the 3 extra input bits

-- Recoding operation is normally :
--
--     co1  o1  o0
--   + co3  o3  o2
--   -         co1
--   =============
--             sum
--
-- Approximation : skip the subtraction of co1, this will result in overestimation
--
--     co1  o1  o0
--   + co3  o3  o2
--   =============
--             sum
--
-- These 2 partial results fit into a compressor 4,4,7:5
-- (cost is 6 LUT6, delay 2 LUT + carry)

-- Analysis of worst cases for over-estimation rate :
--   When one 607:5 has 4 bits in input position 0, result is 5 -> +25% error rate

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity popcount_29_5_approx is
	generic (
		BITS : natural := 29;
		WOUT : natural := 5
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount_29_5_approx;

architecture synth of popcount_29_5_approx is

	signal sigBits : std_logic_vector(28 downto 0) := (others => '0');

	signal c1_do : std_logic_vector(3 downto 0) := (others => '0');
	signal c1_co : std_logic_vector(3 downto 0) := (others => '0');
	signal c2_do : std_logic_vector(3 downto 0) := (others => '0');
	signal c2_co : std_logic_vector(3 downto 0) := (others => '0');

	signal rec_di0 : std_logic_vector(6 downto 0) := (others => '0');
	signal rec_di1 : std_logic_vector(3 downto 0) := (others => '0');
	signal rec_di2 : std_logic_vector(3 downto 0) := (others => '0');
	signal rec_sum : std_logic_vector(4 downto 0) := (others => '0');

	component bitheap_607_5 is
		port (
			cy  : in  std_logic;
			ci  : in  std_logic;
			di0 : in  std_logic_vector(5 downto 0);
			di2 : in  std_logic_vector(5 downto 0);
			do  : out std_logic_vector(3 downto 0);
			co  : out std_logic_vector(3 downto 0)
		);
	end component;

	component bitheap_447_5 is
		port (
			ci  : in  std_logic;
			di0 : in  std_logic_vector(6 downto 0);
			di1 : in  std_logic_vector(3 downto 0);
			di2 : in  std_logic_vector(3 downto 0);
			sum : out std_logic_vector(4 downto 0)
		);
	end component;

begin

	assert BITS <= 29 report "Number of inputs too large for module popcount_29_5_approx" severity failure;

	sigBits(BITS-1 downto 0) <= bits_in;

	comp607_1 : bitheap_607_5
		port map (
			cy  => sigBits(12),
			ci  => '0',
			di0 => sigBits(5 downto 0),
			di2 => sigBits(11 downto 6),
			do  => c1_do,
			co  => c1_co
		);

	comp607_2 : bitheap_607_5
		port map (
			cy  => sigBits(25),
			ci  => '0',
			di0 => sigBits(18 downto 13),
			di2 => sigBits(24 downto 19),
			do  => c2_do,
			co  => c2_co
		);

	rec_di0(0) <= c1_do(0);
	rec_di0(1) <= c1_do(2);
	rec_di0(2) <= c2_do(0);
	rec_di0(3) <= c2_do(2);
	rec_di0(4) <= sigBits(26);
	rec_di0(5) <= sigBits(27);
	rec_di0(6) <= sigBits(28);

	rec_di1(0) <= c1_do(1);
	rec_di1(1) <= c1_do(3);
	rec_di1(2) <= c2_do(1);
	rec_di1(3) <= c2_do(3);

	rec_di2(0) <= c1_co(1);
	rec_di2(1) <= c1_co(3);
	rec_di2(2) <= c2_co(1);
	rec_di2(3) <= c2_co(3);

	comp447 : bitheap_447_5
		port map (
			ci  => '0',
			di0 => rec_di0,
			di1 => rec_di1,
			di2 => rec_di2,
			sum => rec_sum
		);

	-- Output ports

	sum <= std_logic_vector(resize(unsigned(rec_sum), WOUT));

end architecture;

