
-- This is a popcount 13b -> 4b
-- Total size : 4 + 2 = 6 LUTs
-- Delay : 3 LUTs + carry propag

-- Architecture : first one 6,0,7:5 compressor that handles 13 bits
-- Then a recoding stage in 2 LUTs

-- Recoding operation :
--
--     co1  o1  o0
--   + co3  o3  o2
--   -         co1
--   =============
--             sum
--
-- This is simplified into the following operation, which fits into 2 LUTs :
-- (with delay 2 LUTs)
--
--          o1  o0
--   + co3  o3  o2
--   +     co1 co1
--   =============
--             sum
--
-- Note : Recoding is also possible with 1 layer of LUTs, with 1 more LUT
-- This would lead to a total of 7 LUTs, delay of 2 LUTs + carry propag

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT primitives
library unisim;
use unisim.vcomponents.all;

entity popcount_13_4 is
	generic (
		BITS : natural := 13;
		WOUT : natural := 4
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount_13_4;

architecture synth of popcount_13_4 is

	signal sigBits : std_logic_vector(12 downto 0) := (others => '0');
	signal sigSum  : std_logic_vector(3 downto 0) := (others => '0');

	signal do : std_logic_vector(3 downto 0) := (others => '0');
	signal co : std_logic_vector(3 downto 0) := (others => '0');

	component bitheap_607_5 is
		port (
			cy  : in  std_logic;
			ci  : in  std_logic;
			di0 : in  std_logic_vector(5 downto 0);
			di2 : in  std_logic_vector(5 downto 0);
			do  : out std_logic_vector(3 downto 0);
			co  : out std_logic_vector(3 downto 0)
		);
	end component;

begin

	assert BITS <= 13 report "Number of inputs too large for module popcount_13_4" severity failure;

	sigBits(BITS-1 downto 0) <= bits_in;

	comp607 : bitheap_607_5
		port map (
			cy  => sigBits(12),
			ci  => '0',
			di0 => sigBits(5 downto 0),
			di2 => sigBits(11 downto 6),
			do  => do,
			co  => co
		);

	lut01 : LUT6_2
		generic map (
			INIT => x"A5A55A5A36C9936C"
		)
		port map (
			O6 => sigSum(0),
			O5 => sigSum(1),
			I0 => do(0),
			I1 => do(1),
			I2 => do(2),
			I3 => do(3),
			I4 => co(1),
			I5 => '1'   -- To have something different for O6 and O5
		);

	lut23 : LUT6_2
		generic map (
			INIT => x"422BBDD4FDD44000"
		)
		port map (
			O6 => sigSum(2),
			O5 => sigSum(3),
			I0 => sigSum(1),
			I1 => do(1),
			I2 => do(3),
			I3 => co(1),
			I4 => co(3),
			I5 => '1'   -- To have something different for O6 and O5
		);

	sum <= std_logic_vector(resize(unsigned(sigSum), WOUT));

end architecture;

