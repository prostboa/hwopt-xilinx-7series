
-- This is a ternary adder 3x ter (signed only) -> 3b implemented using 2 LUT6

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity adder_3in_ter is
	generic (
		WOUT : natural := 3
	);
	port (
		inA : in  std_logic_vector(1 downto 0);
		inB : in  std_logic_vector(1 downto 0);
		inC : in  std_logic_vector(1 downto 0);
		res : out std_logic_vector(WOUT-1 downto 0)
	);
end adder_3in_ter;

architecture synth of adder_3in_ter is

	signal sigC0  : std_logic := '0';
	signal sigSum : std_logic_vector(2 downto 0) := (others => '0');

	constant INIT_LUT1 : bit_vector(63 downto 0) := x"000000E800000096";
	constant INIT_LUT2 : bit_vector(63 downto 0) := x"0000E87E00006996";

begin

	lut1: LUT6_2
		generic map (
			INIT => INIT_LUT1
		)
		port map (
			O6 => sigC0,
			O5 => sigSum(0),
			I0 => inA(0),
			I1 => inB(0),
			I2 => inC(0),
			I3 => '0',  -- Unused input
			I4 => '0',  -- Unused input
			I5 => '1'   -- To have something different for O6 and O5
		);

	lut2: LUT6_2
		generic map (
			INIT => INIT_LUT2
		)
		port map (
			O6 => sigSum(2),
			O5 => sigSum(1),
			I0 => inA(1),
			I1 => inB(1),
			I2 => inC(1),
			I3 => sigC0,
			I4 => '0',  -- Unused input
			I5 => '1'   -- To have something different for O6 and O5
		);

	res <= std_logic_vector(resize(signed(sigSum), WOUT));

end architecture;

