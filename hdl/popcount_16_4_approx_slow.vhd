
-- This is a popcount 16b -> 4b, the result is approximate, by design -20% to +25% error
-- Total size : 4 + 3 = 7 LUTs
-- Delay : 3 LUTs + 2*carry propag

-- Architecture : first one 6,0,7:5 compressor that handles 13 bits
-- Then a recoding stage that also adds the 3 extra input bits

-- Recoding operation :
--
--     co1  o1  o0
--   + co3  o3  o2
--   -         co1
--   +         i13
--   +         i14
--   +         i15
--   =============
--             sum
--
-- First approximation : skip the subtraction of co1, this will result in overestimation
--
--     co1  o1  o0
--   + co3  o3  o2
--   +         i13
--   +         i14
--   +         i15
--   =============
--             sum
--
-- This fits into a special-purpose compressor 2,2,5:4 that is also approximate (it brings underestimation)
-- (cost is 3 LUT6, delay 2 LUT + carry)

-- Limitation : Result may be up to 12% lower or 25% higher than expected
-- Results up to 3 are correct, results above may be too low by 1 depending on input combinations :
-- Result  4 is approximated to  5
-- Result  5 is approximated to  6
-- Result  6 is approximated to  7
-- Result  7 is approximated to  8
-- Result  8 is approximated to  7-9
-- Result  9 is approximated to  8-10
-- Result 10 is approximated to  9-11
-- Result 11 is approximated to 10-12
-- Result 12 is approximated to 11-13
-- Result 13 is approximated to 12-14
-- Result 14 is approximated to 13-15
-- Result 15 is approximated to 14
-- Result 16 is approximated to 15

-- Note : Alt implementation, of 225:4 brings a different distribution of error rate

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT and CARRY4 primitives
library unisim;
use unisim.vcomponents.all;

entity popcount_16_4_approx_slow is
	generic (
		BITS : natural := 16;
		WOUT : natural := 4
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount_16_4_approx_slow;

architecture synth of popcount_16_4_approx_slow is

	signal sigBits : std_logic_vector(15 downto 0) := (others => '0');

	signal c4_do : std_logic_vector(3 downto 0) := (others => '0');
	signal c4_co : std_logic_vector(3 downto 0) := (others => '0');

	signal rec_di0 : std_logic_vector(4 downto 0) := (others => '0');
	signal rec_di1 : std_logic_vector(1 downto 0) := (others => '0');
	signal rec_di2 : std_logic_vector(1 downto 0) := (others => '0');
	signal rec_sum : std_logic_vector(3 downto 0) := (others => '0');

	component bitheap_607_5 is
		port (
			cy  : in  std_logic;
			ci  : in  std_logic;
			di0 : in  std_logic_vector(5 downto 0);
			di2 : in  std_logic_vector(5 downto 0);
			do  : out std_logic_vector(3 downto 0);
			co  : out std_logic_vector(3 downto 0)
		);
	end component;

	component bitheap_225_4_special is
		port (
			ci  : in  std_logic;
			di0 : in  std_logic_vector(4 downto 0);
			di1 : in  std_logic_vector(1 downto 0);
			di2 : in  std_logic_vector(1 downto 0);
			sum : out std_logic_vector(3 downto 0)
		);
	end component;

begin

	assert BITS <= 16 report "Number of inputs too large for module popcount_16_4_approx_slow" severity failure;

	sigBits(BITS-1 downto 0) <= bits_in;

	comp607 : bitheap_607_5
		port map (
			cy  => sigBits(12),
			ci  => '0',
			di0 => sigBits(5 downto 0),
			di2 => sigBits(11 downto 6),
			do  => c4_do,
			co  => c4_co
		);

	rec_di0(0) <= c4_do(0);
	rec_di0(1) <= c4_do(2);
	rec_di0(2) <= sigBits(13);
	rec_di0(3) <= sigBits(14);
	rec_di0(4) <= sigBits(15);

	rec_di1(0) <= c4_do(1);
	rec_di1(1) <= c4_do(3);

	rec_di2(0) <= c4_co(1);
	rec_di2(1) <= c4_co(3);

	comp225 : bitheap_225_4_special
		port map (
			ci  => '0',
			di0 => rec_di0,
			di1 => rec_di1,
			di2 => rec_di2,
			sum => rec_sum
		);

	-- Output ports

	sum <= std_logic_vector(resize(unsigned(rec_sum), WOUT));

end architecture;

