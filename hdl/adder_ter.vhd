
-- This one ternary adder block, only supports 1 to 7 inputs

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity adder_ter is
	generic(
		NBIN  : natural := 4;
		WOUT  : natural := 4
	);
	port(
		data_in  : in  std_logic_vector(2*NBIN-1 downto 0);
		data_out : out std_logic_vector(WOUT-1 downto 0)
	);
end adder_ter;

architecture synth of adder_ter is

begin

	assert NBIN <= 7 report "Number of inputs too large for module adder_ter" severity failure;

	-- No hardware needed
	gen1 : if NBIN = 1 generate

		data_out <= std_logic_vector(resize(signed(data_in), WOUT));

	end generate;

	-- With this code, Vivado generates 2 LUTs
	gen2 : if NBIN = 2 generate

		data_out <= std_logic_vector(
			resize(signed(data_in(1 downto 0)), WOUT) +
			resize(signed(data_in(3 downto 2)), WOUT)
		);

	end generate;

	-- With this code, Vivado generates 3 LUTs (not optimal)
	gen3a : if NBIN = 3 and false generate

		data_out <= std_logic_vector(
			resize(signed(data_in(1 downto 0)), WOUT) +
			resize(signed(data_in(3 downto 2)), WOUT) +
			resize(signed(data_in(5 downto 4)), WOUT)
		);

	end generate;

	-- With this code, Vivado generates 2 LUTs
	gen3b : if NBIN = 3 and true generate

		component adder_3in_ter is
			generic (
				WOUT : natural := 3
			);
			port (
				inA : in  std_logic_vector(1 downto 0);
				inB : in  std_logic_vector(1 downto 0);
				inC : in  std_logic_vector(1 downto 0);
				res : out std_logic_vector(WOUT-1 downto 0)
			);
		end component;

	begin

		comp : adder_3in_ter
			generic map (
				WOUT => WOUT
			)
			port map (
				inA => data_in(1 downto 0),
				inB => data_in(3 downto 2),
				inC => data_in(5 downto 4),
				res => data_out
			);

	end generate;

	-- With this code, Vivado generates 5 LUTs (not optimal)
	gen4a : if NBIN = 4 and false generate

		data_out <= std_logic_vector(
			resize(signed(data_in(1 downto 0)), WOUT) +
			resize(signed(data_in(3 downto 2)), WOUT) +
			resize(signed(data_in(5 downto 4)), WOUT) +
			resize(signed(data_in(7 downto 6)), WOUT)
		);

	end generate;

	-- With this code, Vivado generates 6 LUTs (not optimal)
	gen4b : if NBIN = 4 and false generate

		signal add3 : signed(2 downto 0);

	begin

		add3 <=
			resize(signed(data_in(1 downto 0)), 3) +
			resize(signed(data_in(3 downto 2)), 3) +
			resize(signed(data_in(5 downto 4)), 3);

		data_out <= std_logic_vector(
			resize(add3, WOUT) +
			signed(data_in(7 downto 6))
		);

	end generate;

	-- With this code, Vivado generates 5 LUTs (not optimal)
	gen4c : if NBIN = 4 and false generate

		constant ROMSIZE : natural := 2 ** 5;
		constant ROMWDATA : natural := 4;
		type lut_type is array (0 to ROMSIZE-1) of std_logic_vector(ROMWDATA-1 downto 0);

		function gen_lut_contents(WDATA : natural) return lut_type is
			variable lut : lut_type;
			variable idx : natural;
			variable vec5 : std_logic_vector(4 downto 0);
		begin
			for i in 0 to ROMSIZE-1 loop
				lut(i) := (others => '0');
			end loop;
			for i3 in -3 to 3 loop
				for i1 in -1 to 1 loop
					vec5     := std_logic_vector(to_signed(i1, 2)) & std_logic_vector(to_signed(i3, 3));
					idx      := to_integer(unsigned(vec5));
					lut(idx) := std_logic_vector(to_signed(i1 + i3, ROMWDATA));
				end loop;
			end loop;
			return lut;
		end function;

		constant lut : lut_type := gen_lut_contents(ROMWDATA);

		signal add3 : std_logic_vector(2 downto 0);
		signal vec5 : std_logic_vector(4 downto 0);

	begin

		add3 <= std_logic_vector(
			resize(signed(data_in(1 downto 0)), 3) +
			resize(signed(data_in(3 downto 2)), 3) +
			resize(signed(data_in(5 downto 4)), 3)
		);

		vec5 <= data_in(7 downto 6) & add3;

		data_out <= std_logic_vector(resize(
			signed(
				lut(to_integer(unsigned(vec5)))
			), WOUT
		));

	end generate;

	-- With this code, Vivado generates 3 LUTs
	gen4d : if NBIN = 4 and true generate

		component adder_4in_ter is
			generic (
				WOUT : natural := 4
			);
			port (
				in_ter : in  std_logic_vector(4*2-1 downto 0);
				res    : out std_logic_vector(WOUT-1 downto 0)
			);
		end component;

	begin

		comp : adder_4in_ter
			generic map (
				WOUT => WOUT
			)
			port map (
				in_ter => data_in,
				res => data_out
			);

	end generate;

	-- With this code, Vivado generates 7 LUTs (not optimal)
	gen5a : if NBIN = 5 and false generate

		data_out <= std_logic_vector(
			resize(signed(data_in(1 downto 0)), WOUT) +
			resize(signed(data_in(3 downto 2)), WOUT) +
			resize(signed(data_in(5 downto 4)), WOUT) +
			resize(signed(data_in(7 downto 6)), WOUT) +
			resize(signed(data_in(9 downto 8)), WOUT)
		);

	end generate;

	-- With this code, Vivado generates 6 LUTs, and delay 2 LUTs
	gen5b : if NBIN = 5 and true generate

		component adder_5in_ter is
			generic (
				WOUT : natural := 5
			);
			port (
				in_ter : in  std_logic_vector(5*2-1 downto 0);
				res    : out std_logic_vector(WOUT-1 downto 0)
			);
		end component;

	begin

		comp : adder_5in_ter
			generic map (
				WOUT => WOUT
			)
			port map (
				in_ter => data_in,
				res => data_out
			);

	end generate;

	-- With this code, Vivado generates 12 LUTs (not optimal)
	gen6a : if NBIN = 6 and false generate

		data_out <= std_logic_vector(
			resize(signed(data_in(1 downto 0)), WOUT) +
			resize(signed(data_in(3 downto 2)), WOUT) +
			resize(signed(data_in(5 downto 4)), WOUT) +
			resize(signed(data_in(7 downto 6)), WOUT) +
			resize(signed(data_in(9 downto 8)), WOUT) +
			resize(signed(data_in(11 downto 10)), WOUT)
		);

	end generate;

	-- With this code, Vivado generates 6 LUTs + 1 CARRY4, and delay 2 LUTs + carry
	gen6b : if NBIN = 6 and true generate

		signal wrapin : std_logic_vector(7*2-1 downto 0) := (others => '0');

		component adder_7in_ter is
			generic (
				WOUT : natural := 4
			);
			port (
				in_ter : in  std_logic_vector(7*2-1 downto 0);
				res    : out std_logic_vector(WOUT-1 downto 0)
			);
		end component;

	begin

		wrapin(NBIN*2-1 downto 0) <= data_in;

		comp : adder_7in_ter
			generic map (
				WOUT => WOUT
			)
			port map (
				in_ter => wrapin,
				res => data_out
			);

	end generate;

	-- With this code, Vivado generates 13 LUTs (not optimal)
	gen7a : if NBIN = 7 and false generate

		data_out <= std_logic_vector(
			resize(signed(data_in(1 downto 0)), WOUT) +
			resize(signed(data_in(3 downto 2)), WOUT) +
			resize(signed(data_in(5 downto 4)), WOUT) +
			resize(signed(data_in(7 downto 6)), WOUT) +
			resize(signed(data_in(9 downto 8)), WOUT) +
			resize(signed(data_in(11 downto 10)), WOUT) +
			resize(signed(data_in(13 downto 12)), WOUT)
		);

	end generate;

	-- With this code, Vivado generates 6 LUTs + 1 CARRY4, and delay 2 LUTs + carry
	gen7b : if NBIN = 7 and true generate

		component adder_7in_ter is
			generic (
				WOUT : natural := 7
			);
			port (
				in_ter : in  std_logic_vector(7*2-1 downto 0);
				res    : out std_logic_vector(WOUT-1 downto 0)
			);
		end component;

	begin

		comp : adder_7in_ter
			generic map (
				WOUT => WOUT
			)
			port map (
				in_ter => data_in,
				res => data_out
			);

	end generate;

end architecture;

