
-- This is a multiplier + adder tree, for binary weights
-- It multiplies input values (arbitrary) by weights (binary), with operation AND

-- Architecture :
--   - one layer of multiplication+addition operations, combinational (radix is 2)
--   - then one adder tree

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity muladdtree_bin_weights is
	generic(
		WDATA : natural := 16;
		SDATA : boolean := false;
		NBIN  : natural := 16;
		WOUT  : natural := 12;
		-- Tag-related information
		TAGW  : natural := 1;
		TAGEN : boolean := false;
		TAGZC : boolean := false;
		-- How to add pipeline registers
		REGEN  : natural := 1;  -- 0=none, 1=all, else every EN stages
		REGIDX : natural := 0   -- Start index (from the leaves)
	);
	port(
		clk       : in  std_logic;
		clear     : in  std_logic;
		-- Weight and data input
		weight_in : in  std_logic_vector(NBIN-1 downto 0);
		data_in   : in  std_logic_vector(NBIN*WDATA-1 downto 0);
		-- Data output
		data_out  : out std_logic_vector(WOUT-1 downto 0);
		-- Tag, input and output
		tag_in    : in  std_logic_vector(TAGW-1 downto 0);
		tag_out   : out std_logic_vector(TAGW-1 downto 0)
	);
end muladdtree_bin_weights;

architecture synth of muladdtree_bin_weights is

	-- Utility function to calculate minimum of two values
	function min(a, b : natural) return natural is
		variable m : natural := 0;
	begin
		m := a when a <= b else b;
		return m;
	end function;

	-- Compute the most appropriate radix for individual combinational MAC blocks
	constant RADIX : natural := 2;

	-- The number of individual MAC blocks
	constant BLOCKS_NB   : natural := (NBIN + RADIX - 1) / RADIX;
	constant BLOCKS_WOUT : natural := min(WDATA+1, WOUT);

	-- Utility function to calculate next REGIDX
	function calc_regidx_next(REG_CREATE : boolean) return natural is
	begin
		if REGEN = 0 then
			return 0;
		end if;
		if REG_CREATE = true then
			return 0;
		end if;
		return (REGIDX + 1) mod REGEN;
	end function;

	-- Add pipeline registers when IDX = EN - 1
	-- Force creation of registers at last stage
	constant REG_CREATE  : boolean := (REGEN > 0) and ((REGIDX = REGEN-1) or (NBIN = 1));
	constant REGIDX_NEXT : natural := calc_regidx_next(REG_CREATE);

	signal regs, regs_n : std_logic_vector(BLOCKS_NB*BLOCKS_WOUT-1 downto 0);
	signal reg_tag : std_logic_vector(TAGW-1 downto 0);

	-- Utility function to perform resize
	function local_resize(A : std_logic_vector) return std_logic_vector is
	begin
		if SDATA = true then
			return std_logic_vector(resize(signed(A), BLOCKS_WOUT));
		end if;
		return std_logic_vector(resize(unsigned(A), BLOCKS_WOUT));
	end function;

	-- Utility function to perform add
	function local_add(inA : std_logic_vector; inB : std_logic_vector) return std_logic_vector is
	begin
		if SDATA = true then
			return std_logic_vector(resize(signed(inA), BLOCKS_WOUT) + resize(signed(inB), BLOCKS_WOUT));
		end if;
		return std_logic_vector(resize(unsigned(inA), BLOCKS_WOUT) + resize(unsigned(inB), BLOCKS_WOUT));
	end function;

	component addtree is
		generic(
			-- Data type and width
			WDATA : natural := 8;
			SDATA : boolean := true;
			NBIN  : natural := 20;
			WOUT  : natural := 12;
			-- User-specified radix, for testing purposes (0 means automatic)
			RADIX : natural := 0;
			-- Special considerations about data nature
			BINARY : boolean := false;
			BINARY_RADIX : natural := 0;
			TERNARY : boolean := false;
			TERNARY_RADIX : natural := 0;
			-- An optional tag
			TAGW  : natural := 1;
			TAGEN : boolean := true;
			TAGZC : boolean := true;
			-- How to add pipeline registers
			REGEN  : natural := 1;  -- 0=none, 1=all, else every EN stages
			REGIDX : natural := 0   -- Start index (from the leaves)
		);
		port(
			clk      : in  std_logic;
			clear    : in  std_logic;
			-- Data, input and output
			data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
			data_out : out std_logic_vector(WOUT-1 downto 0);
			-- Tag, input and output
			tag_in   : in  std_logic_vector(TAGW-1 downto 0);
			tag_out  : out std_logic_vector(TAGW-1 downto 0)
		);
	end component;

begin

	-- Instantiate the multiply-accumulate blocks

	gen_blocks : for i in 0 to BLOCKS_NB-1 generate
		constant BLOCKSIZE : natural := min(RADIX, NBIN - i*RADIX);

		-- Wrappers for inputs
		signal sig_w : std_logic_vector(BLOCKSIZE-1 downto 0) := (others => '0');
		signal sig_d : std_logic_vector(BLOCKSIZE*WDATA-1 downto 0) := (others => '0');

	begin

		-- Wrappers for inputs

		sig_w <= weight_in(i*RADIX+BLOCKSIZE-1 downto i*RADIX);
		sig_d <= data_in((i*RADIX+BLOCKSIZE)*WDATA-1 downto i*RADIX*WDATA);

		-- Block iplementation

		gen : if BLOCKSIZE <= 1 generate

			regs_n((i+1)*BLOCKS_WOUT-1 downto (i+0)*BLOCKS_WOUT) <= local_resize(sig_w(0) and sig_d);

		elsif BLOCKSIZE = 2 generate

			regs_n((i+1)*BLOCKS_WOUT-1 downto (i+0)*BLOCKS_WOUT) <= local_add(
				sig_w(1) and sig_d(2*WDATA-1 downto 1*WDATA),
				sig_w(0) and sig_d(1*WDATA-1 downto 0*WDATA)
			);

		else generate

			assert false report "Entity mactree_bin_weight : Unsupported radix value" severity failure;

		end generate;

	end generate;

	-- Create pipeline registers

	gen_regs_data : if REG_CREATE = false generate

		-- No pipeline registers, just passthrough
		regs <= regs_n;

	else generate

		process(clk)
		begin
			if rising_edge(clk) then

				regs <= regs_n;

			end if;
		end process;

	end generate;

	gen_regs_tag : if (REG_CREATE = false) or (TAGEN = false) generate

		-- No pipeline registers, just passthrough
		reg_tag <= tag_in when TAGEN = true else (others => '0');

	else generate

		process(clk)
		begin
			if rising_edge(clk) then

				reg_tag <= tag_in;
				if (clear = '1') and (TAGZC = true) then
					reg_tag <= (others => '0');
				end if;

			end if;
		end process;

	end generate;

	-- Instantiate the upper stages of the adder tree

	gen_notree : if BLOCKS_NB <= 1 generate

		data_out <= std_logic_vector(resize(unsigned(regs), WOUT));
		tag_out <= reg_tag;

	end generate;

	-- WARNING Many versions of Vivado (2017.2 up to 2021.1 at least) require special recursive instantiation pattern
	-- Vivado segfaults when the instantiation is in an "else generate" statement
	-- So, isolate that in the most direct "if generate" part of the statement
	gen_tree : if BLOCKS_NB > 1 generate

		adder : addtree
			generic map (
				-- Data type and width
				WDATA => BLOCKS_WOUT,
				SDATA => false,
				NBIN  => BLOCKS_NB,
				WOUT  => WOUT,
				-- User-specified radix, for testing purposes (0 means automatic)
				RADIX => 0,
				-- Special considerations about data nature
				BINARY => false,
				BINARY_RADIX => 0,
				TERNARY => false,
				TERNARY_RADIX => 0,
				-- An optional tag
				TAGW  => TAGW,
				TAGEN => TAGEN,
				TAGZC => TAGZC,
				-- How to add pipeline registers
				REGEN  => REGEN,
				REGIDX => REGIDX_NEXT
			)
			port map (
				clk      => clk,
				clear    => clear,
				-- Data, input and output
				data_in  => regs,
				data_out => data_out,
				-- Tag, input and output
				tag_in   => reg_tag,
				tag_out  => tag_out
			);

	end generate;

end architecture;

