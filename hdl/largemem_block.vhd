
-- This is a memory component based on Xilinx BRAM primitives
-- The read latency depends on the size of the memory, to mask the delay of the output multiplexer

-- There are 2 possible optimization objectives, with parameter OPT_SPEED :
-- OPT_SPEED=true  : optimize for speed, this may use up to 100% more blocks than necessary if the number of lines is just above a power of 2
-- OPT_SPEED=false : optimize the number of blocks, this may require more address decoding and multiplexing (with extra logic) and an additional pipeline register

-- FIXME It may be interesting to evaluate the "main" memory as bram36k and use extra bram18k (or even lutram) for the rest of the last lines/bits
-- FIXME Consider BRAM cascading to form memories of 64k lines (7-series) or larger (new cascading with Ultrascale)

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For RAMB18E1 and RAMB36E1
library unisim;
use unisim.vcomponents.all;

-- For BRAM_SDP_MACRO
library unimacro;
use unimacro.vcomponents.all;

entity largemem_block is
	generic (
		-- Parameters for memory
		WDATA   : natural := 8;
		CELLS   : natural := 1024;
		WADDR   : natural := 10;
		-- Optimization objective
		OPT_SPEED : boolean := false;
		-- A tag that may be given for each read
		WTAG    : natural := 1;
		-- Enable extra register after the memory blocks, to improve timings
		REG_DO  : boolean := false;
		-- Enable extra register when there are multiple blocks in height
		REG_MUX : boolean := false
	);
	port (
		clk        : in  std_logic;
		clear      : in  std_logic;
		-- Write port
		write_addr : in  std_logic_vector(WADDR-1 downto 0);
		write_en   : in  std_logic;
		write_data : in  std_logic_vector(WDATA-1 downto 0);
		-- Read port, input
		read_addr  : in  std_logic_vector(WADDR-1 downto 0);
		read_en    : in  std_logic;
		read_itag  : in  std_logic_vector(WTAG-1 downto 0);
		-- Read port, output
		read_data  : out std_logic_vector(WDATA-1 downto 0);
		read_valid : out std_logic;
		read_otag  : out std_logic_vector(WTAG-1 downto 0)
	);
end largemem_block;

architecture synth of largemem_block is

	-- Utility function to generate integer from boolean
	function to_integer(b : boolean) return natural is
	begin
		if b = true then
			return 1;
		end if;
		return 0;
	end function;

	-- A data type to hold BRAM configuration:
	-- 18kb / 36kb, number of blocks in height and in width
	type bram_t is record
		CONFIG     : natural;  -- 18 or 36
		WADDR      : natural;  -- Address port width
		AOFF       : natural;  -- Offset in actual address port
		WDATA      : natural;  -- Total data port width
		WPAR       : natural;  -- Number of data bits that actually use the parity bits for storage (MSBs of user data)
		WE         : natural;  -- Number of bits of the WE signal
		NB_WIDTH   : natural;
		NB_HEIGHT  : natural;
	end record;

	-- Helper function to fill a bram_t structure
	function gen_bram(bram_config, bram_cells, bram_waddr, bram_aoff, bram_wdata, bram_wpar, bram_we : natural) return bram_t is
		variable bram : bram_t := (0, 0, 0, 0, 0, 0, 0, 0);
	begin

		-- Evaluate with bram18k
		bram.CONFIG    := bram_config;
		-- Properties of BRAM primitives
		bram.WADDR     := bram_waddr;
		bram.AOFF      := bram_aoff;
		bram.WDATA     := bram_wdata;
		bram.WPAR      := bram_wpar;
		bram.WE        := bram_we;
		-- Number of BRAM primitives
		bram.NB_WIDTH  := (WDATA + bram.WDATA - 1) / bram.WDATA;
		bram.NB_HEIGHT := (CELLS + bram_cells - 1) / bram_cells;

		return bram;
	end function;

	-- Select the best implem
	function calc_best_config(cur_best, cur_bram : bram_t) return bram_t is
		variable bram_best   : bram_t := (0, 0, 0, 0, 0, 0, 0, 0);
		variable blocks_best : natural := 0;
		variable blocks_num  : natural := 0;
	begin

		-- By default, keep the current best
		bram_best := cur_best;

		-- Calculate size properties, in number of 18kb blocks
		blocks_best := cur_best.NB_WIDTH * cur_best.NB_HEIGHT * (cur_best.CONFIG / 18);
		blocks_num  := cur_bram.NB_WIDTH * cur_bram.NB_HEIGHT * (cur_bram.CONFIG / 18);

		-- Update the best config

		-- Focus on minimizing the number of blocks
		-- For a same number of blocks, prefer the configuration that with lowest height
		if OPT_SPEED = false then
			if (blocks_best = 0) or (blocks_num <= blocks_best) then
				bram_best := cur_bram;
			end if;

		-- Focus on minimizing the number of blocks in height, to reduce the MUX logic after
		else
			if
				(blocks_best = 0) or
				(cur_bram.NB_HEIGHT < cur_best.NB_HEIGHT) or
				((cur_bram.NB_HEIGHT = cur_best.NB_HEIGHT) and (blocks_num <= blocks_best))
			then
				bram_best := cur_bram;
			end if;
		end if;

		return bram_best;
	end function;

	-- Focus on minimizing the number of blocks (for size), or the height (for speed)
	-- For a same number of blocks, prefer the configuration with lowest height
	function calc_bram(phony : boolean) return bram_t is
		variable bram     : bram_t := (0, 0, 0, 0, 0, 0, 0, 0);
		variable bram_best: bram_t := (0, 0, 0, 0, 0, 0, 0, 0);
	begin
		-- Initialization
		bram_best.NB_WIDTH  := 0;
		bram_best.NB_HEIGHT := 0;

		-- Evaluate with bram18k
		bram_best := calc_best_config(bram_best, gen_bram(18,   512,  9, 5, 36, 4, 4));
		bram_best := calc_best_config(bram_best, gen_bram(18,  1024, 10, 4, 18, 2, 2));
		bram_best := calc_best_config(bram_best, gen_bram(18,  2048, 11, 3,  9, 1, 1));
		bram_best := calc_best_config(bram_best, gen_bram(18,  4096, 12, 2,  4, 0, 1));
		bram_best := calc_best_config(bram_best, gen_bram(18,  8192, 13, 1,  2, 0, 1));
		bram_best := calc_best_config(bram_best, gen_bram(18, 16384, 14, 0,  1, 0, 1));

		-- Evaluate with bram36k
		bram_best := calc_best_config(bram_best, gen_bram(36,   512,  9, 6, 72, 8, 8));
		bram_best := calc_best_config(bram_best, gen_bram(36,  1024, 10, 5, 36, 4, 4));
		bram_best := calc_best_config(bram_best, gen_bram(36,  2048, 11, 4, 18, 2, 2));
		bram_best := calc_best_config(bram_best, gen_bram(36,  4096, 12, 3,  9, 1, 1));
		bram_best := calc_best_config(bram_best, gen_bram(36,  8192, 13, 2,  4, 0, 1));
		bram_best := calc_best_config(bram_best, gen_bram(36, 16384, 14, 1,  2, 0, 1));
		bram_best := calc_best_config(bram_best, gen_bram(36, 32768, 15, 0,  1, 0, 1));

		--report "BRAM" & natural'image(bram_best.CONFIG) & "Kb" &
		--	" waddr " & natural'image(bram_best.WADDR) &
		--	" aoff " & natural'image(bram_best.AOFF) &
		--	" wdata " & natural'image(bram_best.WDATA) &
		--	" wpar " & natural'image(bram_best.WPAR) &
		--	" nh " & natural'image(bram_best.NB_HEIGHT) &
		--	" nw " & natural'image(bram_best.NB_WIDTH);

		return bram_best;
	end function;

	-- Select an appropriate BRAM configuration
	constant BRAM : bram_t := calc_bram(false);

	-- Get the total data width that BRAMs output (for a given height position)
	constant WTOTAL : natural := BRAM.NB_WIDTH * BRAM.WDATA;

	-- FIXME Direct instantiation of BRAM primitives leads to simulation errors, and non-functional design even tough synthesis emits no warning/error
	-- Example of error with the BRAM simulation model provided by Vivado (2017.2) :
	--   DRC Error : One of the attribute WRITE_WIDTH_B or READ_WIDTH_A must set to 36 when RAM_MODE = SDP
	-- For now, the BRAM_SDP_MACRO or the fallback generic code should be used as workaroud

	-- To decide what implementation to use
	-- There is a fallback with a generic code
	constant USE_DIRECT_BRAM    : boolean := false;
	constant USE_BRAM_SDP_MACRO : boolean := true;

	-- Buffers that stores the Read Enable, to propagate on the output
	signal buf_re1 : std_logic := '0';
	signal buf_re2 : std_logic := '0';
	-- Buffers that stores the Read Tag, to propagate on the output
	signal buf_tag1 : std_logic_vector(WTAG-1 downto 0) := (others => '0');
	signal buf_tag2 : std_logic_vector(WTAG-1 downto 0) := (others => '0');

	-- The output of the BRAMs for each level
	signal sig_fullwrite : std_logic_vector(WTOTAL-1 downto 0) := (others => '0');
	signal sig_fullread  : std_logic_vector(BRAM.NB_HEIGHT*WTOTAL-1 downto 0) := (others => '0');

	-- The optionally buffered output of the BRAMs
	signal buf_fullread2 : std_logic_vector(BRAM.NB_HEIGHT*WTOTAL-1 downto 0) := (others => '0');

begin

	-- Instantiate the RAM blocks

	sig_fullwrite <= std_logic_vector(resize(unsigned(write_data), WTOTAL));

	gen_height: for h in 0 to BRAM.NB_HEIGHT-1 generate
		signal we : std_logic := '0';
	begin

		gen_we_tall: if BRAM.NB_HEIGHT > 1 generate
			we <= write_en when unsigned(write_addr(WADDR-1 downto BRAM.WADDR)) = h else '0';
		end generate;

		gen_we_small: if BRAM.NB_HEIGHT <= 1 generate
			we <= write_en;
		end generate;

		gen_width: for w in 0 to BRAM.NB_WIDTH-1 generate
			signal sig_write_addr : std_logic_vector(BRAM.WADDR-1 downto 0) := (others => '0');
			signal sig_read_addr  : std_logic_vector(BRAM.WADDR-1 downto 0) := (others => '0');
			signal sig_bram_we    : std_logic_vector(BRAM.WE-1 downto 0) := (others => '1');
		begin

			sig_write_addr <= std_logic_vector(resize(unsigned(write_addr), BRAM.WADDR));
			sig_read_addr  <= std_logic_vector(resize(unsigned(read_addr), BRAM.WADDR));
			sig_bram_we    <= (others => '1');

			-- Instantiate a BRAM18E1
			gen : if USE_DIRECT_BRAM = true and BRAM.CONFIG = 18 generate

				 -- Note from user guide UG473 "7 Series FPGAs Memory Resources"
				 -- - In SDP mode, the read port is port A and the write port is port B
				 -- - In SDP mode, the max width of ports is 36b instead of 18b
				 -- - In SDP mode, the LSBs of data are port A, the MSBs of data are port B

				-- SDP mode : port A is for read operations
				constant READ_WIDTH_A  : natural := BRAM.WDATA;
				constant WRITE_WIDTH_A : natural := 1;
				-- SDP mode : port B is for write operations
				constant READ_WIDTH_B  : natural := 1;
				constant WRITE_WIDTH_B : natural := BRAM.WDATA;

				-- Alias signal for this block
				signal loc_rdata : std_logic_vector(BRAM.WDATA-1 downto 0) := (others => '0');
				signal loc_wdata : std_logic_vector(BRAM.WDATA-1 downto 0) := (others => '0');

				-- Read data signals
				signal inst_raddr : std_logic_vector(13 downto 0) := (others => '0');
				signal inst_rdata : std_logic_vector(31 downto 0) := (others => '0');
				signal inst_rpar  : std_logic_vector(3  downto 0) := (others => '0');
				-- Write data signals
				signal inst_waddr : std_logic_vector(13 downto 0) := (others => '0');
				signal inst_wdata : std_logic_vector(31 downto 0) := (others => '0');
				signal inst_wpar  : std_logic_vector(3  downto 0) := (others => '0');
				signal inst_wen   : std_logic_vector(3  downto 0) := (others => '0');

			begin

				-- Alias signal for this block
				loc_wdata <= sig_fullwrite((w+1)*BRAM.WDATA-1 downto w*BRAM.WDATA);

				-- Read address
				inst_raddr(BRAM.AOFF+BRAM.WADDR-1 downto BRAM.AOFF) <= std_logic_vector(resize(unsigned(read_addr), BRAM.WADDR));

				-- Write address
				inst_waddr(BRAM.AOFF+BRAM.WADDR-1 downto BRAM.AOFF) <= std_logic_vector(resize(unsigned(write_addr), BRAM.WADDR));

				-- Write data
				inst_wdata(BRAM.WDATA-BRAM.WPAR-1 downto 0) <= loc_wdata(BRAM.WDATA-BRAM.WPAR-1 downto 0);
				gen_wpar : if BRAM.WPAR > 0 generate
					inst_wpar(BRAM.WPAR-1 downto 0) <= loc_wdata(BRAM.WDATA-1 downto BRAM.WDATA-BRAM.WPAR);
				end generate;

				gen_wen : for i in 0 to BRAM.WE-1 generate
					inst_wen(i) <= '1';
				end generate;

				ramb18e1_inst : RAMB18E1
				generic map (
					-- Address Collision Mode: "PERFORMANCE" or "DELAYED_WRITE"
					RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
					-- Collision check: Values ("ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE")
					SIM_COLLISION_CHECK => "NONE",
					-- Simulation Device: Must be set to "7SERIES" for simulation behavior
					SIM_DEVICE => "7SERIES",
					-- DOA_REG, DOB_REG: Optional output register (0 or 1)
					DOA_REG => 0,
					DOB_REG => 0,
					-- RAM Mode: "SDP" or "TDP"
					RAM_MODE => "SDP",
					-- READ_WIDTH_A/B, WRITE_WIDTH_A/B: Read/write width per port
					READ_WIDTH_A  => READ_WIDTH_A,   -- 0-36
					READ_WIDTH_B  => READ_WIDTH_B,   -- 0-18
					WRITE_WIDTH_A => WRITE_WIDTH_A,  -- 0-18
					WRITE_WIDTH_B => WRITE_WIDTH_B,  -- 0-36
					-- RSTREG_PRIORITY_A, RSTREG_PRIORITY_B: Reset or enable priority ("RSTREG" or "REGCE")
					RSTREG_PRIORITY_A => "RSTREG",
					RSTREG_PRIORITY_B => "RSTREG",
					-- Write Mode: Value on output upon a write ("WRITE_FIRST", "READ_FIRST", or "NO_CHANGE")
					WRITE_MODE_A => "READ_FIRST",
					WRITE_MODE_B => "READ_FIRST"
				)
				port map (
					-- Port A Data: 16-bit (each) output: Port A data
					DOADO   => inst_rdata(15 downto 0),  -- 16-bit output: A port data/LSB data
					DOPADOP => inst_rpar(1 downto 0),    -- 2-bit output: A port parity/LSB parity
					-- Port B Data: 16-bit (each) output: Port B data
					DOBDO   => inst_rdata(31 downto 16), -- 16-bit output: B port data/MSB data
					DOPBDOP => inst_rpar(3 downto 2),    -- 2-bit output: B port parity/MSB parity
					-- Port A Address/Control Signals: 14-bit (each) input: Port A address and control signals (read port when RAM_MODE="SDP")
					ADDRARDADDR   => inst_raddr,         -- 14-bit input: A port address/Read address
					CLKARDCLK     => clk,                -- 1-bit input: A port clock/Read clock
					ENARDEN       => read_en,            -- 1-bit input: A port enable/Read enable
					REGCEAREGCE   => '0',                -- 1-bit input: A port register enable/Register enable
					RSTRAMARSTRAM => clear,              -- 1-bit input: A port set/reset
					RSTREGARSTREG => '0',                -- 1-bit input: A port register set/reset
					WEA           => (others => '0'),     -- 2-bit input: A port write enable
					-- Port A Data: 16-bit (each) input: Port A data
					DIADI         => inst_wdata(15 downto 0),  -- 16-bit input: A port data/LSB data
					DIPADIP       => inst_wpar(1 downto 0),    -- 2-bit input: A port parity/LSB parity
					-- Port B Address/Control Signals: 14-bit (each) input: Port B address and control signals (write port when RAM_MODE="SDP")
					ADDRBWRADDR => inst_waddr,           -- 14-bit input: B port address/Write address
					CLKBWRCLK   => clk,                  -- 1-bit input: B port clock/Write clock
					ENBWREN     => we,                   -- 1-bit input: B port enable/Write enable
					REGCEB      => '0',                  -- 1-bit input: B port register enable
					RSTRAMB     => clear,                -- 1-bit input: B port set/reset
					RSTREGB     => clear,                -- 1-bit input: B port register set/reset
					WEBWE       => inst_wen,             -- 4-bit input: B port write enable/Write enable
					-- Port B Data: 16-bit (each) input: Port B data
					DIBDI       => inst_wdata(31 downto 16), -- 16-bit input: B port data/MSB data
					DIPBDIP     => inst_wpar(3 downto 2)     -- 2-bit input: B port parity/MSB parity
				);

				-- Data output
				loc_rdata(BRAM.WDATA-BRAM.WPAR-1 downto 0) <= inst_rdata(BRAM.WDATA-BRAM.WPAR-1 DOWNTO 0);
				gen_rpar : if BRAM.WPAR > 0 generate
					loc_rdata(BRAM.WDATA-1 downto BRAM.WDATA-BRAM.WPAR) <= inst_rpar(BRAM.WPAR-1 DOWNTO 0);
				end generate;

				-- Alias signal for this block
				sig_fullread(h*WTOTAL+(w+1)*BRAM.WDATA-1 downto h*WTOTAL+w*BRAM.WDATA) <= loc_rdata;

			-- Instantiate a BRAM36E1
			elsif USE_DIRECT_BRAM = true and BRAM.CONFIG = 36 generate

				 -- Note from user guide UG473 "7 Series FPGAs Memory Resources"
				 -- - In SDP mode, the read port is port A and the write port is port B
				 -- - In SDP mode, the max width of ports is 72b instead of 36b
				 -- - In SDP mode, the LSBs of data are port A, the MSBs of data are port B

				-- SDP mode : port A is for read operations
				constant READ_WIDTH_A  : natural := BRAM.WDATA;
				constant WRITE_WIDTH_A : natural := 1;
				-- SDP mode : port B is for write operations
				constant READ_WIDTH_B  : natural := 1;
				constant WRITE_WIDTH_B : natural := BRAM.WDATA;

				-- Alias signal for this block
				signal loc_rdata : std_logic_vector(BRAM.WDATA-1 downto 0) := (others => '0');
				signal loc_wdata : std_logic_vector(BRAM.WDATA-1 downto 0) := (others => '0');

				-- Read data signals
				signal inst_raddr : std_logic_vector(15 downto 0) := (others => '0');
				signal inst_rdata : std_logic_vector(63 downto 0) := (others => '0');
				signal inst_rpar  : std_logic_vector(7  downto 0) := (others => '0');
				-- Write data signals
				signal inst_waddr : std_logic_vector(15 downto 0) := (others => '0');
				signal inst_wdata : std_logic_vector(63 downto 0) := (others => '0');
				signal inst_wpar  : std_logic_vector(7  downto 0) := (others => '0');
				signal inst_wen   : std_logic_vector(7  downto 0) := (others => '0');

			begin

				-- Alias signal for this block
				loc_wdata <= sig_fullwrite((w+1)*BRAM.WDATA-1 downto w*BRAM.WDATA);

				-- Read address
				inst_raddr(15) <= '1';  -- For non-cascaded
				inst_raddr(BRAM.AOFF+BRAM.WADDR-1 downto BRAM.AOFF) <= std_logic_vector(resize(unsigned(read_addr), BRAM.WADDR));

				-- Write address
				inst_waddr(15) <= '1';  -- For non-cascaded
				inst_waddr(BRAM.AOFF+BRAM.WADDR-1 downto BRAM.AOFF) <= std_logic_vector(resize(unsigned(write_addr), BRAM.WADDR));

				-- Write data
				inst_wdata(BRAM.WDATA-BRAM.WPAR-1 downto 0) <= loc_wdata(BRAM.WDATA-BRAM.WPAR-1 downto 0);
				gen_wpar : if BRAM.WPAR > 0 generate
					inst_wpar(BRAM.WPAR-1 downto 0) <= loc_wdata(BRAM.WDATA-1 downto BRAM.WDATA-BRAM.WPAR);
				end generate;

				gen_wen : for i in 0 to BRAM.WE-1 generate
					inst_wen(i) <= '1';
				end generate;

				ramb36e1_inst : RAMB36E1
				generic map (
					-- Address Collision Mode: "PERFORMANCE" or "DELAYED_WRITE"
					RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
					-- Collision check: Values ("ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE")
					SIM_COLLISION_CHECK => "NONE",
					-- Simulation Device: Must be set to "7SERIES" for simulation behavior
					SIM_DEVICE => "7SERIES",
					-- DOA_REG, DOB_REG: Optional output register (0 or 1)
					DOA_REG => 0,
					DOB_REG => 0,
					-- Enable ECC encoder: FALSE, TRUE
					EN_ECC_READ  => false,
					EN_ECC_WRITE => false,
					-- RAM Mode: "SDP" or "TDP"
					RAM_MODE => "SDP",
					-- RAM_EXTENSION_A, RAM_EXTENSION_B: Selects cascade mode ("UPPER", "LOWER", or "NONE")
					RAM_EXTENSION_A => "NONE",
					RAM_EXTENSION_B => "NONE",
					-- READ_WIDTH_A/B, WRITE_WIDTH_A/B: Read/write width per port
					READ_WIDTH_A  => 0,  -- 0-72
					READ_WIDTH_B  => 0,  -- 0-36
					WRITE_WIDTH_A => 0,  -- 0-36
					WRITE_WIDTH_B => 0,  -- 0-72
					-- RSTREG_PRIORITY_A, RSTREG_PRIORITY_B: Reset or enable priority ("RSTREG" or "REGCE")
					RSTREG_PRIORITY_A => "RSTREG",
					RSTREG_PRIORITY_B => "RSTREG",
					-- Write Mode: Value on output upon a write ("WRITE_FIRST", "READ_FIRST", or "NO_CHANGE")
					WRITE_MODE_A => "READ_FIRST",
					WRITE_MODE_B => "READ_FIRST"
				)
				port map (
					-- Cascade Signals: 1-bit (each) output: BRAM cascade ports (to create 64kx1)
					CASCADEOUTA => open,                 -- 1-bit output: A port cascade
					CASCADEOUTB => open,                 -- 1-bit output: B port cascade
					-- ECC Signals: 1-bit (each) output: Error Correction Circuitry ports
					DBITERR   => open,                   -- 1-bit output: Double bit error status
					ECCPARITY => open,                   -- 8-bit output: Generated error correction parity
					RDADDRECC => open,                   -- 9-bit output: ECC read address
					SBITERR   => open,                   -- 1-bit output: Single bit error status
					-- Port A Data: 32-bit (each) output: Port A data
					DOADO   => inst_rdata(31 downto 0),  -- 32-bit output: A port data/LSB data
					DOPADOP => inst_rpar(3 downto 0),    -- 4-bit output: A port parity/LSB parity
					-- Port B Data: 32-bit (each) output: Port B data
					DOBDO   => inst_rdata(63 downto 32), -- 32-bit output: B port data/MSB data
					DOPBDOP => inst_rpar(7 downto 4),   -- 4-bit output: B port parity/MSB parity
					-- Cascade Signals: 1-bit (each) input: BRAM cascade ports (to create 64kx1)
					CASCADEINA => '0',                   -- 1-bit input: A port cascade
					CASCADEINB => '0',                   -- 1-bit input: B port cascade
					-- ECC Signals: 1-bit (each) input: Error Correction Circuitry ports
					INJECTDBITERR => '0',                -- 1-bit input: Inject a double bit error
					INJECTSBITERR => '0',                -- 1-bit input: Inject a single bit error
					-- Port A Address/Control Signals: 16-bit (each) input: Port A address and control signals (read port when RAM_MODE="SDP")
					ADDRARDADDR   => inst_raddr,         -- 16-bit input: A port address/Read address
					CLKARDCLK     => clk,                -- 1-bit input: A port clock/Read clock
					ENARDEN       => read_en,            -- 1-bit input: A port enable/Read enable
					REGCEAREGCE   => '0',                -- 1-bit input: A port register enable/Register enable
					RSTRAMARSTRAM => clear,              -- 1-bit input: A port set/reset
					RSTREGARSTREG => '0',                -- 1-bit input: A port register set/reset
					WEA           => (others => '0'),    -- 4-bit input: A port write enable
					-- Port A Data: 32-bit (each) input: Port A data
					DIADI         => inst_wdata(31 downto 0), -- 32-bit input: A port data/LSB data
					DIPADIP       => inst_wpar(3 downto 0),   -- 4-bit input: A port parity/LSB parity
					-- Port B Address/Control Signals: 16-bit (each) input: Port B address and control signals (write port when RAM_MODE="SDP")
					ADDRBWRADDR => inst_waddr,           -- 16-bit input: B port address/Write address
					CLKBWRCLK   => clk,                  -- 1-bit input: B port clock/Write clock
					ENBWREN     => we,                   -- 1-bit input: B port enable/Write enable
					REGCEB      => '0',                  -- 1-bit input: B port register enable
					RSTRAMB     => clear,                -- 1-bit input: B port set/reset
					RSTREGB     => clear,                -- 1-bit input: B port register set/reset
					WEBWE       => inst_wen,             -- 8-bit input: B port write enable/Write enable
					-- Port B Data: 32-bit (each) input: Port B data
					DIBDI       => inst_wdata(63 downto 32), -- 32-bit input: B port data/MSB data
					DIPBDIP     => inst_wpar(7 downto 4)     -- 4-bit input: B port parity/MSB parity
				);

				-- Data output
				loc_rdata(BRAM.WDATA-BRAM.WPAR-1 downto 0) <= inst_rdata(BRAM.WDATA-BRAM.WPAR-1 DOWNTO 0);
				gen_rpar : if BRAM.WPAR > 0 generate
					loc_rdata(BRAM.WDATA-1 downto BRAM.WDATA-BRAM.WPAR) <= inst_rpar(BRAM.WPAR-1 DOWNTO 0);
				end generate;

				-- Alias signal for this block
				sig_fullread(h*WTOTAL+(w+1)*BRAM.WDATA-1 downto h*WTOTAL+w*BRAM.WDATA) <= loc_rdata;

			-- Instantiate Vivado-specific macro BRAM_SDP_MACRO
			elsif USE_BRAM_SDP_MACRO = true generate

				bram_i : BRAM_SDP_MACRO
					generic map (
						BRAM_SIZE   => natural'image(BRAM.CONFIG) & "Kb",  -- Target BRAM, 18Kb or 36Kb
						DEVICE      => "7SERIES",    -- Target device: VIRTEX5, VIRTEX6, 7SERIES, SPARTAN6
						WRITE_WIDTH => BRAM.WDATA,   -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE = 36Kb)
						READ_WIDTH  => BRAM.WDATA,   -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE = 36Kb)
						DO_REG      => 0,  -- Optional output register (0 or 1)
						-- Set/Reset value for port output
						SRVAL       => X"000000000000000000",
						-- Specify READ_FIRST for same clock or synchronous clocks, or WRITE_FIRST for asynchronous clocks on ports
						WRITE_MODE  => "READ_FIRST",
						-- Initial values on output port
						INIT        => X"000000000000000000",
						-- Initial values when stored in a file
						INIT_FILE   => "NONE"
					)
					port map (
						-- Output read data port, width defined by READ_WIDTH parameter
						DO     => sig_fullread(h*WTOTAL+(w+1)*BRAM.WDATA-1 downto h*WTOTAL+w*BRAM.WDATA),
						-- Input write data port, width defined by WRITE_WIDTH parameter
						DI     => sig_fullwrite((w+1)*BRAM.WDATA-1 downto w*BRAM.WDATA),
						-- Input read address, width defined by read port depth
						RDADDR => sig_read_addr,
						-- 1-bit input read clock
						RDCLK  => clk,
						-- 1-bit input read port enable
						RDEN   => read_en,
						-- 1-bit input read output register clock enable
						REGCE  => '1',
						-- 1-bit input reset
						RST    => '0',
						-- Input write enable, width defined by write port depth
						WE     => sig_bram_we,
						-- Input write address, width defined by write port depth
						WRADDR => sig_write_addr,
						-- 1-bit input write clock
						WRCLK  => clk,
						-- 1-bit input write port enable
						WREN   => we
					);

			-- For reference and validation, and for interoperability between synthesis tools
			-- Generic code that should be synthesized into one unique BRAM
			else generate

				type mem_type is array(0 to 2 ** WADDR - 1) of std_logic_vector(BRAM.WDATA-1 downto 0);
				signal mem : mem_type;

				attribute ram_style : string;
				attribute ram_style of mem : signal is "block";

			begin

				process(clk)
				begin
					if rising_edge(clk) then

						if read_en = '1' then
							sig_fullread(h*WTOTAL+(w+1)*BRAM.WDATA-1 downto h*WTOTAL+w*BRAM.WDATA) <= mem(to_integer(unsigned(sig_read_addr)));
						end if;

						if we = '1' then
							mem(to_integer(unsigned(sig_write_addr))) <= sig_fullwrite((w+1)*BRAM.WDATA-1 downto w*BRAM.WDATA);
						end if;

					end if;
				end process;

			end generate;

		end generate;  -- Width

	end generate;  -- Height

	-- Sequential process: propagate the read_en signal

	process(clk)
	begin
		if rising_edge(clk) then
			buf_re1  <= read_en and not clear;
			buf_tag1 <= read_itag;
		end if;
	end process;

	-- Sequential process: the register at output of the BRAMs

	gen_reg_do : if REG_DO = true generate
		-- Prevent the register to be absorbed by the BRAM
		-- This way the register can be close to the actual readers, which helps to respect timings constraints
		signal reg_fullread2 : std_logic_vector(BRAM.NB_HEIGHT*WTOTAL-1 downto 0) := (others => '0');
		attribute DONT_TOUCH : string;
		attribute DONT_TOUCH of reg_fullread2 : signal is "true";
	begin

		process(clk)
		begin
			if rising_edge(clk) then
				reg_fullread2 <= sig_fullread;
				buf_re2  <= buf_re1 and not clear;
				buf_tag2 <= buf_tag1;
			end if;
		end process;

		buf_fullread2 <= reg_fullread2;

	else generate

		buf_fullread2 <= sig_fullread;
		buf_re2       <= buf_re1;
		buf_tag2      <= buf_tag1;

	end generate;

	-- Direct assignment of output port if there is no mux after the BRAM

	gen_nomux : if BRAM.NB_HEIGHT = 1 generate

		-- Note : Need to explicitly select the WDATA range because the BRAM data width may be larger than WDATA
		read_data  <= buf_fullread2(WDATA-1 downto 0);
		read_valid <= buf_re2;
		read_otag  <= buf_tag2;

	end generate;

	-- Post-memory multiplexer

	gen_mux: if BRAM.NB_HEIGHT >= 2 generate

		-- Store the MSB bits of the address for the multiplexer
		signal buf_raddr1 : std_logic_vector(WADDR-BRAM.WADDR-1 downto 0) := (others => '0');
		signal buf_raddr2 : std_logic_vector(WADDR-BRAM.WADDR-1 downto 0) := (others => '0');

		-- The output of the multiplexer on the data read side
		signal sig_mux_rd : std_logic_vector(WDATA-1 downto 0) := (others => '0');

	begin

		-- Sequential process: the buffer on MSBs of read address

		process(clk)
		begin
			if rising_edge(clk) then

				buf_raddr1 <= read_addr(WADDR-1 downto BRAM.WADDR);
				buf_raddr2 <= read_addr(WADDR-1 downto BRAM.WADDR);
				if REG_DO = true then
					buf_raddr2 <= buf_raddr1;
				end if;

			end if;
		end process;

		-- The multiplexer
		-- FIXME It is unknown how efficient this gets synthesized, whether it is a priority tree, a binary mux or a decoded mux based on OR operation

		process(all)
		begin

			sig_mux_rd <= (others => '0');

			for h in 0 to BRAM.NB_HEIGHT-1 loop
				if unsigned(buf_raddr2) = h then
					sig_mux_rd <= buf_fullread2(h*WTOTAL+WDATA-1 downto h*WTOTAL);
				end if;
			end loop;

		end process;

		-- Output buffers

		gen_reg : if REG_MUX = true generate

			process(clk)
			begin
				if rising_edge(clk) then
					read_data  <= sig_mux_rd;
					read_valid <= buf_re2 and not clear;
					read_otag  <= buf_tag2;
				end if;
			end process;

		else generate

			read_data  <= sig_mux_rd;
			read_valid <= buf_re2;
			read_otag  <= buf_tag2;

		end generate;

	end generate;

end architecture;

