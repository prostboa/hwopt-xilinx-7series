
-- This is an adder 5b + 5b implemented using 4 LUT6

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT primitives
library unisim;
use unisim.vcomponents.all;

entity adder_2in_5b is
	generic (
		IS_SIGNED : boolean := false;
		WOUT      : natural := 6
	);
	port (
		inA : in  std_logic_vector(4 downto 0);
		inB : in  std_logic_vector(4 downto 0);
		res : out std_logic_vector(WOUT-1 downto 0)
	);
end adder_2in_5b;

architecture synth of adder_2in_5b is

	signal sigC1  : std_logic := '0';
	signal sigSum : std_logic_vector(5 downto 0) := (others => '0');

	function calc_false_true(b : boolean; vf : bit_vector; vt : bit_vector) return bit_vector is
	begin
		if b = false then return vf; end if;
		return vt;
	end;

	constant INIT_LUT01 : bit_vector(63 downto 0) := x"0000877800006666";
	constant INIT_LUTC1 : bit_vector(63 downto 0) := x"000000000000F880";
	constant INIT_LUT23 : bit_vector(63 downto 0) := x"E81717E896969696";
	constant INIT_LUT45 : bit_vector(63 downto 0) := calc_false_true(IS_SIGNED,
		x"FF8E8E008E71718E",
		x"FF7171008E71718E"
	);

begin

	lut01 : LUT6_2
		generic map (
			INIT => INIT_LUT01
		)
		port map (
			O6 => sigSum(1),
			O5 => sigSum(0),
			I0 => inA(0),
			I1 => inB(0),
			I2 => inA(1),
			I3 => inB(1),
			I4 => '0',  -- Unused input
			I5 => '1'   -- To have something different for O6 and O5
		);

	lutC1 : LUT6
		generic map (
			INIT => INIT_LUTC1
		)
		port map (
			O  => sigC1,
			I0 => inA(0),
			I1 => inB(0),
			I2 => inA(1),
			I3 => inB(1),
			I4 => '0',  -- Unused input
			I5 => '0'   -- Unused input
		);

	lut23 : LUT6_2
		generic map (
			INIT => INIT_LUT23
		)
		port map (
			O6 => sigSum(3),
			O5 => sigSum(2),
			I0 => sigC1,
			I1 => inA(2),
			I2 => inB(2),
			I3 => inA(3),
			I4 => inB(3),
			I5 => '1'   -- To have something different for O6 and O5
		);

	lut45 : LUT6_2
		generic map (
			INIT => INIT_LUT45
		)
		port map (
			O6 => sigSum(5),
			O5 => sigSum(4),
			I0 => inA(3),
			I1 => inB(3),
			I2 => sigSum(3),
			I3 => inA(4),
			I4 => inB(4),
			I5 => '1'   -- To have something different for O6 and O5
		);

	res <=
		std_logic_vector(resize(  signed(sigSum), WOUT)) when IS_SIGNED = true else
		std_logic_vector(resize(unsigned(sigSum), WOUT));

end architecture;

