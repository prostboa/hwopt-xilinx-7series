
-- This is a popcount 7b -> 3b
-- Total 3 LUTs, delay 2 LUTs

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT primitives
library unisim;
use unisim.vcomponents.all;

entity popcount_7_3 is
	generic (
		BITS : natural := 7;
		WOUT : natural := 3
	);
	port (
		bits_in : in  std_logic_vector(BITS-1 downto 0);
		sum     : out std_logic_vector(WOUT-1 downto 0)
	);
end popcount_7_3;

architecture synth of popcount_7_3 is

	signal sigBits : std_logic_vector(6 downto 0) := (others => '0');
	signal sigSum  : std_logic_vector(2 downto 0) := (others => '0');

	signal partSum : std_logic_vector(1 downto 0) := (others => '0');

begin

	assert BITS <= 7 report "Number of inputs too large for module popcount_7_3" severity failure;

	sigBits(BITS-1 downto 0) <= bits_in;

	lut_part : LUT6_2
		generic map (
			INIT => x"0000699600007EE8"
		)
		port map (
			O6 => partSum(0),
			O5 => partSum(1),
			I0 => sigBits(0),
			I1 => sigBits(1),
			I2 => sigBits(2),
			I3 => sigBits(3),
			I4 => '0',
			I5 => '1'   -- To have something different for O6 and O5
		);

	lut01 : LUT6_2
		generic map (
			INIT => x"5AA5A55A9336366C"
		)
		port map (
			O6 => sigSum(0),
			O5 => sigSum(1),
			I0 => partSum(0),
			I1 => partSum(1),
			I2 => sigBits(4),
			I3 => sigBits(5),
			I4 => sigBits(6),
			I5 => '1'   -- To have something different for O6 and O5
		);

	lut2 : LUT6
		generic map (
			INIT => x"FEDCDC98DC989810"
		)
		port map (
			O  => sigSum(2),
			I0 => partSum(0),
			I1 => partSum(1),
			I2 => sigBits(3),
			I3 => sigBits(4),
			I4 => sigBits(5),
			I5 => sigBits(6)
		);

	sum <= std_logic_vector(resize(unsigned(sigSum), WOUT));

end architecture;

