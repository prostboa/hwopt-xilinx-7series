
-- This is an adder 4b + 4b implemented using 3 LUT6

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- For LUT primitives
library unisim;
use unisim.vcomponents.all;

entity adder_2in_4b is
	generic (
		IS_SIGNED : boolean := false;
		WOUT      : natural := 5
	);
	port (
		inA : in  std_logic_vector(3 downto 0);
		inB : in  std_logic_vector(3 downto 0);
		res : out std_logic_vector(WOUT-1 downto 0)
	);
end adder_2in_4b;

architecture synth of adder_2in_4b is

	signal sigSum : std_logic_vector(4 downto 0);

	function calc_false_true(b : boolean; vf : bit_vector; vt : bit_vector) return bit_vector is
	begin
		if b = false then return vf; end if;
		return vt;
	end;

	constant INIT_LUT01 : bit_vector(63 downto 0) := x"0000877800006666";
	constant INIT_LUT2  : bit_vector(63 downto 0) := x"F880077F077FF880";
	constant INIT_LUT34 : bit_vector(63 downto 0) := calc_false_true(IS_SIGNED,
		x"FF8E8E008E71718E",
		x"FF7171008E71718E"
	);

begin

	lut01 : LUT6_2
		generic map (
			INIT => INIT_LUT01
		)
		port map (
			O6 => sigSum(1),
			O5 => sigSum(0),
			I0 => inA(0),
			I1 => inB(0),
			I2 => inA(1),
			I3 => inB(1),
			I4 => '0',  -- Unused input
			I5 => '1'   -- To have something different for O6 and O5
		);

	lut2 : LUT6
		generic map (
			INIT => INIT_LUT2
		)
		port map (
			O  => sigSum(2),
			I0 => inA(0),
			I1 => inB(0),
			I2 => inA(1),
			I3 => inB(1),
			I4 => inA(2),
			I5 => inB(2)
		);

	lut34 : LUT6_2
		generic map (
			INIT => INIT_LUT34
		)
		port map (
			O6 => sigSum(4),
			O5 => sigSum(3),
			I0 => inA(2),
			I1 => inB(2),
			I2 => sigSum(2),
			I3 => inA(3),
			I4 => inB(3),
			I5 => '1'   -- To have something different for O6 and O5
		);

	res <=
		std_logic_vector(resize(  signed(sigSum), WOUT)) when IS_SIGNED = true else
		std_logic_vector(resize(unsigned(sigSum), WOUT));

end architecture;

