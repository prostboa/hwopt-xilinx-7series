
-- This is a wrapper for ternary-to-binary compressors

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity terncompress_com_wrap is
	generic(
		NTER : natural := 3;
		BINW : natural := 5
	);
	port(
		ter_in  : in  std_logic_vector(2*NTER-1 downto 0);
		bin_out : out std_logic_vector(BINw-1 downto 0)
	);
end terncompress_com_wrap;

architecture synth of terncompress_com_wrap is

	-- Component to compress 2T -> 3b
	component terncompress_2t_3b is
		port(
			ter_in  : in  std_logic_vector(3 downto 0);
			bin_out : out std_logic_vector(2 downto 0)
		);
	end component;

	-- Component to compress 3T -> 5b
	component terncompress_3t_5b is
		port(
			ter_in  : in  std_logic_vector(5 downto 0);
			bin_out : out std_logic_vector(4 downto 0)
		);
	end component;

	-- Component to compress 5T -> 8b
	component terncompress_5t_8b is
		port(
			ter_in  : in  std_logic_vector(9 downto 0);
			bin_out : out std_logic_vector(7 downto 0)
		);
	end component;

begin

	gen_2t : if NTER = 2 generate

		comp : terncompress_2t_3b
			port map (
				ter_in  => ter_in,
				bin_out => bin_out
			);

	end generate;

	gen_3t : if NTER = 3 generate

		comp : terncompress_3t_5b
			port map (
				ter_in  => ter_in,
				bin_out => bin_out
			);

	end generate;

	gen_5t : if NTER = 5 generate

		comp : terncompress_5t_8b
			port map (
				ter_in  => ter_in,
				bin_out => bin_out
			);

	end generate;

end architecture;

