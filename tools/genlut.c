
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>


//==============================
// Utility functions
//==============================

// Generate a mask of a certain number of lower bits set
unsigned uint_genmask(unsigned bits) {
	if(bits >= 8*sizeof(unsigned)) return (~(unsigned)0);
	return (~(unsigned)0) >> (8*sizeof(unsigned) - bits);
}

// Count the number of bits needed to store the value
// This also returns 1 for the value 0
unsigned uint_bitsnb(unsigned v) {
	if(v==0) return 1;
	unsigned c = 0;
	while(v!=0) { v >>= 1; c++; }
	return c;
}


//==============================
// Utilities to generate LUT configs
//==============================

#define LUTCFG_BIT_SET   0x80
#define LUTCFG_BIT_ERR   0x40
#define LUTCFG_BIT_DATA  0x01

static inline uint8_t lutcfg_getbit(uint8_t* cfg, unsigned idx) {
	return cfg[idx] & LUTCFG_BIT_DATA;
}

static int lutcfg_setbit_check(uint8_t* cfg, unsigned idx, uint8_t val) {
	val = (cfg[idx] & ~LUTCFG_BIT_DATA) | (val & LUTCFG_BIT_DATA) | LUTCFG_BIT_SET;
	if((cfg[idx] ^ val) == LUTCFG_BIT_DATA) return 1;
	cfg[idx] = val;
	return 0;
}

#define lutcfg_setbit_verbose(l, i, v) ( \
	(lutcfg_setbit_check(l, i, v) != 0) ? ( printf("ERROR : Conflict in lut %s at address %u value %u\n", #l, i, v), 1 ) : 0 \
)

static uint8_t* lutcfg_alloc(unsigned size) {
	return calloc(1, size * sizeof(uint8_t));
}

static uint8_t lutcfg_hexval(uint8_t* cfg, unsigned idx) {
	unsigned val = 0;
	val += lutcfg_getbit(cfg, idx+0) * 1;
	val += lutcfg_getbit(cfg, idx+1) * 2;
	val += lutcfg_getbit(cfg, idx+2) * 4;
	val += lutcfg_getbit(cfg, idx+3) * 8;
	return val;
}

static void lutcfg_printhex_bits(uint8_t* cfg, unsigned nbits) {
	nbits = (nbits + 0x03) & ~0x03;
	if(nbits == 0) nbits = 4;
	for(int i=nbits-4; i>=0; i-=4) {
		unsigned val = lutcfg_hexval(cfg, i);
		printf("%01X", val);
	}
}
static inline void lutcfg_printhex_lut3(uint8_t* cfg) {
	lutcfg_printhex_bits(cfg, 8);
}
static inline void lutcfg_printhex_lut4(uint8_t* cfg) {
	lutcfg_printhex_bits(cfg, 16);
}
static inline void lutcfg_printhex_lut5(uint8_t* cfg) {
	lutcfg_printhex_bits(cfg, 32);
}
static inline void lutcfg_printhex_lut6(uint8_t* cfg) {
	lutcfg_printhex_bits(cfg, 64);
}
static inline void lutcfg_printhex_lut3_nl(uint8_t* cfg) {
	lutcfg_printhex_bits(cfg, 8);
	printf("\n");
}
static inline void lutcfg_printhex_lut4_nl(uint8_t* cfg) {
	lutcfg_printhex_bits(cfg, 16);
	printf("\n");
}
static inline void lutcfg_printhex_lut5_nl(uint8_t* cfg) {
	lutcfg_printhex_bits(cfg, 32);
	printf("\n");
}
static inline void lutcfg_printhex_lut6_nl(uint8_t* cfg) {
	lutcfg_printhex_bits(cfg, 64);
	printf("\n");
}
static inline void lutcfg_printhex_lut6_2_nl(uint8_t* cfg6, uint8_t* cfg5) {
	lutcfg_printhex_lut5(cfg6);
	printf(" ");
	lutcfg_printhex_lut5(cfg5);
	printf("\n");
}


//==============================
// Functions to genearte configs
//==============================

static void adderlut6_3to4(bool is_signed) {
	uint8_t* table0 = lutcfg_alloc(64);
	uint8_t* table1 = lutcfg_alloc(64);
	uint8_t* table2 = lutcfg_alloc(64);
	uint8_t* table3 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x7F; i++) {
		unsigned a = (i >> 0) & 0x7;
		unsigned b = (i >> 3) & 0x7;
		unsigned c = (i >> 6) & 0x1;

		if(is_signed) {
			unsigned sh = 32 - 3;
			a = (((int)a) << sh) >> sh;
			b = (((int)b) << sh) >> sh;
		}

		unsigned res = a + b + c;

		unsigned in1 = 0;
		in1 = (in1 << 1) | ((c >> 0) & 0x1);
		in1 = (in1 << 1) | ((b >> 1) & 0x1);
		in1 = (in1 << 1) | ((a >> 1) & 0x1);
		in1 = (in1 << 1) | ((b >> 0) & 0x1);
		in1 = (in1 << 1) | ((a >> 0) & 0x1);

		unsigned in2 = 0;
		in2 = (in2 << 1) | ((b   >> 2) & 0x1);
		in2 = (in2 << 1) | ((a   >> 2) & 0x1);
		in2 = (in2 << 1) | ((res >> 1) & 0x1);
		in2 = (in2 << 1) | ((b   >> 1) & 0x1);
		in2 = (in2 << 1) | ((a   >> 1) & 0x1);

		errors_nb += lutcfg_setbit_verbose(table0, in1, (res >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(table1, in1, (res >> 1) & 0x1);
		errors_nb += lutcfg_setbit_verbose(table2, in2, (res >> 2) & 0x1);
		errors_nb += lutcfg_setbit_verbose(table3, in2, (res >> 3) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUT1 ");
	lutcfg_printhex_lut5(table1);
	printf(" ");
	lutcfg_printhex_lut5(table0);
	printf("\n");

	printf("LUT2 ");
	lutcfg_printhex_lut5(table3);
	printf(" ");
	lutcfg_printhex_lut5(table2);
	printf("\n");
}

static void adderlut6_4to5(bool is_signed) {
	uint8_t* lut0 = lutcfg_alloc(64);
	uint8_t* lut1 = lutcfg_alloc(64);
	uint8_t* lut2 = lutcfg_alloc(64);  // One LUT6
	uint8_t* lut3 = lutcfg_alloc(64);
	uint8_t* lut4 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	unsigned total_nbits = 8;
	unsigned max = uint_genmask(total_nbits);

	unsigned sh = 32 - 4;
	unsigned mask = ((unsigned)~0) >> sh;
	unsigned masksgn = (is_signed==true) ? ((unsigned)~0) : mask;

	for(unsigned i=0; i<=max; i++) {
		#define GETVAL(idx) (( ((int)(i << (sh - idx))) >> sh ) & masksgn)
		unsigned a = GETVAL(0);
		unsigned b = GETVAL(4);
		#undef GETVAL

		unsigned res = a + b;

		unsigned in1 = 0;
		in1 = (in1 << 1) | ((b >> 1) & 0x1);
		in1 = (in1 << 1) | ((a >> 1) & 0x1);
		in1 = (in1 << 1) | ((b >> 0) & 0x1);
		in1 = (in1 << 1) | ((a >> 0) & 0x1);

		unsigned in2 = 0;
		in2 = (in2 << 1) | ((b   >> 2) & 0x1);
		in2 = (in2 << 1) | ((a   >> 2) & 0x1);
		in2 = (in2 << 1) | ((b   >> 1) & 0x1);
		in2 = (in2 << 1) | ((a   >> 1) & 0x1);
		in2 = (in2 << 1) | ((b   >> 0) & 0x1);
		in2 = (in2 << 1) | ((a   >> 0) & 0x1);

		unsigned in3 = 0;
		in3 = (in3 << 1) | ((b   >> 3) & 0x1);
		in3 = (in3 << 1) | ((a   >> 3) & 0x1);
		in3 = (in3 << 1) | ((res >> 2) & 0x1);
		in3 = (in3 << 1) | ((b   >> 2) & 0x1);
		in3 = (in3 << 1) | ((a   >> 2) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lut0, in1, (res >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lut1, in1, (res >> 1) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lut2, in2, (res >> 2) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lut3, in3, (res >> 3) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lut4, in3, (res >> 4) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUT1 ");
	lutcfg_printhex_lut5(lut1);
	printf(" ");
	lutcfg_printhex_lut5(lut0);
	printf("\n");

	printf("LUT2 ");
	lutcfg_printhex_lut6(lut2);
	printf("\n");

	printf("LUT3 ");
	lutcfg_printhex_lut5(lut4);
	printf(" ");
	lutcfg_printhex_lut5(lut3);
	printf("\n");
}

static void adderlut6_5to6(bool is_signed) {
	// One LUT6, generate S0 and S1
	uint8_t* lutS0 = lutcfg_alloc(64);
	uint8_t* lutS1 = lutcfg_alloc(64);
	// One LUT6, generate carry after S1
	uint8_t* lutC1 = lutcfg_alloc(64);
	// One LUT6, generate S2 and S3
	uint8_t* lutS2 = lutcfg_alloc(64);
	uint8_t* lutS3 = lutcfg_alloc(64);
	// One LUT6, generate S4 and S5
	uint8_t* lutS4 = lutcfg_alloc(64);
	uint8_t* lutS5 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x3FF; i++) {
		int a = (i >> 0) & 0x1F;
		int b = (i >> 5) & 0x1F;

		if(is_signed == true) {
			unsigned sh = 32 - 5;
			a = (a << sh) >> sh;
			b = (b << sh) >> sh;
		}

		unsigned res = a + b;
		unsigned resC1 = (a & 0x3) + (b & 0x3);

		unsigned in01 = 0;
		in01 = (in01 << 1) | ((b >> 1) & 0x1);
		in01 = (in01 << 1) | ((a >> 1) & 0x1);
		in01 = (in01 << 1) | ((b >> 0) & 0x1);
		in01 = (in01 << 1) | ((a >> 0) & 0x1);

		unsigned in23 = 0;
		in23 = (in23 << 1) | ((b >> 3) & 0x1);
		in23 = (in23 << 1) | ((a >> 3) & 0x1);
		in23 = (in23 << 1) | ((b >> 2) & 0x1);
		in23 = (in23 << 1) | ((a >> 2) & 0x1);
		in23 = (in23 << 1) | ((resC1 >> 2) & 0x1);

		unsigned in45 = 0;
		in45 = (in45 << 1) | ((b   >> 4) & 0x1);
		in45 = (in45 << 1) | ((a   >> 4) & 0x1);
		in45 = (in45 << 1) | ((res >> 3) & 0x1);
		in45 = (in45 << 1) | ((b   >> 3) & 0x1);
		in45 = (in45 << 1) | ((a   >> 3) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutS0, in01, (res >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutS1, in01, (res >> 1) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutC1, in01, (resC1 >> 2) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutS2, in23, (res >> 2) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutS3, in23, (res >> 3) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutS4, in45, (res >> 4) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutS5, in45, (res >> 5) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTS01 ");
	lutcfg_printhex_lut5(lutS1);
	printf(" ");
	lutcfg_printhex_lut5(lutS0);
	printf("\n");

	printf("LUTC1 ");
	lutcfg_printhex_lut6(lutC1);
	printf("\n");

	printf("LUTS23 ");
	lutcfg_printhex_lut5(lutS3);
	printf(" ");
	lutcfg_printhex_lut5(lutS2);
	printf("\n");

	printf("LUTS45 ");
	lutcfg_printhex_lut5(lutS5);
	printf(" ");
	lutcfg_printhex_lut5(lutS4);
	printf("\n");
}

static void adderlut6_2in_plus() {
	// Adder 2in + 3b

	// Lut index 0
	uint8_t* lutA6 = lutcfg_alloc(64);
	uint8_t* lutA5 = lutcfg_alloc(64);

	// LUT index >= 1
	uint8_t* lutB6 = lutcfg_alloc(64);
	uint8_t* lutB5 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x3F; i++) {
		unsigned a0 = (i >> 0) & 0x1;
		unsigned b0 = (i >> 1) & 0x1;
		unsigned c0 = (i >> 2) & 0x1;  // in0 entered in position 0
		unsigned d0 = (i >> 3) & 0x1;  // in1 entered in position 0

		unsigned a1 = (i >> 4) & 0x1;
		unsigned b1 = (i >> 5) & 0x1;

		unsigned sum0 = a0 + b0 + c0;
		unsigned sum1 = a1 + b1;

		unsigned addr0 = (a0 << 0) + (b0 << 1) + (c0 << 2) + (d0 << 3);
		unsigned addr1 = (a1 << 0) + (b1 << 1) + (a0 << 2) + (b0 << 3) + (c0 << 4);

		errors_nb += lutcfg_setbit_verbose(lutA6, addr0, (sum0 ^ d0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutA5, addr0, d0);

		errors_nb += lutcfg_setbit_verbose(lutB6, addr1, ((sum0 >> 1) ^ (sum1)) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutB5, addr1, (sum0 >> 1) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
	printf("LUTB ");
	lutcfg_printhex_lut6_2_nl(lutB6, lutB5);
}

static void adderlut6_3in() {
	// See paper: Multiple Constant Multiplication With Ternary Adders (2013)
	// Config for 1 LUT6 + one CARRY4, repeat the LUT config for the 4 LUT6 around the CARRY4

	uint8_t* lutA5 = lutcfg_alloc(64);
	uint8_t* lutA6 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0xF; i++) {
		unsigned a = (i >> 0) & 0x1;
		unsigned b = (i >> 1) & 0x1;
		unsigned c = (i >> 2) & 0x1;
		unsigned d = (i >> 3) & 0x1;  // This is previous stage carry out

		// Full adder, as described in the paper
		unsigned o6 = ((a + b + c) & 0x1) ^ d;
		unsigned o5 = (a + b + c) >> 1;

		errors_nb += lutcfg_setbit_verbose(lutA6, i, o6);
		errors_nb += lutcfg_setbit_verbose(lutA5, i, o5);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
}

static void adderlut6_3in_2b() {
	// Architecture :
	//   1 partial sums for bits 0 : 3:2 compressor (1 LUT)
	//   1 LUT to generate sum S1 and S2
	//   1 LUT to generate sum S3 (signed only)
	// Total 2 LUTs (unsigned) or 3 LUTs (signed)

	// FIXME The signed version can also add +1 bit, could the unsigned version do it too ?

	uint8_t* lut_b0_A5 = lutcfg_alloc(64);
	uint8_t* lut_b0_A6 = lutcfg_alloc(64);

	// For unsigned version

	uint8_t* lut_S12_A5_u = lutcfg_alloc(64);
	uint8_t* lut_S12_A6_u = lutcfg_alloc(64);

	uint8_t* lut_S3_A6_u = lutcfg_alloc(64);

	// For signed version

	uint8_t* lut_S12_A5_s = lutcfg_alloc(64);
	uint8_t* lut_S12_A6_s = lutcfg_alloc(64);

	uint8_t* lut_S3_A6_s = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x3F; i++) {
		unsigned a = (i >> 0) & 0x3;
		unsigned b = (i >> 2) & 0x3;
		unsigned c = (i >> 4) & 0x3;

		unsigned sum_u = a + b + c;

		// Sign extension
		a -= (a & 0x02) << 1;
		b -= (b & 0x02) << 1;
		c -= (c & 0x02) << 1;

		unsigned sum_s = a + b + c;

		// This is 2b
		unsigned sum0 = (a & 1) + (b & 1) + (c & 1);
		sum0 = sum0 & 0x03;

		unsigned addr0 = 0;
		addr0 = (addr0 << 1) | (c & 1);
		addr0 = (addr0 << 1) | (b & 1);
		addr0 = (addr0 << 1) | (a & 1);

		errors_nb += lutcfg_setbit_verbose(lut_b0_A5, addr0, (sum0 >> 0) & 0x01);
		errors_nb += lutcfg_setbit_verbose(lut_b0_A6, addr0, (sum0 >> 1) & 0x01);

		unsigned addr1 = 0;
		addr1 = (addr1 << 1) | ((c >> 1) & 1);
		addr1 = (addr1 << 1) | ((b >> 1) & 1);
		addr1 = (addr1 << 1) | ((a >> 1) & 1);
		addr1 = (addr1 << 1) | ((sum0 >> 1) & 1);

		// Unsigned

		errors_nb += lutcfg_setbit_verbose(lut_S12_A5_u, addr1, (sum_u >> 1) & 0x01);
		errors_nb += lutcfg_setbit_verbose(lut_S12_A6_u, addr1, (sum_u >> 2) & 0x01);

		errors_nb += lutcfg_setbit_verbose(lut_S3_A6_u, addr1, (sum_u >> 3) & 0x01);

		// Signed

		errors_nb += lutcfg_setbit_verbose(lut_S12_A5_s, addr1, (sum_s >> 1) & 0x01);
		errors_nb += lutcfg_setbit_verbose(lut_S12_A6_s, addr1, (sum_s >> 2) & 0x01);

		errors_nb += lutcfg_setbit_verbose(lut_S3_A6_s, addr1, (sum_s >> 3) & 0x01);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("bits0 LUT A ");
	lutcfg_printhex_lut6_2_nl(lut_b0_A6, lut_b0_A5);

	printf("Unsigned\n");
	printf("sum12 LUT A ");
	lutcfg_printhex_lut6_2_nl(lut_S12_A6_u, lut_S12_A5_u);
	printf("sum3 LUT A ");
	lutcfg_printhex_lut5_nl(lut_S3_A6_u);

	printf("Signed\n");
	printf("sum12 LUT A ");
	lutcfg_printhex_lut6_2_nl(lut_S12_A6_s, lut_S12_A5_s);
	printf("sum3 LUT A ");
	lutcfg_printhex_lut5_nl(lut_S3_A6_s);

}

static void adderlut6_3ter3b() {
	uint8_t* table0 = lutcfg_alloc(64);
	uint8_t* table1 = lutcfg_alloc(64);
	uint8_t* table2 = lutcfg_alloc(64);
	uint8_t* table3 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	unsigned total_nbits = 6;
	unsigned max = uint_genmask(total_nbits);

	unsigned sh = 32 - 2;

	for(unsigned i=0; i<=max; i++) {
		int a = i << (sh - 0);
		int b = i << (sh - 2);
		int c = i << (sh - 4);

		a >>= sh;
		b >>= sh;
		c >>= sh;

		// Skip forbidden code for ternary
		if(a == -2 || b == -2 || c == -2) continue;

		int res1 = (a & 0x1) + (b & 0x1) + (c & 0x1);
		int res = a + b + c;

		unsigned in1 = 0;
		in1 = (in1 << 1) | ((c >> 0) & 0x1);
		in1 = (in1 << 1) | ((b >> 0) & 0x1);
		in1 = (in1 << 1) | ((a >> 0) & 0x1);

		unsigned in2 = 0;
		in2 = (in2 << 1) | ((res1 >> 1) & 0x1);
		in2 = (in2 << 1) | ((c    >> 1) & 0x1);
		in2 = (in2 << 1) | ((b    >> 1) & 0x1);
		in2 = (in2 << 1) | ((a    >> 1) & 0x1);

		errors_nb += lutcfg_setbit_verbose(table0, in1, (res  >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(table1, in1, (res1 >> 1) & 0x1);
		errors_nb += lutcfg_setbit_verbose(table2, in2, (res  >> 1) & 0x1);
		errors_nb += lutcfg_setbit_verbose(table3, in2, (res  >> 2) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUT1 ");
	lutcfg_printhex_lut5(table1);
	printf(" ");
	lutcfg_printhex_lut5(table0);
	printf("\n");

	printf("LUT2 ");
	lutcfg_printhex_lut5(table3);
	printf(" ");
	lutcfg_printhex_lut5(table2);
	printf("\n");
}

static void adderlut6_4in_2b() {
	// Architecture :
	//   2 partial sums for bits 0, and 1 of input : 4:2 compressors (1 LUT each)
	//   1 LUT to generate sum S1 and S2
	//   1 LUTs to generate sum S3
	// Total 4 LUTs

	uint8_t* lut_b0_A5 = lutcfg_alloc(64);
	uint8_t* lut_b0_A6 = lutcfg_alloc(64);

	uint8_t* lut_b1_A5 = lutcfg_alloc(64);
	uint8_t* lut_b1_A6 = lutcfg_alloc(64);

	// Unsigned

	uint8_t* lut_S12_A5_u = lutcfg_alloc(64);
	uint8_t* lut_S12_A6_u = lutcfg_alloc(64);

	uint8_t* lut_S3_A6_u = lutcfg_alloc(64);

	// Signed

	uint8_t* lut_S12_A5_s = lutcfg_alloc(64);
	uint8_t* lut_S12_A6_s = lutcfg_alloc(64);

	uint8_t* lut_S3_A6_s = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0xFF; i++) {
		unsigned a = (i >> 0) & 0x3;
		unsigned b = (i >> 2) & 0x3;
		unsigned c = (i >> 4) & 0x3;
		unsigned d = (i >> 6) & 0x3;

		unsigned sum_u = a + b + c + d;

		// Sign extension
		a -= (a & 0x02) << 1;
		b -= (b & 0x02) << 1;
		c -= (c & 0x02) << 1;
		d -= (d & 0x02) << 1;

		unsigned sum_s = a + b + c + d;

		// This is 2b
		unsigned sum0 = (a & 1) + (b & 1) + (c & 1) + (d & 1);
		sum0 = sum0 & 0x03;
		// This is 2b
		unsigned sum1 = (a & 2) + (b & 2) + (c & 2) + (d & 2);
		sum1 = (sum1 >> 1) & 0x03;

		unsigned addr0 = 0;
		addr0 = (addr0 << 1) | (d & 1);
		addr0 = (addr0 << 1) | (c & 1);
		addr0 = (addr0 << 1) | (b & 1);
		addr0 = (addr0 << 1) | (a & 1);

		unsigned addr1 = 0;
		addr1 = (addr1 << 1) | (d & 2);
		addr1 = (addr1 << 1) | (c & 2);
		addr1 = (addr1 << 1) | (b & 2);
		addr1 = (addr1 << 1) | (a & 2);
		addr1 >>= 1;

		errors_nb += lutcfg_setbit_verbose(lut_b0_A5, addr0, (sum0 >> 0) & 0x01);
		errors_nb += lutcfg_setbit_verbose(lut_b0_A6, addr0, (sum0 >> 1) & 0x01);

		errors_nb += lutcfg_setbit_verbose(lut_b1_A5, addr1, (sum1 >> 0) & 0x01);
		errors_nb += lutcfg_setbit_verbose(lut_b1_A6, addr1, (sum1 >> 1) & 0x01);

		unsigned addr12 = 0;
		addr12 = (addr12 << 2) | (sum1 & 0x03);
		addr12 = (addr12 << 1) | ((d >> 0) & 1);
		addr12 = (addr12 << 2) | (sum0 & 0x03);

		unsigned addr3 = 0;
		addr3 = (addr3 << 1) | ((d >> 1) & 1);
		addr3 = (addr3 << 2) | (sum1 & 0x03);
		addr3 = (addr3 << 1) | ((d >> 0) & 1);
		addr3 = (addr3 << 2) | (sum0 & 0x03);

		// Unsigned

		errors_nb += lutcfg_setbit_verbose(lut_S12_A5_u, addr12, (sum_u >> 1) & 0x01);
		errors_nb += lutcfg_setbit_verbose(lut_S12_A6_u, addr12, (sum_u >> 2) & 0x01);

		errors_nb += lutcfg_setbit_verbose(lut_S3_A6_u, addr3, (sum_u >> 3) & 0x01);

		// Signed

		errors_nb += lutcfg_setbit_verbose(lut_S12_A5_s, addr12, (sum_s >> 1) & 0x01);
		errors_nb += lutcfg_setbit_verbose(lut_S12_A6_s, addr12, (sum_s >> 2) & 0x01);

		errors_nb += lutcfg_setbit_verbose(lut_S3_A6_s, addr3, (sum_s >> 3) & 0x01);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("bits0 LUT A ");
	lutcfg_printhex_lut6_2_nl(lut_b0_A6, lut_b0_A5);
	printf("bits1 LUT A ");
	lutcfg_printhex_lut6_2_nl(lut_b1_A6, lut_b1_A5);

	printf("Unsigned\n");
	printf("sum12 LUT A ");
	lutcfg_printhex_lut6_2_nl(lut_S12_A6_u, lut_S12_A5_u);
	printf("sum3 LUT A ");
	lutcfg_printhex_lut6_nl(lut_S3_A6_u);

	printf("Signed\n");
	printf("sum12 LUT A ");
	lutcfg_printhex_lut6_2_nl(lut_S12_A6_s, lut_S12_A5_s);
	printf("sum3 LUT A ");
	lutcfg_printhex_lut6_nl(lut_S3_A6_s);
}

static void adderlut6_4ter4b() {
	// LUT to sum the bit index 0
	uint8_t* lut10 = lutcfg_alloc(64);
	uint8_t* lut11 = lutcfg_alloc(64);
	// LUT to sum the bit index 1
	uint8_t* lut20 = lutcfg_alloc(64);
	uint8_t* lut21 = lutcfg_alloc(64);
	// LUT to generate result bits 2-3
	uint8_t* lut3 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	unsigned total_nbits = 8;
	unsigned max = uint_genmask(total_nbits);

	unsigned sh = 32 - 2;

	for(unsigned i=0; i<=max; i++) {
		int a = i << (sh - 0);
		int b = i << (sh - 2);
		int c = i << (sh - 4);
		int d = i << (sh - 6);

		a >>= sh;
		b >>= sh;
		c >>= sh;
		d >>= sh;

		// Skip forbidden code for ternary
		if(a == -2 || b == -2 || c == -2 || d == -2) continue;

		int res1 = ((a & 0x2) + (b & 0x2) + (c & 0x2) + (d & 0x2)) >> 1;
		int res = a + b + c + d;

		unsigned in1 = 0;
		in1 = (in1 << 1) | ((res1 >> 0) & 0x1);
		in1 = (in1 << 1) | ((d >> 0) & 0x1);
		in1 = (in1 << 1) | ((c >> 0) & 0x1);
		in1 = (in1 << 1) | ((b >> 0) & 0x1);
		in1 = (in1 << 1) | ((a >> 0) & 0x1);

		unsigned in2 = 0;
		in2 = (in2 << 1) | ((d >> 1) & 0x1);
		in2 = (in2 << 1) | ((c >> 1) & 0x1);
		in2 = (in2 << 1) | ((b >> 1) & 0x1);
		in2 = (in2 << 1) | ((a >> 1) & 0x1);

		unsigned in3 = 0;
		in3 = (in3 << 1) | ((d >> 0) & 0x1);
		in3 = (in3 << 2) | ((res1 >> 0) & 0x3);
		in3 = (in3 << 1) | ((a >> 1) & 0x1);
		in3 = (in3 << 2) | ((res >> 0) & 0x3);

		errors_nb += lutcfg_setbit_verbose(lut10, in1, (res >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lut11, in1, (res >> 1) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lut20, in2, (res1 >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lut21, in2, (res1 >> 1) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lut3, in3 & 0x1F, (res >> 3) & 0x1);  // sign bit generated by O5
		errors_nb += lutcfg_setbit_verbose(lut3, in3,        (res >> 2) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUT1 ");
	lutcfg_printhex_lut5(lut11);
	printf(" ");
	lutcfg_printhex_lut5(lut10);
	printf("\n");

	printf("LUT2 ");
	lutcfg_printhex_lut5(lut21);
	printf(" ");
	lutcfg_printhex_lut5(lut20);
	printf("\n");

	printf("LUT3 ");
	lutcfg_printhex_lut6(lut3);
	printf("\n");
}

static void adderlut6_5in_2b() {
	// Architecture :
	//   One partial sum for bits 0 of input (2 LUTs, generate 3b)
	//   One partial sum for bits 1 of input : 5:2 compressor (1 LUT)
	//   1 LUT to generate sum S1 and S2
	//   1 LUT to generate sum S3 and S4
	// Total 5 LUTs

	uint8_t* lut_b0_A5 = lutcfg_alloc(64);
	uint8_t* lut_b0_A6 = lutcfg_alloc(64);
	uint8_t* lut_b0_B5 = lutcfg_alloc(64);

	uint8_t* lut_b1_A5 = lutcfg_alloc(64);
	uint8_t* lut_b1_A6 = lutcfg_alloc(64);

	// Unsigned

	uint8_t* lut_S12_A5_u = lutcfg_alloc(64);
	uint8_t* lut_S12_A6_u = lutcfg_alloc(64);

	uint8_t* lut_S34_A5_u = lutcfg_alloc(64);
	uint8_t* lut_S34_A6_u = lutcfg_alloc(64);

	// Signed

	uint8_t* lut_S12_A5_s = lutcfg_alloc(64);
	uint8_t* lut_S12_A6_s = lutcfg_alloc(64);

	uint8_t* lut_S34_A5_s = lutcfg_alloc(64);
	uint8_t* lut_S34_A6_s = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned s=0; s<=1; s++) {

		uint8_t* ptr_S12_A5 = (s == 0) ? lut_S12_A5_u : lut_S12_A5_s;
		uint8_t* ptr_S12_A6 = (s == 0) ? lut_S12_A6_u : lut_S12_A6_s;
		uint8_t* ptr_S34_A5 = (s == 0) ? lut_S34_A5_u : lut_S34_A5_s;
		uint8_t* ptr_S34_A6 = (s == 0) ? lut_S34_A6_u : lut_S34_A6_s;

		for(unsigned i=0; i<=0x3FF; i++) {
			unsigned a = (i >> 0) & 0x3;
			unsigned b = (i >> 2) & 0x3;
			unsigned c = (i >> 4) & 0x3;
			unsigned d = (i >> 6) & 0x3;
			unsigned e = (i >> 8) & 0x3;

			// Sign extension
			if(s != 0) {
				a -= (a & 0x02) << 1;
				b -= (b & 0x02) << 1;
				c -= (c & 0x02) << 1;
				d -= (d & 0x02) << 1;
				e -= (e & 0x02) << 1;
			}

			unsigned sum = a + b + c + d + e;

			// This is 3b
			unsigned sum0 = (a & 1) + (b & 1) + (c & 1) + (d & 1) + (e & 1);
			// This is 2b
			unsigned sum1 = (a & 2) + (b & 2) + (c & 2) + (d & 2) + (e & 2);
			sum1 = (sum1 >> 1) & 0x03;

			unsigned addr0 = 0;
			addr0 = (addr0 << 1) | (e & 1);
			addr0 = (addr0 << 1) | (d & 1);
			addr0 = (addr0 << 1) | (c & 1);
			addr0 = (addr0 << 1) | (b & 1);
			addr0 = (addr0 << 1) | (a & 1);

			unsigned addr1 = 0;
			addr1 = (addr1 << 1) | (e & 2);
			addr1 = (addr1 << 1) | (d & 2);
			addr1 = (addr1 << 1) | (c & 2);
			addr1 = (addr1 << 1) | (b & 2);
			addr1 = (addr1 << 1) | (a & 2);
			addr1 >>= 1;

			errors_nb += lutcfg_setbit_verbose(lut_b0_A5, addr0, (sum0 >> 0) & 0x01);
			errors_nb += lutcfg_setbit_verbose(lut_b0_A6, addr0, (sum0 >> 1) & 0x01);
			errors_nb += lutcfg_setbit_verbose(lut_b0_B5, addr0, (sum0 >> 2) & 0x01);

			errors_nb += lutcfg_setbit_verbose(lut_b1_A5, addr1, (sum1 >> 0) & 0x01);
			errors_nb += lutcfg_setbit_verbose(lut_b1_A6, addr1, (sum1 >> 1) & 0x01);

			unsigned addr12 = 0;
			addr12 = (addr12 << 2) | (sum1 & 0x03);
			addr12 = (addr12 << 2) | ((sum0 >> 1) & 0x03);

			errors_nb += lutcfg_setbit_verbose(ptr_S12_A5, addr12, (sum >> 1) & 0x01);
			errors_nb += lutcfg_setbit_verbose(ptr_S12_A6, addr12, (sum >> 2) & 0x01);

			unsigned addr34 = 0;
			addr34 = (addr34 << 1) | ((e >> 1) & 1);
			addr34 = (addr34 << 1) | ((d >> 1) & 1);
			addr34 = (addr34 << 2) | (sum1 & 0x03);
			addr34 = (addr34 << 1) | ((sum >> 2) & 0x01);

			errors_nb += lutcfg_setbit_verbose(ptr_S34_A5, addr34, (sum >> 3) & 0x01);
			errors_nb += lutcfg_setbit_verbose(ptr_S34_A6, addr34, (sum >> 4) & 0x01);

			inputs_nb++;

		}  // Scan combinations of inputs
	}  // Signedness

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("bits0 LUT A ");
	lutcfg_printhex_lut6_2_nl(lut_b0_A6, lut_b0_A5);
	printf("bits0 LUT B ");
	lutcfg_printhex_lut5_nl(lut_b0_B5);

	printf("bits1 LUT A ");
	lutcfg_printhex_lut6_2_nl(lut_b1_A6, lut_b1_A5);

	printf("Unsigned\n");
	printf("sum12 LUT A ");
	lutcfg_printhex_lut6_2_nl(lut_S12_A6_u, lut_S12_A5_u);
	printf("sum34 LUT A ");
	lutcfg_printhex_lut6_2_nl(lut_S34_A6_u, lut_S34_A5_u);

	printf("Signed\n");
	printf("sum12 LUT A ");
	lutcfg_printhex_lut6_2_nl(lut_S12_A6_s, lut_S12_A5_s);
	printf("sum34 LUT A ");
	lutcfg_printhex_lut6_2_nl(lut_S34_A6_s, lut_S34_A5_s);
}

static void adderlut6_5ter4b() {
	// LUTs for partial sums
	uint8_t* lutP00 = lutcfg_alloc(64);
	uint8_t* lutP01 = lutcfg_alloc(64);
	uint8_t* lutP10 = lutcfg_alloc(64);
	uint8_t* lutP11 = lutcfg_alloc(64);
	uint8_t* lutP02 = lutcfg_alloc(64);
	uint8_t* lutP12 = lutcfg_alloc(64);
	// LUTs for final sum
	uint8_t* lutS0 = lutcfg_alloc(64);
	uint8_t* lutS1 = lutcfg_alloc(64);
	uint8_t* lutS2 = lutcfg_alloc(64);
	uint8_t* lutS3 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	unsigned sh = 32 - 2;

	for(unsigned i=0; i<=0xFFF; i++) {
		int a = i << (sh - 0);
		int b = i << (sh - 2);
		int c = i << (sh - 4);
		int d = i << (sh - 6);
		int e = i << (sh - 8);

		a >>= sh;
		b >>= sh;
		c >>= sh;
		d >>= sh;
		e >>= sh;

		// Skip forbidden code for ternary
		if(a == -2 || b == -2 || c == -2) continue;
		if(d == -2 || e == -2) continue;

		int part0 = (a & 0x1) + (b & 0x1) + (c & 0x1) + (d & 0x1) + (e & 0x1);
		int part1 = ((a >> 1) & 0x1) + ((b >> 1) & 0x1) + ((c >> 1) & 0x1) + ((d >> 1) & 0x1) + ((e >> 1) & 0x1);

		int sum = a + b + c + d + e;

		unsigned inP0 = 0;
		inP0 = (inP0 << 1) | ((e >> 0) & 0x1);
		inP0 = (inP0 << 1) | ((d >> 0) & 0x1);
		inP0 = (inP0 << 1) | ((c >> 0) & 0x1);
		inP0 = (inP0 << 1) | ((b >> 0) & 0x1);
		inP0 = (inP0 << 1) | ((a >> 0) & 0x1);

		unsigned inP1 = 0;
		inP1 = (inP1 << 1) | ((e >> 1) & 0x1);
		inP1 = (inP1 << 1) | ((d >> 1) & 0x1);
		inP1 = (inP1 << 1) | ((c >> 1) & 0x1);
		inP1 = (inP1 << 1) | ((b >> 1) & 0x1);
		inP1 = (inP1 << 1) | ((a >> 1) & 0x1);

		unsigned inS01 = 0;
		inS01 = (inS01 << 1) | ((part1 >> 0) & 0x1);
		inS01 = (inS01 << 2) | ((part0 >> 0) & 0x3);

		unsigned inS23 = 0;
		inS23 = (inS23 << 3) | ((part1 >> 0) & 0x7);
		inS23 = (inS23 << 2) | ((part0 >> 1) & 0x3);

		errors_nb += lutcfg_setbit_verbose(lutP00, inP0, (part0 >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutP01, inP0, (part0 >> 1) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutP02, inP0, (part0 >> 2) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutP10, inP1, (part1 >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutP11, inP1, (part1 >> 1) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutP12, inP1, (part1 >> 2) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutS0, inS01, (sum >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutS1, inS01, (sum >> 1) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutS2, inS23, (sum >> 2) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutS3, inS23, (sum >> 3) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUT P0 ");
	lutcfg_printhex_lut6_2_nl(lutP01, lutP00);
	printf("LUT P1 ");
	lutcfg_printhex_lut6_2_nl(lutP11, lutP10);
	printf("LUT P02 ");
	lutcfg_printhex_lut5_nl(lutP02);
	printf("LUT P12 ");
	lutcfg_printhex_lut5_nl(lutP12);
	printf("LUT S01 ");
	lutcfg_printhex_lut6_2_nl(lutS1, lutS0);
	printf("LUT S23 ");
	lutcfg_printhex_lut6_2_nl(lutS3, lutS2);
}

static void adderlut6_7ter4b() {
	// Use a compressor 6,0,6:5 as base to generate a "signature" of as many bits as possible
	// The 7 bits index 0 enter compressor in index 0, and 6 bits index 1 enter compressor in index 2

	// Use 2 LUTs to generate the final 4-bit result
	uint8_t* lutS1 = lutcfg_alloc(64);
	uint8_t* lutS2 = lutcfg_alloc(64);
	uint8_t* lutS3 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	unsigned total_nbits = 14;
	unsigned max = uint_genmask(total_nbits);

	unsigned sh = 32 - 2;

	for(unsigned i=0; i<=max; i++) {
		int a = i << (sh - 0);
		int b = i << (sh - 2);
		int c = i << (sh - 4);
		int d = i << (sh - 6);
		int e = i << (sh - 8);
		int f = i << (sh - 10);
		int g = i << (sh - 12);

		a >>= sh;
		b >>= sh;
		c >>= sh;
		d >>= sh;
		e >>= sh;
		f >>= sh;
		g >>= sh;

		// Skip forbidden code for ternary
		if(a == -2 || b == -2 || c == -2) continue;
		if(d == -2 || e == -2 || f == -2) continue;
		if(g == -2) continue;

		// The wanted result
		int sum = a + b + c + d + e + f + g;

		// Sum of 7 bits index 0
		unsigned sum0 = (a & 0x1) + (b & 0x1) + (c & 0x1) + (d & 0x1) + (e & 0x1) + (f & 0x1) + (g & 0x1);
		// Sum of 6 bits index 1
		unsigned sum1 = (a & 0x2) + (b & 0x2) + (c & 0x2) + (d & 0x2) + (e & 0x2) + (f & 0x2);
		sum1 >>= 1;

		// The partial sum generated by the 6,0,6:5 compressor
		unsigned partsum = sum0 + (sum1 << 2);

		// Input of the final LUTs: simply the 5-bit partial sum
		unsigned inA = 0;
		unsigned inB = 0;

		inA = (inA << 1) | ((g >> 1) & 0x1);
		inA = (inA << 1) | ((sum0 >> 2) & 0x1);
		inA = (inA << 1) | ((partsum >> 3) & 0x1);
		inA = (inA << 1) | ((partsum >> 2) & 0x1);
		inA = (inA << 1) | ((partsum >> 1) & 0x1);

		inB = (inB << 1) | ((g >> 1) & 0x1);
		inB = (inB << 1) | ((sum0 >> 2) & 0x1);
		inB = (inB << 1) | ((partsum >> 4) & 0x1);
		inB = (inB << 1) | ((partsum >> 3) & 0x1);
		inB = (inB << 1) | ((partsum >> 2) & 0x1);
		inB = (inB << 1) | ((partsum >> 1) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutS1, inA, (sum >> 1) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutS2, inA, (sum >> 2) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutS3, inB, (sum >> 3) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut5(lutS2);
	printf(" ");
	lutcfg_printhex_lut5(lutS1);
	printf("\n");

	printf("LUTB ");
	lutcfg_printhex_lut6(lutS3);
	printf("\n");
}

static void adderlut6_mul_ter2b() {
	uint8_t* lut0 = lutcfg_alloc(64);
	uint8_t* lut1 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	unsigned total_nbits = 4;
	unsigned max = uint_genmask(total_nbits);

	unsigned sh = 32 - 2;

	for(unsigned i=0; i<=max; i++) {
		int d = i << (sh - 0);
		int w = i << (sh - 2);

		d >>= sh;
		w >>= sh;

		// Skip forbidden code for ternary
		if(d == -2 || w == -2) continue;

		int mul = d * w;

		unsigned in = 0;
		in = (in << 2) | ((w >> 0) & 0x3);
		in = (in << 2) | ((d >> 0) & 0x3);

		errors_nb += lutcfg_setbit_verbose(lut0, in, (mul >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lut1, in, (mul >> 1) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUT ");
	lutcfg_printhex_lut5(lut1);
	printf(" ");
	lutcfg_printhex_lut5(lut0);
	printf("\n");
}

static void adderlut6_muladd_2ter3b() {
	// LUT for input A
	uint8_t* lutA0 = lutcfg_alloc(64);
	uint8_t* lutA1 = lutcfg_alloc(64);
	// LUT for input B
	uint8_t* lutB0 = lutcfg_alloc(64);
	uint8_t* lutB1 = lutcfg_alloc(64);
	// LUT for sum
	uint8_t* lutS0 = lutcfg_alloc(64);
	uint8_t* lutS1 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	unsigned total_nbits = 8;
	unsigned max = uint_genmask(total_nbits);

	unsigned sh = 32 - 2;

	for(unsigned i=0; i<=max; i++) {
		int ad = i << (sh - 0);
		int aw = i << (sh - 2);
		int bd = i << (sh - 4);
		int bw = i << (sh - 6);

		ad >>= sh;
		aw >>= sh;
		bd >>= sh;
		bw >>= sh;

		// Skip forbidden code for ternary
		if(ad == -2 || aw == -2 || bd == -2 || bw == -2) continue;

		int mulA = ad * aw;
		int mulB = bd * bw;
		int sum  = mulA + mulB;

		unsigned inA = 0;
		inA = (inA << 2) | ((aw >> 0) & 0x3);
		inA = (inA << 2) | ((ad >> 0) & 0x3);

		unsigned inB = 0;
		inB = (inB << 1) | ((mulA >> 0) & 0x1);
		inB = (inB << 2) | ((bw   >> 0) & 0x3);
		inB = (inB << 2) | ((bd   >> 0) & 0x3);

		unsigned inS = 0;
		inS = (inS << 2) | ((mulA >> 0) & 0x3);
		inS = (inS << 1) | ((mulB >> 1) & 0x1);
		inS = (inS << 1) | ((sum  >> 0) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutA0, inA, (mulA >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutA1, inA, (mulA >> 1) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutB0, inB, (sum  >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutB1, inB, (mulB >> 1) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutS0, inS, (sum  >> 1) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutS1, inS, (sum  >> 2) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUT A ");
	lutcfg_printhex_lut5(lutA1);
	printf(" ");
	lutcfg_printhex_lut5(lutA0);
	printf("\n");

	printf("LUT B ");
	lutcfg_printhex_lut5(lutB1);
	printf(" ");
	lutcfg_printhex_lut5(lutB0);
	printf("\n");

	printf("LUT S ");
	lutcfg_printhex_lut5(lutS1);
	printf(" ");
	lutcfg_printhex_lut5(lutS0);
	printf("\n");
}

static void adderlut6_muladd_st0() {
	// Only stage 0 of mult-adder component

	// The LUTs to perform unsigned-style operation
	uint8_t* lutU5 = lutcfg_alloc(64);
	uint8_t* lutU6 = lutcfg_alloc(64);
	// Final LUT, where MSB of signed result is carry out
	uint8_t* lutS5 = lutcfg_alloc(64);
	uint8_t* lutS6 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	unsigned total_nbits = 5;
	unsigned max = uint_genmask(total_nbits);

	for(unsigned i=0; i<=max; i++) {
		// The ALU code
		unsigned code = (i >> 0) & 0x7;
		// The inputs for the first LUTs
		int au = (i >> 3) & 0x1;
		int bu = (i >> 4) & 0x1;
		// The inputs for the last LUT
		int as = (au << 31) >> 31;
		int bs = (bu << 31) >> 31;

		// Decode the ALU command
		int ma = 0;
		int mb = 0;
		if(code == 0x0) { mb =  0; ma =  0; }
		if(code == 0x1) { mb =  0; ma = -1; }
		if(code == 0x2) { mb =  0; ma =  1; }
		if(code == 0x3) { mb =  1; ma = -1; }
		if(code == 0x4) { mb =  1; ma =  0; }
		if(code == 0x5) { mb = -1; ma =  0; }
		if(code == 0x6) { mb =  1; ma =  1; }
		if(code == 0x7) { mb = -1; ma =  1; }

		// The wanted results
		if(ma == -1) { au = ~au; as = ~as; } else { au *= ma; as *= ma; }
		if(mb == -1) { bu = ~bu; bs = ~bs; } else { bu *= mb; bs *= mb; }
		au &= 0x1;
		bu &= 0x1;
		int sum_u = au + bu;
		int sum_s = as + bs;

		// For sign-extended MSB, the result MSB is generated by output O0 of the upper CARRY4
		// The carry out is negated
		// Truth table
		// CI B A | signed sum | S DI | O3 CO | -CO
		// =========================================
		//  0 0 0 |         00 | 0  1 |  0  1 |   0
		//  0 0 1 |         11 | 1  0 |  1  0 |   1
		//  0 1 0 |         11 | 1  0 |  1  0 |   1
		//  0 1 1 |         10 | 0  0 |  0  0 |   1
		// =========================================
		//  1 0 0 |         01 | 0  1 |  0  1 |   0
		//  1 0 1 |         00 | 1  0 |  1  1 |   0
		//  1 1 0 |         00 | 1  0 |  1  1 |   0
		//  1 1 1 |         11 | 0  0 |  0  0 |   1

		// Notes about LUT outputs O6 and O5:
		// - O6 is mux select and sum output
		// - O5 is carry generation

		// This is for non-MSB bits
		errors_nb += lutcfg_setbit_verbose(lutU6, i, (sum_u >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutU5, i, (sum_u >> 1) & 0x1);

		// Note: The carry generation is only active when the Select input is zero
		errors_nb += lutcfg_setbit_verbose(lutS6, i, (sum_s >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutS5, i, (~sum_s >> 1) & 0x1 & (~sum_s));

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTU ");
	lutcfg_printhex_lut5(lutU6);
	printf(" ");
	lutcfg_printhex_lut5(lutU5);
	printf("\n");

	printf("LUTS ");
	lutcfg_printhex_lut5(lutS6);
	printf(" ");
	lutcfg_printhex_lut5(lutS5);
	printf("\n");
}

static void adderlut6_muladd_bin_4in(bool is_xnor) {
	// 4-input multiply-accumulate component

	// Partial sum 0
	uint8_t* lut_part0_o5 = lutcfg_alloc(64);
	uint8_t* lut_part0_o6 = lutcfg_alloc(64);

	// Partial sum 1 (generates S0)
	uint8_t* lut_part1_o5 = lutcfg_alloc(64);
	uint8_t* lut_part1_o6 = lutcfg_alloc(64);

	// The final results S1 and S2
	uint8_t* lut_S12_o5 = lutcfg_alloc(64);
	uint8_t* lut_S12_o6 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0xFF; i++) {
		unsigned d0 = (i >> 0) & 0x1;
		unsigned d1 = (i >> 1) & 0x1;
		unsigned d2 = (i >> 2) & 0x1;
		unsigned d3 = (i >> 3) & 0x1;
		unsigned w0 = (i >> 4) & 0x1;
		unsigned w1 = (i >> 5) & 0x1;
		unsigned w2 = (i >> 6) & 0x1;
		unsigned w3 = (i >> 7) & 0x1;

		// These are processed in partial sum 0
		unsigned m0 = d0 * w0;
		unsigned m1 = d1 * w1;
		if(is_xnor == true) {
			m0 = (d0 == w0);
			m1 = (d1 == w1);
		}
		// These are processed in partial sum 1
		unsigned m2 = d2 * w2;
		unsigned m3 = d3 * w3;
		if(is_xnor == true) {
			m2 = (d2 == w2);
			m3 = (d3 == w3);
		}

		unsigned sum = m0 + m1 + m2 + m3;

		unsigned part0 = m0 + m1;
		unsigned part1 = m2 + m3 + (part0 & 1);

		unsigned addr0 = 0;
		addr0 = (addr0 << 1) | w1;
		addr0 = (addr0 << 1) | w0;
		addr0 = (addr0 << 1) | d1;
		addr0 = (addr0 << 1) | d0;

		unsigned addr1 = 0;
		addr1 = (addr1 << 1) | (part0 & 1);
		addr1 = (addr1 << 1) | w3;
		addr1 = (addr1 << 1) | w2;
		addr1 = (addr1 << 1) | d3;
		addr1 = (addr1 << 1) | d2;

		// Partial sum 0
		errors_nb += lutcfg_setbit_verbose(lut_part0_o5, addr0, (part0 >> 0) & 1);
		errors_nb += lutcfg_setbit_verbose(lut_part0_o6, addr0, (part0 >> 1) & 1);

		// Partial sum 1 + generation of S0
		errors_nb += lutcfg_setbit_verbose(lut_part1_o5, addr1, (sum >> 0) & 1);
		errors_nb += lutcfg_setbit_verbose(lut_part1_o6, addr1, (part1 >> 1) & 1);

		unsigned addr2 = 0;
		addr2 = (addr2 << 2) | part0;
		addr2 = (addr2 << 1) | ((part1 >> 1) & 1);
		addr2 = (addr2 << 1) | ((sum >> 0) & 1);

		// Generation of S1 and S2
		errors_nb += lutcfg_setbit_verbose(lut_S12_o5, addr2, (sum >> 1) & 1);
		errors_nb += lutcfg_setbit_verbose(lut_S12_o6, addr2, (sum >> 2) & 1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("Operation %s\n", is_xnor ? "XNOR" : "AND");
	printf("LUT P0  ");
	lutcfg_printhex_lut6_2_nl(lut_part0_o6, lut_part0_o5);
	printf("LUT P1  ");
	lutcfg_printhex_lut6_2_nl(lut_part1_o6, lut_part1_o5);
	printf("LUT S12 ");
	lutcfg_printhex_lut6_2_nl(lut_S12_o6, lut_S12_o5);
}

static void adderlut6_comp_3t5b() {

	// Compressor LUTs
	// 6 bits for 3 input trits, generate 5-bit code -> need 5 LUTs
	uint8_t* comlut0 = lutcfg_alloc(64);
	uint8_t* comlut1 = lutcfg_alloc(64);
	uint8_t* comlut2 = lutcfg_alloc(64);
	uint8_t* comlut3 = lutcfg_alloc(64);
	uint8_t* comlut4 = lutcfg_alloc(64);

	// Decompressor LUTs
	// 5-bit input code, generate 6 bits -> need 3 LUTs
	uint8_t* declutA5 = lutcfg_alloc(64);
	uint8_t* declutA6 = lutcfg_alloc(64);
	uint8_t* declutB5 = lutcfg_alloc(64);
	uint8_t* declutB6 = lutcfg_alloc(64);
	uint8_t* declutC5 = lutcfg_alloc(64);
	uint8_t* declutC6 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	unsigned total_nbits = 6;
	unsigned max = uint_genmask(total_nbits);

	unsigned cur_code = 0;

	for(unsigned i=0; i<=max; i++) {
		unsigned a = (i >> 0) & 0x3;
		unsigned b = (i >> 2) & 0x3;
		unsigned c = (i >> 4) & 0x3;

		// Skip forbidden code for ternary
		if(a==2 || b==2 || c==2) continue;

		// Compressor LUTs

		errors_nb += lutcfg_setbit_verbose(comlut0, i, (cur_code >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(comlut1, i, (cur_code >> 1) & 0x1);
		errors_nb += lutcfg_setbit_verbose(comlut2, i, (cur_code >> 2) & 0x1);
		errors_nb += lutcfg_setbit_verbose(comlut3, i, (cur_code >> 3) & 0x1);
		errors_nb += lutcfg_setbit_verbose(comlut4, i, (cur_code >> 4) & 0x1);

		// Decompressor LUTs

		errors_nb += lutcfg_setbit_verbose(declutA5, cur_code, (a >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(declutA6, cur_code, (a >> 1) & 0x1);

		errors_nb += lutcfg_setbit_verbose(declutB5, cur_code, (b >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(declutB6, cur_code, (b >> 1) & 0x1);

		errors_nb += lutcfg_setbit_verbose(declutC5, cur_code, (c >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(declutC6, cur_code, (c >> 1) & 0x1);

		// Next compressed code
		cur_code++;

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config : compressor

	printf("\n");
	printf("com LUT0 : "); lutcfg_printhex_lut6_nl(comlut0);
	printf("com LUT1 : "); lutcfg_printhex_lut6_nl(comlut1);
	printf("com LUT2 : "); lutcfg_printhex_lut6_nl(comlut2);
	printf("com LUT3 : "); lutcfg_printhex_lut6_nl(comlut3);
	printf("com LUT4 : "); lutcfg_printhex_lut6_nl(comlut4);

	// Print LUT config : decompressor

	printf("\n");
	printf("dec LUTA : "); lutcfg_printhex_lut6_2_nl(declutA6, declutA5);
	printf("dec LUTB : "); lutcfg_printhex_lut6_2_nl(declutB6, declutB5);
	printf("dec LUTC : "); lutcfg_printhex_lut6_2_nl(declutC6, declutC5);
	printf("\n");
}

static void adderlut6_tercomp_8b5t() {
	// LUTs for conversion of lower 5b into 3T
	uint8_t* lut_lower_ter_A0 = lutcfg_alloc(64);
	uint8_t* lut_lower_ter_A1 = lutcfg_alloc(64);
	uint8_t* lut_lower_ter_B0 = lutcfg_alloc(64);
	uint8_t* lut_lower_ter_B1 = lutcfg_alloc(64);
	uint8_t* lut_lower_ter_C0 = lutcfg_alloc(64);
	uint8_t* lut_lower_ter_C1 = lutcfg_alloc(64);

	// Generate the flag that indicates that we are in the 5 extra codes (or only in the 4 actually used ones)
	uint8_t* lut_extra_flag = lutcfg_alloc(64);

	// Generate the remaining 2T from the 6b (previous 3b are lower, remaining 3b of bin code are upper)
	uint8_t* lut_upper_2T_A0 = lutcfg_alloc(64);
	uint8_t* lut_upper_2T_A1 = lutcfg_alloc(64);
	uint8_t* lut_upper_2T_B0 = lutcfg_alloc(64);
	uint8_t* lut_upper_2T_B1 = lutcfg_alloc(64);

	// LUTs for conversion of lower 5b into 3T
	uint8_t* lut_recode_ter_A0 = lutcfg_alloc(64);
	uint8_t* lut_recode_ter_A1 = lutcfg_alloc(64);
	uint8_t* lut_recode_ter_B0 = lutcfg_alloc(64);
	uint8_t* lut_recode_ter_B1 = lutcfg_alloc(64);
	uint8_t* lut_recode_ter_C0 = lutcfg_alloc(64);
	uint8_t* lut_recode_ter_C1 = lutcfg_alloc(64);

	/* Description
	 *
	 * Attempt at obtaining very compact decoder
	 * - All binary codes : upper 3b, lower 5b : xxx xxxxx
	 * - Lower 5b always generate 3T + one 3b code that indicates one of the 5 extra codes
	 *
	 * Landscape of all 256 binary codes :
	 *  5 codes
	 * 27 codes
	 *  5 codes
	 * 27 codes
	 *  5 codes
	 * 27 codes
	 *  5 codes
	 * 27 codes
	 *  5 codes
	 * 27 codes
	 *  5 codes
	 * 27 codes
	 *  5 codes
	 * 27 codes
	 *  5 codes
	 * 27 codes
	 *
	 * The 3 upper bits of the binary code can systematically generate 8 codes, among the 9 codes to generate for the remaining 3T
	 *
	 * 3 upper binary bits => generate  8 ter codes, for the 2 upper trits (the code "33" is skipped)
	 * 5 lower binary bits => generate 27 ter codes, for the 3 lower trits
	 *
	 * Remaining to generate : all ter codes of pattern : 3 3 x x x
	 * Total : 27 codes to map on the 5*8 = 40 available codes (the sections of 5 codes distributed between the groups of 27 codes)
	 *
	 * It is enough to use at most 4 codes from each section of 5 -> directly use the 2 LSB of the binary code
	 *
	 * Problem : depending on which 5-codes section we are in, the lower 3T to generate may not be constant
	 * So it may be needed to add an additional recoding stage for the lower 3T:
	 *   from 2b ter result + 1b that indicates we are in the 5 extra codes + the 3b upper bin codes => recode the lower 3T (but need 6 LUT6)
	 *
	 * Total : 14 LUT6 / LUT6_2
	 *
	 * FIXME Need to study if the recoding of lower 3T is REALLY necessary
	 */

	char bin2char[1024][6];  // Characters are '0', '1', and '3', and value 0 means unset
	char allchar[1024][6];  // All
	for(unsigned i=0; i<1024; i++) {
		for(unsigned c=0; c<5; c++) bin2char[i][c] = 'x';
		for(unsigned c=0; c<5; c++) allchar[i][c] = 'x';
		bin2char[i][5] = 0;
		allchar[i][5] = 0;
	}

	// List all possible Ter codes
	unsigned numter = 0;
	for(unsigned t=0; t<=0x3FF; t++) {
		unsigned t0 = (t >> 0) & 0x03;
		unsigned t1 = (t >> 2) & 0x03;
		unsigned t2 = (t >> 4) & 0x03;
		unsigned t3 = (t >> 6) & 0x03;
		unsigned t4 = (t >> 8) & 0x03;
		// Skip forbidden code for ternary
		if(t0 == 2 || t1 == 2 || t2 == 2 || t3 == 2 || t4 == 2) continue;
		allchar[numter][0] = '0' + t0;
		allchar[numter][1] = '0' + t1;
		allchar[numter][2] = '0' + t2;
		allchar[numter][3] = '0' + t3;
		allchar[numter][4] = '0' + t4;
		numter++;
	}

	printf("Listed all %u ternary codes\n", numter);

	printf("Distributing the 8b binary codes directly...\n");

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	#define CHARSET(cell, c) do { \
		if(cell != 'x' && cell != c) { \
			printf("Error line %u: Data conflict in %s with data %c\n", __LINE__, #cell, c); \
			errors_nb ++; \
		} \
		else { \
			cell = c; \
		} \
	} while(0)

	for(unsigned i=0; i<256; i++) {

		unsigned bin_masked3 = (i >> 5) & 0x07;
		unsigned bin_masked5 = i & 0x1F;

		if(bin_masked5 < 27) {

			// Scan ternary codes to generate
			unsigned pos_ter = 0;
			for(unsigned t=0; t<=0x3F; t++) {
				unsigned t0 = (t >> 0) & 0x03;
				unsigned t1 = (t >> 2) & 0x03;
				unsigned t2 = (t >> 4) & 0x03;

				// Skip forbidden code for ternary
				if(t0 == 2 || t1 == 2 || t2 == 2) continue;
				if(t > 0) pos_ter++;

				if(pos_ter != bin_masked5) continue;

				// Partially set the output T
				CHARSET(bin2char[i][0], '0' + t0);
				CHARSET(bin2char[i][1], '0' + t1);
				CHARSET(bin2char[i][2], '0' + t2);

				unsigned pos_ter2 = 0;
				for(unsigned t=0; t<=0xF; t++) {
					unsigned t0 = (t >> 0) & 0x03;
					unsigned t1 = (t >> 2) & 0x03;
					// Skip forbidden code for ternary
					if(t0 == 2 || t1 == 2) continue;

					// Experiment : always skip the same code each time, but this may lead to a bad distribution
					if(t0 == 3 && t1 == 3) continue;
					if(t > 0) pos_ter2++;
					if(pos_ter2 != bin_masked3) continue;

					CHARSET(bin2char[i][3], '0' + t0);
					CHARSET(bin2char[i][4], '0' + t1);

					break;
				}

				break;
			}

		}  // 27 main codes

		// We are in the 5 extra codes, use it to generate the remaining 9th code for the 2 uppper T : 33
		else {

			// Scan ternary codes to generate
			unsigned pos_ter = 0;
			for(unsigned t=0; t<=0x3F; t++) {
				unsigned t0 = (t >> 0) & 0x03;
				unsigned t1 = (t >> 2) & 0x03;
				unsigned t2 = (t >> 4) & 0x03;

				// Skip forbidden code for ternary
				if(t0 == 2 || t1 == 2 || t2 == 2) continue;
				if(t > 0) pos_ter++;

				if(((pos_ter + 4*bin_masked3) & 0x1F) != bin_masked5) continue;

				// Set the output lower 3T
				CHARSET(bin2char[i][0], '0' + t0);
				CHARSET(bin2char[i][1], '0' + t1);
				CHARSET(bin2char[i][2], '0' + t2);

				// Set the upper 2T
				CHARSET(bin2char[i][3], '0' + 3);
				CHARSET(bin2char[i][4], '0' + 3);
				break;
			}

		}  // 5 extra codes

	}

	#undef CHARSET

	// Print unfinished vectors
	unsigned unfinished_nb = 0;
	for(unsigned i=0; i<256; i++) {
		bool unfinished = false;
		for(unsigned c=0; c<5; c++) if(bin2char[i][c] == 'x') unfinished = true;
		if(unfinished == true)  {
			printf("Unfinished entry %d -> %s\n", i, bin2char[i]);
			unfinished_nb++;
		}
	}
	if(unfinished_nb > 0) {
		printf("Unfinished entries : %u\n", unfinished_nb);
	}

	// Print unassigned Ter codes
	unsigned unassigned_nb = 0;
	for(unsigned t=0; t<numter; t++) {
		bool unassigned = true;
		for(unsigned i=0; i<256; i++) if(strcmp(bin2char[i], allchar[t]) == 0) unassigned = false;
		if(unassigned == true)  {
			printf("Unassigned entry : %s\n", allchar[t]);
			unassigned_nb++;
		}
	}
	if(unassigned_nb > 0) {
		printf("Unassigned occurrences : %u\n", unassigned_nb);
	}

	if(errors_nb > 0) {
		printf("Error occurrences : %u\n", errors_nb);
	}

	if(unassigned_nb > 0 || errors_nb > 0) {
		exit(EXIT_FAILURE);
	}

	printf("Filling LUT configs...\n");

	errors_nb = 0;
	inputs_nb = 0;
	for(unsigned i=0; i<256; i++) {

		bool unfinished = false;
		for(unsigned c=0; c<5; c++) if(bin2char[i][c] == 'x') unfinished = true;
		if(unfinished == true) continue;

		unsigned t0 = bin2char[i][0] - '0';
		unsigned t1 = bin2char[i][1] - '0';
		unsigned t2 = bin2char[i][2] - '0';
		unsigned t3 = bin2char[i][3] - '0';
		unsigned t4 = bin2char[i][4] - '0';

		unsigned bin_masked3 = (i >> 5) & 0x07;
		unsigned bin_masked5 = i & 0x1F;

		// Generate the 3b code to identify the 5 extra codes
		unsigned extra_codes_flag = 0;
		unsigned extra_codes_2b = 0;

		unsigned gen_t0 = t0;
		unsigned gen_t1 = t1;
		unsigned gen_t2 = t2;

		if(bin_masked5 >= 27) {
			extra_codes_flag = 1;

			//extra_codes_2b = (bin_masked5 - 27) & 0x3;
			extra_codes_2b = bin_masked5 & 0x3;

			// Skip the 5th extra code
			if(bin_masked5 - 27 >= 4) continue;
			// The 5 lower bits of the bin code always produce the lower 3T
			gen_t0 = extra_codes_2b;
			gen_t1 = extra_codes_2b;
			gen_t2 = extra_codes_2b;
		}

		// The 5 lower bits of the bin code always produce the lower 3T
		errors_nb += lutcfg_setbit_verbose(lut_lower_ter_A0, bin_masked5, (gen_t0 >> 0) & 0x01);
		errors_nb += lutcfg_setbit_verbose(lut_lower_ter_A1, bin_masked5, (gen_t0 >> 1) & 0x01);
		errors_nb += lutcfg_setbit_verbose(lut_lower_ter_B0, bin_masked5, (gen_t1 >> 0) & 0x01);
		errors_nb += lutcfg_setbit_verbose(lut_lower_ter_B1, bin_masked5, (gen_t1 >> 1) & 0x01);
		errors_nb += lutcfg_setbit_verbose(lut_lower_ter_C0, bin_masked5, (gen_t2 >> 0) & 0x01);
		errors_nb += lutcfg_setbit_verbose(lut_lower_ter_C1, bin_masked5, (gen_t2 >> 1) & 0x01);

		errors_nb += lutcfg_setbit_verbose(lut_extra_flag, bin_masked5, extra_codes_flag);

		unsigned addr_upper_2t = (bin_masked3 << 3) + (extra_codes_flag << 2) + (bin_masked5 & 0x3);
		errors_nb += lutcfg_setbit_verbose(lut_upper_2T_A0, addr_upper_2t, (t3 >> 0) & 0x01);
		errors_nb += lutcfg_setbit_verbose(lut_upper_2T_A1, addr_upper_2t, (t3 >> 1) & 0x01);
		errors_nb += lutcfg_setbit_verbose(lut_upper_2T_B0, addr_upper_2t, (t4 >> 0) & 0x01);
		errors_nb += lutcfg_setbit_verbose(lut_upper_2T_B1, addr_upper_2t, (t4 >> 1) & 0x01);

		// The 5 lower bits of the bin code always produce the lower 3T
		unsigned addr_recode_3t = (bin_masked3 << 3) + (extra_codes_flag << 2);
		errors_nb += lutcfg_setbit_verbose(lut_recode_ter_A0, addr_recode_3t + gen_t0, (t0 >> 0) & 0x01);
		errors_nb += lutcfg_setbit_verbose(lut_recode_ter_A1, addr_recode_3t + gen_t0, (t0 >> 1) & 0x01);
		errors_nb += lutcfg_setbit_verbose(lut_recode_ter_B0, addr_recode_3t + gen_t1, (t1 >> 0) & 0x01);
		errors_nb += lutcfg_setbit_verbose(lut_recode_ter_B1, addr_recode_3t + gen_t1, (t1 >> 1) & 0x01);
		errors_nb += lutcfg_setbit_verbose(lut_recode_ter_C0, addr_recode_3t + gen_t2, (t2 >> 0) & 0x01);
		errors_nb += lutcfg_setbit_verbose(lut_recode_ter_C1, addr_recode_3t + gen_t2, (t2 >> 1) & 0x01);

		inputs_nb++;

	}  // Scan binary inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("\n");
	printf("Table for compressor :\n");
	printf("\n");

	for(unsigned i=0; i<256; i++) {

		bool unfinished = false;
		for(unsigned c=0; c<5; c++) if(bin2char[i][c] == 'x') unfinished = true;
		if(unfinished == true) continue;

		bool already_represented = false;
		for(unsigned c=0; c<i; c++) if(strcmp(bin2char[i], bin2char[c]) == 0) already_represented = true;
		if(already_represented == true) continue;

		unsigned ter_code = 0;
		for(int c=4; c>=0; c--) ter_code = (ter_code << 2) | (unsigned char)(bin2char[i][c] - '0');

		char str_bin[32];
		char str_ter[32];
		for (unsigned p = 0 ; p < 8; p++) str_bin[p] = '0' + ((i & (1 << p)) != 0);
		for (unsigned p = 0 ; p < 10; p++) str_ter[p] = '0' + ((ter_code & (1 << p)) != 0);
		str_bin[8] = 0;
		str_ter[10] = 0;

		printf("\"%s\" when \"%s\",\n", str_bin, str_ter);
	}

	printf("\n");
	printf("Final LUT configs for decompressor :\n");
	printf("\n");

	printf("Lower 3T code T0 : "); lutcfg_printhex_lut6_2_nl(lut_lower_ter_A1, lut_lower_ter_A0);
	printf("Lower 3T code T1 : "); lutcfg_printhex_lut6_2_nl(lut_lower_ter_B1, lut_lower_ter_B0);
	printf("Lower 3T code T2 : "); lutcfg_printhex_lut6_2_nl(lut_lower_ter_C1, lut_lower_ter_C0);
	printf("\n");
	printf("Extra code flag  : "); lutcfg_printhex_lut5_nl(lut_extra_flag);
	printf("\n");
	printf("Final T0 b0      : "); lutcfg_printhex_lut6_nl(lut_recode_ter_A0);
	printf("Final T0 b1      : "); lutcfg_printhex_lut6_nl(lut_recode_ter_A1);
	printf("Final T1 b0      : "); lutcfg_printhex_lut6_nl(lut_recode_ter_B0);
	printf("Final T1 b1      : "); lutcfg_printhex_lut6_nl(lut_recode_ter_B1);
	printf("Final T2 b0      : "); lutcfg_printhex_lut6_nl(lut_recode_ter_C0);
	printf("Final T2 b1      : "); lutcfg_printhex_lut6_nl(lut_recode_ter_C1);
	printf("\n");
	printf("Final T3 b0      : "); lutcfg_printhex_lut6_nl(lut_upper_2T_A0);
	printf("Final T3 b1      : "); lutcfg_printhex_lut6_nl(lut_upper_2T_A1);
	printf("Final T4 b0      : "); lutcfg_printhex_lut6_nl(lut_upper_2T_B0);
	printf("Final T4 b1      : "); lutcfg_printhex_lut6_nl(lut_upper_2T_B1);
	printf("\n");
}

// Popcounts

static void adderlut6_popc_3_2() {
	// O6 is sum0, O5 is sum1
	uint8_t* lutA5 = lutcfg_alloc(64);
	uint8_t* lutA6 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x7; i++) {
		unsigned i0 = (i >> 0) & 0x1;
		unsigned i1 = (i >> 1) & 0x1;
		unsigned i2 = (i >> 2) & 0x1;

		unsigned sum = i0 + i1 + i2;

		errors_nb += lutcfg_setbit_verbose(lutA6, i, (sum >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutA5, i, (sum >> 1) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
}

static void adderlut6_popc_5_3() {
	// O6 is sum0, O5 is sum1
	uint8_t* lutA5 = lutcfg_alloc(64);
	uint8_t* lutA6 = lutcfg_alloc(64);

	// Output O is sum2
	uint8_t* lutB6 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x1F; i++) {
		unsigned i0 = (i >> 0) & 0x1;
		unsigned i1 = (i >> 1) & 0x1;
		unsigned i2 = (i >> 2) & 0x1;
		unsigned i3 = (i >> 3) & 0x1;
		unsigned i4 = (i >> 4) & 0x1;

		unsigned sum = i0 + i1 + i2 + i3 + i4;

		errors_nb += lutcfg_setbit_verbose(lutA6, i, (sum >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutA5, i, (sum >> 1) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutB6, i, (sum >> 2) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
	printf("LUTB ");
	lutcfg_printhex_lut5_nl(lutB6);
}

static void adderlut6_popc_6_3() {
	uint8_t* lutA = lutcfg_alloc(64);
	uint8_t* lutB = lutcfg_alloc(64);
	uint8_t* lutC = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x3F; i++) {
		unsigned i0 = (i >> 0) & 0x1;
		unsigned i1 = (i >> 1) & 0x1;
		unsigned i2 = (i >> 2) & 0x1;
		unsigned i3 = (i >> 3) & 0x1;
		unsigned i4 = (i >> 4) & 0x1;
		unsigned i5 = (i >> 5) & 0x1;

		unsigned sum = i0 + i1 + i2 + i3 + i4 + i5;

		errors_nb += lutcfg_setbit_verbose(lutA, i, (sum >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutB, i, (sum >> 1) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutC, i, (sum >> 2) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_nl(lutA);
	printf("LUTB ");
	lutcfg_printhex_lut6_nl(lutB);
	printf("LUTC ");
	lutcfg_printhex_lut6_nl(lutC);
}

static void adderlut6_popc_7_3() {

	// O6 is partSum0, O5 is partSum1
	uint8_t* lutP5 = lutcfg_alloc(64);
	uint8_t* lutP6 = lutcfg_alloc(64);

	// O6 is sum0, O5 is sum1
	uint8_t* lutA5 = lutcfg_alloc(64);
	uint8_t* lutA6 = lutcfg_alloc(64);

	// Output O is sum2
	uint8_t* lutB6 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x7F; i++) {
		unsigned i0 = (i >> 0) & 0x1;
		unsigned i1 = (i >> 1) & 0x1;
		unsigned i2 = (i >> 2) & 0x1;
		unsigned i3 = (i >> 3) & 0x1;
		unsigned i4 = (i >> 4) & 0x1;
		unsigned i5 = (i >> 5) & 0x1;
		unsigned i6 = (i >> 6) & 0x1;

		unsigned part_addr = (i0 << 0) + (i1 << 1) + (i2 << 2) + (i3 << 3);
		unsigned partSum = i0 + i1 + i2 + i3;

		unsigned sum01_addr = (partSum & 0x3) + (i4 << 2) + (i5 << 3) + (i6 << 4);
		unsigned sum2_addr  = (partSum & 0x3) + (i3 << 2) + (i4 << 3) + (i5 << 4) + (i6 << 5);

		unsigned sum = i0 + i1 + i2 + i3 + i4 + i5 + i6;

		errors_nb += lutcfg_setbit_verbose(lutP6, part_addr, (partSum >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutP5, part_addr, (partSum >> 1) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutA6, sum01_addr, (sum >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutA5, sum01_addr, (sum >> 1) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutB6, sum2_addr, (sum >> 2) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTP ");
	lutcfg_printhex_lut6_2_nl(lutP6, lutP5);
	printf("LUTA ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
	printf("LUTB ");
	lutcfg_printhex_lut6_nl(lutB6);
}

static void adderlut6_popc_7_3_carry4() {
	// Usage of the lower half of a CARRY4
	// Only 2 LUTs are used
	// Only 6 input bits are manipulated, the 7th is given directly in input CYINIT of the CARRY4

	uint8_t* lutA = lutcfg_alloc(64);

	uint8_t* lutB5 = lutcfg_alloc(64);
	uint8_t* lutB6 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x3F; i++) {
		unsigned i0 = (i >> 0) & 0x1;
		unsigned i1 = (i >> 1) & 0x1;
		unsigned i2 = (i >> 2) & 0x1;
		unsigned i3 = (i >> 3) & 0x1;
		unsigned i4 = (i >> 4) & 0x1;
		unsigned i5 = (i >> 5) & 0x1;

		unsigned addrA = i & 0x3F;
		unsigned addrB = i & 0x1F;

		unsigned part5 = i0 + i1 + i2 + i3 + i4;

		errors_nb += lutcfg_setbit_verbose(lutA, addrA,        (part5 & 0x1) ^ i5);
		errors_nb += lutcfg_setbit_verbose(lutA, addrA & 0x1F, (part5 & 0x1));

		errors_nb += lutcfg_setbit_verbose(lutB6, addrB, (part5 >> 1) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutB5, addrB, (part5 >> 2) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_nl(lutA);
	printf("LUTB ");
	lutcfg_printhex_lut6_2_nl(lutB6, lutB5);
}

static void adderlut6_popc_8_4() {
	// Architecture : First one popc 5:3, then 2 recoding LUTs that merge with the 3 extra input bits

	// One LUT6_2
	uint8_t* lutS0 = lutcfg_alloc(64);
	uint8_t* lutS1 = lutcfg_alloc(64);

	// One LUT6_2
	uint8_t* lutS2 = lutcfg_alloc(64);
	uint8_t* lutS3 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0xFF; i++) {
		unsigned i0 = (i >> 0) & 0x1;
		unsigned i1 = (i >> 1) & 0x1;
		unsigned i2 = (i >> 2) & 0x1;
		unsigned i3 = (i >> 3) & 0x1;
		unsigned i4 = (i >> 4) & 0x1;
		unsigned i5 = (i >> 5) & 0x1;
		unsigned i6 = (i >> 6) & 0x1;
		unsigned i7 = (i >> 7) & 0x1;

		unsigned sum = i0 + i1 + i2 + i3 + i4 + i5 + i6 + i7;
		unsigned sum5 = i0 + i1 + i2 + i3 + i4;

		unsigned addr01 = (sum5 & 0x3) + (i5 << 2) + (i6 << 3) + (i7 << 4);
		unsigned addr23 = (sum5 & 0x7) + ((sum & 0x3) << 3);

		errors_nb += lutcfg_setbit_verbose(lutS0, addr01, (sum >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutS1, addr01, (sum >> 1) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutS2, addr23, (sum >> 2) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutS3, addr23, (sum >> 3) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUT01 ");
	lutcfg_printhex_lut6_2_nl(lutS1, lutS0);
	printf("LUT23 ");
	lutcfg_printhex_lut6_2_nl(lutS3, lutS2);
}

static void adderlut6_popc_9_4() {
	// Architecture : First one popc 7:3, then 2 recoding LUTs that merge with the 2 extra input bits

	// One LUT6_2
	uint8_t* lutS0 = lutcfg_alloc(64);
	uint8_t* lutS1 = lutcfg_alloc(64);

	// One LUT6_2
	uint8_t* lutS2 = lutcfg_alloc(64);
	uint8_t* lutS3 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x1FF; i++) {
		unsigned i0 = (i >> 0) & 0x1;
		unsigned i1 = (i >> 1) & 0x1;
		unsigned i2 = (i >> 2) & 0x1;
		unsigned i3 = (i >> 3) & 0x1;
		unsigned i4 = (i >> 4) & 0x1;
		unsigned i5 = (i >> 5) & 0x1;
		unsigned i6 = (i >> 6) & 0x1;
		unsigned i7 = (i >> 7) & 0x1;
		unsigned i8 = (i >> 7) & 0x1;

		unsigned sum = i0 + i1 + i2 + i3 + i4 + i5 + i6 + i7 + i8;
		unsigned sum7 = i0 + i1 + i2 + i3 + i4 + i5 + i6;

		unsigned addr = (sum7 & 0x7) + (i7 << 3) + (i8 << 4);

		errors_nb += lutcfg_setbit_verbose(lutS0, addr, (sum >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutS1, addr, (sum >> 1) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutS2, addr, (sum >> 2) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutS3, addr, (sum >> 3) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUT01 ");
	lutcfg_printhex_lut6_2_nl(lutS1, lutS0);
	printf("LUT23 ");
	lutcfg_printhex_lut6_2_nl(lutS3, lutS2);
}

static void adderlut6_popc_8_4_strange() {

	// O6 is partSum0, O5 is partSum1
	uint8_t* lutA5 = lutcfg_alloc(64);
	uint8_t* lutA6 = lutcfg_alloc(64);

	// O6 is partSum1, O5 is partSum0
	uint8_t* lutB5 = lutcfg_alloc(64);
	uint8_t* lutB6 = lutcfg_alloc(64);

	// 2 LUTs for S2 and S3
	uint8_t* lutS2 = lutcfg_alloc(64);
	uint8_t* lutS3 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0xFF; i++) {
		unsigned i0 = (i >> 0) & 0x1;
		unsigned i1 = (i >> 1) & 0x1;
		unsigned i2 = (i >> 2) & 0x1;
		unsigned i3 = (i >> 3) & 0x1;
		unsigned i4 = (i >> 4) & 0x1;
		unsigned i5 = (i >> 5) & 0x1;
		unsigned i6 = (i >> 6) & 0x1;
		unsigned i7 = (i >> 7) & 0x1;

		unsigned sum = i0 + i1 + i2 + i3 + i4 + i5 + i6 + i7;

		unsigned sumA = i0 + i1 + i2 + i3;
		unsigned sumB = i4 + i5 + i6 + i7;

		unsigned A6 = sum & 0x1;
		unsigned A5 = (sumA >> 1) & 0x1;
		unsigned B6 = (sum >> 1) & 0x1;
		unsigned B5 = (sumB >> 0) & 0x1;

		unsigned addrA = ((i >> 0) & 0xF) + (((sumB >> 0) & 0x1) << 4);
		unsigned addrB = ((i >> 4) & 0xF) + (((sumA >> 1) & 0x1) << 4) + ((sum & 0x1) << 5);

		unsigned addr2 = (A5 << 0) + (A6 << 1) + (B5 << 2) + (B6 << 3) + (i0 << 4) + (i4 << 5);

		// LUT A
		// O6 is partSum0, O5 is partSum1
		errors_nb += lutcfg_setbit_verbose(lutA6, addrA, A6);
		errors_nb += lutcfg_setbit_verbose(lutA5, addrA & 0x1F, A5);

		// LUT B
		// O6 is partSum1, O5 is partSum0
		errors_nb += lutcfg_setbit_verbose(lutB6, addrB, B6);
		errors_nb += lutcfg_setbit_verbose(lutB5, addrB & 0x1F, B5);

		// Generation of S2, S3
		errors_nb += lutcfg_setbit_verbose(lutS2, addr2, (sum >> 2) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutS3, addr2, (sum >> 3) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
	printf("LUTB ");
	lutcfg_printhex_lut6_2_nl(lutB6, lutB5);
	printf("LUTS2 ");
	lutcfg_printhex_lut6_nl(lutS2);
	printf("LUTS3 ");
	lutcfg_printhex_lut6_nl(lutS3);
}

static void adderlut6_popc_12_4() {
	// This is just the recoding stage

	// O6 is sum0, O5 is sum1
	uint8_t* lutA5 = lutcfg_alloc(64);
	uint8_t* lutA6 = lutcfg_alloc(64);

	// O6 is sum2, O5 is sum3
	uint8_t* lutB5 = lutcfg_alloc(64);
	uint8_t* lutB6 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x3F; i++) {
		unsigned d0 = (i >> 0) & 0x1;
		unsigned d1 = (i >> 1) & 0x1;
		unsigned d2 = (i >> 2) & 0x1;
		unsigned d3 = (i >> 3) & 0x1;
		unsigned c1 = (i >> 4) & 0x1;
		unsigned c3 = (i >> 5) & 0x1;

		unsigned addr01 = (d0 << 0) + (d1 << 1) + (d2 << 2) + (d3 << 3) + (c1 << 4);
		unsigned sum01 = d0 + d2 + c1 + (d1 << 1) + (d3 << 1) + (c1 << 1);

		unsigned addr23 = ((sum01 >> 1)& 0x1) + (d1 << 1) + (d3 << 2) + (c1 << 3) + (c3 << 4);

		unsigned sum = d0 + d2 + c1 + (d1 << 1) + (d3 << 1) + (c1 << 1) + (c3 << 2);

		errors_nb += lutcfg_setbit_verbose(lutA6, addr01, (sum01 >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutA5, addr01, (sum01 >> 1) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutB6, addr23, (sum >> 2) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutB5, addr23, (sum >> 3) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
	printf("LUTB ");
	lutcfg_printhex_lut6_2_nl(lutB6, lutB5);
}

// General-purpose compressors

static void adderlut6_comp_4_2() {
	// See paper: Efficient High Speed Compression Trees on Xilinx FPGAs (2014)
	// Config for 1 LUT6 + one CARRY4, repeat the LUT config for the 4 LUT6 around the CARRY4
	// Note: The output O5 is not used as input DI of CARRY4, it's the second output bit

	uint8_t* lutA5 = lutcfg_alloc(64);
	uint8_t* lutA6 = lutcfg_alloc(64);

	// This is for the sign extension variant
	uint8_t* lutA5s = lutcfg_alloc(64);
	uint8_t* lutA6s = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x1F; i++) {
		unsigned a = (i >> 0) & 0x1;
		unsigned b = (i >> 1) & 0x1;
		unsigned c = (i >> 2) & 0x1;
		unsigned d = (i >> 3) & 0x1;
		unsigned sgn = (i >> 4) & 0x1;

		// Full adder, as described in the paper
		unsigned o6 = (a + b + c) ^ d;
		unsigned o5 = (a + b + c) >> 1;

		errors_nb += lutcfg_setbit_verbose(lutA6, i & 0xF, o6);
		errors_nb += lutcfg_setbit_verbose(lutA5, i & 0xF, o5);

		if(sgn == 0) {
			o6 = 0;
			o5 = 0;
		}

		errors_nb += lutcfg_setbit_verbose(lutA6s, i, o6);
		errors_nb += lutcfg_setbit_verbose(lutA5s, i, o5);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA no sign  ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
	printf("LUTA sign ext ");
	lutcfg_printhex_lut6_2_nl(lutA6s, lutA5s);
}

static void adderlut6_comp_15_3() {
	// Config for 2 LUT6, delay 2 LUTs

	// Generate S0 + partial sum index1
	uint8_t* lutA6 = lutcfg_alloc(64);
	uint8_t* lutA5 = lutcfg_alloc(64);

	// Generate S1 + S2
	uint8_t* lutB6 = lutcfg_alloc(64);
	uint8_t* lutB5 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x3F; i++) {
		unsigned a = (i >> 0) & 0x1;
		unsigned b = (i >> 1) & 0x1;
		unsigned c = (i >> 2) & 0x1;
		unsigned d = (i >> 3) & 0x1;
		unsigned e = (i >> 4) & 0x1;

		unsigned a1 = (i >> 5) & 0x1;

		unsigned sum0 = a + b + c + d + e;
		unsigned sum = a + b + c + d + e + (a1 << 1);

		unsigned addrA = i & 0x1F;
		errors_nb += lutcfg_setbit_verbose(lutA5, addrA, (sum  >> 0) & 1);
		errors_nb += lutcfg_setbit_verbose(lutA6, addrA, (sum0 >> 1) & 1);

		unsigned addrB = (a1 << 4) | (e << 3) | (d << 2) | (sum0 & 0x3);
		errors_nb += lutcfg_setbit_verbose(lutB5, addrB, (sum >> 1) & 1);
		errors_nb += lutcfg_setbit_verbose(lutB6, addrB, (sum >> 2) & 1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
	printf("LUTB ");
	lutcfg_printhex_lut6_2_nl(lutB6, lutB5);
}

static void adderlut6_comp_44_4() {
	// Config for 3 LUT6, delay 2 LUTs + carry

	uint8_t* lutA6 = lutcfg_alloc(64);
	uint8_t* lutA5 = lutcfg_alloc(64);

	uint8_t* lutB = lutcfg_alloc(64);

	uint8_t* lutC6 = lutcfg_alloc(64);
	uint8_t* lutC5 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0xFF; i++) {
		unsigned a0 = (i >> 0) & 0x1;
		unsigned b0 = (i >> 1) & 0x1;
		unsigned c0 = (i >> 2) & 0x1;
		unsigned d0 = (i >> 3) & 0x1;  // This directly enters di0

		unsigned a1 = (i >> 4) & 0x1;
		unsigned b1 = (i >> 5) & 0x1;
		unsigned c1 = (i >> 6) & 0x1;
		unsigned d1 = (i >> 7) & 0x1;

		unsigned part0 = a0 + b0 + c0;
		unsigned p1 = (part0 >> 1) & 0x1;
		unsigned part1 = a1 + b1 + c1 + d1;

		unsigned addrA = (a0 << 0) + (b0 << 1) + (c0 << 2) + (d0 << 3);
		unsigned addrB = (a1 << 0) + (b1 << 1) + (c1 << 2) + (d1 << 3) + (p1 << 4);
		unsigned addrC = (a1 << 0) + (b1 << 1) + (c1 << 2) + (d1 << 3);

		errors_nb += lutcfg_setbit_verbose(lutA5, addrA, (part0 >> 1) & 1);
		errors_nb += lutcfg_setbit_verbose(lutA6, addrA, ((part0 >> 0) & 1) ^ d0);

		errors_nb += lutcfg_setbit_verbose(lutB, addrB, (part1 & 1) ^ p1);

		errors_nb += lutcfg_setbit_verbose(lutC5, addrC, (part1 >> 2) & 1);
		errors_nb += lutcfg_setbit_verbose(lutC6, addrC, (part1 >> 1) & 1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
	printf("LUTB ");
	lutcfg_printhex_lut6_nl(lutB);
	printf("LUTC ");
	lutcfg_printhex_lut6_2_nl(lutC6, lutC5);
}

static void adderlut6_comp_67_5(bool is_signed) {
	// This is just the recoding stage after the compressor 6,0,7:5
	// Basically it is a compressor 1,1,3:4 in 2 luts

	// Recoding operation :
	//             o1  o0
	//  + co3  o3  o2
	//  +         co1
	// ==================
	// S4  S3  S2  S1  S0

	uint8_t* lutA5 = lutcfg_alloc(64);
	uint8_t* lutA6 = lutcfg_alloc(64);

	uint8_t* lutB5 = lutcfg_alloc(64);
	uint8_t* lutB6 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x1F; i++) {
		unsigned o1  = (i >> 0) & 0x1;
		unsigned o2  = (i >> 1) & 0x1;
		unsigned co1 = (i >> 2) & 0x1;
		unsigned o3  = (i >> 3) & 0x1;
		unsigned co3 = (i >> 4) & 0x1;

		unsigned sum = (o1 + o2 + co1) + 2*o3 + 4*co3;
		if(is_signed == true) {
			unsigned sum0 = o1 + 2*co1;
			unsigned sum1 = o2 + 2*o3 + 4*co3;
			if(sum1 == 0 && co1 == 1) continue;  // Unreachable when recoding the output of a 607:5
			sum1 -= co1;
			sum = sum0 - sum1;
		}

		errors_nb += lutcfg_setbit_verbose(lutA6, i, (sum >> 0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutA5, i, (sum >> 1) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutB6, i, (sum >> 2) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutB5, i, (sum >> 3) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("Config for %s variant\n", is_signed ? "signed" : "unsigned");
	printf("LUTA ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
	printf("LUTB ");
	lutcfg_printhex_lut6_2_nl(lutB6, lutB5);
}

static void adderlut6_comp_77_5() {
	// This is just the recoding stage after the compressor 6,0,7:5
	// Basically it is a compressor 1,1,4:4 in 2 luts, with the LUTs in series

	// Recoding operation :
	//             o1  o0
	//  + co3  o3  o2
	//  +         co1
	//  +          g1
	// ==================
	// S4  S3  S2  S1  S0

	// First LUT : generate S1 and S2
	uint8_t* lutA5 = lutcfg_alloc(64);
	uint8_t* lutA6 = lutcfg_alloc(64);

	// Second LUT : generate S3 and S4
	// Inputs : S1, S2, g1, o3, co3
	uint8_t* lutB5 = lutcfg_alloc(64);
	uint8_t* lutB6 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x3F; i++) {
		unsigned o1  = (i >> 0) & 0x1;
		unsigned o2  = (i >> 1) & 0x1;
		unsigned co1 = (i >> 2) & 0x1;
		unsigned g1  = (i >> 3) & 0x1;
		unsigned o3  = (i >> 4) & 0x1;
		unsigned co3 = (i >> 5) & 0x1;

		unsigned sum = (o1 + o2 + co1 + g1) + 2*o3 + 4*co3;
		unsigned s1 = (sum >> 0) & 0x1;
		unsigned s2 = (sum >> 1) & 0x1;

		unsigned addrA = (o1 << 0) + (o2 << 1) + (co1 << 2) + (g1 << 3) + (o3 << 4);
		unsigned addrB = (s1 << 0) + (s2 << 1) + (g1 << 2) + (o3 << 3) + (co3 << 4);

		errors_nb += lutcfg_setbit_verbose(lutA6, addrA, s1);
		errors_nb += lutcfg_setbit_verbose(lutA5, addrA, s2);

		errors_nb += lutcfg_setbit_verbose(lutB6, addrB, (sum >> 2) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutB5, addrB, (sum >> 3) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
	printf("LUTB ");
	lutcfg_printhex_lut6_2_nl(lutB6, lutB5);
}

static void adderlut6_comp_77_5_luts3(bool is_signed) {
	// This is just the recoding stage after the compressor 6,0,7:5
	// Note : the unsigned version can fit in 2 luts, delay 2 LUTs, but the signed version seems to need 3 luts, delay reduced to 1 LUT
	//   FIXME It is unknown why there is this difference

	// Recoding operation :
	//             o1  o0
	//  + co3  o3  o2
	//  +         co1
	//  +          g1
	// ==================
	// S4  S3  S2  S1  S0

	// First LUT : generate S1 and S2
	uint8_t* lutA5 = lutcfg_alloc(64);
	uint8_t* lutA6 = lutcfg_alloc(64);

	// Second LUT : generate S3
	uint8_t* lutB6 = lutcfg_alloc(64);

	// Second LUT : generate S4
	uint8_t* lutC6 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x3F; i++) {
		unsigned o1  = (i >> 0) & 0x1;
		unsigned o2  = (i >> 1) & 0x1;
		unsigned co1 = (i >> 2) & 0x1;
		unsigned g1  = (i >> 3) & 0x1;
		unsigned o3  = (i >> 4) & 0x1;
		unsigned co3 = (i >> 5) & 0x1;

		unsigned sum = (o1 + o2 + co1 + g1) + 2*o3 + 4*co3;
		if(is_signed == true) {
			unsigned sum0 = o1 + 2*co1;
			unsigned sum1 = o2 + 2*o3 + 4*co3;
			if(sum1 == 0 && co1 == 1) continue;  // Unreachable when recoding the output of a 607:5
			sum1 = sum1 + g1 - co1;
			sum = sum0 - sum1;
		}

		unsigned s1 = (sum >> 0) & 0x1;
		unsigned s2 = (sum >> 1) & 0x1;

		unsigned addrA  = (o1 << 0) + (o2 << 1) + (co1 << 2) + (g1 << 3) + (o3 << 4);
		unsigned addrBC = (o1 << 0) + (o2 << 1) + (co1 << 2) + (g1 << 3) + (o3 << 4) + (co3 << 5);

		errors_nb += lutcfg_setbit_verbose(lutA6, addrA, s1);
		errors_nb += lutcfg_setbit_verbose(lutA5, addrA, s2);

		errors_nb += lutcfg_setbit_verbose(lutB6, addrBC, (sum >> 2) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutC6, addrBC, (sum >> 3) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("Config for %s variant\n", is_signed ? "signed" : "unsigned");
	printf("LUTA ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
	printf("LUTB ");
	lutcfg_printhex_lut6_nl(lutB6);
	printf("LUTC ");
	lutcfg_printhex_lut6_nl(lutC6);
}

static void adderlut6_comp_4_2_signed() {
	// See paper: Efficient High Speed Compression Trees on Xilinx FPGAs (2014)
	// This is only the recoding of the 2 MSBs of result sum to perform signed conversion

	uint8_t* lutA5 = lutcfg_alloc(64);
	uint8_t* lutA6 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x3F; i++) {
		unsigned a = (i >> 0) & 0x1;
		unsigned b = (i >> 1) & 0x1;
		unsigned c = (i >> 2) & 0x1;
		unsigned d = (i >> 3) & 0x1;
		unsigned cy = (i >> 4) & 0x1;
		unsigned sgn = (i >> 5) & 0x1;

		unsigned sum_unsigned = a + b + c + d + cy;

		unsigned res_o6 = ((a + b + c) & 0x1) ^ d;
		unsigned res_o5 = (a + b + c) >> 1;
		unsigned res_s0 = res_o6 ^ cy;
		unsigned res_c0 = (res_o6 != 0) ? cy : d;
		unsigned res_s1 = (res_o6 & sgn) ^ res_c0;

		// Sign extension
		// 0 -> -1 -> 0
		// 1 -> 0 -> -1
		a = ~(a - 1);
		b = ~(b - 1);
		c = ~(c - 1);
		d = ~(d - 1);

		unsigned addr = 0;
		addr = (addr << 1) | (sgn    >> 0);
		addr = (addr << 1) | (res_s1 >> 0);
		addr = (addr << 1) | (res_o5 >> 0);
		addr = (addr << 1) | (res_s0 >> 0);

		unsigned sum_signed = a + b + c + d + cy;
		if(sgn == 0) sum_signed = sum_unsigned;
		unsigned want_res_sum = sum_signed - (res_o5 << 1);

		errors_nb += lutcfg_setbit_verbose(lutA6, addr, (want_res_sum >> 3) & 0x01);
		errors_nb += lutcfg_setbit_verbose(lutA5, addr, (want_res_sum >> 2) & 0x01);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUT A ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
}

static void adderlut6_comp_223_4() {
  // FIXME This now duplicates component adder 3to4

	// First LUT that generates S0 and S1
	uint8_t* lutA6 = lutcfg_alloc(64);
	uint8_t* lutA5 = lutcfg_alloc(64);

	// Second LUT that generates S2 and S3
	uint8_t* lutB6 = lutcfg_alloc(64);
	uint8_t* lutB5 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x7F; i++) {
		unsigned a = (i >> 0) & 0x7;
		unsigned b = (i >> 3) & 0x7;
		unsigned c = (i >> 6) & 0x1;

		unsigned addr_lut1 = ((a & 0x3) << 0) + ((b & 0x3) << 2) + (c << 4);
		unsigned res_lut1 = (a & 0x3) + (b & 0x3) + c;

		unsigned addr_lut2 = ((a & 0x6) << 0) + ((b & 0x6) << 2) + ((res_lut1 >> 1) & 0x1);
		unsigned res_lut2 = a + b + c;

		errors_nb += lutcfg_setbit_verbose(lutA6, addr_lut1, (res_lut1 >> 1) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutA5, addr_lut1, (res_lut1 >> 0) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutB6, addr_lut2, (res_lut2 >> 3) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutB5, addr_lut2, (res_lut2 >> 2) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
	printf("LUTB ");
	lutcfg_printhex_lut6_2_nl(lutB6, lutB5);
}

static void adderlut6_comp_446_5() {
	// This is a compressor 4,4,6:5
	// Note : The input f0 directly enters the input cy of the carry4

	uint8_t* lutA = lutcfg_alloc(64);

	uint8_t* lutB5 = lutcfg_alloc(64);
	uint8_t* lutB6 = lutcfg_alloc(64);

	uint8_t* lutC = lutcfg_alloc(64);

	uint8_t* lutD5 = lutcfg_alloc(64);
	uint8_t* lutD6 = lutcfg_alloc(64);

	uint8_t* lutP5 = lutcfg_alloc(64);
	uint8_t* lutP6 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x1FFF; i++) {

		unsigned a0 = (i >> 0) & 0x1;
		unsigned b0 = (i >> 1) & 0x1;
		unsigned c0 = (i >> 2) & 0x1;
		unsigned d0 = (i >> 3) & 0x1;
		unsigned e0 = (i >> 4) & 0x1;

		unsigned a1 = (i >> 5) & 0x1;
		unsigned b1 = (i >> 6) & 0x1;
		unsigned c1 = (i >> 7) & 0x1;
		unsigned d1 = (i >> 8) & 0x1;

		unsigned a2 = (i >> 9) & 0x1;
		unsigned b2 = (i >> 10) & 0x1;
		unsigned c2 = (i >> 11) & 0x1;
		unsigned d2 = (i >> 12) & 0x1;

		// Partial sums
		unsigned part0 = a0 + b0 + c0 + d0 + 2*a1;
		unsigned p0 = (part0 >> 0) & 0x1;
		unsigned p1 = (part0 >> 1) & 0x1;
		unsigned p2 = (part0 >> 2) & 0x1;
		unsigned part1 = b1 + c1 + d1;
		unsigned q1 = (part1 >> 0) & 0x1;
		unsigned q2 = (part1 >> 1) & 0x1;
		unsigned part2 = a2 + b2 + c2 + d2 + q2;

		unsigned addrA = (a0 << 0) + (b0 << 1) + (c0 << 2) + (d0 << 3) + (a1 << 4) + (e0 << 5);
		unsigned addrP = (a0 << 0) + (b0 << 1) + (c0 << 2) + (d0 << 3) + (a1 << 4);

		unsigned addrB = (b1 << 0) + (c1 << 1) + (d1 << 2) + (p1 << 3);

		unsigned addrC = (a2 << 0) + (b2 << 1) + (c2 << 2) + (d2 << 3) + (q2 << 4) + (p2 << 5);
		unsigned addrD = (a2 << 0) + (b2 << 1) + (c2 << 2) + (d2 << 3) + (q2 << 4);

		errors_nb += lutcfg_setbit_verbose(lutP5, addrP, p2);
		errors_nb += lutcfg_setbit_verbose(lutP6, addrP, p1);

		errors_nb += lutcfg_setbit_verbose(lutA, addrA & 0x1F, p0);
		errors_nb += lutcfg_setbit_verbose(lutA, addrA, p0 ^ e0);

		errors_nb += lutcfg_setbit_verbose(lutB5, addrB, q2);
		errors_nb += lutcfg_setbit_verbose(lutB6, addrB, q1 ^ p1);

		errors_nb += lutcfg_setbit_verbose(lutC, addrC, (part2 & 0x1) ^ p2);

		errors_nb += lutcfg_setbit_verbose(lutD5, addrD, (part2 >> 2) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutD6, addrD, (part2 >> 1) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTP ");
	lutcfg_printhex_lut6_2_nl(lutP6, lutP5);

	printf("LUTA ");
	lutcfg_printhex_lut6_nl(lutA);
	printf("LUTB ");
	lutcfg_printhex_lut6_2_nl(lutB6, lutB5);
	printf("LUTC ");
	lutcfg_printhex_lut6_nl(lutC);
	printf("LUTD ");
	lutcfg_printhex_lut6_2_nl(lutD6, lutD5);
}

static void adderlut6_comp_447_5() {
	// This is a compressor 4,4,7:5
	// Note : The input g0 directly enters the input cy of the carry4

	uint8_t* lutA = lutcfg_alloc(64);

	uint8_t* lutB5 = lutcfg_alloc(64);
	uint8_t* lutB6 = lutcfg_alloc(64);

	uint8_t* lutC = lutcfg_alloc(64);

	uint8_t* lutD5 = lutcfg_alloc(64);
	uint8_t* lutD6 = lutcfg_alloc(64);

	uint8_t* lutP1 = lutcfg_alloc(64);
	uint8_t* lutP2 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x3FFF; i++) {

		unsigned a0 = (i >> 0) & 0x1;
		unsigned b0 = (i >> 1) & 0x1;
		unsigned c0 = (i >> 2) & 0x1;
		unsigned d0 = (i >> 3) & 0x1;
		unsigned e0 = (i >> 4) & 0x1;
		unsigned f0 = (i >> 5) & 0x1;

		unsigned a1 = (i >> 6) & 0x1;
		unsigned b1 = (i >> 7) & 0x1;
		unsigned c1 = (i >> 8) & 0x1;
		unsigned d1 = (i >> 6) & 0x1;

		unsigned a2 = (i >> 10) & 0x1;
		unsigned b2 = (i >> 11) & 0x1;
		unsigned c2 = (i >> 12) & 0x1;
		unsigned d2 = (i >> 13) & 0x1;

		// Partial sums
		unsigned part0 = a0 + b0 + c0 + d0 + e0 + 2*a1;
		unsigned p0 = (part0 >> 0) & 0x1;
		unsigned p1 = (part0 >> 1) & 0x1;
		unsigned p2 = (part0 >> 2) & 0x1;
		unsigned part1 = b1 + c1 + d1;
		unsigned q1 = (part1 >> 0) & 0x1;
		unsigned q2 = (part1 >> 1) & 0x1;
		unsigned part2 = a2 + b2 + c2 + d2 + q2;

		unsigned addrP = (a0 << 0) + (b0 << 1) + (c0 << 2) + (d0 << 3) + (e0 << 4) + (a1 << 5);

		unsigned addrA = (a0 << 0) + (b0 << 1) + (c0 << 2) + (d0 << 3) + (e0 << 4) + (f0 << 5);

		unsigned addrB = (b1 << 0) + (c1 << 1) + (d1 << 2) + (p1 << 3);

		unsigned addrC = (a2 << 0) + (b2 << 1) + (c2 << 2) + (d2 << 3) + (q2 << 4) + (p2 << 5);
		unsigned addrD = (a2 << 0) + (b2 << 1) + (c2 << 2) + (d2 << 3) + (q2 << 4);

		errors_nb += lutcfg_setbit_verbose(lutP1, addrP, p1);
		errors_nb += lutcfg_setbit_verbose(lutP2, addrP, p2);

		errors_nb += lutcfg_setbit_verbose(lutA, addrA & 0x1F, p0);
		errors_nb += lutcfg_setbit_verbose(lutA, addrA, p0 ^ f0);

		errors_nb += lutcfg_setbit_verbose(lutB5, addrB, q2);
		errors_nb += lutcfg_setbit_verbose(lutB6, addrB, q1 ^ p1);

		errors_nb += lutcfg_setbit_verbose(lutC, addrC, (part2 & 0x1) ^ p2);

		errors_nb += lutcfg_setbit_verbose(lutD5, addrD, (part2 >> 2) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutD6, addrD, (part2 >> 1) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTP1 ");
	lutcfg_printhex_lut6_nl(lutP1);
	printf("LUTP2 ");
	lutcfg_printhex_lut6_nl(lutP2);

	printf("LUTA ");
	lutcfg_printhex_lut6_nl(lutA);
	printf("LUTB ");
	lutcfg_printhex_lut6_2_nl(lutB6, lutB5);
	printf("LUTC ");
	lutcfg_printhex_lut6_nl(lutC);
	printf("LUTD ");
	lutcfg_printhex_lut6_2_nl(lutD6, lutD5);
}

static void adderlut6_comp_506_5() {
	// See paper: Efficient High Speed Compression Trees on Xilinx FPGAs (MBMV2014)
	// Config for 2 LUT6 + one CARRY4, repeat the LUT config for the 4 LUT6 around the CARRY4

	uint8_t* lutA6 = lutcfg_alloc(64);
	uint8_t* lutA5 = lutcfg_alloc(64);

	uint8_t* lutB6 = lutcfg_alloc(64);
	uint8_t* lutB5 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x1F; i++) {
		unsigned a = (i >> 0) & 0x1;
		unsigned b = (i >> 1) & 0x1;
		unsigned c = (i >> 2) & 0x1;
		unsigned d = (i >> 3) & 0x1;
		unsigned e = (i >> 4) & 0x1;

		// First : Full adder
		unsigned fa1s = (a + b + c) & 0x1;
		unsigned fa1c = (a + b + c) >> 1;
		// Second : Half adder
		unsigned ha2s = (fa1s + d) & 0x1;
		unsigned ha2c = (fa1s + d) >> 1;

		errors_nb += lutcfg_setbit_verbose(lutA6, i, ha2s ^ e);
		errors_nb += lutcfg_setbit_verbose(lutA5, i, e);

		errors_nb += lutcfg_setbit_verbose(lutB6, i & 0x1F, fa1c ^ ha2c);
		errors_nb += lutcfg_setbit_verbose(lutB5, i & 0x1F, fa1c);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
	printf("LUTB ");
	lutcfg_printhex_lut6_2_nl(lutB6, lutB5);
}

static void adderlut6_comp_606_5() {
	// See paper: Pipelined Compressor Tree Optimization using Integer Linear Programming (FPL2014)
	// Config for 2 LUT6 + one CARRY4, repeat the LUT config for the 4 LUT6 around the CARRY4
	// The output O5 is not used in 1st LUT => feed input DI of CARRY4 with bit 5
	uint8_t* lutA6 = lutcfg_alloc(64);
	uint8_t* lutB5 = lutcfg_alloc(64);
	uint8_t* lutB6 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x3F; i++) {
		unsigned a = (i >> 0) & 0x1;
		unsigned b = (i >> 1) & 0x1;
		unsigned c = (i >> 2) & 0x1;
		unsigned d = (i >> 3) & 0x1;
		unsigned e = (i >> 4) & 0x1;
		unsigned f = (i >> 5) & 0x1;

		// Full adders, as described in FPL paper
		unsigned fa1s = (a + b + c) & 0x1;
		unsigned fa1c = (a + b + c) >> 1;
		// Second full adder
		unsigned fa2s = (fa1s + d + e) & 0x1;
		unsigned fa2c = (fa1s + d + e) >> 1;

		errors_nb += lutcfg_setbit_verbose(lutA6, i, fa2s ^ f);

		errors_nb += lutcfg_setbit_verbose(lutB6, i & 0x1F, fa1c ^ fa2c);
		errors_nb += lutcfg_setbit_verbose(lutB5, i & 0x1F, fa1c);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_nl(lutA6);

	printf("LUTB ");
	lutcfg_printhex_lut6_2_nl(lutB6, lutB5);
}

static void adderlut6_comp_606_5_signed() {
	// See paper: Pipelined Compressor Tree Optimization using Integer Linear Programming (FPL2014)
	// This is to convert the 6,0,6:5 adder into one that would handle input bits of position 2 as sign bits
	// This is 2 LUTs to fix the 3 MSB to convert the result into a signed result
	uint8_t* lutA5 = lutcfg_alloc(64);
	uint8_t* lutA6 = lutcfg_alloc(64);

	uint8_t* lutB5 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0xFF; i++) {
		unsigned a = (i >> 0) & 0x1;
		unsigned b = (i >> 1) & 0x1;
		unsigned c = (i >> 2) & 0x1;
		unsigned d = (i >> 3) & 0x1;
		unsigned e = (i >> 4) & 0x1;
		unsigned f = (i >> 5) & 0x1;

		unsigned cy = (i >> 6) & 0x1;
		unsigned sgn = (i >> 7) & 0x1;

		unsigned sum_u0 = a + b + c + d + e + f;

		// Sign extension
		// 0 -> -1 -> 0
		// 1 -> 0 -> -1
		a = ~(a - 1);
		b = ~(b - 1);
		c = ~(c - 1);
		d = ~(d - 1);
		e = ~(e - 1);
		f = ~(f - 1);

		unsigned sum_s0 = a + b + c + d + e + f;

		unsigned sum_unsigned = sum_u0 + cy;
		unsigned sum_signed = sum_s0 + cy;

		unsigned addr = (sum_unsigned << 1) + cy;
		addr += (sgn << 4);

		if(sgn == 0) sum_signed = sum_unsigned;

		errors_nb += lutcfg_setbit_verbose(lutA6, addr, (sum_signed >> 2) & 0x01);
		errors_nb += lutcfg_setbit_verbose(lutA5, addr, (sum_signed >> 1) & 0x01);

		errors_nb += lutcfg_setbit_verbose(lutB5, addr, (sum_signed >> 3) & 0x01);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUT A ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
	printf("LUT B ");
	lutcfg_printhex_lut5_nl(lutB5);
}

static void adderlut6_comp_1066_5() {
	// This is a compressor 1,6,6:5
	// Note : The input f0 directly enters the input cy of the carry4

	uint8_t* lutP5 = lutcfg_alloc(64);
	uint8_t* lutP6 = lutcfg_alloc(64);

	uint8_t* lutA = lutcfg_alloc(64);
	uint8_t* lutB = lutcfg_alloc(64);
	uint8_t* lutC = lutcfg_alloc(64);
	uint8_t* lutD = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0xFFF; i++) {

		unsigned a0 = (i >> 0) & 0x1;
		unsigned b0 = (i >> 1) & 0x1;
		unsigned c0 = (i >> 2) & 0x1;
		unsigned d0 = (i >> 3) & 0x1;
		unsigned e0 = (i >> 4) & 0x1;

		unsigned a1 = (i >> 5) & 0x1;
		unsigned b1 = (i >> 6) & 0x1;
		unsigned c1 = (i >> 7) & 0x1;
		unsigned d1 = (i >> 8) & 0x1;
		unsigned e1 = (i >> 9) & 0x1;
		unsigned f1 = (i >> 10) & 0x1;

		unsigned a3 = (i >> 11) & 0x1;

		// Partial sums
		unsigned part0 = a0 + b0 + c0 + d0 + 2*a1;
		unsigned p0 = (part0 >> 0) & 0x1;
		unsigned p1 = (part0 >> 1) & 0x1;
		unsigned p2 = (part0 >> 2) & 0x1;
		unsigned part1 = b1 + c1 + d1 + e1 + f1;
		unsigned q1 = (part1 >> 0) & 0x1;
		unsigned q2 = (part1 >> 1) & 0x1;
		unsigned q3 = (part1 >> 2) & 0x1;

		unsigned addrP = (a0 << 0) + (b0 << 1) + (c0 << 2) + (d0 << 3) + (a1 << 4);
		unsigned addrA = (a0 << 0) + (b0 << 1) + (c0 << 2) + (d0 << 3) + (a1 << 4) + (e0 << 5);
		unsigned addrB = (b1 << 0) + (c1 << 1) + (d1 << 2) + (e1 << 3) + (f1 << 4) + (p1 << 5);
		unsigned addrC = (b1 << 0) + (c1 << 1) + (d1 << 2) + (e1 << 3) + (f1 << 4) + (p2 << 5);
		unsigned addrD = (b1 << 0) + (c1 << 1) + (d1 << 2) + (e1 << 3) + (f1 << 4) + (a3 << 5);

		errors_nb += lutcfg_setbit_verbose(lutP5, addrP, p2);
		errors_nb += lutcfg_setbit_verbose(lutP6, addrP, p1);

		errors_nb += lutcfg_setbit_verbose(lutA, addrA & 0x1F, p0);
		errors_nb += lutcfg_setbit_verbose(lutA, addrA,        p0 ^ e0);

		errors_nb += lutcfg_setbit_verbose(lutB, addrB & 0x1F, q1);
		errors_nb += lutcfg_setbit_verbose(lutB, addrB,        q1 ^ p1);

		errors_nb += lutcfg_setbit_verbose(lutC, addrC & 0x1F, q2);
		errors_nb += lutcfg_setbit_verbose(lutC, addrC,        q2 ^ p2);

		errors_nb += lutcfg_setbit_verbose(lutD, addrD & 0x1F, q3);
		errors_nb += lutcfg_setbit_verbose(lutD, addrD,        q3 ^ a3);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTP ");
	lutcfg_printhex_lut6_2_nl(lutP6, lutP5);

	printf("LUTA ");
	lutcfg_printhex_lut6_nl(lutA);
	printf("LUTB ");
	lutcfg_printhex_lut6_nl(lutB);
	printf("LUTC ");
	lutcfg_printhex_lut6_nl(lutC);
	printf("LUTD ");
	lutcfg_printhex_lut6_nl(lutD);
}

static void adderlut6_comp_1325_5() {
	// See paper: Pipelined Compressor Tree Optimization using Integer Linear Programming (FPL2014)
	// Config for 4 LUT6 + one CARRY4

	uint8_t* lutA6 = lutcfg_alloc(64);
	uint8_t* lutA5 = lutcfg_alloc(64);

	uint8_t* lutB6 = lutcfg_alloc(64);
	uint8_t* lutB5 = lutcfg_alloc(64);

	uint8_t* lutC6 = lutcfg_alloc(64);
	uint8_t* lutC5 = lutcfg_alloc(64);

	uint8_t* lutD6 = lutcfg_alloc(64);
	uint8_t* lutD5 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x3FF; i++) {
		unsigned a0 = (i >> 0) & 0x1;
		unsigned b0 = (i >> 1) & 0x1;
		unsigned c0 = (i >> 2) & 0x1;
		unsigned d0 = (i >> 3) & 0x1;

		unsigned a1 = (i >> 4) & 0x1;
		unsigned b1 = (i >> 5) & 0x1;

		unsigned a2 = (i >> 6) & 0x1;
		unsigned b2 = (i >> 7) & 0x1;
		unsigned c2 = (i >> 8) & 0x1;

		unsigned a3 = (i >> 9) & 0x1;

		unsigned sum0 = a0 + b0 + c0;
		unsigned sum1 = a1 + b1;
		unsigned sum2 = a2 + b2 + c2;
		unsigned sum3 = a3;

		unsigned addr0 = (a0 << 0) + (b0 << 1) + (c0 << 2) + (d0 << 3);
		unsigned addr1 = (a0 << 0) + (b0 << 1) + (c0 << 2) + (a1 << 3) + (b1 << 4);
		unsigned addr2 = (a1 << 0) + (b1 << 1) + (a2 << 2) + (b2 << 3) + (c2 << 4);
		unsigned addr3 = (a2 << 0) + (b2 << 1) + (c2 << 2) + (a3 << 3);

		errors_nb += lutcfg_setbit_verbose(lutA6, addr0, (sum0 ^ d0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutA5, addr0, d0);

		errors_nb += lutcfg_setbit_verbose(lutB6, addr1, ((sum0 >> 1) ^ (sum1)) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutB5, addr1, (sum0 >> 1) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutC6, addr2, ((sum1 >> 1) ^ (sum2)) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutC5, addr2, (sum2 >> 0) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutD6, addr3, ((sum2 >> 1) ^ (sum3)) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutD5, addr3, (sum3 >> 0) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
	printf("LUTB ");
	lutcfg_printhex_lut6_2_nl(lutB6, lutB5);
	printf("LUTC ");
	lutcfg_printhex_lut6_2_nl(lutC6, lutC5);
	printf("LUTD ");
	lutcfg_printhex_lut6_2_nl(lutD6, lutD5);
}

static void adderlut6_comp_2117_5() {
	// See paper : Area Optimized Synthesis of Compressor Trees on Xilinx FPGAs Using Generalized Parallel Counters (2019)
	// Note : The input g0 directly enters the input cy of the carry4

	uint8_t* lutA = lutcfg_alloc(64);
	uint8_t* lutB = lutcfg_alloc(64);
	uint8_t* lutC = lutcfg_alloc(64);

	uint8_t* lutD5 = lutcfg_alloc(64);
	uint8_t* lutD6 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x3FF; i++) {

		unsigned a0 = (i >> 0) & 0x1;
		unsigned b0 = (i >> 1) & 0x1;
		unsigned c0 = (i >> 2) & 0x1;
		unsigned d0 = (i >> 3) & 0x1;
		unsigned e0 = (i >> 4) & 0x1;
		unsigned f0 = (i >> 5) & 0x1;

		unsigned a1 = (i >> 6) & 0x1;  // This also enters c4_di(1)

		unsigned a2 = (i >> 7) & 0x1;  // This also enters c4_di(2)

		unsigned a3 = (i >> 8) & 0x1;
		unsigned b3 = (i >> 9) & 0x1;

		// Partial sums
		unsigned part0 = a0 + b0 + c0;
		unsigned part1 = d0 + e0 + (part0 & 0x1);
		unsigned part2 = ((part0 >> 1) & 0x1) + ((part1 >> 1) & 0x1);
		unsigned part3 = a3 + b3;

		unsigned addrA = (a0 << 0) + (b0 << 1) + (c0 << 2) + (d0 << 3) + (e0 << 4) + (f0 << 5);
		unsigned addrB = (a0 << 0) + (b0 << 1) + (c0 << 2) + (d0 << 3) + (e0 << 4) + (a1 << 5);
		unsigned addrC = (a0 << 0) + (b0 << 1) + (c0 << 2) + (d0 << 3) + (e0 << 4) + (a2 << 5);
		unsigned addrD = (a3 << 0) + (b3 << 1);

		errors_nb += lutcfg_setbit_verbose(lutA, addrA,        (part1 & 0x1) ^ f0);
		errors_nb += lutcfg_setbit_verbose(lutA, addrA & 0x1F, (part1 & 0x1));

		errors_nb += lutcfg_setbit_verbose(lutB, addrB, ((part2 >> 0) & 0x1) ^ a1);
		errors_nb += lutcfg_setbit_verbose(lutC, addrC, ((part2 >> 1) & 0x1) ^ a2);

		errors_nb += lutcfg_setbit_verbose(lutD5, addrD, (part3 >> 1) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutD6, addrD, (part3 >> 0) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_nl(lutA);
	printf("LUTB ");
	lutcfg_printhex_lut6_nl(lutB);
	printf("LUTC ");
	lutcfg_printhex_lut6_nl(lutC);
	printf("LUTD ");
	lutcfg_printhex_lut6_2_nl(lutD6, lutD5);
}

static void adderlut6_comp_2135_5() {
	// This is an extention of the previously-published compressor 1,3,5:4
	// See paper : Area Optimized Synthesis of Compressor Trees on Xilinx FPGAs Using Generalized Parallel Counters (2019)
	// Note : The input e0 directly enters the input cy of the carry4

	uint8_t* lutA5 = lutcfg_alloc(64);
	uint8_t* lutA6 = lutcfg_alloc(64);

	uint8_t* lutB = lutcfg_alloc(64);
	uint8_t* lutC = lutcfg_alloc(64);

	uint8_t* lutD5 = lutcfg_alloc(64);
	uint8_t* lutD6 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x3FF; i++) {

		unsigned a0 = (i >> 0) & 0x1;
		unsigned b0 = (i >> 1) & 0x1;
		unsigned c0 = (i >> 2) & 0x1;
		unsigned d0 = (i >> 3) & 0x1;

		unsigned a1 = (i >> 4) & 0x1;
		unsigned b1 = (i >> 5) & 0x1;
		unsigned c1 = (i >> 6) & 0x1;  // This also enters c4_di(1)

		unsigned a2 = (i >> 7) & 0x1;  // This also enters c4_di(2)

		unsigned a3 = (i >> 8) & 0x1;
		unsigned b3 = (i >> 9) & 0x1;

		// Partial sums
		unsigned part0 = a0 + b0 + c0;
		unsigned part1 = a1 + b1 + (part0 >> 1);
		unsigned part3 = a3 + b3;

		unsigned addrA = (a0 << 0) + (b0 << 1) + (c0 << 2) + (d0 << 3);
		unsigned addrB = (a0 << 0) + (b0 << 1) + (c0 << 2) + (a1 << 3) + (b1 << 4) + (c1 << 5);
		unsigned addrC = (a0 << 0) + (b0 << 1) + (c0 << 2) + (a1 << 3) + (b1 << 4) + (a2 << 5);
		unsigned addrD = (a3 << 0) + (b3 << 1);

		errors_nb += lutcfg_setbit_verbose(lutA5, addrA, d0);
		errors_nb += lutcfg_setbit_verbose(lutA6, addrA, (part0 & 0x1) ^ d0);

		errors_nb += lutcfg_setbit_verbose(lutB, addrB, ((part1 >> 0) & 0x1) ^ c1);
		errors_nb += lutcfg_setbit_verbose(lutC, addrC, ((part1 >> 1) & 0x1) ^ a2);

		errors_nb += lutcfg_setbit_verbose(lutD5, addrD, (part3 >> 1) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutD6, addrD, (part3 >> 0) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
	printf("LUTB ");
	lutcfg_printhex_lut6_nl(lutB);
	printf("LUTC ");
	lutcfg_printhex_lut6_nl(lutC);
	printf("LUTD ");
	lutcfg_printhex_lut6_2_nl(lutD6, lutD5);
}

static void adderlut6_comp_2223_5() {
	// This is a compressor 2,2,2,3:5
	// Config for 3 LUT6 + one CARRY4, the carry4 is only half used
	// One LUT generates S0 and S1, the carry chain is only used from S2

	// Fist stage, 1 LUT, it generates S0 ad S1
	uint8_t* lutF6 = lutcfg_alloc(64);
	uint8_t* lutF5 = lutcfg_alloc(64);

	// LUT that processes bits index 2
	uint8_t* lutA = lutcfg_alloc(64);

	// LUT that processes bits index 3
	uint8_t* lutB6 = lutcfg_alloc(64);
	uint8_t* lutB5 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x1FF; i++) {
		unsigned a0 = (i >> 0) & 0x1;
		unsigned b0 = (i >> 1) & 0x1;
		unsigned c0 = (i >> 2) & 0x1;

		unsigned a1 = (i >> 3) & 0x1;
		unsigned b1 = (i >> 4) & 0x1;

		unsigned a2 = (i >> 5) & 0x1;
		//unsigned b2 = (i >> 6) & 0x1;  // This one is actually sent as carry input in VHDL implem only

		unsigned a3 = (i >> 7) & 0x1;
		unsigned b3 = (i >> 8) & 0x1;

		unsigned sum01 = (a0 + b0 + c0) + 2 * (a1 + b1);

		unsigned addrF = (a0 << 0) + (b0 << 1) + (c0 << 2) + (a1 << 3) + (b1 << 4);

		errors_nb += lutcfg_setbit_verbose(lutF6, addrF, (sum01 >> 1) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutF5, addrF, (sum01 >> 0) & 0x1);

		unsigned addrA = (a0 << 0) + (b0 << 1) + (c0 << 2) + (a1 << 3) + (b1 << 4) + (a2 << 5);

		errors_nb += lutcfg_setbit_verbose(lutA, addrA,        (sum01 >> 2) ^ a2);
		errors_nb += lutcfg_setbit_verbose(lutA, addrA & 0x1F, (sum01 >> 2));

		unsigned addrB = (a3 << 0) + (b3 << 1);

		errors_nb += lutcfg_setbit_verbose(lutB6, addrB, a3 ^ b3);
		errors_nb += lutcfg_setbit_verbose(lutB5, addrB, a3 & b3);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTF ");
	lutcfg_printhex_lut6_2_nl(lutF6, lutF5);
	printf("LUTA ");
	lutcfg_printhex_lut6_nl(lutA);
	printf("LUTB ");
	lutcfg_printhex_lut6_2_nl(lutB6, lutB5);
}

// Special-purpose compressors

static void adderlut6_comp_16_3_special() {
	// This is a special variant of compressor 5,0,6:5 : the input bit at position 1 is mutually exlusive with one input bit at position 0
	// Config for 2 LUT6 + one CARRY4, repeat the LUT config for the 4 LUT6 around the CARRY4

	uint8_t* lutA6 = lutcfg_alloc(64);
	uint8_t* lutA5 = lutcfg_alloc(64);

	uint8_t* lutB6 = lutcfg_alloc(64);
	uint8_t* lutB5 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x3F; i++) {
		unsigned a = (i >> 0) & 0x1;
		unsigned b = (i >> 1) & 0x1;
		unsigned c = (i >> 2) & 0x1;
		unsigned d = (i >> 3) & 0x1;  // This is mutually exclusive with a1
		unsigned e = (i >> 4) & 0x1;

		unsigned a1 = (i >> 5) & 0x1;  // This is mutually exclusive with d

		// First : Full adder
		unsigned fa1s = (a + b + c) & 0x1;
		unsigned fa1c = (a + b + c) >> 1;
		// Second : Half adder
		unsigned ha2s = (fa1s + d) & 0x1;
		unsigned ha2c = (fa1s + d) >> 1;
		// Add support for the input bit position 1
		ha2c |= a1;

		unsigned addrA = i & 0x1F;
		errors_nb += lutcfg_setbit_verbose(lutA6, addrA, ha2s ^ e);
		errors_nb += lutcfg_setbit_verbose(lutA5, addrA, e);

		unsigned addrB = (i & 0xF) | (a1 << 4);
		errors_nb += lutcfg_setbit_verbose(lutB6, addrB, fa1c ^ ha2c);
		errors_nb += lutcfg_setbit_verbose(lutB5, addrB, fa1c);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
	printf("LUTB ");
	lutcfg_printhex_lut6_2_nl(lutB6, lutB5);
}

static void adderlut6_comp_136_4_special() {
	// The purpose is to recode the output of a 6,0,7:5 for popcount application

	uint8_t* lutA = lutcfg_alloc(64);
	uint8_t* lutB = lutcfg_alloc(64);
	uint8_t* lutC = lutcfg_alloc(64);

	//          o1  o0
	//   + co3  o3  o2
	//   +     co1 co1
	//   +          i0
	//   +          i1
	//   +          i2    <= This directly enters the input cy of the carry4
	//   =============
	//             sum

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0xFF; i++) {
		unsigned o0  = (i >> 0) & 0x1;
		unsigned o1  = (i >> 1) & 0x1;
		unsigned o2  = (i >> 2) & 0x1;
		unsigned o3  = (i >> 3) & 0x1;  // This directly enters di1
		unsigned co1 = (i >> 4) & 0x1;
		unsigned co3 = (i >> 5) & 0x1;  // This directly enters di2
		unsigned i0  = (i >> 6) & 0x1;
		unsigned i1  = (i >> 7) & 0x1;  // This directly enters di0

		// Partial sum that is saturated at 7 for closer approximation
		unsigned part = o0 + o2 + 2*o1 + 3*co1 + i0;
		if(part > 7) part = 7;

		unsigned addrA = (o0 << 0) + (o2 << 1) + (o1 << 2) + (co1 << 3) + (i0 << 4) + (i1 << 5);
		unsigned addrB = (o0 << 0) + (o2 << 1) + (o1 << 2) + (co1 << 3) + (i0 << 4) + (o3  << 5);
		unsigned addrC = (o0 << 0) + (o2 << 1) + (o1 << 2) + (co1 << 3) + (i0 << 4) + (co3 << 5);

		errors_nb += lutcfg_setbit_verbose(lutA, addrA & 0x1F, ((part >> 0) & 0x1));
		errors_nb += lutcfg_setbit_verbose(lutA, addrA,        ((part >> 0) & 0x1) ^ i1);

		errors_nb += lutcfg_setbit_verbose(lutB, addrB & 0x1F, ((part >> 1) & 0x1));
		errors_nb += lutcfg_setbit_verbose(lutB, addrB,        ((part >> 1) & 0x1) ^ o3);

		errors_nb += lutcfg_setbit_verbose(lutC, addrC & 0x1F, ((part >> 2) & 0x1));
		errors_nb += lutcfg_setbit_verbose(lutC, addrC,        ((part >> 2) & 0x1) ^ co3);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_nl(lutA);
	printf("LUTB ");
	lutcfg_printhex_lut6_nl(lutB);
	printf("LUTC ");
	lutcfg_printhex_lut6_nl(lutC);
}

static void adderlut6_comp_1136_5_special() {
	// Special : One input is shared between positions 0 and 1
	// Note : The 5th input pos 0 directly enters the carry4

	uint8_t* lutA = lutcfg_alloc(64);
	uint8_t* lutB = lutcfg_alloc(64);
	uint8_t* lutC = lutcfg_alloc(64);
	uint8_t* lutD = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x1FF; i++) {
		unsigned a0  = (i >> 0) & 0x1;
		unsigned b0  = (i >> 1) & 0x1;
		unsigned c0  = (i >> 2) & 0x1;
		unsigned d0  = (i >> 3) & 0x1;
		unsigned a01 = (i >> 4) & 0x1;
		unsigned a1  = (i >> 5) & 0x1;
		unsigned b1  = (i >> 6) & 0x1;
		unsigned a2  = (i >> 7) & 0x1;
		unsigned a3  = (i >> 8) & 0x1;

		// Partial sum
		unsigned part = a0 + b0 + c0 + 3*a01 + 2*a1;

		unsigned addrA = (a0 << 0) + (b0 << 1) + (c0 << 2) + (a01 << 3) + (a1 << 4) + (d0 << 5);
		unsigned addrB = (a0 << 0) + (b0 << 1) + (c0 << 2) + (a01 << 3) + (a1 << 4) + (b1 << 5);
		unsigned addrC = (a0 << 0) + (b0 << 1) + (c0 << 2) + (a01 << 3) + (a1 << 4) + (a2 << 5);
		unsigned addrD = (a0 << 0) + (b0 << 1) + (c0 << 2) + (a01 << 3) + (a1 << 4) + (a3 << 5);

		errors_nb += lutcfg_setbit_verbose(lutA, addrA & 0x1F, ((part >> 0) & 0x1));
		errors_nb += lutcfg_setbit_verbose(lutA, addrA,        ((part >> 0) & 0x1) ^ d0);

		errors_nb += lutcfg_setbit_verbose(lutB, addrB & 0x1F, ((part >> 1) & 0x1));
		errors_nb += lutcfg_setbit_verbose(lutB, addrB,        ((part >> 1) & 0x1) ^ b1);

		errors_nb += lutcfg_setbit_verbose(lutC, addrC & 0x1F, ((part >> 2) & 0x1));
		errors_nb += lutcfg_setbit_verbose(lutC, addrC,        ((part >> 2) & 0x1) ^ a2);

		errors_nb += lutcfg_setbit_verbose(lutD, addrD & 0x1F, ((part >> 3) & 0x1));
		errors_nb += lutcfg_setbit_verbose(lutD, addrD,        ((part >> 3) & 0x1) ^ a3);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_nl(lutA);
	printf("LUTB ");
	lutcfg_printhex_lut6_nl(lutB);
	printf("LUTC ");
	lutcfg_printhex_lut6_nl(lutC);
	printf("LUTD ");
	lutcfg_printhex_lut6_nl(lutD);
}

static void adderlut6_comp_225_4_special() {
	// This is a special compressor 2,2,5:5
	// It is special because its output is correct only if the theoretical result fits on 4 bits
	// Note : Input e0 directly enters input cy of the carry4
	// Config for 3 LUT6 + one CARRY4, delay 1 LUT + carry

	uint8_t* lutA = lutcfg_alloc(64);
	uint8_t* lutB = lutcfg_alloc(64);
	uint8_t* lutC = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0xFF; i++) {
		unsigned a0 = (i >> 0) & 0x1;
		unsigned b0 = (i >> 1) & 0x1;
		unsigned c0 = (i >> 2) & 0x1;
		unsigned d0 = (i >> 3) & 0x1;

		unsigned a1 = (i >> 4) & 0x1;
		unsigned b1 = (i >> 5) & 0x1;

		unsigned a2 = (i >> 6) & 0x1;
		unsigned b2 = (i >> 7) & 0x1;

		unsigned part = (a0 + b0 + c0) + 2*a1 + 4*a2;
		if(part > 7) part = 7;  // This is the approximate part
		unsigned p0 = (part >> 0) & 0x1;
		unsigned p1 = (part >> 1) & 0x1;
		unsigned p2 = (part >> 2) & 0x1;

		unsigned addrA = (a0 << 0) + (b0 << 1) + (c0 << 2) + (a1 << 3) + (a2 << 4) + (d0 << 5);
		unsigned addrB = (a0 << 0) + (b0 << 1) + (c0 << 2) + (a1 << 3) + (a2 << 4) + (b1 << 5);
		unsigned addrC = (a0 << 0) + (b0 << 1) + (c0 << 2) + (a1 << 3) + (a2 << 4) + (b2 << 5);

		errors_nb += lutcfg_setbit_verbose(lutA, addrA & 0x1F, p0);
		errors_nb += lutcfg_setbit_verbose(lutA, addrA       , p0 ^ d0);

		errors_nb += lutcfg_setbit_verbose(lutB, addrB & 0x1F, p1);
		errors_nb += lutcfg_setbit_verbose(lutB, addrB       , p1 ^ b1);

		errors_nb += lutcfg_setbit_verbose(lutC, addrC & 0x1F, p2);
		errors_nb += lutcfg_setbit_verbose(lutC, addrC       , p2 ^ b2);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_nl(lutA);
	printf("LUTB ");
	lutcfg_printhex_lut6_nl(lutB);
	printf("LUTC ");
	lutcfg_printhex_lut6_nl(lutC);
}

static void adderlut6_comp_225_4_special_alt() {
	// This is a special compressor 2,2,5:5
	// It is special because its output is correct only if the theoretical result fits on 4 bits
	// Note : Input e0 directly enters input cy of the carry4
	// Config for 3 LUT6 + one CARRY4, delay 2 LUTs + carry

	uint8_t* lutA6 = lutcfg_alloc(64);
	uint8_t* lutA5 = lutcfg_alloc(64);

	uint8_t* lutB6 = lutcfg_alloc(64);
	uint8_t* lutB5 = lutcfg_alloc(64);

	uint8_t* lutC6 = lutcfg_alloc(64);
	uint8_t* lutC5 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0xFF; i++) {
		unsigned a0 = (i >> 0) & 0x1;
		unsigned b0 = (i >> 1) & 0x1;
		unsigned c0 = (i >> 2) & 0x1;
		unsigned d0 = (i >> 3) & 0x1;

		unsigned a1 = (i >> 4) & 0x1;
		unsigned b1 = (i >> 5) & 0x1;

		unsigned a2 = (i >> 6) & 0x1;
		unsigned b2 = (i >> 7) & 0x1;

		unsigned sum0 = a0 + b0 + c0;
		unsigned sum1 = a1 + b1;

		unsigned sum = (a0 + b0 + c0 + d0) + 2*(a1 + b1) + 4*(a2 + b2);

		unsigned addr0 = (a0 << 0) + (b0 << 1) + (c0 << 2) + (d0 << 3);
		unsigned addr1 = (a0 << 0) + (b0 << 1) + (c0 << 2) + (a1 << 3) + (b1 << 4);

		unsigned sum_co1 = (a0 + b0 + c0 + d0) + 2*((a1 + b1) & 0x1);
		sum_co1 = (sum_co1 >> 2) & 0x1;
		unsigned sum2 = sum >> 2;
		if(sum2 > 3) sum2 = 3;  // This is the special part
		unsigned addr2 = sum_co1 + (a1 << 1) + (b1 << 2) + (a2 << 3) + (b2 << 4);

		errors_nb += lutcfg_setbit_verbose(lutA6, addr0, (sum0 ^ d0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutA5, addr0, sum0);

		errors_nb += lutcfg_setbit_verbose(lutB6, addr1, ((sum0 >> 1) ^ sum1) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutB5, addr1, (sum0 >> 1) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutC6, addr2, (sum2 >> 1) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutC5, addr2, (sum2 >> 0) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
	printf("LUTB ");
	lutcfg_printhex_lut6_2_nl(lutB6, lutB5);
	printf("LUTC ");
	lutcfg_printhex_lut6_2_nl(lutC6, lutC5);
}

static void adderlut6_comp_555_5_special() {
	// This is a compressor 5,5,5:5
	// Note : The input e0 directly enters the input cy of the carry4

	uint8_t* lutA = lutcfg_alloc(64);
	uint8_t* lutB = lutcfg_alloc(64);
	uint8_t* lutC = lutcfg_alloc(64);

	uint8_t* lutD5 = lutcfg_alloc(64);
	uint8_t* lutD6 = lutcfg_alloc(64);

	uint8_t* lutP5 = lutcfg_alloc(64);
	uint8_t* lutP6 = lutcfg_alloc(64);

	uint8_t* lutQ5 = lutcfg_alloc(64);
	uint8_t* lutQ6 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x3FFF; i++) {

		unsigned a0 = (i >> 0) & 0x1;
		unsigned b0 = (i >> 1) & 0x1;
		unsigned c0 = (i >> 2) & 0x1;
		unsigned d0 = (i >> 3) & 0x1;

		unsigned a1 = (i >> 4) & 0x1;
		unsigned b1 = (i >> 5) & 0x1;
		unsigned c1 = (i >> 6) & 0x1;
		unsigned d1 = (i >> 7) & 0x1;
		unsigned e1 = (i >> 8) & 0x1;

		unsigned a2 = (i >> 9) & 0x1;
		unsigned b2 = (i >> 10) & 0x1;
		unsigned c2 = (i >> 11) & 0x1;
		unsigned d2 = (i >> 12) & 0x1;
		unsigned e2 = (i >> 13) & 0x1;

		// Partial sums
		unsigned part0 = a0 + b0 + c0 + 2*(a1 + b1);
		unsigned p1 = (part0 >> 1) & 0x1;
		unsigned p2 = (part0 >> 2) & 0x1;
		unsigned part1 = c1 + d1 + e1 + 2*(a2 + b2);
		unsigned q2 = (part1 >> 1) & 0x1;
		unsigned q3 = (part1 >> 2) & 0x1;
		unsigned part2 = c2 + d2 + e2 + (part1 >> 1);
		if(part2 > 5) part2 = 5;  // This is the approximation, necessary for lutD to work

		unsigned addrA = (a0 << 0) + (b0 << 1) + (c0 << 2) + (a1 << 3) + (b1 << 4) + (d0 << 5);
		unsigned addrP = (a0 << 0) + (b0 << 1) + (c0 << 2) + (a1 << 3) + (b1 << 4);

		unsigned addrB = (c1 << 0) + (d1 << 1) + (e1 << 2) + (a2 << 3) + (b2 << 4) + (p1 << 5);
		unsigned addrQ = (c1 << 0) + (d1 << 1) + (e1 << 2) + (a2 << 3) + (b2 << 4);

		unsigned addrC = (c2 << 0) + (d2 << 1) + (e2 << 2) + (q2 << 3) + (q3 << 4) + (p2 << 5);
		unsigned addrD = (c2 << 0) + (d2 << 1) + (e2 << 2) + (q2 << 3) + (q3 << 4);

		errors_nb += lutcfg_setbit_verbose(lutP5, addrP, p2);
		errors_nb += lutcfg_setbit_verbose(lutP6, addrP, p1);

		errors_nb += lutcfg_setbit_verbose(lutQ5, addrQ, q3);
		errors_nb += lutcfg_setbit_verbose(lutQ6, addrQ, q2);

		errors_nb += lutcfg_setbit_verbose(lutA, addrA & 0x1F, part0 & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutA, addrA, (part0 & 0x1) ^ d0);

		errors_nb += lutcfg_setbit_verbose(lutB, addrB & 0x1F, part1 & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutB, addrB, (part1 & 0x1) ^ p1);

		errors_nb += lutcfg_setbit_verbose(lutC, addrC & 0x1F, part2 & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutC, addrC, (part2 & 0x1) ^ p2);

		errors_nb += lutcfg_setbit_verbose(lutD5, addrD, (part2 >> 2) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutD6, addrD, (part2 >> 1) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTP ");
	lutcfg_printhex_lut6_2_nl(lutP6, lutP5);
	printf("LUTQ ");
	lutcfg_printhex_lut6_2_nl(lutQ6, lutQ5);

	printf("LUTA ");
	lutcfg_printhex_lut6_nl(lutA);
	printf("LUTB ");
	lutcfg_printhex_lut6_nl(lutB);
	printf("LUTC ");
	lutcfg_printhex_lut6_nl(lutC);
	printf("LUTD ");
	lutcfg_printhex_lut6_2_nl(lutD6, lutD5);
}

static void adderlut6_comp_1235_5_special() {
	// This is a special variant of compressor 1,3,2,5:5 : one input bit is common to positions 0 and 1
	// Config for 4 LUT6 + one CARRY4

	uint8_t* lutA6 = lutcfg_alloc(64);
	uint8_t* lutA5 = lutcfg_alloc(64);

	uint8_t* lutB6 = lutcfg_alloc(64);
	uint8_t* lutB5 = lutcfg_alloc(64);

	uint8_t* lutC6 = lutcfg_alloc(64);
	uint8_t* lutC5 = lutcfg_alloc(64);

	uint8_t* lutD6 = lutcfg_alloc(64);
	uint8_t* lutD5 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x1FF; i++) {
		unsigned a0 = (i >> 0) & 0x1;  // Common to positions 0 and 1
		unsigned b0 = (i >> 1) & 0x1;
		unsigned c0 = (i >> 2) & 0x1;
		unsigned d0 = (i >> 3) & 0x1;

		unsigned a1 = (i >> 4) & 0x1;
		unsigned b1 = (i >> 5) & 0x1;
		unsigned c1 = a0;  // Common to positions 0 and 1

		unsigned a2 = (i >> 6) & 0x1;
		unsigned b2 = (i >> 7) & 0x1;

		unsigned a3 = (i >> 8) & 0x1;

		unsigned sum0 = a0 + b0 + c0;
		unsigned sum1 = a1 + b1 + c1;
		unsigned sum2 = a2 + b2;
		unsigned sum3 = a3;

		unsigned addr0 = (a0 << 0) + (b0 << 1) + (c0 << 2) + (d0 << 3);
		unsigned addr1 = (a0 << 0) + (b0 << 1) + (c0 << 2) + (a1 << 3) + (b1 << 4);
		unsigned addr2 = (a1 << 0) + (b1 << 1) + (c1 << 2) + (a2 << 3) + (b2 << 4);
		unsigned addr3 = (a2 << 0) + (b2 << 1) + (a3 << 2);

		errors_nb += lutcfg_setbit_verbose(lutA6, addr0, (sum0 ^ d0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutA5, addr0, d0);

		errors_nb += lutcfg_setbit_verbose(lutB6, addr1, ((sum0 >> 1) ^ (sum1)) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutB5, addr1, (sum0 >> 1) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutC6, addr2, ((sum1 >> 1) ^ (sum2)) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutC5, addr2, (sum2 >> 0) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutD6, addr3, ((sum2 >> 1) ^ (sum3)) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutD5, addr3, (sum3 >> 0) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
	printf("LUTB ");
	lutcfg_printhex_lut6_2_nl(lutB6, lutB5);
	printf("LUTC ");
	lutcfg_printhex_lut6_2_nl(lutC6, lutC5);
	printf("LUTD ");
	lutcfg_printhex_lut6_2_nl(lutD6, lutD5);
}

static void adderlut6_comp_2225_5_special() {
	// This is a special compressor 2,2,2,5:5
	// It is special because its output is correct only if the theoretical result fits on 5 bits
	// Config for 4 LUT6 + one CARRY4

	uint8_t* lutA6 = lutcfg_alloc(64);
	uint8_t* lutA5 = lutcfg_alloc(64);

	uint8_t* lutB6 = lutcfg_alloc(64);
	uint8_t* lutB5 = lutcfg_alloc(64);

	uint8_t* lutC6 = lutcfg_alloc(64);
	uint8_t* lutC5 = lutcfg_alloc(64);

	uint8_t* lutD6 = lutcfg_alloc(64);
	uint8_t* lutD5 = lutcfg_alloc(64);

	// Fill LUT config

	unsigned errors_nb = 0;
	unsigned inputs_nb = 0;

	for(unsigned i=0; i<=0x3FF; i++) {
		unsigned a0 = (i >> 0) & 0x1;
		unsigned b0 = (i >> 1) & 0x1;
		unsigned c0 = (i >> 2) & 0x1;
		unsigned d0 = (i >> 3) & 0x1;

		unsigned a1 = (i >> 4) & 0x1;
		unsigned b1 = (i >> 5) & 0x1;

		unsigned a2 = (i >> 6) & 0x1;
		unsigned b2 = (i >> 7) & 0x1;

		unsigned a3 = (i >> 8) & 0x1;
		unsigned b3 = (i >> 9) & 0x1;

		unsigned sum0 = a0 + b0 + c0;
		unsigned sum1 = a1 + b1;
		unsigned sum2 = a2 + b2;

		unsigned sum = (a0 + b0 + c0 + d0) + 2*(a1 + b1) + 4*(a2 + b2) + 8*(a3 + b3);

		unsigned addr0 = (a0 << 0) + (b0 << 1) + (c0 << 2) + (d0 << 3);
		unsigned addr1 = (a0 << 0) + (b0 << 1) + (c0 << 2) + (a1 << 3) + (b1 << 4);
		unsigned addr2 = (a1 << 0) + (b1 << 1) + (a2 << 2) + (b2 << 3);

		unsigned sum_co2 = (a0 + b0 + c0 + d0) + 2*(a1 + b1) + 4*((a2 + b2) & 0x1);
		sum_co2 = (sum_co2 >> 3) & 0x1;
		unsigned sum3 = sum >> 3;
		if(sum3 > 3) sum3 = 3;  // This is the special part
		unsigned addr3 = sum_co2 + (a2 << 1) + (b2 << 2) + (a3 << 3) + (b3 << 4);

		errors_nb += lutcfg_setbit_verbose(lutA6, addr0, (sum0 ^ d0) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutA5, addr0, sum0);

		errors_nb += lutcfg_setbit_verbose(lutB6, addr1, ((sum0 >> 1) ^ sum1) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutB5, addr1, (sum0 >> 1) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutC6, addr2, ((sum1 >> 1) ^ sum2) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutC5, addr2, (sum2 >> 0) & 0x1);

		errors_nb += lutcfg_setbit_verbose(lutD6, addr3, (sum3 >> 1) & 0x1);
		errors_nb += lutcfg_setbit_verbose(lutD5, addr3, (sum3 >> 0) & 0x1);

		inputs_nb++;

	}  // Scan combinations of inputs

	if(errors_nb > 0) printf("Errors: %u in %u input vectors\n", errors_nb, inputs_nb);

	// Print LUT config

	printf("LUTA ");
	lutcfg_printhex_lut6_2_nl(lutA6, lutA5);
	printf("LUTB ");
	lutcfg_printhex_lut6_2_nl(lutB6, lutB5);
	printf("LUTC ");
	lutcfg_printhex_lut6_2_nl(lutC6, lutC5);
	printf("LUTD ");
	lutcfg_printhex_lut6_2_nl(lutD6, lutD5);
}

// Wrapper function to launch generation LUT configurations of optimized components

static int adderlut6_all(char* want_what) {
	bool want_all = false;
	bool want_list = false;

	if(want_what != NULL) {
		if(strcasecmp(want_what, "all")==0) want_all = true;
		if(strcasecmp(want_what, "list")==0 || strcasecmp(want_what, "ls")==0 || strcasecmp(want_what, "help")==0) want_list = true;
	}
	else {
		want_what = "";
		want_all = true;
	}

	char* msg = NULL;
	char* what = NULL;

	{ what = "u3to4";
		msg  = "ADD 2x 3b -> 4b, unsigned";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_3to4(false);
			if(want_all==false) return 0;
		}
	}

	{ what = "s3to4";
		msg  = "ADD 2x 3b -> 4b, signed";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_3to4(true);
			if(want_all==false) return 0;
		}
	}

	{ what = "u4to5";
		msg  = "ADD 2x 4b -> 5b, unsigned";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_4to5(false);
			if(want_all==false) return 0;
		}
	}

	{ what = "s4to5";
		msg  = "ADD 2x 4b -> 5b, signed";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_4to5(true);
			if(want_all==false) return 0;
		}
	}

	{ what = "u5to6";
		msg  = "ADD 2x 5b -> 6b, unsigned";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_5to6(false);
			if(want_all==false) return 0;
		}
	}

	{ what = "s5to6";
		msg  = "ADD 2x 5b -> 6b, signed";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_5to6(true);
			if(want_all==false) return 0;
		}
	}

	{ what = "add2plus";
		msg  = "ADD 2 inputs + 3 bits";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_2in_plus(true);
			if(want_all==false) return 0;
		}
	}

	{ what = "add3in";
		msg  = "ADD 3 inputs";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_3in(true);
			if(want_all==false) return 0;
		}
	}

	{ what = "add3in2b";
		msg  = "ADD 3x 2b -> 4b (unsigned and signed)";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_3in_2b();
			if(want_all==false) return 0;
		}
	}

	{ what = "3ter3b";
		msg  = "ADD 3x ternary -> 3b";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_3ter3b();
			if(want_all==false) return 0;
		}
	}

	{ what = "add4in2b";
		msg  = "ADD 4x 2b -> 4b (unsigned and signed)";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_4in_2b();
			if(want_all==false) return 0;
		}
	}

	{ what = "4ter4b";
		msg  = "ADD 4x ternary -> 4b";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_4ter4b();
			if(want_all==false) return 0;
		}
	}

	{	what = "add5in2b";
		msg  = "ADD 5x 2b -> 5b (unsigned and signed)";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_5in_2b();
			if(want_all==false) return 0;
		}
	}

	{ what = "5ter4b";
		msg  = "ADD 6x ternary -> 4b";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_5ter4b();
			if(want_all==false) return 0;
		}
	}

	{ what = "7ter4b";
		msg  = "ADD 7x ternary -> 4b";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_7ter4b();
			if(want_all==false) return 0;
		}
	}

	{ what = "mul2ter2b";
		msg  = "MUL ternary -> 2b";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_mul_ter2b();
			if(want_all==false) return 0;
		}
	}

	{ what = "mac2ter3b";
		msg  = "MULADD 2x ternary -> 3b";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_muladd_2ter3b();
			if(want_all==false) return 0;
		}
	}

	{ what = "madd-st0";
		msg  = "MULADD stage 0";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_muladd_st0();
			if(want_all==false) return 0;
		}
	}

	{ what = "madd-bin-4in";
		msg  = "MULADD 4x 1b";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_muladd_bin_4in(false);
			adderlut6_muladd_bin_4in(true);
			if(want_all==false) return 0;
		}
	}

	{ what = "3t5b";
		msg  = "TERNARY compression 3t5b";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_3t5b();
			if(want_all==false) return 0;
		}
	}

	{ what = "tercomp8b5t";
		msg  = "TERNARY DECOMPRESSION 8b -> 5T";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_tercomp_8b5t();
			if(want_all==false) return 0;
		}
	}

	// Popcounts

	{ what = "popc3:2";
		msg  = "POPCOUNT 3b -> 2b";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_popc_3_2(true);
			if(want_all==false) return 0;
		}
	}

	{ what = "popc5:3";
		msg  = "POPCOUNT 5b -> 3b";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_popc_5_3(true);
			if(want_all==false) return 0;
		}
	}

	{ what = "popc6:3";
		msg  = "POPCOUNT 6b -> 3b";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_popc_6_3(true);
			if(want_all==false) return 0;
		}
	}

	{ what = "popc7:3";
		msg  = "POPCOUNT 7b -> 3b";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_popc_7_3(true);
			if(want_all==false) return 0;
		}
	}

	{ what = "popc7:3carry4";
		msg  = "POPCOUNT 7b -> 3b that uses half of a CARRY4";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_popc_7_3_carry4(true);
			if(want_all==false) return 0;
		}
	}

	{ what = "popc8:4";
		msg  = "POPCOUNT 8b -> 4b";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_popc_8_4(true);
			if(want_all==false) return 0;
		}
	}

	{ what = "popc8:4strange";
		msg  = "POPCOUNT 8b -> 4b with strange config and high delay";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_popc_8_4_strange(true);
			if(want_all==false) return 0;
		}
	}

	{ what = "popc9:4";
		msg  = "POPCOUNT 9b -> 4b";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_popc_9_4(true);
			if(want_all==false) return 0;
		}
	}

	{ what = "popc12:4";
		msg  = "POPCOUNT 12b -> 4b";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_popc_12_4(true);
			if(want_all==false) return 0;
		}
	}

	// General-purpose compressors

	{ what = "heap4:2";
		msg  = "COMPR 4:2";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_4_2();
			if(want_all==false) return 0;
		}
	}

	{ what = "heap4:2s";
		msg  = "COMPR 4:2 signed";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_4_2_signed();
			if(want_all==false) return 0;
		}
	}

	{ what = "heap15:3";
		msg  = "COMPR 1,5:3";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_15_3(true);
			if(want_all==false) return 0;
		}
	}

	{ what = "heap44:4";
		msg  = "COMPR 44:4";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_44_4();
			if(want_all==false) return 0;
		}
	}

	{ what = "heap67:5";
		msg  = "COMPR 67:5";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_67_5(false);
			adderlut6_comp_67_5(true);
			if(want_all==false) return 0;
		}
	}

	{ what = "heap77:5";
		msg  = "COMPR 77:5, recoding with 2 luts, delay 2 luts";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_77_5();
			if(want_all==false) return 0;
		}
	}

	{ what = "heap77:5luts3";
		msg  = "COMPR 77:5, recoding with 3 luts, delay 1 lut";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_77_5_luts3(false);
			adderlut6_comp_77_5_luts3(true);
			if(want_all==false) return 0;
		}
	}

	{ what = "heap223:4";
		msg  = "ADD 2x 3b + 1, unsigned";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_223_4(true);
			if(want_all==false) return 0;
		}
	}

	{ what = "heap446:5";
		msg  = "COMPR 4,4,6:5";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_446_5();
			if(want_all==false) return 0;
		}
	}

	{ what = "heap447:5";
		msg  = "COMPR 4,4,7:5";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_447_5();
			if(want_all==false) return 0;
		}
	}

	{ what = "heap506:5";
		msg  = "COMPR 5,0,6:5";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_506_5();
			if(want_all==false) return 0;
		}
	}

	{ what = "heap606:5";
		msg  = "COMPR 6,0,6:5";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_606_5();
			if(want_all==false) return 0;
		}
	}

	{ what = "heap606:5s";
		msg  = "COMPR 6,0,6:5 signed";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_606_5_signed();
			if(want_all==false) return 0;
		}
	}

	{ what = "heap1066:5";
		msg  = "COMPR 1,0,6,6:5";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_1066_5();
			if(want_all==false) return 0;
		}
	}

	{ what = "heap1325:5";
		msg  = "COMPR 1,3,2,5:5";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_1325_5();
			if(want_all==false) return 0;
		}
	}

	{ what = "heap2117:5";
		msg  = "COMPR 2,1,1,7:5";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_2117_5();
			if(want_all==false) return 0;
		}
	}

	{ what = "heap2135:5";
		msg  = "COMPR 2,1,3,5:5";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_2135_5();
			if(want_all==false) return 0;
		}
	}

	{ what = "heap2223:5";
		msg  = "COMPR 2,2,2,3:5";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_2223_5();
			if(want_all==false) return 0;
		}
	}

	// Special-purpose compressors

	{ what = "heap16:3special";
		msg  = "COMPR 1,6:3 special because the input bit at position 1 is mutually exclusive with an input bit at position 0";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_16_3_special();
			if(want_all==false) return 0;
		}
	}

	{ what = "heap136:4special";
		msg  = "COMPR 1,3,6:4 special : purpose it to recode output of a 6,0,7:5 for popcount applications";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_136_4_special();
			if(want_all==false) return 0;
		}
	}

	{ what = "heap1136:5special";
		msg  = "COMPR 1,1,3,6:5 special : purpose it to recode output of a 6,0,7:5 for popcount applications";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_1136_5_special();
			if(want_all==false) return 0;
		}
	}

	{ what = "heap225:4special";
		msg  = "COMPR 2,2,5:4 special because output is reduced to 4 bits";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			printf("Default implem\n");
			adderlut6_comp_225_4_special();
			printf("Alt implem\n");
			adderlut6_comp_225_4_special_alt();
			if(want_all==false) return 0;
		}
	}

	{ what = "heap555:5special";
		msg  = "COMPR 5,5,7:5 special because sum of higher bits is maxed";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_555_5_special();
			if(want_all==false) return 0;
		}
	}

	{ what = "heap1235:5special";
		msg  = "COMPR 1,2,3,5:5 special because one input is shared between positions 0 and 1";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_1235_5_special();
			if(want_all==false) return 0;
		}
	}

	{ what = "heap2225:5special";
		msg  = "COMPR 2,2,2,5:5 special because output is reduced to 5 bits";
		if(want_list==true) printf("%s\t%s\n", what, msg);
		if(want_all==true) printf("%s\n", msg);
		if(strcasecmp(want_what, what)==0 || want_all==true) {
			adderlut6_comp_2225_5_special();
			if(want_all==false) return 0;
		}
	}

	// FIXME print error message if wanted something not found ?
	if(want_all==false && want_list==false) return -1;

	return 0;
}


//==============================
// Main function
//==============================

void print_usage(int argc, char** argv) {
	printf("\n");
	printf("This tool generates the LUT configurations for specialized adders, compressors, etc.\n");
	printf("\n");
	printf("Usage:\n");
	printf("  %s [options] <name>\n", argv[0]);
	printf("\n");
	printf("Options:\n");
	printf("  -u -h --help      Display this help and exit\n");
	printf("  <name>            Print the LUT config for <name> (see below)\n");
	printf("    help | list | ls  Print the available configurations\n");
	printf("    all               Print results for all configurations\n");
	printf("\n");
}

int main(int argc, char** argv) {

	if(argc <= 1) {
		print_usage(argc, argv);
		return 0;
	}

	unsigned argi = 1;
	for( ; argi<argc; argi++) {
		char* arg = argv[argi];

		// Local utility functions

		// Get parameters
		#if 0
		char* getparam_str() {
			argi++;
			if(argi >= argc) {
				printf("Error: Missing parameters after '%s'\n", arg);
				exit(EXIT_FAILURE);
			}
			return argv[argi];
		}
		#endif

		// Parse arguments

		if(strcmp(arg, "-u") == 0 || strcmp(arg, "-h") == 0 || strcmp(arg, "--help") == 0) {
			print_usage(argc, argv);
			return 0;
		}

		else if(arg[0] == '-') {
			printf("Error: Unknown parameter '%s'\n", arg);
			exit(EXIT_FAILURE);
		}

		else {
			int z = adderlut6_all(arg);
			if(z!=0) {
				printf("Error : Invalid configuration name '%s'\n", arg);
				exit(EXIT_FAILURE);
			}
		}

	}  // Parse arguments

	return 0;
}

