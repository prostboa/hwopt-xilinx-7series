
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity popcount_bench is
end popcount_bench;

architecture simu of popcount_bench is

	signal clk      : std_logic := '0';
	signal clk_next : std_logic := '0';
	signal clear    : std_logic := '1';
	signal clk_want_stop : boolean := false;

	constant BITS_IN : natural := 18;

	signal in_bits : std_logic_vector(BITS_IN-1 downto 0) := (others => '0');

	signal res5    : std_logic_vector(2 downto 0) := (others => '0');
	signal res6    : std_logic_vector(2 downto 0) := (others => '0');
	signal res7    : std_logic_vector(2 downto 0) := (others => '0');
	signal res7_c4 : std_logic_vector(2 downto 0) := (others => '0');
	signal res8    : std_logic_vector(3 downto 0) := (others => '0');
	signal res9    : std_logic_vector(3 downto 0) := (others => '0');
	signal res11   : std_logic_vector(3 downto 0) := (others => '0');
	signal res12   : std_logic_vector(3 downto 0) := (others => '0');
	signal res13   : std_logic_vector(3 downto 0) := (others => '0');
	signal res13_alt : std_logic_vector(3 downto 0) := (others => '0');
	signal res15   : std_logic_vector(3 downto 0) := (others => '0');
	signal res15_alt : std_logic_vector(3 downto 0) := (others => '0');
	signal res16   : std_logic_vector(4 downto 0) := (others => '0');
	signal res17   : std_logic_vector(4 downto 0) := (others => '0');
	signal res18   : std_logic_vector(4 downto 0) := (others => '0');

	component popcount_5_3 is
		generic (
			BITS : natural := 5;
			WOUT : natural := 3
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component popcount_6_3 is
		generic (
			BITS : natural := 6;
			WOUT : natural := 3
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component popcount_7_3 is
		generic (
			BITS : natural := 7;
			WOUT : natural := 3
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component popcount_7_3_carry4 is
		generic (
			BITS : natural := 7;
			WOUT : natural := 3
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component popcount_8_4 is
		generic (
			BITS : natural := 8;
			WOUT : natural := 4
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component popcount_9_4 is
		generic (
			BITS : natural := 9;
			WOUT : natural := 4
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component popcount_11_4 is
		generic (
			BITS : natural := 11;
			WOUT : natural := 4
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component popcount_12_4 is
		generic (
			BITS : natural := 12;
			WOUT : natural := 4
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component popcount_13_4 is
		generic (
			BITS : natural := 13;
			WOUT : natural := 4
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component popcount_13_4_alt is
		generic (
			BITS : natural := 13;
			WOUT : natural := 4
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component popcount_15_4 is
		generic (
			BITS : natural := 15;
			WOUT : natural := 4
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component popcount_15_4_alt is
		generic (
			BITS : natural := 15;
			WOUT : natural := 4
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component popcount_16_5 is
		generic (
			BITS : natural := 16;
			WOUT : natural := 5
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component popcount_17_5 is
		generic (
			BITS : natural := 17;
			WOUT : natural := 5
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component popcount_18_5 is
		generic (
			BITS : natural := 18;
			WOUT : natural := 5
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	-- Simple popcount calculation
	function func_popcount(A : std_logic_vector) return natural is
		variable c : natural;
	begin
		c := 0;
		for i in A'low to A'high loop
			if A(i) = '1' then
				c := c + 1;
			end if;
		end loop;
		return c;
	end function;

begin

	popc5 : popcount_5_3
		generic map (
			BITS => 5,
			WOUT => 3
		)
		port map (
			bits_in => in_bits(4 downto 0),
			sum     => res5
		);

	popc6 : popcount_6_3
		generic map (
			BITS => 6,
			WOUT => 3
		)
		port map (
			bits_in => in_bits(5 downto 0),
			sum     => res6
		);

	popc7 : popcount_7_3
		generic map (
			BITS => 7,
			WOUT => 3
		)
		port map (
			bits_in => in_bits(6 downto 0),
			sum     => res7
		);

	popc7_c4 : popcount_7_3_carry4
		generic map (
			BITS => 7,
			WOUT => 3
		)
		port map (
			bits_in => in_bits(6 downto 0),
			sum     => res7_c4
		);

	popc8 : popcount_8_4
		generic map (
			BITS => 8,
			WOUT => 4
		)
		port map (
			bits_in => in_bits(7 downto 0),
			sum     => res8
		);

	popc9 : popcount_9_4
		generic map (
			BITS => 9,
			WOUT => 4
		)
		port map (
			bits_in => in_bits(8 downto 0),
			sum     => res9
		);

	popc11 : popcount_11_4
		generic map (
			BITS => 11,
			WOUT => 4
		)
		port map (
			bits_in => in_bits(10 downto 0),
			sum     => res11
		);

	popc12 : popcount_12_4
		generic map (
			BITS => 12,
			WOUT => 4
		)
		port map (
			bits_in => in_bits(11 downto 0),
			sum     => res12
		);

	popc13 : popcount_13_4
		generic map (
			BITS => 13,
			WOUT => 4
		)
		port map (
			bits_in => in_bits(12 downto 0),
			sum     => res13
		);

	popc13_alt : popcount_13_4_alt
		generic map (
			BITS => 13,
			WOUT => 4
		)
		port map (
			bits_in => in_bits(12 downto 0),
			sum     => res13_alt
		);

	popc15 : popcount_15_4
		generic map (
			BITS => 15,
			WOUT => 4
		)
		port map (
			bits_in => in_bits(14 downto 0),
			sum     => res15
		);

	popc15_alt : popcount_15_4_alt
		generic map (
			BITS => 15,
			WOUT => 4
		)
		port map (
			bits_in => in_bits(14 downto 0),
			sum     => res15_alt
		);

	popc16 : popcount_16_5
		generic map (
			BITS => 16,
			WOUT => 5
		)
		port map (
			bits_in => in_bits(15 downto 0),
			sum     => res16
		);

	popc17 : popcount_17_5
		generic map (
			BITS => 17,
			WOUT => 5
		)
		port map (
			bits_in => in_bits(16 downto 0),
			sum     => res17
		);

	popc18 : popcount_18_5
		generic map (
			BITS => 18,
			WOUT => 5
		)
		port map (
			bits_in => in_bits(17 downto 0),
			sum     => res18
		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	-- Process that generates stimuli
	process
		variable want_res5  : integer := 0;
		variable want_res6  : integer := 0;
		variable want_res7  : integer := 0;
		variable want_res8  : integer := 0;
		variable want_res9  : integer := 0;
		variable want_res11 : integer := 0;
		variable want_res12 : integer := 0;
		variable want_res13 : integer := 0;
		variable want_res14 : integer := 0;
		variable want_res15 : integer := 0;
		variable want_res16 : integer := 0;
		variable want_res17 : integer := 0;
		variable want_res18 : integer := 0;
		variable var_errors_nb : integer := 0;
		variable var_tests_nb  : integer := 0;
	begin

		wait until rising_edge(clk);

		for i in 0 to 2**BITS_IN-1 loop
			in_bits <= std_logic_vector(to_unsigned(i, BITS_IN));

			wait until rising_edge(clk);

			-- Generate inputs

			wait until falling_edge(clk);

			-- Compare with expected outputs

			want_res5  := func_popcount( in_bits(4 downto 0) );
			want_res6  := func_popcount( in_bits(5 downto 0) );
			want_res7  := func_popcount( in_bits(6 downto 0) );
			want_res8  := func_popcount( in_bits(7 downto 0) );
			want_res9  := func_popcount( in_bits(8 downto 0) );
			want_res11 := func_popcount( in_bits(10 downto 0) );
			want_res12 := func_popcount( in_bits(11 downto 0) );
			want_res13 := func_popcount( in_bits(12 downto 0) );
			want_res14 := func_popcount( in_bits(13 downto 0) );
			want_res15 := func_popcount( in_bits(14 downto 0) );
			want_res16 := func_popcount( in_bits(15 downto 0) );
			want_res17 := func_popcount( in_bits(16 downto 0) );
			want_res18 := func_popcount( in_bits(17 downto 0) );

			if unsigned(res5) /= want_res5 then
				report "ERROR Input " & natural'image(i) & " / popc5 have " & natural'image(to_integer(unsigned(res5))) & " want " & integer'image(want_res5);
				var_errors_nb := var_errors_nb + 1;
			end if;

			if unsigned(res6) /= want_res6 then
				report "ERROR Input " & natural'image(i) & " / popc6 have " & natural'image(to_integer(unsigned(res6))) & " want " & integer'image(want_res6);
				var_errors_nb := var_errors_nb + 1;
			end if;

			if (unsigned(res7) /= want_res7) or (unsigned(res7_c4) /= want_res7) then
				report
					"ERROR Input " & natural'image(i) &
					" / popc7 have "    & natural'image(to_integer(unsigned(res7))) &
					" / popc7_c4 have " & natural'image(to_integer(unsigned(res7_c4))) &
					" want "            & integer'image(want_res7);
				var_errors_nb := var_errors_nb + 1;
			end if;

			if unsigned(res8) /= want_res8 then
				report "ERROR Input " & natural'image(i) & " / popc8 have " & natural'image(to_integer(unsigned(res8))) & " want " & integer'image(want_res8);
				var_errors_nb := var_errors_nb + 1;
			end if;

			if unsigned(res9) /= want_res9 then
				report "ERROR Input " & natural'image(i) & " / popc9 have " & natural'image(to_integer(unsigned(res9))) & " want " & integer'image(want_res9);
				var_errors_nb := var_errors_nb + 1;
			end if;

			if unsigned(res11) /= want_res11 then
				report "ERROR Input " & natural'image(i) & " / popc11 have " & natural'image(to_integer(unsigned(res11))) & " want " & integer'image(want_res11);
				var_errors_nb := var_errors_nb + 1;
			end if;

			if unsigned(res12) /= want_res12 then
				report "ERROR Input " & natural'image(i) & " / popc12 have " & natural'image(to_integer(unsigned(res12))) & " want " & integer'image(want_res12);
				var_errors_nb := var_errors_nb + 1;
			end if;

			if (unsigned(res13) /= want_res13) or (unsigned(res13_alt) /= want_res13) then
				report
					"ERROR Input " & natural'image(i) &
					" / popc13 have "     & natural'image(to_integer(unsigned(res13))) &
					" / popc13_alt have " & natural'image(to_integer(unsigned(res13_alt))) &
					" want "              & integer'image(want_res13);
				var_errors_nb := var_errors_nb + 1;
			end if;

			if (unsigned(res15) /= want_res15) or (unsigned(res15_alt) /= want_res15) then
				report
					"ERROR Input " & natural'image(i) &
					" / popc15 have "     & natural'image(to_integer(unsigned(res15))) &
					" / popc15_alt have " & natural'image(to_integer(unsigned(res15_alt))) &
					" want "              & integer'image(want_res15);
				var_errors_nb := var_errors_nb + 1;
			end if;

			if unsigned(res16) /= want_res16 then
				report "ERROR Input " & natural'image(i) & " / popc16 have " & natural'image(to_integer(unsigned(res16))) & " want " & integer'image(want_res16);
				var_errors_nb := var_errors_nb + 1;
			end if;

			if unsigned(res17) /= want_res17 then
				report "ERROR Input " & natural'image(i) & " / popc17 have " & natural'image(to_integer(unsigned(res17))) & " want " & integer'image(want_res17);
				var_errors_nb := var_errors_nb + 1;
			end if;

			if unsigned(res18) /= want_res18 then
				report "ERROR Input " & natural'image(i) & " / popc18 have " & natural'image(to_integer(unsigned(res18))) & " want " & integer'image(want_res18);
				var_errors_nb := var_errors_nb + 1;
			end if;

			var_tests_nb := var_tests_nb + 1;

		end loop;

		report "Results : " & natural'image(var_tests_nb) & " test vectors, " & natural'image(var_errors_nb) & " errors";

		-- End of simulation
		clk_want_stop <= true;
		wait;

	end process;

end architecture;


