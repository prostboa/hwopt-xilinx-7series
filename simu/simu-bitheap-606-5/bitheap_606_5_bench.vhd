
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity bitheap_606_5_bench is
end bitheap_606_5_bench;

architecture simu of bitheap_606_5_bench is

	signal clk      : std_logic := '0';
	signal clk_next : std_logic := '0';
	signal clear    : std_logic := '1';
	signal clk_want_stop : boolean := false;

	constant BITS_IN : natural := 12 + 1;

	signal ci  : std_logic := '0';
	signal di0 : std_logic_vector(5 downto 0) := (others => '0');
	signal di2 : std_logic_vector(5 downto 0) := (others => '0');
	signal do  : std_logic_vector(3 downto 0) := (others => '0');
	signal co  : std_logic_vector(3 downto 0) := (others => '0');

	component bitheap_606_5 is
		port (
			cy  : in  std_logic;
			ci  : in  std_logic;
			di0 : in  std_logic_vector(5 downto 0);
			di2 : in  std_logic_vector(5 downto 0);
			do  : out std_logic_vector(3 downto 0);
			co  : out std_logic_vector(3 downto 0)
		);
	end component;

begin

	comp : bitheap_606_5
		port map (
			cy  => '0',
			ci  => ci,
			di0 => di0,
			di2 => di2,
			do  => do,
			co  => co
		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	-- Process that generates stimuli
	process
		variable in_number  : std_logic_vector(BITS_IN-1 downto 0) := (others => '0');
		variable out_number : std_logic_vector(4 downto 0) := (others => '0');
		variable popc       : natural;
		variable var_errors_nb : integer := 0;
		variable var_tests_nb : integer := 0;
	begin

		wait until rising_edge(clk);

		for i in 0 to 2**BITS_IN-1 loop
			in_number := std_logic_vector(to_unsigned(i, BITS_IN));

			ci  <= in_number(0);
			di0 <= in_number(6 downto 1);
			di2 <= in_number(12 downto 7);

			wait until falling_edge(clk);

			popc := 0;
			for p in 0 to 5 loop
				if di0(p) = '1' then popc := popc + 1; end if;
				if di2(p) = '1' then popc := popc + 4; end if;
			end loop;
			if ci = '1' then
				popc := popc + 1;
			end if;

			out_number := co(3) & do;

			if unsigned(out_number) /= popc then
				report
					"ERROR Wrong output" &
					" input " & natural'image(i) &
					" got "   & natural'image(to_integer(unsigned(out_number))) &
					" expected " & integer'image(popc);
				var_errors_nb := var_errors_nb + 1;
			end if;

			var_tests_nb := var_tests_nb + 1;

		end loop;

		report "Results : " & natural'image(var_tests_nb) & " test vectors, " & natural'image(var_errors_nb) & " errors";

		-- End of simulation
		clk_want_stop <= true;
		wait;

	end process;

end architecture;


