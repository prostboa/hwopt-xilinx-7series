
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity bitheap_2117_5_bench is
end bitheap_2117_5_bench;

architecture simu of bitheap_2117_5_bench is

	signal clk      : std_logic := '0';
	signal clk_next : std_logic := '0';
	signal clear    : std_logic := '1';
	signal clk_want_stop : boolean := false;

	constant BITS_IN : natural := 2 + 1 + 1 + 7;

	signal di0 : std_logic_vector(6 downto 0) := (others => '0');
	signal di1 : std_logic_vector(0 downto 0) := (others => '0');
	signal di2 : std_logic_vector(0 downto 0) := (others => '0');
	signal di3 : std_logic_vector(1 downto 0) := (others => '0');

	signal sum4 : std_logic_vector(3 downto 0) := (others => '0');
	signal sum5 : std_logic_vector(4 downto 0) := (others => '0');

	component bitheap_117_4 is
		port (
			ci  : in  std_logic;
			di0 : in  std_logic_vector(6 downto 0);
			di1 : in  std_logic_vector(0 downto 0);
			di2 : in  std_logic_vector(0 downto 0);
			do  : out std_logic_vector(3 downto 0);
			co  : out std_logic_vector(3 downto 0);
			sum : out std_logic_vector(3 downto 0)
		);
	end component;

	component bitheap_2117_5 is
		port (
			ci  : in  std_logic;
			di0 : in  std_logic_vector(6 downto 0);
			di1 : in  std_logic_vector(0 downto 0);
			di2 : in  std_logic_vector(0 downto 0);
			di3 : in  std_logic_vector(1 downto 0);
			do  : out std_logic_vector(3 downto 0);
			co  : out std_logic_vector(3 downto 0);
			sum : out std_logic_vector(4 downto 0)
		);
	end component;

	-- Simple popcount calculation
	function func_popcount(A : std_logic_vector) return natural is
		variable c : natural;
	begin
		c := 0;
		for i in A'low to A'high loop
			if A(i) = '1' then
				c := c + 1;
			end if;
		end loop;
		return c;
	end function;

begin

	comp4 : bitheap_117_4
		port map (
			ci  => '0',
			di0 => di0,
			di1 => di1,
			di2 => di2,
			do  => open,
			co  => open,
			sum => sum4
		);

	comp5 : bitheap_2117_5
		port map (
			ci  => '0',
			di0 => di0,
			di1 => di1,
			di2 => di2,
			di3 => di3,
			do  => open,
			co  => open,
			sum => sum5
		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	-- Process that generates stimuli
	process
		variable in_number  : std_logic_vector(BITS_IN-1 downto 0) := (others => '0');
		variable want4      : natural := 0;
		variable want5      : natural := 0;
		variable var_errors_nb : integer := 0;
		variable var_tests_nb : integer := 0;
	begin

		wait until rising_edge(clk);

		for i in 0 to 2**BITS_IN-1 loop
			in_number := std_logic_vector(to_unsigned(i, BITS_IN));

			di0 <= in_number(6 downto 0);
			di1 <= in_number(7 downto 7);
			di2 <= in_number(8 downto 8);
			di3 <= in_number(10 downto 9);

			wait until falling_edge(clk);

			want4 :=
				1 * func_popcount(di0) +
				2 * func_popcount(di1) +
				4 * func_popcount(di2);

			want5 :=
				1 * func_popcount(di0) +
				2 * func_popcount(di1) +
				4 * func_popcount(di2) +
				8 * func_popcount(di3);

			if unsigned(sum4) /= want4 then
				report
					"ERROR Wrong output for 117:4 :" &
					" input " & natural'image(i) &
					" got "   & natural'image(to_integer(unsigned(sum4))) &
					" expected " & integer'image(want4);
				var_errors_nb := var_errors_nb + 1;
			end if;

			if unsigned(sum5) /= want5 then
				report
					"ERROR Wrong output for 2117:5 :" &
					" input " & natural'image(i) &
					" got "   & natural'image(to_integer(unsigned(sum5))) &
					" expected " & integer'image(want5);
				var_errors_nb := var_errors_nb + 1;
			end if;

			var_tests_nb := var_tests_nb + 1;

		end loop;

		report "Results : " & natural'image(var_tests_nb) & " test vectors, " & natural'image(var_errors_nb) & " errors";

		-- End of simulation
		clk_want_stop <= true;
		wait;

	end process;

end architecture;


