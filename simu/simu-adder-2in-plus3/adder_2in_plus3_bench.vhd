
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity adder_2in_plus3_bench is
end adder_2in_plus3_bench;

architecture simu of adder_2in_plus3_bench is

	signal clk      : std_logic := '0';
	signal clk_next : std_logic := '0';
	signal clear    : std_logic := '1';
	signal clk_want_stop : boolean := false;


	constant WDATA : natural := 5;
	constant NB1   : natural := 3;  -- FIXME Need to test with both NB1=3 and NB1=1 because there is a threshold internally
	constant BITS_IN : natural := 2*WDATA + NB1;

	signal diA : std_logic_vector(WDATA-1 downto 0) := (others => '0');
	signal diB : std_logic_vector(WDATA-1 downto 0) := (others => '0');
	signal di1 : std_logic_vector(NB1-1 downto 0)   := (others => '0');

	signal res_u : std_logic_vector(WDATA+1 downto 0) := (others => '0');
	signal res_s : std_logic_vector(WDATA+1 downto 0) := (others => '0');

	component adder_2in_plus3 is
		generic (
			WDATA : natural := 8;
			WOUT  : natural := 10;
			NB1   : natural := 3;
			SIGN  : boolean := false
		);
		port (
			data_A : in  std_logic_vector(WDATA-1 downto 0);
			data_B : in  std_logic_vector(WDATA-1 downto 0);
			data_1 : in  std_logic_vector(NB1-1 downto 0);
			res    : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

begin

	comp_u : adder_2in_plus3
		generic map (
			WDATA => WDATA,
			WOUT  => WDATA+2,
			NB1   => NB1,
			SIGN  => false
		)
		port map (
			data_A => diA,
			data_B => diB,
			data_1 => di1,
			res    => res_u
		);

	comp_s : adder_2in_plus3
		generic map (
			WDATA => WDATA,
			WOUT  => WDATA+2,
			NB1   => NB1,
			SIGN  => true
		)
		port map (
			data_A => diA,
			data_B => diB,
			data_1 => di1,
			res    => res_s
		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	-- Process that generates stimuli
	process
		variable in_bits : std_logic_vector(BITS_IN-1 downto 0) := (others => '0');
		variable want_u : integer := 0;
		variable want_s : integer := 0;
		variable var_errors_nb : integer := 0;
		variable var_tests_nb  : integer := 0;
	begin

		wait until rising_edge(clk);

		for i in 0 to 2**BITS_IN-1 loop
			in_bits := std_logic_vector(to_unsigned(i, BITS_IN));

			diA <= in_bits(  WDATA-1 downto 0);
			diB <= in_bits(2*WDATA-1 downto WDATA);
			di1 <= in_bits(2*WDATA+NB1-1 downto 2*WDATA);

			wait until falling_edge(clk);

			want_u := to_integer(unsigned(diA)) + to_integer(unsigned(diB));
			want_s := to_integer(  signed(diA)) + to_integer(  signed(diB));

			for p in 0 to NB1-1 loop
				if di1(p) = '1' then
					want_u := want_u   + 1;
					want_s := want_s + 1;
				end if;
			end loop;

			if (unsigned(res_u) /= want_u) or (signed(res_s) /= want_s) then
				report
					"ERROR Wrong output" &
					" input " & natural'image(i) &
					" got U " & natural'image(to_integer(unsigned(res_u))) & " expected " & integer'image(want_u) &
					" got S " & natural'image(to_integer(signed(res_s))) & " expected " & integer'image(want_s);
				var_errors_nb := var_errors_nb + 1;
			end if;

			var_tests_nb := var_tests_nb + 1;

		end loop;

		report "Results : " & natural'image(var_tests_nb) & " test vectors, " & natural'image(var_errors_nb) & " errors";

		-- End of simulation
		clk_want_stop <= true;
		wait;

	end process;

end architecture;


