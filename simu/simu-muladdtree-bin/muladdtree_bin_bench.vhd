
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity muladdtree_bin_bench is
	generic (
		MAX_NBIN : integer := 8
	);
end muladdtree_bin_bench;

architecture simu of muladdtree_bin_bench is

	component muladdtree_bin is
		generic(
			NBIN  : natural := 8;
			WOUT  : natural := 4;
			-- The type of operation to perform : false for operation AND, true for operation XNOR
			IS_XNOR : boolean := false;
			-- Tag-related information
			TAGW  : natural := 1;
			TAGEN : boolean := false;
			TAGZC : boolean := false;
			-- How to add pipeline registers
			REGEN  : natural := 0;  -- 0=none, 1=all, else every EN stages
			REGIDX : natural := 0   -- Start index (from the leaves)
		);
		port(
			clk       : in  std_logic;
			clear     : in  std_logic;
			-- Weight and data input
			weight_in : in  std_logic_vector(NBIN-1 downto 0);
			data_in   : in  std_logic_vector(NBIN-1 downto 0);
			-- Data output
			data_out  : out std_logic_vector(WOUT-1 downto 0);
			-- Tag, input and output
			tag_in    : in  std_logic_vector(TAGW-1 downto 0);
			tag_out   : out std_logic_vector(TAGW-1 downto 0)
		);
	end component;

begin

	-- Test different sizes
	gen_wd_data : for NBIN in 1 to MAX_NBIN generate

		constant WOUT  : natural := 4;
		constant TAGW  : natural := 1;
		constant TAGEN : boolean := false;
		constant TAGZC : boolean := false;
		constant REGEN  : natural := 0;  -- 0=none, 1=all, else every EN stages
		constant REGIDX : natural := 0;  -- Start index (from the leaves)

		signal clk      : std_logic := '0';
		signal clk_next : std_logic := '0';
		signal clear    : std_logic := '1';
		signal clk_want_stop : boolean := false;

		signal tag_in  : std_logic_vector(TAGW-1 downto 0) := (others => '0');
		signal tag_out : std_logic_vector(TAGW-1 downto 0) := (others => '0');

		-- for construction of a unique vector, feeding both weight_in & data_in inputs
		constant BITS_IN : natural := 2*NBIN;
		signal in_bits : std_logic_vector(BITS_IN-1 downto 0) := (others => '0');

		signal din      : std_logic_vector(NBIN-1 downto 0) := (others => '0');
		signal win      : std_logic_vector(NBIN-1 downto 0) := (others => '0');

		signal data_and_out : std_logic_vector(WOUT-1 downto 0) := (others => '0');
		signal data_xnor_out : std_logic_vector(WOUT-1 downto 0) := (others => '0');

	begin

		madd_and : muladdtree_bin
			generic map (
				NBIN   => NBIN,
				WOUT   => WOUT,
				IS_XNOR => false,
				TAGW   => TAGW,
				TAGEN  => TAGEN,
				TAGZC  => TAGZC,
				REGEN  => REGEN,
				REGIDX => REGIDX
			)
			port map (
				clk       => clk,
				clear     => clear,
				weight_in => win,
				data_in   => din,
				data_out  => data_and_out,
				tag_in    => tag_in,
				tag_out   => tag_out
			);

		madd_xnor : muladdtree_bin
			generic map (
				NBIN   => NBIN,
				WOUT   => WOUT,
				IS_XNOR => true,
				TAGW   => TAGW,
				TAGEN  => TAGEN,
				TAGZC  => TAGZC,
				REGEN  => REGEN,
				REGIDX => REGIDX
			)
			port map (
				clk       => clk,
				clear     => clear,
				weight_in => win,
				data_in   => din,
				data_out  => data_xnor_out,
				tag_in    => tag_in,
				tag_out   => tag_out
			);

		clk_next <= not clk when clk_want_stop = false else clk;
		clk <= clk_next after 5 ns;

		-- Process that generates stimuli
		process

			variable have_and_out  : unsigned(WOUT-1 downto 0) := (others => '0');
			variable want_and_out  : unsigned(WOUT-1 downto 0) := (others => '0');
			variable have_xnor_out : unsigned(WOUT-1 downto 0) := (others => '0');
			variable want_xnor_out : unsigned(WOUT-1 downto 0) := (others => '0');

			variable var_errors_nb : integer := 0;
			variable var_tests_nb  : integer := 0;

		begin

			report "Beginning simulation with NBIN = " & natural'image(NBIN) & ", based on AND & XNOR operations..." ;

			wait until rising_edge(clk);

			for i in 0 to 2**BITS_IN-1 loop

				in_bits <= std_logic_vector(to_unsigned(i, BITS_IN));

				wait until rising_edge(clk);

				-- Generate inputs

				din <= in_bits(NBIN-1 downto 0);
				win <= in_bits(2*NBIN-1 downto NBIN);

				wait until falling_edge(clk);
				wait until falling_edge(clk);

				-- Compute outputs

				have_and_out  := unsigned(data_and_out);
				have_xnor_out := unsigned(data_xnor_out);

				want_and_out  := (others => '0');
				want_xnor_out := (others => '0');

				for p in 0 to NBIN-1 loop
					want_and_out  := want_and_out  + unsigned(din(p downto p)  and win(p));
					want_xnor_out := want_xnor_out + unsigned(din(p downto p) xnor win(p));
				end loop;

				-- Compare real & expected outputs

				if (have_and_out /= want_and_out) then
					report
						"ERROR Size NBIN=" & natural'image(NBIN) &
						" : Inputs data=" & natural'image(to_integer(unsigned(din))) & " weight=" & natural'image(to_integer(unsigned(win))) &
						" : AND results : have " & natural'image(to_integer(have_and_out)) & " want " & natural'image(to_integer(want_and_out));
					var_errors_nb := var_errors_nb + 1;
				end if;

				if (have_xnor_out /= want_xnor_out) then
					report
						"ERROR Size NBIN=" & natural'image(NBIN) &
						" : Inputs data=" & natural'image(to_integer(unsigned(din))) & " weight=" & natural'image(to_integer(unsigned(win))) &
						" : XNOR results : have " & natural'image(to_integer(have_xnor_out)) & " want " & natural'image(to_integer(want_xnor_out));
					var_errors_nb := var_errors_nb + 1;
				end if;

				var_tests_nb := var_tests_nb + 1;

			end loop;

			report "Size NBIN=" & natural'image(NBIN) & " : Results : " & natural'image(var_tests_nb) & " test vectors, " & natural'image(var_errors_nb) & " errors";

			-- End of simulation
			clk_want_stop <= true;
			wait;

		end process;

	end generate;

end architecture;

