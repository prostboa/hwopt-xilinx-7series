-- Author : Adrien Prost-Boucle <adrien.prost-boucle@laposte.net>
-- This file is distributed under the GPLv3 license
-- License information : https://www.gnu.org/licenses/gpl-3.0.txt

-- This package provides functionality for a few Xilinx unisim components
-- It is meant to be compiled in library "unisim" to simulate and synthesize outside Vivado

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package vcomponents is

	component LUT6_2 is
		generic (
			INIT : bit_vector
		);
		port (
			O6 : out std_logic;
			O5 : out std_logic;
			I0 : in  std_logic;
			I1 : in  std_logic;
			I2 : in  std_logic;
			I3 : in  std_logic;
			I4 : in  std_logic;
			I5 : in  std_logic
		);
	end component;

	component LUT6 is
		generic (
			INIT : bit_vector
		);
		port (
			O  : out std_logic;
			I0 : in  std_logic;
			I1 : in  std_logic;
			I2 : in  std_logic;
			I3 : in  std_logic;
			I4 : in  std_logic;
			I5 : in  std_logic
		);
	end component;

	component LUT5 is
		generic (
			INIT : bit_vector
		);
		port (
			O  : out std_logic;
			I0 : in  std_logic;
			I1 : in  std_logic;
			I2 : in  std_logic;
			I3 : in  std_logic;
			I4 : in  std_logic
		);
	end component;

	component CARRY4 is
		port (
			CO     : out std_logic_vector(3 downto 0);
			O      : out std_logic_vector(3 downto 0);
			DI     : in  std_logic_vector(3 downto 0);
			S      : in  std_logic_vector(3 downto 0);
			CI     : in  std_logic;
			CYINIT : in  std_logic
		);
	end component;

	component RAMB18E1 is
		generic (
			-- Address Collision Mode: "PERFORMANCE" or "DELAYED_WRITE"
			RDADDR_COLLISION_HWCONFIG : string := "PERFORMANCE";
			-- Collision check: Values ("ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE")
			SIM_COLLISION_CHECK : string := "ALL";
			-- Simulation Device: Must be set to "7SERIES" for simulation behavior
			SIM_DEVICE :string := "7SERIES";
			-- DOA_REG, DOB_REG: Optional output register (0 or 1)
			DOA_REG : integer := 0;
			DOB_REG : integer := 0;
			-- INITP_00 to INITP_07: Initial contents of parity memory array
			INITP_00 : bit_vector(255 downto 0) := (others => '0');
			INITP_01 : bit_vector(255 downto 0) := (others => '0');
			INITP_02 : bit_vector(255 downto 0) := (others => '0');
			INITP_03 : bit_vector(255 downto 0) := (others => '0');
			INITP_04 : bit_vector(255 downto 0) := (others => '0');
			INITP_05 : bit_vector(255 downto 0) := (others => '0');
			INITP_06 : bit_vector(255 downto 0) := (others => '0');
			INITP_07 : bit_vector(255 downto 0) := (others => '0');
			-- INIT_00 to INIT_3F: Initial contents of data memory array
			INIT_00 : bit_vector(255 downto 0) := (others => '0');
			INIT_01 : bit_vector(255 downto 0) := (others => '0');
			INIT_02 : bit_vector(255 downto 0) := (others => '0');
			INIT_03 : bit_vector(255 downto 0) := (others => '0');
			INIT_04 : bit_vector(255 downto 0) := (others => '0');
			INIT_05 : bit_vector(255 downto 0) := (others => '0');
			INIT_06 : bit_vector(255 downto 0) := (others => '0');
			INIT_07 : bit_vector(255 downto 0) := (others => '0');
			INIT_08 : bit_vector(255 downto 0) := (others => '0');
			INIT_09 : bit_vector(255 downto 0) := (others => '0');
			INIT_0A : bit_vector(255 downto 0) := (others => '0');
			INIT_0B : bit_vector(255 downto 0) := (others => '0');
			INIT_0C : bit_vector(255 downto 0) := (others => '0');
			INIT_0D : bit_vector(255 downto 0) := (others => '0');
			INIT_0E : bit_vector(255 downto 0) := (others => '0');
			INIT_0F : bit_vector(255 downto 0) := (others => '0');
			INIT_10 : bit_vector(255 downto 0) := (others => '0');
			INIT_11 : bit_vector(255 downto 0) := (others => '0');
			INIT_12 : bit_vector(255 downto 0) := (others => '0');
			INIT_13 : bit_vector(255 downto 0) := (others => '0');
			INIT_14 : bit_vector(255 downto 0) := (others => '0');
			INIT_15 : bit_vector(255 downto 0) := (others => '0');
			INIT_16 : bit_vector(255 downto 0) := (others => '0');
			INIT_17 : bit_vector(255 downto 0) := (others => '0');
			INIT_18 : bit_vector(255 downto 0) := (others => '0');
			INIT_19 : bit_vector(255 downto 0) := (others => '0');
			INIT_1A : bit_vector(255 downto 0) := (others => '0');
			INIT_1B : bit_vector(255 downto 0) := (others => '0');
			INIT_1C : bit_vector(255 downto 0) := (others => '0');
			INIT_1D : bit_vector(255 downto 0) := (others => '0');
			INIT_1E : bit_vector(255 downto 0) := (others => '0');
			INIT_1F : bit_vector(255 downto 0) := (others => '0');
			INIT_20 : bit_vector(255 downto 0) := (others => '0');
			INIT_21 : bit_vector(255 downto 0) := (others => '0');
			INIT_22 : bit_vector(255 downto 0) := (others => '0');
			INIT_23 : bit_vector(255 downto 0) := (others => '0');
			INIT_24 : bit_vector(255 downto 0) := (others => '0');
			INIT_25 : bit_vector(255 downto 0) := (others => '0');
			INIT_26 : bit_vector(255 downto 0) := (others => '0');
			INIT_27 : bit_vector(255 downto 0) := (others => '0');
			INIT_28 : bit_vector(255 downto 0) := (others => '0');
			INIT_29 : bit_vector(255 downto 0) := (others => '0');
			INIT_2A : bit_vector(255 downto 0) := (others => '0');
			INIT_2B : bit_vector(255 downto 0) := (others => '0');
			INIT_2C : bit_vector(255 downto 0) := (others => '0');
			INIT_2D : bit_vector(255 downto 0) := (others => '0');
			INIT_2E : bit_vector(255 downto 0) := (others => '0');
			INIT_2F : bit_vector(255 downto 0) := (others => '0');
			INIT_30 : bit_vector(255 downto 0) := (others => '0');
			INIT_31 : bit_vector(255 downto 0) := (others => '0');
			INIT_32 : bit_vector(255 downto 0) := (others => '0');
			INIT_33 : bit_vector(255 downto 0) := (others => '0');
			INIT_34 : bit_vector(255 downto 0) := (others => '0');
			INIT_35 : bit_vector(255 downto 0) := (others => '0');
			INIT_36 : bit_vector(255 downto 0) := (others => '0');
			INIT_37 : bit_vector(255 downto 0) := (others => '0');
			INIT_38 : bit_vector(255 downto 0) := (others => '0');
			INIT_39 : bit_vector(255 downto 0) := (others => '0');
			INIT_3A : bit_vector(255 downto 0) := (others => '0');
			INIT_3B : bit_vector(255 downto 0) := (others => '0');
			INIT_3C : bit_vector(255 downto 0) := (others => '0');
			INIT_3D : bit_vector(255 downto 0) := (others => '0');
			INIT_3E : bit_vector(255 downto 0) := (others => '0');
			INIT_3F : bit_vector(255 downto 0) := (others => '0');
			-- INIT_A, INIT_B: Initial values on output ports
			INIT_A : bit_vector(17 downto 0) := (others => '0');
			INIT_B : bit_vector(17 downto 0) := (others => '0');
			-- Initialization File: RAM initialization file
			INIT_FILE : string := "NONE";
			-- RAM Mode: "SDP" or "TDP"
			RAM_MODE : string := "SDP";
			-- READ_WIDTH_A/B, WRITE_WIDTH_A/B: Read/write width per port
			READ_WIDTH_A  : integer := 36;  -- 0-36
			READ_WIDTH_B  : integer := 18;  -- 0-18
			WRITE_WIDTH_A : integer := 18;  -- 0-18
			WRITE_WIDTH_B : integer := 36;  -- 0-36
			-- RSTREG_PRIORITY_A, RSTREG_PRIORITY_B: Reset or enable priority ("RSTREG" or "REGCE")
			RSTREG_PRIORITY_A : string := "RSTREG";
			RSTREG_PRIORITY_B : string := "RSTREG";
			-- SRVAL_A, SRVAL_B: Set/reset value for output
			SRVAL_A : bit_vector(17 downto 0) := (others => '0');
			SRVAL_B : bit_vector(17 downto 0) := (others => '0');
			-- Write Mode: Value on output upon a write ("WRITE_FIRST", "READ_FIRST", or "NO_CHANGE")
			WRITE_MODE_A : string := "WRITE_FIRST";
			WRITE_MODE_B : string := "WRITE_FIRST"
		);
		port (
			-- Port A Data: 16-bit (each) output: Port A data
			DOADO         : out std_logic_vector(15 downto 0); -- 16-bit output: A port data/LSB data
			DOPADOP       : out std_logic_vector(1 downto 0);  -- 2-bit output: A port parity/LSB parity
			-- Port B Data: 16-bit (each) output: Port B data
			DOBDO         : out std_logic_vector(15 downto 0); -- 16-bit output: B port data/MSB data
			DOPBDOP       : out std_logic_vector(1 downto 0);  -- 2-bit output: B port parity/MSB parity
			-- Port A Address/Control Signals: 14-bit (each) input: Port A address and control signals (read port when RAM_MODE="SDP")
			ADDRARDADDR   : in std_logic_vector(13 downto 0);  -- 14-bit input: A port address/Read address
			CLKARDCLK     : in std_logic;                      -- 1-bit input: A port clock/Read clock
			ENARDEN       : in std_logic;                      -- 1-bit input: A port enable/Read enable
			REGCEAREGCE   : in std_logic;                      -- 1-bit input: A port register enable/Register enable
			RSTRAMARSTRAM : in std_logic;                      -- 1-bit input: A port set/reset
			RSTREGARSTREG : in std_logic;                      -- 1-bit input: A port register set/reset
			WEA           : in std_logic_vector(1 downto 0);   -- 2-bit input: A port write enable
			-- Port A Data: 16-bit (each) input: Port A data
			DIADI         : in std_logic_vector(15 downto 0);  -- 16-bit input: A port data/LSB data
			DIPADIP       : in std_logic_vector(1 downto 0);   -- 2-bit input: A port parity/LSB parity
			-- Port B Address/Control Signals: 14-bit (each) input: Port B address and control signals (write port when RAM_MODE="SDP")
			ADDRBWRADDR   : in std_logic_vector(13 downto 0);  -- 14-bit input: B port address/Write address
			CLKBWRCLK     : in std_logic;                      -- 1-bit input: B port clock/Write clock
			ENBWREN       : in std_logic;                      -- 1-bit input: B port enable/Write enable
			REGCEB        : in std_logic;                      -- 1-bit input: B port register enable
			RSTRAMB       : in std_logic;                      -- 1-bit input: B port set/reset
			RSTREGB       : in std_logic;                      -- 1-bit input: B port register set/reset
			WEBWE         : in std_logic_vector(3 downto 0);   -- 4-bit input: B port write enable/Write enable
			-- Port B Data: 16-bit (each) input: Port B data
			DIBDI         : in std_logic_vector(15 downto 0);  -- 16-bit input: B port data/MSB data
			DIPBDIP       : in std_logic_vector(1 downto 0)    -- 2-bit input: B port parity/MSB parity
		);
	end component;

	component RAMB36E1 is
		generic (
			-- Address Collision Mode: "PERFORMANCE" or "DELAYED_WRITE"
			RDADDR_COLLISION_HWCONFIG : string := "PERFORMANCE";
			-- Collision check: Values ("ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE")
			SIM_COLLISION_CHECK : string := "NONE";
			-- Simulation Device: Must be set to "7SERIES" for simulation behavior
			SIM_DEVICE : string := "7SERIES";
			-- DOA_REG, DOB_REG: Optional output register (0 or 1)
			DOA_REG : integer := 0;
			DOB_REG : integer := 0;
			-- Enable ECC encoder: FALSE, TRUE
			EN_ECC_READ  : boolean := false;
			EN_ECC_WRITE : boolean := false;
			-- INITP_00 to INITP_0F: Initial contents of the parity memory array
			INITP_00 : bit_vector(255 downto 0) := (others => '0');
			INITP_01 : bit_vector(255 downto 0) := (others => '0');
			INITP_02 : bit_vector(255 downto 0) := (others => '0');
			INITP_03 : bit_vector(255 downto 0) := (others => '0');
			INITP_04 : bit_vector(255 downto 0) := (others => '0');
			INITP_05 : bit_vector(255 downto 0) := (others => '0');
			INITP_06 : bit_vector(255 downto 0) := (others => '0');
			INITP_07 : bit_vector(255 downto 0) := (others => '0');
			INITP_08 : bit_vector(255 downto 0) := (others => '0');
			INITP_09 : bit_vector(255 downto 0) := (others => '0');
			INITP_0A : bit_vector(255 downto 0) := (others => '0');
			INITP_0B : bit_vector(255 downto 0) := (others => '0');
			INITP_0C : bit_vector(255 downto 0) := (others => '0');
			INITP_0D : bit_vector(255 downto 0) := (others => '0');
			INITP_0E : bit_vector(255 downto 0) := (others => '0');
			INITP_0F : bit_vector(255 downto 0) := (others => '0');
			-- INIT_00 to INIT_7F: Initial contents of the data memory array
			INIT_00 : bit_vector(255 downto 0) := (others => '0');
			INIT_01 : bit_vector(255 downto 0) := (others => '0');
			INIT_02 : bit_vector(255 downto 0) := (others => '0');
			INIT_03 : bit_vector(255 downto 0) := (others => '0');
			INIT_04 : bit_vector(255 downto 0) := (others => '0');
			INIT_05 : bit_vector(255 downto 0) := (others => '0');
			INIT_06 : bit_vector(255 downto 0) := (others => '0');
			INIT_07 : bit_vector(255 downto 0) := (others => '0');
			INIT_08 : bit_vector(255 downto 0) := (others => '0');
			INIT_09 : bit_vector(255 downto 0) := (others => '0');
			INIT_0A : bit_vector(255 downto 0) := (others => '0');
			INIT_0B : bit_vector(255 downto 0) := (others => '0');
			INIT_0C : bit_vector(255 downto 0) := (others => '0');
			INIT_0D : bit_vector(255 downto 0) := (others => '0');
			INIT_0E : bit_vector(255 downto 0) := (others => '0');
			INIT_0F : bit_vector(255 downto 0) := (others => '0');
			INIT_10 : bit_vector(255 downto 0) := (others => '0');
			INIT_11 : bit_vector(255 downto 0) := (others => '0');
			INIT_12 : bit_vector(255 downto 0) := (others => '0');
			INIT_13 : bit_vector(255 downto 0) := (others => '0');
			INIT_14 : bit_vector(255 downto 0) := (others => '0');
			INIT_15 : bit_vector(255 downto 0) := (others => '0');
			INIT_16 : bit_vector(255 downto 0) := (others => '0');
			INIT_17 : bit_vector(255 downto 0) := (others => '0');
			INIT_18 : bit_vector(255 downto 0) := (others => '0');
			INIT_19 : bit_vector(255 downto 0) := (others => '0');
			INIT_1A : bit_vector(255 downto 0) := (others => '0');
			INIT_1B : bit_vector(255 downto 0) := (others => '0');
			INIT_1C : bit_vector(255 downto 0) := (others => '0');
			INIT_1D : bit_vector(255 downto 0) := (others => '0');
			INIT_1E : bit_vector(255 downto 0) := (others => '0');
			INIT_1F : bit_vector(255 downto 0) := (others => '0');
			INIT_20 : bit_vector(255 downto 0) := (others => '0');
			INIT_21 : bit_vector(255 downto 0) := (others => '0');
			INIT_22 : bit_vector(255 downto 0) := (others => '0');
			INIT_23 : bit_vector(255 downto 0) := (others => '0');
			INIT_24 : bit_vector(255 downto 0) := (others => '0');
			INIT_25 : bit_vector(255 downto 0) := (others => '0');
			INIT_26 : bit_vector(255 downto 0) := (others => '0');
			INIT_27 : bit_vector(255 downto 0) := (others => '0');
			INIT_28 : bit_vector(255 downto 0) := (others => '0');
			INIT_29 : bit_vector(255 downto 0) := (others => '0');
			INIT_2A : bit_vector(255 downto 0) := (others => '0');
			INIT_2B : bit_vector(255 downto 0) := (others => '0');
			INIT_2C : bit_vector(255 downto 0) := (others => '0');
			INIT_2D : bit_vector(255 downto 0) := (others => '0');
			INIT_2E : bit_vector(255 downto 0) := (others => '0');
			INIT_2F : bit_vector(255 downto 0) := (others => '0');
			INIT_30 : bit_vector(255 downto 0) := (others => '0');
			INIT_31 : bit_vector(255 downto 0) := (others => '0');
			INIT_32 : bit_vector(255 downto 0) := (others => '0');
			INIT_33 : bit_vector(255 downto 0) := (others => '0');
			INIT_34 : bit_vector(255 downto 0) := (others => '0');
			INIT_35 : bit_vector(255 downto 0) := (others => '0');
			INIT_36 : bit_vector(255 downto 0) := (others => '0');
			INIT_37 : bit_vector(255 downto 0) := (others => '0');
			INIT_38 : bit_vector(255 downto 0) := (others => '0');
			INIT_39 : bit_vector(255 downto 0) := (others => '0');
			INIT_3A : bit_vector(255 downto 0) := (others => '0');
			INIT_3B : bit_vector(255 downto 0) := (others => '0');
			INIT_3C : bit_vector(255 downto 0) := (others => '0');
			INIT_3D : bit_vector(255 downto 0) := (others => '0');
			INIT_3E : bit_vector(255 downto 0) := (others => '0');
			INIT_3F : bit_vector(255 downto 0) := (others => '0');
			INIT_40 : bit_vector(255 downto 0) := (others => '0');
			INIT_41 : bit_vector(255 downto 0) := (others => '0');
			INIT_42 : bit_vector(255 downto 0) := (others => '0');
			INIT_43 : bit_vector(255 downto 0) := (others => '0');
			INIT_44 : bit_vector(255 downto 0) := (others => '0');
			INIT_45 : bit_vector(255 downto 0) := (others => '0');
			INIT_46 : bit_vector(255 downto 0) := (others => '0');
			INIT_47 : bit_vector(255 downto 0) := (others => '0');
			INIT_48 : bit_vector(255 downto 0) := (others => '0');
			INIT_49 : bit_vector(255 downto 0) := (others => '0');
			INIT_4A : bit_vector(255 downto 0) := (others => '0');
			INIT_4B : bit_vector(255 downto 0) := (others => '0');
			INIT_4C : bit_vector(255 downto 0) := (others => '0');
			INIT_4D : bit_vector(255 downto 0) := (others => '0');
			INIT_4E : bit_vector(255 downto 0) := (others => '0');
			INIT_4F : bit_vector(255 downto 0) := (others => '0');
			INIT_50 : bit_vector(255 downto 0) := (others => '0');
			INIT_51 : bit_vector(255 downto 0) := (others => '0');
			INIT_52 : bit_vector(255 downto 0) := (others => '0');
			INIT_53 : bit_vector(255 downto 0) := (others => '0');
			INIT_54 : bit_vector(255 downto 0) := (others => '0');
			INIT_55 : bit_vector(255 downto 0) := (others => '0');
			INIT_56 : bit_vector(255 downto 0) := (others => '0');
			INIT_57 : bit_vector(255 downto 0) := (others => '0');
			INIT_58 : bit_vector(255 downto 0) := (others => '0');
			INIT_59 : bit_vector(255 downto 0) := (others => '0');
			INIT_5A : bit_vector(255 downto 0) := (others => '0');
			INIT_5B : bit_vector(255 downto 0) := (others => '0');
			INIT_5C : bit_vector(255 downto 0) := (others => '0');
			INIT_5D : bit_vector(255 downto 0) := (others => '0');
			INIT_5E : bit_vector(255 downto 0) := (others => '0');
			INIT_5F : bit_vector(255 downto 0) := (others => '0');
			INIT_60 : bit_vector(255 downto 0) := (others => '0');
			INIT_61 : bit_vector(255 downto 0) := (others => '0');
			INIT_62 : bit_vector(255 downto 0) := (others => '0');
			INIT_63 : bit_vector(255 downto 0) := (others => '0');
			INIT_64 : bit_vector(255 downto 0) := (others => '0');
			INIT_65 : bit_vector(255 downto 0) := (others => '0');
			INIT_66 : bit_vector(255 downto 0) := (others => '0');
			INIT_67 : bit_vector(255 downto 0) := (others => '0');
			INIT_68 : bit_vector(255 downto 0) := (others => '0');
			INIT_69 : bit_vector(255 downto 0) := (others => '0');
			INIT_6A : bit_vector(255 downto 0) := (others => '0');
			INIT_6B : bit_vector(255 downto 0) := (others => '0');
			INIT_6C : bit_vector(255 downto 0) := (others => '0');
			INIT_6D : bit_vector(255 downto 0) := (others => '0');
			INIT_6E : bit_vector(255 downto 0) := (others => '0');
			INIT_6F : bit_vector(255 downto 0) := (others => '0');
			INIT_70 : bit_vector(255 downto 0) := (others => '0');
			INIT_71 : bit_vector(255 downto 0) := (others => '0');
			INIT_72 : bit_vector(255 downto 0) := (others => '0');
			INIT_73 : bit_vector(255 downto 0) := (others => '0');
			INIT_74 : bit_vector(255 downto 0) := (others => '0');
			INIT_75 : bit_vector(255 downto 0) := (others => '0');
			INIT_76 : bit_vector(255 downto 0) := (others => '0');
			INIT_77 : bit_vector(255 downto 0) := (others => '0');
			INIT_78 : bit_vector(255 downto 0) := (others => '0');
			INIT_79 : bit_vector(255 downto 0) := (others => '0');
			INIT_7A : bit_vector(255 downto 0) := (others => '0');
			INIT_7B : bit_vector(255 downto 0) := (others => '0');
			INIT_7C : bit_vector(255 downto 0) := (others => '0');
			INIT_7D : bit_vector(255 downto 0) := (others => '0');
			INIT_7E : bit_vector(255 downto 0) := (others => '0');
			INIT_7F : bit_vector(255 downto 0) := (others => '0');
			-- INIT_A, INIT_B: Initial values on output ports
			INIT_A : bit_vector(35 downto 0) := (others => '0');
			INIT_B : bit_vector(35 downto 0) := (others => '0');
			-- Initialization File: RAM initialization file
			INIT_FILE : string := "NONE";
			-- RAM Mode: "SDP" or "TDP"
			RAM_MODE : string := "SDP";
			-- RAM_EXTENSION_A, RAM_EXTENSION_B: Selects cascade mode ("UPPER", "LOWER", or "NONE")
			RAM_EXTENSION_A : string := "NONE";
			RAM_EXTENSION_B : string := "NONE";
			-- READ_WIDTH_A/B, WRITE_WIDTH_A/B: Read/write width per port
			READ_WIDTH_A  : integer := 72;  -- 0-72
			READ_WIDTH_B  : integer := 36;  -- 0-36
			WRITE_WIDTH_A : integer := 36;  -- 0-36
			WRITE_WIDTH_B : integer := 72;  -- 0-72
			-- RSTREG_PRIORITY_A, RSTREG_PRIORITY_B: Reset or enable priority ("RSTREG" or "REGCE")
			RSTREG_PRIORITY_A : string := "RSTREG";
			RSTREG_PRIORITY_B : string := "RSTREG";
			-- SRVAL_A, SRVAL_B: Set/reset value for output
			SRVAL_A : bit_vector(35 downto 0) := (others => '0');
			SRVAL_B : bit_vector(35 downto 0) := (others => '0');
			-- Write Mode: Value on output upon a write ("WRITE_FIRST", "READ_FIRST", or "NO_CHANGE")
			WRITE_MODE_A : string := "WRITE_FIRST";
			WRITE_MODE_B : string := "WRITE_FIRST"
		);
		port (
			-- Cascade Signals: 1-bit (each) output: BRAM cascade ports (to create 64kx1)
			CASCADEOUTA   : out std_logic;                     -- 1-bit output: A port cascade
			CASCADEOUTB   : out std_logic;                     -- 1-bit output: B port cascade
			-- ECC Signals: 1-bit (each) output: Error Correction Circuitry ports
			DBITERR       : out std_logic;                     -- 1-bit output: Double bit error status
			ECCPARITY     : out std_logic_vector(7 downto 0);  -- 8-bit output: Generated error correction parity
			RDADDRECC     : out std_logic_vector(8 downto 0);  -- 9-bit output: ECC read address
			SBITERR       : out std_logic;                     -- 1-bit output: Single bit error status
			-- Port A Data: 32-bit (each) output: Port A data
			DOADO         : out std_logic_vector(31 downto 0); -- 32-bit output: A port data/LSB data
			DOPADOP       : out std_logic_vector(3 downto 0);  -- 4-bit output: A port parity/LSB parity
			-- Port B Data: 32-bit (each) output: Port B data
			DOBDO         : out std_logic_vector(31 downto 0); -- 32-bit output: B port data/MSB data
			DOPBDOP       : out std_logic_vector(3 downto 0);  -- 4-bit output: B port parity/MSB parity
			-- Cascade Signals: 1-bit (each) input: BRAM cascade ports (to create 64kx1)
			CASCADEINA    : in std_logic;                      -- 1-bit input: A port cascade
			CASCADEINB    : in std_logic;                      -- 1-bit input: B port cascade
			-- ECC Signals: 1-bit (each) input: Error Correction Circuitry ports
			INJECTDBITERR : in std_logic;                      -- 1-bit input: Inject a double bit error
			INJECTSBITERR : in std_logic;                      -- 1-bit input: Inject a single bit error
			-- Port A Address/Control Signals: 16-bit (each) input: Port A address and control signals (read port when RAM_MODE="SDP")
			ADDRARDADDR   : in std_logic_vector(15 downto 0);  -- 16-bit input: A port address/Read address
			CLKARDCLK     : in std_logic;                      -- 1-bit input: A port clock/Read clock
			ENARDEN       : in std_logic;                      -- 1-bit input: A port enable/Read enable
			REGCEAREGCE   : in std_logic;                      -- 1-bit input: A port register enable/Register enable
			RSTRAMARSTRAM : in std_logic;                      -- 1-bit input: A port set/reset
			RSTREGARSTREG : in std_logic;                      -- 1-bit input: A port register set/reset
			WEA           : in std_logic_vector(3 downto 0);   -- 4-bit input: A port write enable
			-- Port A Data: 32-bit (each) input: Port A data
			DIADI         : in std_logic_vector(31 downto 0);  -- 32-bit input: A port data/LSB data
			DIPADIP       : in std_logic_vector(3 downto 0);   -- 4-bit input: A port parity/LSB parity
			-- Port B Address/Control Signals: 16-bit (each) input: Port B address and control signals (write port when RAM_MODE="SDP")
			ADDRBWRADDR   : in std_logic_vector(15 downto 0);  -- 16-bit input: B port address/Write address
			CLKBWRCLK     : in std_logic;                      -- 1-bit input: B port clock/Write clock
			ENBWREN       : in std_logic;                      -- 1-bit input: B port enable/Write enable
			REGCEB        : in std_logic;                      -- 1-bit input: B port register enable
			RSTRAMB       : in std_logic;                      -- 1-bit input: B port set/reset
			RSTREGB       : in std_logic;                      -- 1-bit input: B port register set/reset
			WEBWE         : in std_logic_vector(7 downto 0);   -- 8-bit input: B port write enable/Write enable
			-- Port B Data: 32-bit (each) input: Port B data
			DIBDI         : in std_logic_vector(31 downto 0);  -- 32-bit input: B port data/MSB data
			DIPBDIP       : in std_logic_vector(3 downto 0)    -- 4-bit input: B port parity/MSB parity
		);
	end component;

	component DSP48E1 is
		generic (
			-- Feature Control Attributes: Data Path Selection
			A_INPUT   : string := "DIRECT";    -- Selects A input source, "DIRECT" (A port) or "CASCADE" (ACIN port)
			B_INPUT   : string := "DIRECT";    -- Selects B input source, "DIRECT" (B port) or "CASCADE" (BCIN port)
			USE_DPORT : boolean := false;      -- Select D port usage (TRUE or FALSE)
			USE_MULT  : string := "MULTIPLY";  -- Select multiplier usage ("MULTIPLY", "DYNAMIC", or "NONE")
			USE_SIMD  : string := "ONE48";     -- SIMD selection ("ONE48", "TWO24", "FOUR12")
			-- Pattern Detector Attributes: Pattern Detection Configuration
			AUTORESET_PATDET : string := "NO_RESET";              -- "NO_RESET", "RESET_MATCH", "RESET_NOT_MATCH"
			MASK    : bit_vector(47 downto 0) := (others => '1'); -- 48-bit mask value for pattern detect (1=ignore)
			PATTERN : bit_vector(47 downto 0) := (others => '0'); -- 48-bit pattern match for pattern detect
			SEL_MASK : string := "MASK";                          -- "C", "MASK", "ROUNDING_MODE1", "ROUNDING_MODE2"
			SEL_PATTERN : string := "PATTERN";                    -- Select pattern value ("PATTERN" or "C")
			USE_PATTERN_DETECT : string := "NO_PATDET";           -- Enable pattern detect ("PATDET" or "NO_PATDET")
			-- Register Control Attributes: Pipeline Register Configuration
			ACASCREG      : integer := 0;      -- Number of pipeline stages between A/ACIN and ACOUT (0, 1 or 2)
			ADREG         : integer := 0;      -- Number of pipeline stages for pre-adder (0 or 1)
			ALUMODEREG    : integer := 0;      -- Number of pipeline stages for ALUMODE (0 or 1)
			AREG          : integer := 0;      -- Number of pipeline stages for A (0, 1 or 2)
			BCASCREG      : integer := 0;      -- Number of pipeline stages between B/BCIN and BCOUT (0, 1 or 2)
			BREG          : integer := 0;      -- Number of pipeline stages for B (0, 1 or 2)
			CARRYINREG    : integer := 0;      -- Number of pipeline stages for CARRYIN (0 or 1)
			CARRYINSELREG : integer := 0;      -- Number of pipeline stages for CARRYINSEL (0 or 1)
			CREG          : integer := 0;      -- Number of pipeline stages for C (0 or 1)
			DREG          : integer := 0;      -- Number of pipeline stages for D (0 or 1)
			INMODEREG     : integer := 0;      -- Number of pipeline stages for INMODE (0 or 1)
			MREG          : integer := 0;      -- Number of multiplier pipeline stages (0 or 1)
			OPMODEREG     : integer := 0;      -- Number of pipeline stages for OPMODE (0 or 1)
			PREG          : integer := 0       -- Number of pipeline stages for P (0 or 1)
		);
		port (
			-- Cascade: 30-bit (each) output: Cascade Ports
			ACOUT        : out std_logic_vector(29 downto 0);  -- 30-bit output: A port cascade output
			BCOUT        : out std_logic_vector(17 downto 0);  -- 18-bit output: B port cascade output
			CARRYCASCOUT : out std_logic ;                     -- 1-bit output: Cascade carry output
			MULTSIGNOUT  : out std_logic ;                     -- 1-bit output: Multiplier sign cascade output
			PCOUT        : out std_logic_vector(47 downto 0);  -- 48-bit output: Cascade output
			-- Control: 1-bit (each) output: Control Inputs/Status Bits
			OVERFLOW       : out std_logic;                    -- 1-bit output: Overflow in add/acc output
			PATTERNBDETECT : out std_logic;                    -- 1-bit output: Pattern bar detect output
			PATTERNDETECT  : out std_logic;                    -- 1-bit output: Pattern detect output
			UNDERFLOW      : out std_logic;                    -- 1-bit output: Underflow in add/acc output
			-- Data: 4-bit (each) output: Data Ports
			CARRYOUT      : out std_logic_vector(3 downto 0);  -- 4-bit output: Carry output
			P             : out std_logic_vector(47 downto 0); -- 48-bit output: Primary data output
			-- Cascade: 30-bit (each) input: Cascade Ports
			ACIN          : in std_logic_vector(29 downto 0);  -- 30-bit input: A cascade data input
			BCIN          : in std_logic_vector(17 downto 0);  -- 18-bit input: B cascade input
			CARRYCASCIN   : in std_logic;                      -- 1-bit input: Cascade carry input
			MULTSIGNIN    : in std_logic;                      -- 1-bit input: Multiplier sign input
			PCIN          : in std_logic_vector(47 downto 0);  -- 48-bit input: P cascade input
			-- Clock
			CLK           : in std_logic;                      -- 1-bit input: Clock input
			-- Control: 4-bit (each) input: Control Inputs/Status Bits
			ALUMODE       : in std_logic_vector(3 downto 0);   -- 4-bit input: ALU control input
			CARRYINSEL    : in std_logic_vector(2 downto 0);   -- 3-bit input: Carry select input
			INMODE        : in std_logic_vector(4 downto 0);   -- 5-bit input: INMODE control input
			OPMODE        : in std_logic_vector(6 downto 0);   -- 7-bit input: Operation mode input
			-- Data: 30-bit (each) input: Data Ports
			A             : in std_logic_vector(29 downto 0);  -- 30-bit input: A data input
			B             : in std_logic_vector(17 downto 0);  -- 18-bit input: B data input
			C             : in std_logic_vector(47 downto 0);  -- 48-bit input: C data input
			CARRYIN       : in std_logic;                      -- 1-bit input: Carry input signal
			D             : in std_logic_vector(24 downto 0);  -- 25-bit input: D data input
			-- Reset/Clock Enable: 1-bit (each) input: Reset/Clock Enable Inputs
			CEA1          : in std_logic;  -- 1-bit input: Clock enable input for 1st stage AREG
			CEA2          : in std_logic;  -- 1-bit input: Clock enable input for 2nd stage AREG
			CEAD          : in std_logic;  -- 1-bit input: Clock enable input for ADREG
			CEALUMODE     : in std_logic;  -- 1-bit input: Clock enable input for ALUMODE
			CEB1          : in std_logic;  -- 1-bit input: Clock enable input for 1st stage BREG
			CEB2          : in std_logic;  -- 1-bit input: Clock enable input for 2nd stage BREG
			CEC           : in std_logic;  -- 1-bit input: Clock enable input for CREG
			CECARRYIN     : in std_logic;  -- 1-bit input: Clock enable input for CARRYINREG
			CECTRL        : in std_logic;  -- 1-bit input: Clock enable input for OPMODEREG and CARRYINSELREG
			CED           : in std_logic;  -- 1-bit input: Clock enable input for DREG
			CEINMODE      : in std_logic;  -- 1-bit input: Clock enable input for INMODEREG
			CEM           : in std_logic;  -- 1-bit input: Clock enable input for MREG
			CEP           : in std_logic;  -- 1-bit input: Clock enable input for PREG
			RSTA          : in std_logic;  -- 1-bit input: Reset input for AREG
			RSTALLCARRYIN : in std_logic;  -- 1-bit input: Reset input for CARRYINREG
			RSTALUMODE    : in std_logic;  -- 1-bit input: Reset input for ALUMODEREG
			RSTB          : in std_logic;  -- 1-bit input: Reset input for BREG
			RSTC          : in std_logic;  -- 1-bit input: Reset input for CREG
			RSTCTRL       : in std_logic;  -- 1-bit input: Reset input for OPMODEREG and CARRYINSELREG
			RSTD          : in std_logic;  -- 1-bit input: Reset input for DREG and ADREG
			RSTINMODE     : in std_logic;  -- 1-bit input: Reset input for INMODEREG
			RSTM          : in std_logic;  -- 1-bit input: Reset input for MREG
			RSTP          : in std_logic   -- 1-bit input: Reset input for PREG
		);
	end component;

end package;

