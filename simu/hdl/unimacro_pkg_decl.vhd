-- Author : Adrien Prost-Boucle <adrien.prost-boucle@laposte.net>
-- This file is distributed under the GPLv3 license
-- License information : https://www.gnu.org/licenses/gpl-3.0.txt

-- This package provides functionality for a few Xilinx unimacro components
-- It is meant to be compiled in library "unimacro"

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package vcomponents is

	component BRAM_SDP_MACRO is
		generic (
			BRAM_SIZE   : string;    -- Target BRAM, 18Kb or 36Kb
			DEVICE      : string;    -- Target device: VIRTEX5, VIRTEX6, 7SERIES, SPARTAN6
			WRITE_WIDTH : integer;   -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE = 36Kb)
			READ_WIDTH  : integer;   -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE = 36Kb)
			DO_REG      : integer;   -- Optional output register (0 or 1)
			-- Set/Reset value for port output
			SRVAL       : bit_vector;
			-- Specify READ_FIRST for same clock or synchronous clocks, or WRITE_FIRST for asynchronous clocks on ports
			WRITE_MODE  : string;
			-- Initial values on output port
			INIT        : bit_vector;
			-- Initial values when stored in a file
			INIT_FILE   : string
		);
		port (
			-- Output read data port, width defined by READ_WIDTH parameter
			DO     : out std_logic_vector;
			-- Input write data port, width defined by WRITE_WIDTH parameter
			DI     : in std_logic_vector;
			-- Input read address, width defined by read port depth
			RDADDR : in std_logic_vector;
			-- 1-bit input read clock
			RDCLK  : in std_logic;
			-- 1-bit input read port enable
			RDEN   : in std_logic;
			-- 1-bit input read output register clock enable
			REGCE  : in std_logic;
			-- 1-bit input reset
			RST    : in std_logic;
			-- Input write enable, width defined by write port depth
			WE     : in std_logic_vector;
			-- Input write address, width defined by write port depth
			WRADDR : in std_logic_vector;
			-- 1-bit input write clock
			WRCLK  : in std_logic;
			-- 1-bit input write port enable
			WREN   : in std_logic
		);
	end component;

	component BRAM_SINGLE_MACRO is
		generic (
			BRAM_SIZE   : string;    -- Target BRAM, 18Kb or 36Kb
			DEVICE      : string;    -- Target device: VIRTEX5, VIRTEX6, 7SERIES, SPARTAN6
			WRITE_WIDTH : integer;   -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE = 36Kb)
			READ_WIDTH  : integer;   -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE = 36Kb)
			DO_REG      : integer;   -- Optional output register (0 or 1)
			-- Set/Reset value for port output
			SRVAL       : bit_vector;
			-- Specify READ_FIRST for same clock or synchronous clocks, or WRITE_FIRST for asynchronous clocks on ports
			WRITE_MODE  : string;
			-- Initial values on output port
			INIT        : bit_vector;
			-- Initial values when stored in a file
			INIT_FILE   : string
		);
		port (
			-- Output data port, width defined by READ_WIDTH parameter
			DO     : out std_logic_vector;
			-- Input data port, width defined by WRITE_WIDTH parameter
			DI     : in std_logic_vector;
			-- Input address, width defined by read/address port depth
			ADDR : in std_logic_vector;
			-- 1-bit input clock
			CLK  : in std_logic;
			-- 1-bit input RAM enable
			EN   : in std_logic;
			-- 1-bit input output register clock enable
			REGCE  : in std_logic;
			-- 1-bit input reset
			RST    : in std_logic;
			-- Input write enable, width defined by write port depth
			WE     : in std_logic_vector
		);
	end component;

	-- FIXME : WIDTH_P does not exist in Vivado definition
	component MULT_MACRO is
		generic (
			DEVICE  : string;   -- Target Device: "VIRTEX5", "7SERIES", "SPARTAN6"
			LATENCY : integer;  -- Desired clock cycle latency, 0-4
			--WIDTH_P : integer;  -- Multiplier output bus width, 1-48
			WIDTH_A : integer;  -- Multiplier A-input bus width, 1-25
			WIDTH_B : integer   -- Multiplier B-input bus width, 1-18
		);
		port (
			P   : out std_logic_vector;  -- Multiplier output bus, width determined by WIDTH_P generic
			A   : in  std_logic_vector;  -- Multiplier input A bus, width determined by WIDTH_A generic
			B   : in  std_logic_vector;  -- Multiplier input B bus, width determined by WIDTH_B generic
			CE  : in  std_logic;         -- 1-bit active high input clock enable
			CLK : in  std_logic;         -- 1-bit positive edge clock input
			RST : in  std_logic          -- 1-bit input active high reset
		);
	end component;

end package;

