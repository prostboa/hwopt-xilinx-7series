
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity bitheap_607_5_pair_bench is
end bitheap_607_5_pair_bench;

architecture simu of bitheap_607_5_pair_bench is

	signal clk      : std_logic := '0';
	signal clk_next : std_logic := '0';
	signal clear    : std_logic := '1';
	signal clk_want_stop : boolean := false;

	constant PAIR_WDATA : natural := 4;
	constant PAIR_WOUT  : natural := 8;
	constant PAIR_NBIN  : natural := 6;

	constant BITS_IN : natural := 12 + 1 + 1;

	signal in_bits : std_logic_vector(BITS_IN-1 downto 0) := (others => '0');

	signal di         : std_logic_vector(PAIR_NBIN*PAIR_WDATA-1 downto 0) := (others => '0');
	signal di1        : std_logic := '0';
	signal di2        : std_logic := '0';

	signal res_u_idx0  : std_logic_vector(PAIR_WOUT-1 downto 0) := (others => '0');
	signal res_u_idx1  : std_logic_vector(PAIR_WOUT-1 downto 0) := (others => '0');
	signal res_ss_idx0 : std_logic_vector(PAIR_WOUT-1 downto 0) := (others => '0');
	signal res_ss_idx1 : std_logic_vector(PAIR_WOUT-1 downto 0) := (others => '0');
	signal res_su_idx0 : std_logic_vector(PAIR_WOUT-1 downto 0) := (others => '0');
	signal res_su_idx1 : std_logic_vector(PAIR_WOUT-1 downto 0) := (others => '0');

	component bitheap_607_5_pair is
		generic (
			WDATA : natural := 8;
			WOUT  : natural := 12;
			NBIN  : natural := 6;
			SIGN  : boolean := false
		);
		port (
			data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
			data_1   : in  std_logic;
			data_2   : in  std_logic;
			sign_in  : in  std_logic;
			res_idx0 : out std_logic_vector(WOUT-1 downto 0);
			res_idx1 : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

begin

	comp_u : bitheap_607_5_pair
		generic map (
			WDATA => PAIR_WDATA,
			WOUT  => PAIR_WOUT,
			NBIN  => PAIR_NBIN,
			SIGN  => false
		)
		port map (
			data_in  => di,
			data_1   => di1,
			data_2   => di2,
			sign_in  => '0',
			res_idx0 => res_u_idx0,
			res_idx1 => res_u_idx1
		);

	comp_s : bitheap_607_5_pair
		generic map (
			WDATA => PAIR_WDATA,
			WOUT  => PAIR_WOUT,
			NBIN  => PAIR_NBIN,
			SIGN  => true
		)
		port map (
			data_in  => di,
			data_1   => di1,
			data_2   => di2,
			sign_in  => '1',
			res_idx0 => res_ss_idx0,
			res_idx1 => res_ss_idx1
		);

	comp_su : bitheap_607_5_pair
		generic map (
			WDATA => PAIR_WDATA,
			WOUT  => PAIR_WOUT,
			NBIN  => PAIR_NBIN,
			SIGN  => true
		)
		port map (
			data_in  => di,
			data_1   => di1,
			data_2   => di2,
			sign_in  => '0',
			res_idx0 => res_su_idx0,
			res_idx1 => res_su_idx1
		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	-- Process that generates stimuli
	process
		variable have_u  : unsigned(PAIR_WOUT-1 downto 0) := (others => '0');
		variable have_ss :   signed(PAIR_WOUT-1 downto 0) := (others => '0');
		variable have_su :   signed(PAIR_WOUT-1 downto 0) := (others => '0');
		variable want_u  : unsigned(PAIR_WOUT-1 downto 0) := (others => '0');
		variable want_s  :   signed(PAIR_WOUT-1 downto 0) := (others => '0');
		variable var_errors_nb : integer := 0;
		variable var_tests_nb : integer := 0;
	begin

		wait until rising_edge(clk);

		for i in 0 to 2**BITS_IN-1 loop
			in_bits <= std_logic_vector(to_unsigned(i, BITS_IN));

			wait until rising_edge(clk);

			-- Generate inputs

			di <= (others => '0');
			di1 <= '0';

			for p in 0 to 5 loop
				di(p*PAIR_WDATA + 2) <= in_bits(6*0 + p);
				di(p*PAIR_WDATA + 3) <= in_bits(6*1 + p);
			end loop;
			di1 <= in_bits(12);

			-- Make sure there is a carry sent to MSBs
			if in_bits(13) = '1' then
				for p in 0 to 5 loop
					di(p*PAIR_WDATA + 1) <= '1';
				end loop;
			end if;

			wait until falling_edge(clk);

			-- Compare with expected outputs

			want_u := (others => '0');
			want_s := (others => '0');

			for p in 0 to 5 loop
				want_u := want_u + unsigned(di((p+1)*PAIR_WDATA-1 downto p*PAIR_WDATA));
				want_s := want_s +   signed(di((p+1)*PAIR_WDATA-1 downto p*PAIR_WDATA));
			end loop;
			if di1 = '1' then
				want_u := want_u + 1;
				want_s := want_s + 1;
			end if;

			have_u  := unsigned(res_u_idx0) + unsigned(res_u_idx1);
			have_ss :=   signed(res_ss_idx0) +  signed(res_ss_idx1);
			have_su :=   signed(res_su_idx0) +  signed(res_su_idx1);

			if (have_u /= want_u) or (have_ss /= want_s) or (unsigned(have_su) /= want_u)  then
				report
					"ERROR Input " & natural'image(i) &
					" / Unsigned have " & natural'image(to_integer(have_u)) &
					" want "            & integer'image(to_integer(want_u)) &
					" / Signed Ext have " & natural'image(to_integer(have_ss)) &
					" want "            & integer'image(to_integer(want_s)) &
					" / Signed noExt have " & natural'image(to_integer(have_su)) &
					" want "            & integer'image(to_integer(want_u));
				var_errors_nb := var_errors_nb + 1;
			end if;

			var_tests_nb := var_tests_nb + 1;

		end loop;

		report "Results : " & natural'image(var_tests_nb) & " test vectors, " & natural'image(var_errors_nb) & " errors";

		-- End of simulation
		clk_want_stop <= true;
		wait;

	end process;

end architecture;


