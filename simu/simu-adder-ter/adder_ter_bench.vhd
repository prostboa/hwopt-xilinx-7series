
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity adder_ter_bench is
end adder_ter_bench;

architecture simu of adder_ter_bench is

	constant NBIN : natural := 7;

	signal clk      : std_logic := '0';
	signal clk_next : std_logic := '0';
	signal clear    : std_logic := '1';
	signal clk_want_stop : boolean := false;

	signal data_in  : std_logic_vector(NBIN*2-1 downto 0) := (others => '0');

	signal data_out2 : std_logic_vector(2 downto 0) := (others => '0');
	signal data_out3 : std_logic_vector(2 downto 0) := (others => '0');
	signal data_out4 : std_logic_vector(3 downto 0) := (others => '0');
	signal data_out5 : std_logic_vector(3 downto 0) := (others => '0');
	signal data_out6 : std_logic_vector(3 downto 0) := (others => '0');
	signal data_out7 : std_logic_vector(3 downto 0) := (others => '0');

	function is_tern_code(vec : std_logic_vector) return boolean is
	begin
		for t in 0 to NBIN-1 loop
			if vec(2*t+1 downto 2*t) = "10" then
				return false;
			end if;
		end loop;
		return true;
	end function;

	component adder_ter is
		generic(
			NBIN  : natural := 4;
			WOUT  : natural := 4
		);
		port(
			data_in  : in  std_logic_vector(2*NBIN-1 downto 0);
			data_out : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

begin

	comp2 : adder_ter
		generic map (
			NBIN  => 2,
			WOUT  => 3
		)
		port map (
			data_in  => data_in(2*2-1 downto 0),
			data_out => data_out2
		);

	comp3 : adder_ter
		generic map (
			NBIN  => 3,
			WOUT  => 3
		)
		port map (
			data_in  => data_in(3*2-1 downto 0),
			data_out => data_out3
		);

	comp4 : adder_ter
		generic map (
			NBIN  => 4,
			WOUT  => 4
		)
		port map (
			data_in  => data_in(4*2-1 downto 0),
			data_out => data_out4
		);

	comp5 : adder_ter
		generic map (
			NBIN  => 5,
			WOUT  => 4
		)
		port map (
			data_in  => data_in(5*2-1 downto 0),
			data_out => data_out5
		);

	comp6 : adder_ter
		generic map (
			NBIN  => 6,
			WOUT  => 4
		)
		port map (
			data_in  => data_in(6*2-1 downto 0),
			data_out => data_out6
		);

	comp7 : adder_ter
		generic map (
			NBIN  => 7,
			WOUT  => 4
		)
		port map (
			data_in  => data_in(7*2-1 downto 0),
			data_out => data_out7
		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	-- Process that generates stimuli
	process
		variable var_data_in : std_logic_vector(NBIN*2-1 downto 0) := (others => '0');
		variable var_trit : integer := 0;
		variable var_out2 : integer := 0;
		variable var_out3 : integer := 0;
		variable var_out4 : integer := 0;
		variable var_out5 : integer := 0;
		variable var_out6 : integer := 0;
		variable var_out7 : integer := 0;
		variable var_errors_nb : integer := 0;
		variable var_tests_nb : integer := 0;
	begin

		wait until rising_edge(clk);

		for i in 0 to (2 ** (NBIN * 2)) - 1 loop
			var_data_in := std_logic_vector(to_unsigned(i, 2*NBIN));

			if is_tern_code(var_data_in) = false then next; end if;

			data_in <= var_data_in;

			wait until falling_edge(clk);

			var_out2 := 0;
			var_out3 := 0;
			var_out4 := 0;
			var_out5 := 0;
			var_out6 := 0;
			var_out7 := 0;

			for t in 0 to NBIN-1 loop
				var_trit := to_integer(signed(var_data_in(2*t+1 downto 2*t)));
				if t < 2 then
					var_out2 := var_out2 + var_trit;
				end if;
				if t < 3 then
					var_out3 := var_out3 + var_trit;
				end if;
				if t < 4 then
					var_out4 := var_out4 + var_trit;
				end if;
				if t < 5 then
					var_out5 := var_out5 + var_trit;
				end if;
				if t < 6 then
					var_out6 := var_out6 + var_trit;
				end if;
				if t < 7 then
					var_out7 := var_out7 + var_trit;
				end if;
			end loop;

			if signed(data_out2) /= var_out2 then
				report
					"ERROR Wrong output 2T" &
					" input " & natural'image(i) &
					" got "   & natural'image(to_integer(signed(data_out2))) &
					" expected " & integer'image(var_out2);
				var_errors_nb := var_errors_nb + 1;
			end if;

			if signed(data_out3) /= var_out3 then
				report
					"ERROR Wrong output 3T" &
					" input " & natural'image(i) &
					" got "   & natural'image(to_integer(signed(data_out3))) &
					" expected " & integer'image(var_out3);
				var_errors_nb := var_errors_nb + 1;
			end if;

			if signed(data_out4) /= var_out4 then
				report
					"ERROR Wrong output 4T" &
					" input " & natural'image(i) &
					" got "   & natural'image(to_integer(signed(data_out4))) &
					" expected " & integer'image(var_out4);
				var_errors_nb := var_errors_nb + 1;
			end if;

			if signed(data_out5) /= var_out5 then
				report
					"ERROR Wrong output 5T" &
					" input " & natural'image(i) &
					" got "   & natural'image(to_integer(signed(data_out5))) &
					" expected " & integer'image(var_out5);
				var_errors_nb := var_errors_nb + 1;
			end if;

			if signed(data_out6) /= var_out6 then
				report
					"ERROR Wrong output 6T" &
					" input " & natural'image(i) &
					" got "   & natural'image(to_integer(signed(data_out6))) &
					" expected " & integer'image(var_out6);
				var_errors_nb := var_errors_nb + 1;
			end if;

			if signed(data_out7) /= var_out7 then
				report
					"ERROR Wrong output 7T" &
					" input " & natural'image(i) &
					" got "   & natural'image(to_integer(signed(data_out7))) &
					" expected " & integer'image(var_out7);
				var_errors_nb := var_errors_nb + 1;
			end if;

			var_tests_nb := var_tests_nb + 1;

		end loop;

		report "Results : " & natural'image(var_tests_nb) & " test vectors, " & natural'image(var_errors_nb) & " errors";

		-- End of simulation
		clk_want_stop <= true;
		wait;

	end process;

end architecture;


