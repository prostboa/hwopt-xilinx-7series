
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity bitheap_135_4_bench is
end bitheap_135_4_bench;

architecture simu of bitheap_135_4_bench is

	signal clk      : std_logic := '0';
	signal clk_next : std_logic := '0';
	signal clear    : std_logic := '1';
	signal clk_want_stop : boolean := false;

	constant BITS_IN : natural := 1 + 3 + 5;

	signal di0 : std_logic_vector(4 downto 0) := (others => '0');
	signal di1 : std_logic_vector(2 downto 0) := (others => '0');
	signal di2 : std_logic_vector(0 downto 0) := (others => '0');
	signal sum : std_logic_vector(3 downto 0) := (others => '0');

	component bitheap_135_4 is
		port (
			ci  : in  std_logic;
			di0 : in  std_logic_vector(4 downto 0);
			di1 : in  std_logic_vector(2 downto 0);
			di2 : in  std_logic_vector(0 downto 0);
			sum : out std_logic_vector(3 downto 0)
		);
	end component;

begin

	comp : bitheap_135_4
		port map (
			ci  => '0',
			di0 => di0,
			di1 => di1,
			di2 => di2,
			sum => sum
		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	-- Process that generates stimuli
	process
		variable in_number  : std_logic_vector(BITS_IN-1 downto 0) := (others => '0');
		variable have       : natural := 0;
		variable want       : natural := 0;
		variable var_errors_nb : integer := 0;
		variable var_tests_nb : integer := 0;
	begin

		wait until rising_edge(clk);

		for i in 0 to 2**BITS_IN-1 loop
			in_number := std_logic_vector(to_unsigned(i, BITS_IN));

			di0 <= in_number(4 downto 0);
			di1 <= in_number(7 downto 5);
			di2 <= in_number(8 downto 8);

			wait until falling_edge(clk);

			want := 0;
			for p in 0 to 4 loop
				if di0(p) = '1' then want := want + 1; end if;
			end loop;
			for p in 0 to 2 loop
				if di1(p) = '1' then want := want + 2; end if;
			end loop;
			for p in 0 to 0 loop
				if di2(p) = '1' then want := want + 4; end if;
			end loop;

			have := to_integer(unsigned(sum));

			if have /= want then
				report
					"ERROR Wrong output" &
					" input " & natural'image(i) &
					" got "   & natural'image(have) &
					" expected " & integer'image(want);
				var_errors_nb := var_errors_nb + 1;
			end if;

			var_tests_nb := var_tests_nb + 1;

		end loop;

		report "Results : " & natural'image(var_tests_nb) & " test vectors, " & natural'image(var_errors_nb) & " errors";

		-- End of simulation
		clk_want_stop <= true;
		wait;

	end process;

end architecture;


