
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity bitheap_4_2_bench is
end bitheap_4_2_bench;

architecture simu of bitheap_4_2_bench is

	signal clk      : std_logic := '0';
	signal clk_next : std_logic := '0';
	signal clear    : std_logic := '1';
	signal clk_want_stop : boolean := false;

	constant WDATA : natural := 3;
	constant WOUT  : natural := 6;
	constant NBIN  : natural := 4;

	constant BITS_IN : natural := WDATA*NBIN + 1;

	signal di      : std_logic_vector(NBIN*WDATA-1 downto 0) := (others => '0');
	signal di1     : std_logic := '0';
	signal di2     : std_logic := '0';

	signal res_uu_sum : std_logic_vector(WOUT-1 downto 0) := (others => '0');
	signal res_uu_c   : std_logic_vector(WOUT-1 downto 0) := (others => '0');
	signal res_ss_sum : std_logic_vector(WOUT-1 downto 0) := (others => '0');
	signal res_ss_c   : std_logic_vector(WOUT-1 downto 0) := (others => '0');
	signal res_su_sum : std_logic_vector(WOUT-1 downto 0) := (others => '0');
	signal res_su_c   : std_logic_vector(WOUT-1 downto 0) := (others => '0');

	component bitheap_4_2 is
		generic (
			WDATA : natural := 8;
			WOUT  : natural := 11;
			NBIN  : natural := 4;
			SIGN  : boolean := false
		);
		port (
			data_in : in  std_logic_vector(NBIN*WDATA-1 downto 0);
			data_1  : in  std_logic;
			data_2  : in  std_logic;
			sign_in : in  std_logic;
			res_sum : out std_logic_vector(WOUT-1 downto 0);
			res_c   : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

begin

	comp_u : bitheap_4_2
		generic map (
			WDATA => WDATA,
			WOUT  => WOUT,
			NBIN  => NBIN,
			SIGN  => false
		)
		port map (
			data_in => di,
			data_1  => di1,
			data_2  => di2,
			sign_in => '0',
			res_sum => res_uu_sum,
			res_c   => res_uu_c
		);

	comp_s : bitheap_4_2
		generic map (
			WDATA => WDATA,
			WOUT  => WOUT,
			NBIN  => NBIN,
			SIGN  => true
		)
		port map (
			data_in => di,
			data_1  => di1,
			data_2  => di2,
			sign_in => '1',
			res_sum => res_ss_sum,
			res_c   => res_ss_c
		);

	comp_su : bitheap_4_2
		generic map (
			WDATA => WDATA,
			WOUT  => WOUT,
			NBIN  => NBIN,
			SIGN  => true
		)
		port map (
			data_in => di,
			data_1  => di1,
			data_2  => di2,
			sign_in => '0',
			res_sum => res_su_sum,
			res_c   => res_su_c
		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	-- Process that generates stimuli
	process
		variable in_bits  : std_logic_vector(BITS_IN-1 downto 0) := (others => '0');
		variable have_uu_res : unsigned(WOUT-1 downto 0) := (others => '0');
		variable have_ss_res :   signed(WOUT-1 downto 0) := (others => '0');
		variable have_su_res :   signed(WOUT-1 downto 0) := (others => '0');
		variable want_u_res  : unsigned(WOUT-1 downto 0) := (others => '0');
		variable want_s_res  :   signed(WOUT-1 downto 0) := (others => '0');
		variable var_errors_nb : integer := 0;
		variable var_tests_nb : integer := 0;
	begin

		wait until rising_edge(clk);

		for i in 0 to 2**BITS_IN-1 loop
			in_bits := std_logic_vector(to_unsigned(i, BITS_IN));

			wait until rising_edge(clk);

			-- Generate inputs

			di  <= in_bits(BITS_IN-2 downto 0);
			di1 <= in_bits(BITS_IN-1);

			wait until falling_edge(clk);

			-- Compare with expected outputs

			want_u_res := (others => '0');
			want_s_res := (others => '0');

			for p in 0 to NBIN-1 loop
				want_u_res := want_u_res + unsigned(di((p+1)*WDATA-1 downto p*WDATA));
				want_s_res := want_s_res +   signed(di((p+1)*WDATA-1 downto p*WDATA));
			end loop;
			if di1 = '1' then
				want_u_res := want_u_res + 1;
				want_s_res := want_s_res + 1;
			end if;

			have_uu_res := unsigned(res_uu_sum) + unsigned(res_uu_c);
			have_ss_res :=   signed(res_ss_sum) +   signed(res_ss_c);
			have_su_res :=   signed(res_su_sum) +   signed(res_su_c);

			if (have_uu_res /= want_u_res) or (have_ss_res /= want_s_res) or (have_su_res /= signed(want_u_res)) then
				report
					"ERROR Input " & natural'image(i) &
					" UU have " & natural'image(to_integer(have_uu_res)) &
					" want "    & integer'image(to_integer(want_u_res)) &
					" SS have " & natural'image(to_integer(have_ss_res)) &
					" want "    & integer'image(to_integer(want_s_res)) &
					" SU have " & natural'image(to_integer(have_su_res)) &
					" want "    & integer'image(to_integer(want_u_res));
				var_errors_nb := var_errors_nb + 1;
			end if;

			var_tests_nb := var_tests_nb + 1;

		end loop;

		report "Results : " & natural'image(var_tests_nb) & " test vectors, " & natural'image(var_errors_nb) & " errors";

		-- End of simulation
		clk_want_stop <= true;
		wait;

	end process;

end architecture;


