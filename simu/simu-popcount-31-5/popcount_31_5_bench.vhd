-- This testbench is based on popcount_bench.vhd,
-- but intended for large number of input bits, so adding pseudo-random vectors generation functionalities.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity popcount_31_5_bench is
	generic (
		NB_VEC : natural := 10000
	);
end popcount_31_5_bench;

architecture simu of popcount_31_5_bench is

	signal clk      : std_logic := '0';
	signal clk_next : std_logic := '0';
	signal clear    : std_logic := '1';
	signal clk_want_stop : boolean := false;

	constant BITS_IN : natural := 31;

	signal in_bits : std_logic_vector(BITS_IN-1 downto 0) := (others => '0');
	signal res31   : std_logic_vector(4 downto 0) := (others => '0');

	-- 1499999957 is a decimal number composed of 31 bits, based on several '0' & '1' bits (0101 1001 0110 1000 0010 1110 1101 0101)
	constant prime : integer := 1499999957;

	-- Type for pre-generated test vectors
	type array_type is array(0 to NB_VEC-1) of std_logic_vector(BITS_IN-1 downto 0);

	-- Function that generates the precomputed test vectors:
	function func_gen_vec(p : integer) return array_type is

		variable vec_test : unsigned(63 downto 0) := (others => '0');
		variable arr : array_type := (others => (others => '0'));

		variable i : integer := 0;

	begin
		i := 0;
		arr(i) := (others => '0');

		i := i + 1;
		arr(i) := (others => '1');

		-- Create specific vectors with MSBs at '1' (and LSBs at '0'), and then opposite:
		i := i + 1;
		arr(i) := (others => '0');
		--arr(i) := (j+5-1 downto j => '1');
		for k in 0 to (BITS_IN/2) loop arr(i)(k) := '1'; end loop;
		i := i + 1;
		arr(i) := (others => '1');
		--arr(i) := (j+5-1 downto j => '1');
		for k in 0 to (BITS_IN/2) loop arr(i)(k) := '0'; end loop;

		-- Create specific vectors with a group of 5x1s and the rest is zero, and the opposite:
		for j in 0 to BITS_IN-5 loop
			i := i + 1;
			arr(i) := (others => '0');
			--arr(i) := (j+5-1 downto j => '1');
			for k in 0 to 5-1 loop arr(i)(j+k) := '1'; end loop;
			i := i + 1;
			arr(i) := (others => '1');
			--arr(i) := (j+5-1 downto j => '1');
			for k in 0 to 5-1 loop arr(i)(j+k) := '0'; end loop;
		end loop;

		-- Pseudo-random vectors
		for m in i to NB_VEC-1-1 loop
			i := i + 1;
			vec_test := to_unsigned(p, vec_test'length);
			vec_test := resize(vec_test * (m + 1), vec_test'length);
			arr(i) := std_logic_vector(vec_test(BITS_IN-1 downto 0));
		end loop;

		return arr;
	end function;

	-- Populate table of test vectors
	constant array_vec : array_type := func_gen_vec(prime);

	component popcount_31_5 is
		generic (
			BITS : natural := 31;
			WOUT : natural := 5
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	-- Simple popcount calculation
	function func_popcount(A : std_logic_vector) return natural is
		variable c : natural;
	begin
		c := 0;
		for i in A'low to A'high loop
			if A(i) = '1' then
				c := c + 1;
			end if;
		end loop;
		return c;
	end function;

begin

	popc31 : popcount_31_5
		generic map (
			BITS => 31,
			WOUT => 5
		)
		port map (
			bits_in => in_bits,
			sum     => res31
		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	-- Process that generates stimuli
	process

		variable have_res31 : integer := 0;
		variable want_res31 : integer := 0;
		variable var_errors_nb : integer := 0;
		variable var_tests_nb  : integer := 0;

	begin

		wait until rising_edge(clk);

		report "Beginning simulation..." ;

		for i in 0 to NB_VEC-1 loop

			-- Get the pre-generated test vector
			in_bits <= array_vec(i);

			wait until rising_edge(clk);
			--report "Testing imput number " & natural'image(i) & " (being: " & natural'image(to_integer(unsigned(in_bits))) & ")";

			-- Compare with expected outputs
			have_res31 := to_integer(unsigned(res31));
			want_res31 := func_popcount(in_bits(30 downto 0));

			if (have_res31 /= want_res31) then
				report
					"ERROR : Input number " & natural'image(i) & " (being " & natural'image(to_integer(unsigned(in_bits))) &
					") : popc31 have " & natural'image(have_res31) & " VS want " & integer'image(want_res31);
				var_errors_nb := var_errors_nb + 1;
			end if;

			var_tests_nb := var_tests_nb + 1;

		end loop;

		report "Results : " & natural'image(var_tests_nb) & " test vectors, " & natural'image(var_errors_nb) & " errors";

		-- End of simulation
		clk_want_stop <= true;
		wait;

	end process;

end architecture;


