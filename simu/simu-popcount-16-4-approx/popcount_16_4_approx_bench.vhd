
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity popcount_16_4_approx_bench is
	generic (
		SIMU_PRINT : boolean := false
	);
end popcount_16_4_approx_bench;

architecture simu of popcount_16_4_approx_bench is

	signal clk      : std_logic := '0';
	signal clk_next : std_logic := '0';
	signal clear    : std_logic := '1';
	signal clk_want_stop : boolean := false;

	constant BITS_IN : natural := 16;

	signal in_bits : std_logic_vector(BITS_IN-1 downto 0) := (others => '0');

	signal res16def  : std_logic_vector(3 downto 0) := (others => '0');
	signal res16alt  : std_logic_vector(3 downto 0) := (others => '0');
	signal res16slow : std_logic_vector(3 downto 0) := (others => '0');

	component popcount_16_4_approx is
		generic (
			BITS : natural := 16;
			WOUT : natural := 4
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component popcount_16_4_approx_alt is
		generic (
			BITS : natural := 16;
			WOUT : natural := 4
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	component popcount_16_4_approx_slow is
		generic (
			BITS : natural := 16;
			WOUT : natural := 4
		);
		port (
			bits_in : in  std_logic_vector(BITS-1 downto 0);
			sum     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

	-- Simple popcount calculation
	function func_popcount(A : std_logic_vector) return natural is
		variable c : natural;
	begin
		c := 0;
		for i in A'low to A'high loop
			if A(i) = '1' then
				c := c + 1;
			end if;
		end loop;
		return c;
	end function;

begin

	popc16 : popcount_16_4_approx
		generic map (
			BITS => 16,
			WOUT => 4
		)
		port map (
			bits_in => in_bits(15 downto 0),
			sum     => res16def
		);

	popc16alt : popcount_16_4_approx_alt
		generic map (
			BITS => 16,
			WOUT => 4
		)
		port map (
			bits_in => in_bits(15 downto 0),
			sum     => res16alt
		);

	popc16slow : popcount_16_4_approx_slow
		generic map (
			BITS => 16,
			WOUT => 4
		)
		port map (
			bits_in => in_bits(15 downto 0),
			sum     => res16slow
		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	-- Process that generates stimuli
	process
		variable have_res16 : integer := 0;
		variable want_res16 : integer := 0;
		variable var_err_perc : integer := 0;

		variable var_under_perc_def : integer := 0;
		variable var_over_perc_def  : integer := 0;

		variable var_under_perc_alt : integer := 0;
		variable var_over_perc_alt  : integer := 0;

		variable var_under_perc_slow : integer := 0;
		variable var_over_perc_slow  : integer := 0;

		variable var_errors_nb : integer := 0;
		variable var_tests_nb  : integer := 0;
	begin

		wait until rising_edge(clk);

		for i in 0 to 2**BITS_IN-1 loop
			in_bits <= std_logic_vector(to_unsigned(i, BITS_IN));

			wait until rising_edge(clk);

			-- Generate inputs

			wait until falling_edge(clk);

			-- Compute expected output

			want_res16 := func_popcount( in_bits(15 downto 0) );

			-- Process default component

			have_res16 := to_integer(unsigned(res16def));

			if (have_res16 /= want_res16) and (want_res16 = 0) then
				report
					"ERROR Unacceptable error, for input = " & natural'image(i) &
					" : got "   & natural'image(have_res16) & " expected " & integer'image(want_res16);
				var_errors_nb := var_errors_nb + 1;
			end if;

			if (have_res16 /= want_res16) and (want_res16 /= 0) then
				if SIMU_PRINT = true then
					report "INFO Result " & natural'image(want_res16) & " is approximated to " & natural'image(have_res16);
				end if;
				var_err_perc := (have_res16 - want_res16) * 100 / want_res16;
				if var_err_perc < var_under_perc_def then var_under_perc_def := var_err_perc; end if;
				if var_err_perc > var_over_perc_def  then  var_over_perc_def := var_err_perc; end if;
			end if;

			-- Process "alt" component

			have_res16 := to_integer(unsigned(res16alt));

			if (have_res16 /= want_res16) and (want_res16 = 0) then
				report
					"ERROR Unacceptable error (alt), for input = " & natural'image(i) &
					" : got "   & natural'image(have_res16) & " expected " & integer'image(want_res16);
				var_errors_nb := var_errors_nb + 1;
			end if;

			if (have_res16 /= want_res16) and (want_res16 /= 0) then
				if SIMU_PRINT = true then
					report "INFO Result (alt) " & natural'image(want_res16) & " is approximated to " & natural'image(have_res16);
				end if;
				var_err_perc := (have_res16 - want_res16) * 100 / want_res16;
				if var_err_perc < var_under_perc_alt then var_under_perc_alt := var_err_perc; end if;
				if var_err_perc > var_over_perc_alt  then  var_over_perc_alt := var_err_perc; end if;
			end if;

			-- Process "slow" component

			have_res16 := to_integer(unsigned(res16slow));

			if (have_res16 /= want_res16) and (want_res16 = 0) then
				report
					"ERROR Unacceptable error (slow), for input = " & natural'image(i) &
					" : got "   & natural'image(have_res16) & " expected " & integer'image(want_res16);
				var_errors_nb := var_errors_nb + 1;
			end if;

			if (have_res16 /= want_res16) and (want_res16 /= 0) then
				if SIMU_PRINT = true then
					report "INFO Result (slow) " & natural'image(want_res16) & " is approximated to " & natural'image(have_res16);
				end if;
				var_err_perc := (have_res16 - want_res16) * 100 / want_res16;
				if var_err_perc < var_under_perc_slow then var_under_perc_slow := var_err_perc; end if;
				if var_err_perc > var_over_perc_slow  then  var_over_perc_slow := var_err_perc; end if;
			end if;

			var_tests_nb := var_tests_nb + 1;

		end loop;

		-- Report unexpected range of accuracy

		report "Results : Worst approximations (in percent) : " & natural'image(var_under_perc_def) & " to " & natural'image(var_over_perc_def);
		if (var_under_perc_def < -25) or (var_over_perc_def > 0) then
			report "ERROR Worst error ratio exceeds the acceptable tolerance for this design";
			var_errors_nb := var_errors_nb + 1;
		end if;

		report "Results : (alt) Worst approximations (in percent) : " & natural'image(var_under_perc_alt) & " to " & natural'image(var_over_perc_alt);
		if (var_under_perc_alt < -25) or (var_over_perc_alt > 0) then
			report "ERROR Worst error ratio exceeds the acceptable tolerance for this design";
			var_errors_nb := var_errors_nb + 1;
		end if;

		report "Results : (slow) Worst approximations (in percent) : " & natural'image(var_under_perc_slow) & " to " & natural'image(var_over_perc_slow);
		if (var_under_perc_slow < -20) or (var_over_perc_slow > 25) then
			report "ERROR Worst error ratio exceeds the acceptable tolerance for this design";
			var_errors_nb := var_errors_nb + 1;
		end if;

		report "Results : " & natural'image(var_tests_nb) & " test vectors, " & natural'image(var_errors_nb) & " errors";

		-- End of simulation
		clk_want_stop <= true;
		wait;

	end process;

end architecture;


