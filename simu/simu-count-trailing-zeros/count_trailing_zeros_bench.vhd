
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity count_trailing_zeros_bench is
	generic (
		BITS : natural := 16;
		-- Thresholds for implementation selection
		USE_BEHV_UPTO  : natural := 3;
		USE_RADIX_FROM : natural := 5;
		RADIX : natural := 4
	);
end count_trailing_zeros_bench;

architecture simu of count_trailing_zeros_bench is

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	signal clk      : std_logic := '0';
	signal clk_next : std_logic := '0';
	signal clear    : std_logic := '1';
	signal clk_want_stop : boolean := false;

	constant BITS_IN : natural := BITS;
	signal in_bits : std_logic_vector(BITS_IN-1 downto 0) := (others => '0');

	constant WOUT : natural := storebitsnb(BITS);

	signal res_ctz : std_logic_vector(WOUT-1 downto 0) := (others => '0');
	signal res_msk : std_logic_vector(BITS-1 downto 0) := (others => '0');
	signal res_emp : std_logic := '0';

	component count_trailing_zeros is
		generic (
			BITS  : natural := 16;
			WOUT  : natural := 5;
			-- Enable output of the number of trailing zeros
			CTZ_EN : boolean := true;
			-- Enable output of the "mask" that marks the trailing zeros
			MASK_EN : boolean := false;
			-- Enable output of the "empty" flag
			EMPTY_EN : boolean := false;
			-- Thresholds for implementation selection
			USE_BEHV_UPTO  : natural := 7;
			USE_RADIX_FROM : natural := 64;
			RADIX : natural := 32
		);
		port (
			bits_in   : in  std_logic_vector(BITS-1 downto 0);
			ctz_out   : out std_logic_vector(WOUT-1 downto 0);
			mask_out  : out std_logic_vector(BITS-1 downto 0);
			empty_out : out std_logic
		);
	end component;

	-- Functions for reference calculations

	function func_ctz(A : std_logic_vector) return natural is
	begin
		for i in A'low to A'high loop
			if A(i) = '1' then
				return i;
			end if;
		end loop;
		return A'length;
	end function;

	function func_msk(A : std_logic_vector) return std_logic_vector is
		variable v : std_logic_vector(A'length-1 downto 0);
	begin
		v := (others => '0');
		for i in A'low to A'high loop
			if A(i) = '1' then
				return v;
			end if;
			v(i) := '1';
		end loop;
		return v;
	end function;

begin

	ctz : count_trailing_zeros
		generic map (
			BITS  => BITS,
			WOUT  => WOUT,
			-- Enable output of the number of trailing zeros
			CTZ_EN => true,
			-- Enable output of the "mask" that marks the trailing zeros
			MASK_EN => true,
			-- Enable output of the "empty" flag
			EMPTY_EN => true,
			-- Thresholds for implementation selection
			USE_BEHV_UPTO  => USE_BEHV_UPTO,
			USE_RADIX_FROM => USE_RADIX_FROM,
			RADIX => RADIX
		)
		port map (
			bits_in   => in_bits,
			ctz_out   => res_ctz,
			mask_out  => res_msk,
			empty_out => res_emp
		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	-- Process that generates stimuli
	process
		variable want_ctz  : integer := 0;
		variable want_msk  : std_logic_vector(BITS-1 downto 0) := (others => '0');
		variable want_emp  : STD_LOGIC := '0';
		variable var_errors_nb : integer := 0;
		variable var_tests_nb  : integer := 0;
	begin

		wait until rising_edge(clk);

		for i in 0 to 2**BITS_IN-1 loop
			in_bits <= std_logic_vector(to_unsigned(i, BITS_IN));

			wait until rising_edge(clk);

			-- Generate inputs

			wait until falling_edge(clk);

			-- Compare with expected outputs

			want_ctz := func_ctz(in_bits);
			want_msk := func_msk(in_bits);
			want_emp := '1' when want_ctz = BITS else '0';

			if (unsigned(res_ctz) /= want_ctz) or (res_msk /= want_msk) or (res_emp /= want_emp) then
				report
					"ERROR Input " & to_string(in_bits) &
					" / ctz have " & natural'image(to_integer(unsigned(res_ctz))) & " want " & integer'image(want_ctz) &
					" / msk have " & to_string(res_msk) & " want " & to_string(want_msk) &
					" / emp have " & to_string(res_emp) & " want " & to_string(want_emp);
				var_errors_nb := var_errors_nb + 1;
			end if;

			var_tests_nb := var_tests_nb + 1;

		end loop;

		report "Results : " & natural'image(var_tests_nb) & " test vectors, " & natural'image(var_errors_nb) & " errors";

		-- End of simulation
		clk_want_stop <= true;
		wait;

	end process;

end architecture;

