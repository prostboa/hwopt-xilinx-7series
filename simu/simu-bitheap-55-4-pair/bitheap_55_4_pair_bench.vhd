
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity bitheap_55_4_pair_bench is
end bitheap_55_4_pair_bench;

architecture simu of bitheap_55_4_pair_bench is

	component bitheap_55_4_pair is
		generic (
			WDATA : natural := 8;
			WOUT  : natural := 12;
			NBIN  : natural := 5;
			SIGN  : boolean := false
		);
		port (
			data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
			data_1   : in  std_logic;
			res_idx0 : out std_logic_vector(WOUT-1 downto 0);
			res_idx2 : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

begin

	gen_wdata : for w in 3 to 5 generate

		signal clk      : std_logic := '0';
		signal clk_next : std_logic := '0';
		signal clear    : std_logic := '1';
		signal clk_want_stop : boolean := false;

		constant WDATA : natural := w;
		constant WOUT  : natural := w + 4;
		constant NBIN  : natural := 5;

		-- Only set the 3 MSBs of data, and use 1 bit for input data_1
		constant BITS_IN : natural := 5*3 + 1;

		signal in_bits : std_logic_vector(BITS_IN-1 downto 0) := (others => '0');

		signal di         : std_logic_vector(NBIN*WDATA-1 downto 0) := (others => '0');
		signal di1        : std_logic := '0';

		signal res_u_idx0 : std_logic_vector(WOUT-1 downto 0) := (others => '0');
		signal res_u_idx2 : std_logic_vector(WOUT-1 downto 0) := (others => '0');
		signal res_s_idx0 : std_logic_vector(WOUT-1 downto 0) := (others => '0');
		signal res_s_idx2 : std_logic_vector(WOUT-1 downto 0) := (others => '0');

	begin

		comp_u : bitheap_55_4_pair
			generic map (
				WDATA => WDATA,
				WOUT  => WOUT,
				NBIN  => NBIN,
				SIGN  => false
			)
			port map (
				data_in  => di,
				data_1   => di1,
				res_idx0 => res_u_idx0,
				res_idx2 => res_u_idx2
			);

		comp_s : bitheap_55_4_pair
			generic map (
				WDATA => WDATA,
				WOUT  => WOUT,
				NBIN  => NBIN,
				SIGN  => true
			)
			port map (
				data_in  => di,
				data_1   => di1,
				res_idx0 => res_s_idx0,
				res_idx2 => res_s_idx2
			);

		clk_next <= not clk when clk_want_stop = false else clk;
		clk <= clk_next after 5 ns;

		-- Process that generates stimuli
		process
			variable have_u : unsigned(WOUT-1 downto 0) := (others => '0');
			variable have_s :   signed(WOUT-1 downto 0) := (others => '0');
			variable want_u : unsigned(WOUT-1 downto 0) := (others => '0');
			variable want_s :   signed(WOUT-1 downto 0) := (others => '0');
			variable var_errors_nb : integer := 0;
			variable var_tests_nb : integer := 0;
		begin

			report "Beginning simulation with width " & natural'image(w) & " bits";

			wait until rising_edge(clk);

			for i in 0 to 2**BITS_IN-1 loop
				in_bits <= std_logic_vector(to_unsigned(i, BITS_IN));

				wait until rising_edge(clk);

				-- Generate inputs

				di <= (others => '0');
				di1 <= '0';

				for p in 0 to NBIN-1 loop
					di(p*WDATA + WDATA-1 downto p*WDATA + WDATA-3) <= in_bits(p*3+2 downto p*3);
				end loop;
				di1 <= in_bits(15);

				wait until falling_edge(clk);

				-- Compare with expected outputs

				want_u := (others => '0');
				want_s := (others => '0');

				for p in 0 to NBIN-1 loop
					want_u := want_u + unsigned(di((p+1)*WDATA-1 downto p*WDATA));
					want_s := want_s +   signed(di((p+1)*WDATA-1 downto p*WDATA));
				end loop;
				if di1 = '1' then
					want_u := want_u + 1;
					want_s := want_s + 1;
				end if;

				have_u := unsigned(res_u_idx0) + unsigned(res_u_idx2);
				have_s :=   signed(res_s_idx0) +   signed(res_s_idx2);

				if (have_u /= want_u) or (have_s /= want_s) then
					report
						"ERROR Input " & natural'image(i) & " width " & natural'image(w) &
						" / Unsigned have " & natural'image(to_integer(have_u)) &
						" want "            & integer'image(to_integer(want_u)) &
						" / Signed have "   & natural'image(to_integer(have_s)) &
						" want "            & integer'image(to_integer(want_s));
					var_errors_nb := var_errors_nb + 1;
				end if;

				var_tests_nb := var_tests_nb + 1;

			end loop;

			report "Size " & natural'image(w) & " : Results : " & natural'image(var_tests_nb) & " test vectors, " & natural'image(var_errors_nb) & " errors";

			-- End of simulation
			clk_want_stop <= true;
			wait;

		end process;

	end generate;

end architecture;

