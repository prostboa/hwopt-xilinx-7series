
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity muladdtree_ter_weights_merged_bench is
end muladdtree_ter_weights_merged_bench;

architecture simu of muladdtree_ter_weights_merged_bench is

	function bool_to_nat(b : boolean) return natural is
	begin
		if b = false then return 0; end if;
		return 1;
	end function;

	constant WDATA : natural := 3;
	constant NBIN  : natural := 5;
	constant WOUT  : natural := 16;

	-- To do simulation with large number of inputs, checking only weights may be enough
	-- FIXME With FIXDATA=false, GHDL emits an overflow error at 59%
	constant FIXDATA : boolean := true;
	constant DATAVAL : integer := 1;

	constant VEC_WIDTH : natural := (NBIN * (WDATA + 2)) * bool_to_nat(not FIXDATA) + (NBIN * 2) * bool_to_nat(FIXDATA);
	constant VEC_NB    : natural := 2 ** VEC_WIDTH;

	constant VEC_MUL_IDX : natural := NBIN * WDATA;

	signal clk      : std_logic := '0';
	signal clk_next : std_logic := '0';
	signal clear    : std_logic := '1';
	signal clk_want_stop : boolean := false;

	-- Data input
	signal mult_in       : std_logic_vector(NBIN*2-1 downto 0) := (others => '0');
	signal data_in       : std_logic_vector(NBIN*WDATA-1 downto 0) := (others => '0');
	signal data_in_valid : std_logic := '0';

	-- Data output
	signal data_out       : std_logic_vector(WOUT-1 downto 0) := (others => '0');
	signal add1_out       : std_logic := '0';
	signal data_out_valid : std_logic := '0';

	function is_tern_code(vec : std_logic_vector) return boolean is
	begin
		for t in 0 to NBIN-1 loop
			if vec(2*t+1 downto 2*t) = "10" then
				return false;
			end if;
		end loop;
		return true;
	end function;

	function is_data_valid(vec : std_logic_vector) return boolean is
		variable forbid_data : std_logic_vector(WDATA-1 downto 0);
	begin
		forbid_data := (others => '0');
		forbid_data(WDATA-1) := '1';
		for i in 0 to NBIN-1 loop
			if vec((i+1)*WDATA-1 downto i*WDATA) = forbid_data then
				return false;
			end if;
		end loop;
		return true;
	end function;

	component muladdtree_ter_weights_merged is
		generic(
			-- Data type and width
			WDATA : natural := 8;
			NBIN  : natural := 20;
			WOUT  : natural := 12;
			-- An optional tag
			TAGW  : natural := 1;
			TAGEN : boolean := true;
			TAGZC : boolean := true;
			-- How to add pipeline registers
			REGEN  : natural := 1;  -- 0=none, 1=all, else every EN stages
			REGIDX : natural := 0;  -- Start index (from the leaves)
			-- Stage number, for internal implem
			STAGE : natural := 0
		);
		port(
			clk      : in  std_logic;
			clear    : in  std_logic;
			-- Data, input and output
			data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
			data_out : out std_logic_vector(WOUT-1 downto 0);
			-- Multiplier and carry, input and output
			mult_in  : in  std_logic_vector(NBIN*2-1 downto 0);
			add1_in  : in  std_logic_vector(NBIN-1 downto 0);
			add1_out : out std_logic;
			-- Tag, input and output
			tag_in   : in  std_logic_vector(TAGW-1 downto 0);
			tag_out  : out std_logic_vector(TAGW-1 downto 0)
		);
	end component;

begin

	dut : muladdtree_ter_weights_merged
		generic map (
			-- Data type and width
			WDATA => WDATA,
			NBIN  => NBIN,
			WOUT  => WOUT,
			-- An optional tag
			TAGW  => 1,
			TAGEN => true,
			TAGZC => false,
			-- How to add pipeline registers
			REGEN  => 2,  -- 0=none, 1=all, else every EN stages
			REGIDX => 0,  -- Start index (from the leaves)
			-- Stage number, for internal implem
			STAGE => 0
		)
		port map (
			clk      => clk,
			clear    => clear,
			-- Data, input and output
			data_in  => data_in,
			data_out => data_out,
			-- Multiplier and carry, input and output
			mult_in  => mult_in,
			add1_in  => (others => '0'),
			add1_out => add1_out,
			-- Tag, input and output
			tag_in(0)   => data_in_valid,
			tag_out(0)  => data_out_valid
		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	-- Process that generates stimuli
	process
		variable var_vec_in : std_logic_vector(VEC_WIDTH-1 downto 0) := (others => '0');
	begin

		-- Clear
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);

		clear <= '0';

		wait until rising_edge(clk);
		wait until rising_edge(clk);

		if FIXDATA = true then
			for i in 0 to NBIN - 1 loop
				data_in((i+1)*WDATA-1 downto i*WDATA) <= std_logic_vector(to_signed(DATAVAL, WDATA));
			end loop;
		end if;

		for i in 0 to VEC_NB - 1 loop
			var_vec_in := std_logic_vector(to_unsigned(i, VEC_WIDTH));

			if FIXDATA = false then
				data_in <= var_vec_in(NBIN*WDATA-1 downto 0);
				mult_in <= var_vec_in(VEC_MUL_IDX + NBIN*2-1 downto VEC_MUL_IDX);
			else
				mult_in <= var_vec_in;
			end if;

			data_in_valid <= '1';
			wait until rising_edge(clk);

		end loop;

		data_in_valid <= '0';
		wait until rising_edge(clk);

		-- End of simulation
		wait;

	end process;

	-- Process that checks outputs
	process
		variable var_vec_in  : std_logic_vector(VEC_WIDTH-1 downto 0) := (others => '0');
		variable var_data_in : std_logic_vector(NBIN*WDATA-1 downto 0) := (others => '0');
		variable var_mult_in : std_logic_vector(NBIN*2-1 downto 0) := (others => '0');
		variable var_out_want : integer := 0;
		variable var_out_int : integer := 0;
		variable var_data : integer := 0;
		variable var_mult : integer := 0;
		variable var_simu_mod : natural := 0;
		variable var_errors_nb : integer := 0;
		variable var_tests_nb  : integer := 0;
	begin

		wait until clear = '0';

		report "Begin simulation with " & natural'image(VEC_NB) & " vectors";

		if FIXDATA = true then
			for i in 0 to NBIN - 1 loop
				var_data_in((i+1)*WDATA-1 downto i*WDATA) := std_logic_vector(to_signed(DATAVAL, WDATA));
			end loop;
		end if;

		for v in 0 to VEC_NB - 1 loop

			loop
				wait until rising_edge(clk);
				if data_out_valid = '1' then exit; end if;
			end loop;

			var_simu_mod := v mod (VEC_NB / 10);
			if (v > 0) and (var_simu_mod = 0) then
				report "Status: " & natural'image(v) & " / " & natural'image(VEC_NB) & " (" & natural'image(v * 100 / VEC_NB) & "%)";
			end if;

			var_vec_in := std_logic_vector(to_unsigned(v, VEC_WIDTH));

			if FIXDATA = false then
				var_mult_in := var_vec_in(VEC_MUL_IDX + NBIN*2-1 downto VEC_MUL_IDX);
				var_data_in := var_vec_in(NBIN*WDATA-1 downto 0);
				if is_data_valid(var_data_in) = false then next; end if;
			else
				var_mult_in := var_vec_in;
			end if;

			if is_tern_code(var_mult_in) = false then next; end if;

			var_out_want := 0;
			for i in 0 to NBIN-1 loop
				var_data := to_integer(signed(var_data_in((i+1)*WDATA-1 downto i*WDATA)));
				var_mult := to_integer(signed(var_mult_in((i+1)*2-1 downto i*2)));
				var_out_want := var_out_want + var_data * var_mult;
			end loop;

			var_out_int := to_integer(signed(data_out));
			if add1_out = '1' then var_out_int := var_out_int + 1; end if;

			if var_out_want /= var_out_int then
				report
					"ERROR Wrong output" &
					" input "    & natural'image(v) &
					" got "      & integer'image(var_out_int) &
					" expected " & integer'image(var_out_want);
				var_errors_nb := var_errors_nb + 1;
				-- For debug : force end of simulation
				--clk_want_stop <= true;
			end if;

			var_tests_nb := var_tests_nb + 1;

		end loop;

		report "End of simulation";
		report "Results : " & natural'image(var_tests_nb) & " test vectors, " & natural'image(var_errors_nb) & " errors";

		-- End of simulation
		clk_want_stop <= true;
		wait;

	end process;

end architecture;


