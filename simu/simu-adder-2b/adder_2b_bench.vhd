
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity adder_2b_bench is
end adder_2b_bench;

architecture simu of adder_2b_bench is

	component adder_2b is
		generic (
			NBIN : natural := 7;
			SIGN : boolean := false;
			WOUT : natural := 5
		);
		port (
			data_in : in  std_logic_vector(NBIN*2-1 downto 0);
			res_sum : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

begin

	gen_nbin : for NBIN in 1 to 7 generate

		signal clk      : std_logic := '0';
		signal clk_next : std_logic := '0';
		signal clear    : std_logic := '1';
		signal clk_want_stop : boolean := false;

		constant WDATA : natural := 2;
		constant WOUT  : natural := WDATA + 3;

		-- Only set the 3 MSBs of data, and use 1 bit for input data_1
		constant BITS_IN : natural := NBIN*WDATA;

		signal in_bits : std_logic_vector(BITS_IN-1 downto 0) := (others => '0');

		signal di : std_logic_vector(NBIN*WDATA-1 downto 0) := (others => '0');

		signal res_u : std_logic_vector(WOUT-1 downto 0) := (others => '0');
		signal res_s : std_logic_vector(WOUT-1 downto 0) := (others => '0');

	begin

		comp_u : adder_2b
			generic map (
				NBIN  => NBIN,
				SIGN  => false,
				WOUT  => WOUT
			)
			port map (
				data_in => di,
				res_sum => res_u
			);

		comp_s : adder_2b
			generic map (
				NBIN  => NBIN,
				SIGN  => true,
				WOUT  => WOUT
			)
			port map (
				data_in => di,
				res_sum => res_s
			);

		clk_next <= not clk when clk_want_stop = false else clk;
		clk <= clk_next after 5 ns;

		-- Process that generates stimuli
		process
			variable have_u : unsigned(WOUT-1 downto 0) := (others => '0');
			variable have_s :   signed(WOUT-1 downto 0) := (others => '0');
			variable want_u : unsigned(WOUT-1 downto 0) := (others => '0');
			variable want_s :   signed(WOUT-1 downto 0) := (others => '0');
			variable var_errors_nb : integer := 0;
			variable var_tests_nb : integer := 0;
		begin

			report "Beginning simulation with NBIN=" & natural'image(NBIN) & " inputs";

			wait until rising_edge(clk);

			for i in 0 to 2**BITS_IN-1 loop
				in_bits <= std_logic_vector(to_unsigned(i, BITS_IN));

				wait until rising_edge(clk);

				-- Generate inputs

				di <= in_bits;

				wait until falling_edge(clk);

				-- Compare with expected outputs

				want_u := (others => '0');
				want_s := (others => '0');

				for p in 0 to NBIN-1 loop
					want_u := want_u + unsigned(di((p+1)*WDATA-1 downto p*WDATA));
					want_s := want_s +   signed(di((p+1)*WDATA-1 downto p*WDATA));
				end loop;

				have_u := unsigned(res_u);
				have_s :=   signed(res_s);

				if (have_u /= want_u) or (have_s /= want_s) then
					report
						"ERROR Input " & natural'image(i) & " NBIN " & natural'image(NBIN) &
						" / Unsigned have " & natural'image(to_integer(have_u)) &
						" want "            & integer'image(to_integer(want_u)) &
						" / Signed have "   & natural'image(to_integer(have_s)) &
						" want "            & integer'image(to_integer(want_s));
					var_errors_nb := var_errors_nb + 1;
				end if;

				var_tests_nb := var_tests_nb + 1;

			end loop;

			report "Results : NBIN=" & natural'image(NBIN) & " : " & natural'image(var_tests_nb) & " test vectors, " & natural'image(var_errors_nb) & " errors";

			-- End of simulation
			clk_want_stop <= true;
			wait;

		end process;

	end generate;  -- Iterate over last1

end architecture;


