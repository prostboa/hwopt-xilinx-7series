
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity adder_3in_bench is
end adder_3in_bench;

architecture simu of adder_3in_bench is

	signal clk      : std_logic := '0';
	signal clk_next : std_logic := '0';
	signal clear    : std_logic := '1';
	signal clk_want_stop : boolean := false;

	constant WDATA : natural := 3;
	constant WOUT  : natural := 5;
	constant NBIN  : natural := 3;

	constant BITS_IN : natural := WDATA*NBIN + 2;

	signal di      : std_logic_vector(NBIN*WDATA-1 downto 0) := (others => '0');
	signal di1     : std_logic := '0';
	signal di2     : std_logic := '0';

	signal res_u   : std_logic_vector(WOUT-1 downto 0) := (others => '0');
	signal res_s   : std_logic_vector(WOUT-1 downto 0) := (others => '0');

	component adder_3in is
		generic (
			WDATA : natural := 8;
			WOUT  : natural := 10;
			NBIN  : natural := 3;
			SIGN  : boolean := false
		);
		port (
			data_in : in  std_logic_vector(NBIN*WDATA-1 downto 0);
			data_1  : in  std_logic;
			data_2  : in  std_logic;
			res     : out std_logic_vector(WOUT-1 downto 0)
		);
	end component;

begin

	comp_u : adder_3in
		generic map (
			WDATA => WDATA,
			WOUT  => WOUT,
			NBIN  => NBIN,
			SIGN  => false
		)
		port map (
			data_in => di,
			data_1  => di1,
			data_2  => di2,
			res     => res_u
		);

	comp_s : adder_3in
		generic map (
			WDATA => WDATA,
			WOUT  => WOUT,
			NBIN  => NBIN,
			SIGN  => true
		)
		port map (
			data_in => di,
			data_1  => di1,
			data_2  => di2,
			res     => res_s
		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	-- Process that generates stimuli
	process
		variable in_bits : std_logic_vector(BITS_IN-1 downto 0) := (others => '0');
		variable want_u : unsigned(WOUT-1 downto 0) := (others => '0');
		variable want_s :   signed(WOUT-1 downto 0) := (others => '0');
		variable var_errors_nb : integer := 0;
		variable var_tests_nb : integer := 0;
	begin

		wait until rising_edge(clk);

		for i in 0 to 2**BITS_IN-1 loop
			in_bits := std_logic_vector(to_unsigned(i, BITS_IN));

			wait until rising_edge(clk);

			-- Generate inputs

			di  <= in_bits(BITS_IN-3 downto 0);
			di1 <= in_bits(BITS_IN-2);
			di2 <= in_bits(BITS_IN-1);

			wait until falling_edge(clk);

			-- Compare with expected outputs

			want_u := (others => '0');
			want_s := (others => '0');

			for p in 0 to NBIN-1 loop
				want_u := want_u + unsigned(di((p+1)*WDATA-1 downto p*WDATA));
				want_s := want_s +   signed(di((p+1)*WDATA-1 downto p*WDATA));
			end loop;
			if di1 = '1' then
				want_u := want_u + 1;
				want_s := want_s + 1;
			end if;
			if di2 = '1' then
				want_u := want_u + 1;
				want_s := want_s + 1;
			end if;

			if (unsigned(res_u) /= want_u) or (signed(res_s) /= want_s) then
				report
					"ERROR Input " & natural'image(i) &
					" UNSIGNED have " & natural'image(to_integer(unsigned(res_u))) &
					" want "          & integer'image(to_integer(want_u)) &
					" SIGNED have "   & natural'image(to_integer(signed(res_s))) &
					" want "          & integer'image(to_integer(want_s));
				var_errors_nb := var_errors_nb + 1;
			end if;

			var_tests_nb := var_tests_nb + 1;

		end loop;

		report "Results : " & natural'image(var_tests_nb) & " test vectors, " & natural'image(var_errors_nb) & " errors";

		-- End of simulation
		clk_want_stop <= true;
		wait;

	end process;

end architecture;


