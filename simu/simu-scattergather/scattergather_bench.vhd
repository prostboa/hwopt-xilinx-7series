
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity scattergather_bench is
end scattergather_bench;

architecture simu of scattergather_bench is

	constant WGATHER  : natural := 16;
	constant WSCATTER : natural := 8;
	constant NBIN     : natural := 14;
	constant WSEL     : natural := 8;
	constant EGATHER  : boolean := true;
	constant ESCATTER : boolean := false;
	constant RADIX    : natural := 2;
	constant REGALL   : boolean := false;

	signal clk      : std_logic := '0';
	signal clk_next : std_logic := '0';
	signal clear    : std_logic := '1';
	signal clk_want_stop : boolean := false;

	-- Selection input
	signal sel         : std_logic_vector(WSEL-1 downto 0) := (others => '0');
	-- Enable input and output
	signal en_in       : std_logic := '0';
	signal en_out      : std_logic_vector(NBIN-1 downto 0) := (others => '0');
	-- Gather data, input and output
	signal gather_in   : std_logic_vector(NBIN*WGATHER-1 downto 0) := (others => '0');
	signal gather_out  : std_logic_vector(WGATHER-1 downto 0) := (others => '0');
	-- Scatter data, input and output
	signal scatter_in  : std_logic_vector(WSCATTER-1 downto 0) := (others => '0');
	signal scatter_out : std_logic_vector(NBIN*WSCATTER-1 downto 0) := (others => '0');

	component scattergather is
		generic(
			WGATHER  : natural := 8;
			WSCATTER : natural := 1;
			NBIN     : natural := 20;
			WSEL     : natural := 12;
			EGATHER  : boolean := true;
			ESCATTER : boolean := false;
			RADIX    : natural := 2;
			REGALL   : boolean := false
		);
		port(
			clk         : in  std_logic;
			-- Selection input
			sel         : in  std_logic_vector(WSEL-1 downto 0);
			-- Enable input and output
			en_in       : in  std_logic;
			en_out      : out std_logic_vector(NBIN-1 downto 0);
			-- Gather data, input and output
			gather_in   : in  std_logic_vector(NBIN*WGATHER-1 downto 0);
			gather_out  : out std_logic_vector(WGATHER-1 downto 0);
			-- Scatter data, input and output
			scatter_in  : in  std_logic_vector(WSCATTER-1 downto 0);
			scatter_out : out std_logic_vector(NBIN*WSCATTER-1 downto 0)
		);
	end component;

begin

	comp_i: scattergather
		generic map (
			WGATHER  => WGATHER,
			WSCATTER => WSCATTER,
			NBIN     => NBIN,
			WSEL     => WSEL,
			EGATHER  => EGATHER,
			ESCATTER => ESCATTER,
			RADIX    => RADIX,
			REGALL   => REGALL
		)
		port map (
			clk         => clk,
			-- Selection input
			sel         => sel,
			-- Enable input and output
			en_in       => en_in,
			en_out      => en_out,
			-- Gather data, input and output
			gather_in   => gather_in,
			gather_out  => gather_out,
			-- Scatter data, input and output
			scatter_in  => scatter_in,
			scatter_out => scatter_out
		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	-- Process that generates stimuli
	process
		variable want_en_out : std_logic_vector(NBIN-1 downto 0) := (others => '0');
		variable var_errors_nb : integer := 0;
		variable var_tests_nb : integer := 0;
	begin

		for i in 0 to NBIN-1 loop
			gather_in((i+1)*WGATHER-1 downto i*WGATHER) <= std_logic_vector(to_unsigned(i, WGATHER));
		end loop;

		for i in 0 to NBIN-1 loop

			sel   <= std_logic_vector(to_unsigned(i, WSEL));
			en_in <= '0';

			-- Wait a bit
			for w in 1 to 10 loop
				wait until rising_edge(clk);
			end loop;

			want_en_out := (others => '0');
			want_en_out(i) := en_in;
			if (en_out /= want_en_out) or (unsigned(gather_out) /= 0) then
				report "ERROR Sel " & natural'image(i) & " en=0";
				var_errors_nb := var_errors_nb + 1;
			end if;

			var_tests_nb := var_tests_nb + 1;

			sel   <= std_logic_vector(to_unsigned(i, WSEL));
			en_in <= '1';

			-- Wait a bit
			for w in 1 to 10 loop
				wait until rising_edge(clk);
			end loop;

			want_en_out := (others => '0');
			want_en_out(i) := en_in;
			if (en_out /= want_en_out) or (gather_out /= std_logic_vector(to_unsigned(i, WGATHER))) then
				report "ERROR Sel " & natural'image(i) & " en=1";
				var_errors_nb := var_errors_nb + 1;
			end if;

			var_tests_nb := var_tests_nb + 1;

		end loop;

		report "Results : " & natural'image(var_tests_nb) & " test vectors, " & natural'image(var_errors_nb) & " errors";

		-- End of simulation
		clk_want_stop <= true;
		wait;

	end process;

end architecture;


