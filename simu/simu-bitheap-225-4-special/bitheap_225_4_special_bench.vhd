
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity bitheap_225_4_special_bench is
	generic (
		SIMU_PRINT : boolean := false
	);
end bitheap_225_4_special_bench;

architecture simu of bitheap_225_4_special_bench is

	signal clk      : std_logic := '0';
	signal clk_next : std_logic := '0';
	signal clear    : std_logic := '1';
	signal clk_want_stop : boolean := false;

	constant BITS_IN : natural := 2 + 2 + 5;

	signal di0 : std_logic_vector(4 downto 0) := (others => '0');
	signal di1 : std_logic_vector(1 downto 0) := (others => '0');
	signal di2 : std_logic_vector(1 downto 0) := (others => '0');

	signal sum_def : std_logic_vector(3 downto 0) := (others => '0');
	signal sum_alt : std_logic_vector(3 downto 0) := (others => '0');

	component bitheap_225_4_special is
		port (
			ci  : in  std_logic;
			di0 : in  std_logic_vector(4 downto 0);
			di1 : in  std_logic_vector(1 downto 0);
			di2 : in  std_logic_vector(1 downto 0);
			sum : out std_logic_vector(3 downto 0)
		);
	end component;

	component bitheap_225_4_special_alt is
		port (
			ci  : in  std_logic;
			di0 : in  std_logic_vector(4 downto 0);
			di1 : in  std_logic_vector(1 downto 0);
			di2 : in  std_logic_vector(1 downto 0);
			sum : out std_logic_vector(3 downto 0)
		);
	end component;

	-- Simple popcount calculation
	function func_popcount(A : std_logic_vector) return natural is
		variable c : natural;
	begin
		c := 0;
		for i in A'low to A'high loop
			if A(i) = '1' then
				c := c + 1;
			end if;
		end loop;
		return c;
	end function;

begin

	comp_def : bitheap_225_4_special
		port map (
			ci  => '0',
			di0 => di0,
			di1 => di1,
			di2 => di2,
			sum => sum_def
		);

	comp_alt : bitheap_225_4_special_alt
		port map (
			ci  => '0',
			di0 => di0,
			di1 => di1,
			di2 => di2,
			sum => sum_alt
		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	-- Process that generates stimuli
	process
		variable in_number  : std_logic_vector(BITS_IN-1 downto 0) := (others => '0');
		variable have       : natural := 0;
		variable want       : natural := 0;
		variable var_errors_nb : integer := 0;
		variable var_tests_nb : integer := 0;
	begin

		report "Beginning simulation..." ;

		wait until rising_edge(clk);

		for i in 0 to 2**BITS_IN-1 loop

			in_number := std_logic_vector(to_unsigned(i, BITS_IN));

			di0 <= in_number(4 downto 0);
			di1 <= in_number(6 downto 5);
			di2 <= in_number(8 downto 7);

			wait until falling_edge(clk);

			want :=
				1 * func_popcount(di0) +
				2 * func_popcount(di1) +
				4 * func_popcount(di2);

			-- Default implementation

			have := to_integer(unsigned(sum_def));

			if (have /= want) and (SIMU_PRINT = true) then
				report "INFO (def) Result " & natural'image(want) & " is approximated to " & natural'image(have);
			end if;

			if (have /= want) and (want <= 7 or have < want-2 or have > want) then
				report
					"ERROR Unacceptable error (def), for input = " & natural'image(i) &
					" : got "   & natural'image(have) & " expected " & integer'image(want);
				var_errors_nb := var_errors_nb + 1;
			end if;

			-- Alt implementation

			have := to_integer(unsigned(sum_alt));

			if (have /= want) and (SIMU_PRINT = true) then
				report "INFO (alt) Result " & natural'image(want) & " is approximated to " & natural'image(have);
			end if;

			if (have /= want) and (want <= 15) then
				report
					"ERROR Unacceptable error (alt), for input = " & natural'image(i) &
					" : got "   & natural'image(have) & " expected " & integer'image(want);
				var_errors_nb := var_errors_nb + 1;
			end if;

			var_tests_nb := var_tests_nb + 1;

		end loop;

		report "Results : " & natural'image(var_tests_nb) & " test vectors, " & natural'image(var_errors_nb) & " errors";

		-- End of simulation
		clk_want_stop <= true;
		wait;

	end process;

end architecture;


