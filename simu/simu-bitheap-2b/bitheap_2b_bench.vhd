
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity bitheap_2b_bench is
end bitheap_2b_bench;

architecture simu of bitheap_2b_bench is

	signal clk      : std_logic := '0';
	signal clk_next : std_logic := '0';
	signal clear    : std_logic := '1';
	signal clk_want_stop : boolean := false;

	constant BITS_IN : natural := 7 + 7;

	signal di0 : std_logic_vector(6 downto 0) := (others => '0');
	signal di1 : std_logic_vector(6 downto 0) := (others => '0');

	signal sum44 : std_logic_vector(3 downto 0) := (others => '0');
	signal sum55 : std_logic_vector(3 downto 0) := (others => '0');
	signal sum67 : std_logic_vector(4 downto 0) := (others => '0');
	signal sum77 : std_logic_vector(4 downto 0) := (others => '0');

	component bitheap_44_4 is
		port (
			ci  : in  std_logic;
			di0 : in  std_logic_vector(3 downto 0);
			di1 : in  std_logic_vector(3 downto 0);
			sum : out std_logic_vector(3 downto 0)
		);
	end component;

	component bitheap_55_4 is
		port (
			di0 : in  std_logic_vector(4 downto 0);
			di1 : in  std_logic_vector(4 downto 0);
			p53 : out std_logic_vector(2 downto 0);
			do  : out std_logic_vector(1 downto 0);
			co  : out std_logic_vector(1 downto 0);
			sum : out std_logic_vector(3 downto 0)
		);
	end component;

	component bitheap_67_5 is
		port (
			di0 : in  std_logic_vector(6 downto 0);
			di1 : in  std_logic_vector(5 downto 0);
			sum : out std_logic_vector(4 downto 0)
		);
	end component;

	component bitheap_77_5 is
		port (
			di0 : in  std_logic_vector(6 downto 0);
			di1 : in  std_logic_vector(6 downto 0);
			sum : out std_logic_vector(4 downto 0)
		);
	end component;

	-- Simple popcount calculation
	function func_popcount(A : std_logic_vector) return natural is
		variable c : natural;
	begin
		c := 0;
		for i in A'low to A'high loop
			if A(i) = '1' then
				c := c + 1;
			end if;
		end loop;
		return c;
	end function;

begin

	comp44 : bitheap_44_4
		port map (
			ci  => '0',
			di0 => di0(3 downto 0),
			di1 => di1(3 downto 0),
			sum => sum44
		);

	comp55 : bitheap_55_4
		port map (
			di0 => di0(4 downto 0),
			di1 => di1(4 downto 0),
			p53 => open,
			do  => open,
			co  => open,
			sum => sum55
		);

	comp67 : bitheap_67_5
		port map (
			di0 => di0(6 downto 0),
			di1 => di1(5 downto 0),
			sum => sum67
		);

	comp77 : bitheap_77_5
		port map (
			di0 => di0(6 downto 0),
			di1 => di1(6 downto 0),
			sum => sum77
		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	-- Process that generates stimuli
	process
		variable in_number  : std_logic_vector(BITS_IN-1 downto 0) := (others => '0');
		variable have       : natural := 0;
		variable want       : natural := 0;
		variable var_errors_nb : integer := 0;
		variable var_tests_nb : integer := 0;
	begin

		wait until rising_edge(clk);

		for i in 0 to 2**BITS_IN-1 loop
			in_number := std_logic_vector(to_unsigned(i, BITS_IN));

			di0 <= in_number(6 downto 0);
			di1 <= in_number(13 downto 7);

			wait until falling_edge(clk);

			-- Test the 44:4

			want := 1 * func_popcount(di0(3 downto 0)) + 2 * func_popcount(di1(3 downto 0));
			have := to_integer(unsigned(sum44));

			if have /= want then
				report "ERROR Wrong output for 44:4 : input " & natural'image(i) & " got "   & natural'image(have) & " expected " & integer'image(want);
				var_errors_nb := var_errors_nb + 1;
			end if;

			-- Test the 55:4

			want := 1 * func_popcount(di0(4 downto 0)) + 2 * func_popcount(di1(4 downto 0));
			have := to_integer(unsigned(sum55));

			if have /= want then
				report "ERROR Wrong output for 55:4 : input " & natural'image(i) & " got "   & natural'image(have) & " expected " & integer'image(want);
				var_errors_nb := var_errors_nb + 1;
			end if;

			-- Test the 67:5

			want := 1 * func_popcount(di0(6 downto 0)) + 2 * func_popcount(di1(5 downto 0));
			have := to_integer(unsigned(sum67));

			if have /= want then
				report "ERROR Wrong output for 67:4 : input " & natural'image(i) & " got "   & natural'image(have) & " expected " & integer'image(want);
				var_errors_nb := var_errors_nb + 1;
			end if;

			-- Test the 77:5

			want := 1 * func_popcount(di0(6 downto 0)) + 2 * func_popcount(di1(6 downto 0));
			have := to_integer(unsigned(sum77));

			if have /= want then
				report "ERROR Wrong output for 77:4 : input " & natural'image(i) & " got "   & natural'image(have) & " expected " & integer'image(want);
				var_errors_nb := var_errors_nb + 1;
			end if;

			var_tests_nb := var_tests_nb + 1;

		end loop;

		report "Results : " & natural'image(var_tests_nb) & " test vectors, " & natural'image(var_errors_nb) & " errors";

		-- End of simulation
		clk_want_stop <= true;
		wait;

	end process;

end architecture;


