**`THIS REPOSITORY IS STILL IN CONSTRUCTION`**

This repository contains hand-optimized hardware components and operators,
specialized to the Xilinx 7-series FPGA technology.

Objectives
----------

The objectives of these implementations are multiple :
- optimization for size (as long as delay remains manageable),
- genericity about size of inputs,
- scalablity about number of inputs,
- compatibility with the rest of the design about pipeline latency,
- compatibility with very high device utilization,
- predictability of quality of synthesis result,
- ability to also simulate and synthesize with free and open-source tools.

For these reasons, the implementations are provided as RTL level, in a HDL language (VHDL).
Many low-level components are implemented at netlist level with direct instantiation of FPGA primitives.

When available, pipelining is optional and configurable in number of register stages of whatever the component is doing internally.
The total number of pipelining registers is often not easily predictable.
To ease integration in larger designs, an additional tag propagation path is provided with the same latency as the main operation path.

Provided components
-------------------

The provided components are of the following types :
- add tree,
- multiply-add tree,
- population count, count of trailing zeros,
- logic operations `and` and `or`,
- operations `min` and `max`,
- arithmetic operations with balanced ternary numbers,
- accumulators and multiplier-accumulators,
- FIFO in distributed logic with exposed counters on both sides,
- large BRAM-based memories,
- BRAM-based memories with asymmetric read and write widths,
- distribution of values to potentially many and distant readers,
- simultaneous scatter and gather of data to/from selected channel.

Each component has its own parameters to specify various objectives, among which :
- number, size and signedness of main operation inputs,
- size of user tag that is propagated along the main operation path,
- insertion of pipelining registers,
- optimization objective for performance or area,
- etc.

Convenient wrappers are provided to ease integration into larger projects that are otherwise technology independent.

Experimental works
------------------

Experiments with approximate popcount components are included in this repository.
For now, their presence in this repository is mostly for convenience.
These may be removed or thoroughly revised in future changes.

Testing
-------

Simulation testbenches are provided for the most critical components.
Testing is exhaustive when possible according to the size of inputs.

Some components are considered tested within larger designs.
Correctness in all conceivable usage conditions is not formally proven and not guaranteed.

The simulation recipes use the VHDL simulator `ghdl`.
The reasons are multiple :
- convenience for end users about not having to manage licenses,
- support of vhdl2008 to a large enough extent,
- ability to convert to simpler vhdl or verilog for synthesis purposes,
- and simulation speed at least in its `mcode` flavour in which launch of simulations is near immediate.

Simple RTL simulation models are provided for common primitives, in particular LUTs and carry chains.
For more complex components and macros (BRAM, DSP), end users need to have access to the simulation models provided with Xilinx tools.

Documentation
-------------

Illustration of the structure of the main optimized adders and compressors can be found as images in svg format in directory `svg`.

License
-------

The contents of this repository are licensed under the EUPL license.

A copy of the license is provided in this repository in file [LICENSE-EUPL-1.2-EN.txt](LICENSE-EUPL-1.2-EN.txt).

More information on the [EUPL website](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12).

Contact
-------

In case you obtained this work through a fork, a copy or an archive,
here is the address of the original repository :

https://gricad-gitlab.univ-grenoble-alpes.fr/prostboa/hwopt-xilinx-7series

Main contacts :
- Adrien Prost-Boucle, [personal page](https://tima.univ-grenoble-alpes.fr/laboratory/internal-services/adrien-prost-boucle)
- Frédéric Pétrot, [personal page](https://tima.univ-grenoble-alpes.fr/research/sls/members/frederic-petrot)

Acknowledgments
---------------

(in chronological order)

Some of these works did indirectly benefit from the research project Nano2017 funded by France national research agency (ANR) in 2015-2017.

These works indirectly benefit from the research project Holigrail funded by France national research agency (ANR) since 2023.

